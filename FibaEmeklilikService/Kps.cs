﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq; 

namespace FibaEmeklilikService
{
    public class Kps
    {
        cTcknInfo tcknInfo;

        public cTcknInfo TCKNValid(string strTCKN)
        {
            using (AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient())
            {
                try
                {
                    var kpsBireySorgu = aileService.kpsBireySorgu(new AileBireySorgu.bireySorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTCKN),
                        kimlikNoSpecified = true
                    });

                    tcknInfo = new cTcknInfo();
                    tcknInfo.strTCKN = strTCKN;
                    tcknInfo.strBirthplace = kpsBireySorgu.dogumYeri;
                    tcknInfo.strFatherName = kpsBireySorgu.babaAdi;
                    tcknInfo.strGender = kpsBireySorgu.cinsiyeti;
                    tcknInfo.strMaritalStatus = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B";
                    tcknInfo.strMotherName = kpsBireySorgu.anneAdi;
                    tcknInfo.strName = kpsBireySorgu.adi;
                    tcknInfo.strSurname = kpsBireySorgu.soyadi;
                    tcknInfo.dtBirthDate = kpsBireySorgu.dogumTarihi;

                    using (DBEntities db = new DBEntities())
                    {
                        var kpsTCKisiAcikAdresSorgu = aileService.kpsTCKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });

                        if (kpsTCKisiAcikAdresSorgu.kimlikNo > 0)
                        {
                            tcknInfo.strAdress = kpsTCKisiAcikAdresSorgu.acikAdres;
                            tcknInfo.strDistrictCode = kpsTCKisiAcikAdresSorgu.ilceKodu;
                            tcknInfo.strRegionCode = kpsTCKisiAcikAdresSorgu.ilKodu;
                            tcknInfo.strCountryCode = "TR";
                        }

                        tcknInfo.strBirthRegionCode = "";
                        tcknInfo.strBirthDistrictCode = "";

                        var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                        if (il != null)
                            tcknInfo.strBirthRegionCode = il.strCode.Trim();

                        var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                        if (ilce != null)
                            tcknInfo.strBirthDistrictCode = ilce.strCode.Trim();

                        if (tcknInfo.strBirthRegionCode == "" && tcknInfo.strBirthDistrictCode != "")
                        {
                            tcknInfo.strBirthRegionCode = tcknInfo.strBirthDistrictCode.Substring(0, 3);
                        }

                        if (tcknInfo.strBirthRegionCode == "")
                        {
                            tcknInfo.strBirthRegionCode = "99";
                        }

                        if (tcknInfo.strBirthRegionCode != "" && tcknInfo.strBirthDistrictCode == "")
                        {
                            tcknInfo.strBirthDistrictCode = "99900";
                        }
                    }
                }
                catch
                {
                    tcknInfo = null;
                    try
                    {
                        var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new AileBireySorgu.bireySorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });

                        tcknInfo = new cTcknInfo();
                        tcknInfo.strTCKN = strTCKN;
                        tcknInfo.strFatherName = kpsYabanciSorgu.babaAdi;
                        tcknInfo.strGender = kpsYabanciSorgu.cinsiyeti;
                        tcknInfo.strName = kpsYabanciSorgu.adi;
                        tcknInfo.dtBirthDate = kpsYabanciSorgu.dogumTarihi;
                        tcknInfo.strSurname = kpsYabanciSorgu.soyadi;

                        var kpsYabanciAcikAdresSorgu = aileService.kpsYabanciKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });

                        if (kpsYabanciAcikAdresSorgu.kimlikNo > 0)
                        {
                            tcknInfo.strAdress = kpsYabanciAcikAdresSorgu.acikAdres;
                            tcknInfo.strDistrictCode = "99900";
                            tcknInfo.strRegionCode = "99";

                            using (DBEntities db = new DBEntities())
                            {
                                var uyr = db.tblMapping.Where(w => w.strType == "UYR" && !string.IsNullOrEmpty(w.strName.Trim()) && w.strCode == kpsYabanciSorgu.uyrukKod.ToString()).FirstOrDefault();

                                if (uyr != null)
                                    tcknInfo.strCountryCode = uyr != null ? uyr.strCountryCode : kpsYabanciSorgu.uyrukAd;
                            }
                        }

                        tcknInfo.strBirthRegionCode = "99";
                        tcknInfo.strBirthDistrictCode = "99900";
                    }
                    catch
                    {
                        tcknInfo = null;
                    }
                }
            }

            return tcknInfo;
        }
    }
}
