﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace FibaEmeklilikService
{
    public class Policy : Logs
    {
        public bool Create(int intScratchWinSaleID, string strTariffCode, bool bolPaycell, cTcknInfo tcknInfo, ref cPolicyData policyData, ref string strMsg)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    int intCallcenterID = Utilities.NullFixInt(ConfigurationManager.AppSettings["CallcenterID"]);

                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID && w.bolIsActive && !w.bolIsDelete).FirstOrDefault();
                    if (callcenter == null)
                    {
                        strMsg = "Satış kanalı bulunamadı!";
                        return false;
                    }

                    var product = db.tblProducts.Where(w => w.strTariffCode == strTariffCode && w.bolScratchWin && w.bolIsActive && !w.bolIsDelete).FirstOrDefault();
                    if (product == null)
                    {
                        strMsg = "Ürün bulunamadı!";
                        return false;
                    }

                    var packet = product.tblPackets.FirstOrDefault(f => !f.bolIsDelete);
                    if (packet == null)
                    {
                        strMsg = "Paket bulunamadı!";
                        return false;
                    }

                    FIBAWS2.Fibaws2PolreqUser user = new FIBAWS2.Fibaws2PolreqUser();

                    #region adres
                    user.adres = new FIBAWS2.Fibaws2polreqOzlukadresreqUser
                    {
                        adres = tcknInfo.strAdress,
                        adrestip = "E",
                        email = "",
                        ilcekod = tcknInfo.strDistrictCode,
                        iletisimno = tcknInfo.strPhone,
                        iletisimtip = "CEP",
                        ilkod = tcknInfo.strRegionCode,
                        ulkekod = tcknInfo.strCountryCode
                    };
                    #endregion

                    #region ozluk
                    user.ozluk = new FIBAWS2.Fibaws2polreqOzlukreqUser
                    {
                        ad = tcknInfo.strName,
                        anneadi = tcknInfo.strMotherName,
                        babaad = tcknInfo.strFatherName,
                        cinsiyet = tcknInfo.strGender,
                        dogumil = tcknInfo.strBirthRegionCode,
                        dogumilce = tcknInfo.strBirthDistrictCode,
                        dogumtarih = tcknInfo.dtBirthDate,
                        medenidurum = tcknInfo.strMaritalStatus,
                        soyad = tcknInfo.strSurname,
                        tckimlikno = tcknInfo.strTCKN,
                        musteritip = "G",
                        meslekkod = "065",
                        uyruk = tcknInfo.strCountryCode
                    };
                    #endregion

                    #region araci
                    user.araci = new FIBAWS2.Fibaws2polreqAracireqUser
                    {
                        bankasube = Utilities.NullFixDecimal(callcenter.strBankNo),
                        sicilno = callcenter.strRegisterNo
                    };
                    #endregion

                    #region kredi
                    FIBAWS2.Fibaws2polreqkredireqBedelta yillikkreditaksit = new FIBAWS2.Fibaws2polreqkredireqBedelta();

                    foreach (var assurance in packet.tblAssurances.ToList())
                    {
                        yillikkreditaksit.Add(new FIBAWS2.BnktypesBedelTabloUser
                        {
                            bedel = assurance.flPrice,
                            tmntkod = assurance.strCode,
                            yil = assurance.intYear
                        });
                    }

                    user.kredi = new FIBAWS2.Fibaws2polreqKredireqUser
                    {
                        kredisure = packet.intCrediTime,
                        kredidoviztip = packet.strCrediCurrencyType,
                        kreditaksittip = "S",
                        kredidonustutar = packet.flCrediReturnPrice,
                        yillikkreditaksit = yillikkreditaksit,
                        kreditutar = 1,
                        kreditip = packet.strIntegrationTariffCode,
                        sure = packet.intTime,
                        kredino = "x" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString()
                    };
                    #endregion

                    #region odeme
                    user.odeme = new FIBAWS2.Fibaws2polreqOdemereqUser
                    {
                        odemetip = "NKT",
                        dovizkod = "TL",
                        primodemeperiyod = packet.strPremiumPaymentPeriod,
                        primodemesure = packet.intPremiumPaymentTime
                    };
                    #endregion

                    #region yenileme 
                    if (packet.tblProducts.bolIsRefresh)
                    {
                        user.yenileme = "F";
                    }
                    #endregion

                    using (FIBAWS2.fibaws2Client service = new FIBAWS2.fibaws2Client())
                    {
                        service.Open();

                        JavaScriptSerializer java = new JavaScriptSerializer();
                        int intWSLogID = WSLog(0, intScratchWinSaleID, "hayat", "New", "Input", java.Serialize(user), "policelesme");

                        var policelesme = service.policelesme(user, "T");

                        WSLog(intWSLogID, intScratchWinSaleID, "hayat", "New", "Output", java.Serialize(policelesme), "policelesme");

                        if (string.IsNullOrEmpty(policelesme.cevap))
                        {
                            policyData = new cPolicyData
                            {
                                PacketID = packet.intPacketID,
                                IlkPrim = policelesme.ilkprim.Value,
                                ToplamPirim = packet.intCrediTime <= 12 ? policelesme.toplamprim.Value : Convert.ToDecimal(((decimal)(policelesme.toplamprim.Value * 12 / packet.intCrediTime)).ToString("0.00")),
                                intCrediTime = packet.intCrediTime,
                                PoliceNo = (int)policelesme.policeno.Value,
                                IlkPrimKomisyon = policelesme.ilkprimkomisyon.Value,
                                KrediNo = policelesme.kredino,
                                TeklifDurum = policelesme.teklifdurum,
                                ToplamKomisyon = policelesme.toplamkomisyon.Value
                            };

                            if (!bolPaycell)
                            {
                                var teklifpolicelesme = service.teklifpolicelesme(policelesme.policeno, 1, "", "");

                                if (string.IsNullOrEmpty(teklifpolicelesme.cevap))
                                { 
                                    return true;
                                }
                                else
                                {
                                    strMsg = "Teklif policelesme hata [" + teklifpolicelesme.cevap + "]";
                                }
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else if (policelesme.cevap.Contains("Err|"))
                        {
                            strMsg = policelesme.cevap.Split('|')[1].Trim();
                            strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strMsg = ex.Message;
            }

            return false;
        }
    }
}
