﻿#region Directives
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Data.OleDb;
using System.Reflection;
using System.Collections.Generic;
using FibaEmeklilikService;
#endregion

#region ExcelExportSheet
public class ExcelExportSheet
{
    private string[] _strIntList;
    private int[] _intSum;
    const int rowLimit = 65000;

    public ExcelExportSheet(string[] strIntList)
    {
        this._strIntList = strIntList;
        if (strIntList != null)
        {
            this._intSum = new int[strIntList.Length];
        }
    }

    private string getWorkbookTemplate()
    {
        StringBuilder sb = new StringBuilder(818);
        sb.AppendFormat(@"<?xml version=""1.0""?>{0}", Environment.NewLine);
        sb.AppendFormat(@"<?mso-application progid=""Excel.Sheet""?>{0}", Environment.NewLine);
        sb.AppendFormat(@"<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""{0}", Environment.NewLine);
        sb.AppendFormat(@" xmlns:o=""urn:schemas-microsoft-com:office:office""{0}", Environment.NewLine);
        sb.AppendFormat(@" xmlns:x=""urn:schemas-microsoft-com:office:excel""{0}", Environment.NewLine);
        sb.AppendFormat(@" xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""{0}", Environment.NewLine);
        sb.AppendFormat(@" xmlns:html=""http://www.w3.org/TR/REC-html40"">{0}", Environment.NewLine);
        sb.AppendFormat(@" <Styles>{0}", Environment.NewLine);
        sb.AppendFormat(@"  <Style ss:ID=""Default"" ss:Name=""Normal"">{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Alignment ss:Vertical=""Bottom""/>{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Borders/>{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Font ss:FontName=""Tahoma"" x:Family=""Swiss"" ss:Size=""8"" ss:Color=""#000000""/>{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Interior/>{0}", Environment.NewLine);
        sb.AppendFormat(@"   <NumberFormat/>{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Protection/>{0}", Environment.NewLine);
        sb.AppendFormat(@"  </Style>{0}", Environment.NewLine);
        sb.AppendFormat(@"  <Style ss:ID=""s62"">{0}", Environment.NewLine);
        sb.AppendFormat(@"   <Font ss:FontName=""Tahoma"" x:Family=""Swiss"" ss:Size=""8"" ss:Color=""#eb8500""{0}", Environment.NewLine);
        sb.AppendFormat(@"    ss:Bold=""1""/>{0}", Environment.NewLine);
        sb.AppendFormat(@"  </Style>{0}", Environment.NewLine);
        sb.AppendFormat(@" </Styles>{0}", Environment.NewLine);
        sb.Append(@"{0}\r\n</Workbook>");

        return sb.ToString();
    }

    private string replaceXmlChar(string input)
    {
        input = input.Replace("&", "&amp;");
        input = input.Replace("<", "&lt;");
        input = input.Replace(">", "&gt;");
        input = input.Replace("\"", "&quot;");
        input = input.Replace("'", "&apos;");
        input = input.Replace("ç", "&#231;");
        input = input.Replace("Ç", "&#199;");
        input = input.Replace("İ", "&#304;");
        input = input.Replace("ı", "&#305;");
        input = input.Replace("Ö", "&#214;");
        input = input.Replace("ö", "&#246;");
        input = input.Replace("Ü", "&#220;");
        input = input.Replace("ü", "&#252;");
        input = input.Replace("Ğ", "&#286;");
        input = input.Replace("ğ", "&#287;");
        input = input.Replace("Ş", "&#350;");
        input = input.Replace("ş", "&#351;");
        input = input.Replace("ß", "&#223;");
        input = input.Replace("€", "&#8364;");
        input = input.Replace("é", "&#233;");
        input = input.Replace("ë", "&#235;");
        input = input.Replace("ê", "&#234;");
        input = input.Replace("è", "&#232;");
        input = input.Replace("ì", "&#236;");
        input = input.Replace("í", "&#237;");
        input = input.Replace("î", "&#238;");
        input = input.Replace("ï", "&#239;");
        input = input.Replace("æ", "&#230;");
        input = input.Replace("å", "&#229;");
        input = input.Replace("ä", "&#228;");
        input = input.Replace("ã", "&#227;");
        input = input.Replace("â", "&#226;");
        input = input.Replace("á", "&#225;");
        input = input.Replace("à", "&#224;");
        input = input.Replace("ƒ", "&#402;");
        input = input.Replace("Ÿ", "&#376;");
        input = input.Replace("š", "&#353;");
        input = input.Replace("Š", "&#352;");
        input = input.Replace("œ", "&#339;");
        input = input.Replace("Œ", "&#338;");
        input = input.Replace("ÿ", "&#255;");
        input = input.Replace("þ", "&#254;");
        input = input.Replace("ý", "&#253;");
        input = input.Replace("û", "&#251;");
        input = input.Replace("ú", "&#250;");
        input = input.Replace("ù", "&#249;");
        input = input.Replace("ø", "&#248;");
        input = input.Replace("õ", "&#245;");
        input = input.Replace("ô", "&#244;");
        input = input.Replace("ó", "&#243;");
        input = input.Replace("ò", "&#242;");
        input = input.Replace("ñ", "&#241;");
        input = input.Replace("ð", "&#240;");
        input = input.Replace("Þ", "&#222;");
        input = input.Replace("Ý", "&#221;");
        input = input.Replace("Ü", "&#220;");
        input = input.Replace("Û", "&#219;");
        input = input.Replace("Ú", "&#218;");
        input = input.Replace("Ù", "&#217;");
        input = input.Replace("Ø", "&#216;");
        input = input.Replace("Ö", "&#214;");
        input = input.Replace("Õ", "&#213;");
        input = input.Replace("Ô", "&#212;");
        input = input.Replace("Ó", "&#211;");
        input = input.Replace("Ò", "&#210;");
        input = input.Replace("Ñ", "&#209;");
        input = input.Replace("Ð", "&#208;");
        input = input.Replace("Ï", "&#207;");
        input = input.Replace("Î", "&#206;");
        input = input.Replace("Í", "&#205;");
        input = input.Replace("Ì", "&#204;");
        input = input.Replace("Ë", "&#203;");
        input = input.Replace("Ê", "&#202;");
        input = input.Replace("É", "&#201;");
        input = input.Replace("È", "&#200;");
        input = input.Replace("Æ", "&#198;");
        input = input.Replace("Å", "&#197;");
        input = input.Replace("Ä", "&#196;");
        input = input.Replace("Ã", "&#195;");
        input = input.Replace("Â", "&#194;");
        input = input.Replace("Á", "&#193;");
        input = input.Replace("À", "&#192;");
        input = input.Replace("™", "&#8482;");
        input = input.Replace("‰", "&#8240;");
        input = input.Replace("…", "&#8230;");
        input = input.Replace("•", "&#8226;");
        input = input.Replace("‡", "&#8225;");
        input = input.Replace("†", "&#8224;");
        input = input.Replace("„", "&#8222;");
        input = input.Replace("”", "&#8221;");
        input = input.Replace("“", "&#8220;");
        input = input.Replace("‚", "&#8218;");
        input = input.Replace("’", "&#8217;");
        input = input.Replace("‘", "&#8216;");
        input = input.Replace("—", "&#8212;");
        input = input.Replace("–", "&#8211;");
        input = input.Replace("¿", "&#191;");
        input = input.Replace("¾", "&#190;");
        input = input.Replace("½", "&#189;");
        input = input.Replace("¼", "&#188;");
        input = input.Replace("º", "&#186;");
        input = input.Replace("¹", "&#185;");
        input = input.Replace("¸", "&#184;");
        input = input.Replace("·", "&#183;");
        input = input.Replace("¶", "&#182;");
        input = input.Replace("µ", "&#181;");
        input = input.Replace("´", "&#180;");
        input = input.Replace("³", "&#179;");
        input = input.Replace("²", "&#178;");
        input = input.Replace("±", "&#177;");
        input = input.Replace("°", "&#176;");
        input = input.Replace("¯", "&#175;");
        input = input.Replace("®", "&#174;");
        input = input.Replace("¬", "&#172;");
        input = input.Replace("«", "&#171;");
        input = input.Replace("ª", "&#170;");
        input = input.Replace("©", "&#169;");
        input = input.Replace("¨", "&#168;");
        input = input.Replace("§", "&#167;");
        input = input.Replace("¦", "&#166;");
        input = input.Replace("¥", "&#165;");
        input = input.Replace("¤", "&#164;");
        input = input.Replace("£", "&#163;");
        input = input.Replace("¢", "&#162;");
        input = input.Replace("¡", "&#161;");
        input = input.Replace("~", "&#126;");
        input = input.Replace("`", "&#96;");
        input = input.Replace("_", "&#95;");
        input = input.Replace("^", "&#94;");

        return input;
    }

    private string getCell(Type type, object cellData, string strColumn)
    {
        object data = (cellData is DBNull) ? "" : cellData;

        if (ColumnControl(strColumn, cellData))
        {
            if (type.Name.Contains("Int") || type.Name.Contains("Double") || type.Name.Contains("Decimal"))
            {
                return string.Format("<Cell><Data ss:Type=\"Number\">{0}</Data></Cell>", data);
            }
        }

        return string.Format("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", replaceXmlChar(data.ToString()));
    }

    private bool ColumnControl(string strColumn, object cellData)
    {
        if (_strIntList != null)
        {
            for (int i = 0; i < _strIntList.Length; i++)
            {
                if (_strIntList[i] == strColumn)
                {
                    _intSum[i] += Utilities.NullFixInt(cellData.ToString());
                    return true;
                }
            }
        }
        return false;
    }

    #region getWorksheets
    private string getWorksheets(DataSet source)
    {
        StringWriter sw = new StringWriter();

        if (source == null || source.Tables.Count == 0)
        {
            sw.Write("<Worksheet ss:Name=\"Sheet1\">\r\n<Table>\r\n<Row><Cell><Data ss:Type=\"String\"></Data></Cell></Row>\r\n</Table>\r\n</Worksheet>");
            return sw.ToString();
        }

        foreach (DataTable dt in source.Tables)
        {
            if (_strIntList != null)
            {
                this._intSum = new int[_strIntList.Length];
            }

            if (dt.Rows.Count == 0)
                sw.Write("<Worksheet ss:Name=\"" + replaceXmlChar(dt.TableName) + "\">\r\n<Table>\r\n<Row><Cell ss:StyleID=\"s62\"><Data ss:Type=\"String\"></Data></Cell></Row>\r\n</Table>\r\n</Worksheet>");
            else
            {
                int sheetCount = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((i % rowLimit) == 0)
                    {
                        if ((i / rowLimit) > sheetCount)
                        {
                            sw.Write("\r\n</Table>\r\n</Worksheet>");
                            sheetCount = (i / rowLimit);
                        }
                        sw.Write("\r\n<Worksheet ss:Name=\"" + replaceXmlChar(dt.TableName) + (((i / rowLimit) == 0) ? "" : Convert.ToString(i / rowLimit)) + "\">\r\n<Table>");

                        sw.Write("\r\n<Row>");
                        foreach (DataColumn dc in dt.Columns)
                            sw.Write(string.Format("<Cell ss:StyleID=\"s62\"><Data ss:Type=\"String\">{0}</Data></Cell>", replaceXmlChar(dc.ColumnName)));
                        sw.Write("</Row>");
                    }
                    sw.Write("\r\n<Row>");

                    foreach (DataColumn dc in dt.Columns)
                        sw.Write(getCell(dc.DataType, dt.Rows[i][dc.ColumnName], dc.ColumnName));
                    sw.Write("</Row>");
                }

                if (_strIntList != null)
                {
                    sw.Write("\r\n<Row>");
                    foreach (DataColumn dc in dt.Columns)
                    {
                        bool bolIn = false;
                        for (int i = 0; i < _strIntList.Length; i++)
                        {
                            if (_strIntList[i] == dc.ColumnName)
                            {
                                bolIn = true;
                                sw.Write(getCell(dc.DataType, _intSum[i], dc.ColumnName));
                                break;
                            }
                            bolIn = false;
                        }

                        if (!bolIn)
                        {
                            sw.Write(getCell(dc.DataType, "", dc.ColumnName));
                        }
                    }
                    sw.Write("</Row>");
                }
                sw.Write("\r\n</Table>\r\n</Worksheet>");
            }
        }
        return sw.ToString();
    }

    private string getWorksheets(DataTable[] dtInput)
    {
        StringWriter sw = new StringWriter();

        if (dtInput == null || dtInput.Length == 0)
        {
            sw.Write("<Worksheet ss:Name=\"Sheet1\">\r\n<Table>\r\n<Row><Cell><Data ss:Type=\"String\"></Data></Cell></Row>\r\n</Table>\r\n</Worksheet>");
            return sw.ToString();
        }

        foreach (DataTable dt in dtInput)
        {
            if (_strIntList != null)
            {
                this._intSum = new int[_strIntList.Length];
            }

            if (dt.Rows.Count == 0)
                sw.Write("<Worksheet ss:Name=\"" + replaceXmlChar(dt.TableName) + "\">\r\n<Table>\r\n<Row><Cell ss:StyleID=\"s62\"><Data ss:Type=\"String\"></Data></Cell></Row>\r\n</Table>\r\n</Worksheet>");
            else
            {
                int sheetCount = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((i % rowLimit) == 0)
                    {
                        if ((i / rowLimit) > sheetCount)
                        {
                            sw.Write("\r\n</Table>\r\n</Worksheet>");
                            sheetCount = (i / rowLimit);
                        }
                        sw.Write("\r\n<Worksheet ss:Name=\"" + replaceXmlChar(dt.TableName) + (((i / rowLimit) == 0) ? "" : Convert.ToString(i / rowLimit)) + "\">\r\n<Table>");

                        sw.Write("\r\n<Row>");
                        foreach (DataColumn dc in dt.Columns)
                            sw.Write(string.Format("<Cell ss:StyleID=\"s62\"><Data ss:Type=\"String\">{0}</Data></Cell>", replaceXmlChar(dc.ColumnName)));
                        sw.Write("</Row>");
                    }
                    sw.Write("\r\n<Row>");

                    foreach (DataColumn dc in dt.Columns)
                        sw.Write(getCell(dc.DataType, dt.Rows[i][dc.ColumnName], dc.ColumnName));
                    sw.Write("</Row>");
                }

                if (_strIntList != null)
                {
                    sw.Write("\r\n<Row>");
                    foreach (DataColumn dc in dt.Columns)
                    {
                        bool bolIn = false;
                        for (int i = 0; i < _strIntList.Length; i++)
                        {
                            if (_strIntList[i] == dc.ColumnName)
                            {
                                bolIn = true;
                                sw.Write(getCell(dc.DataType, _intSum[i], dc.ColumnName));
                                break;
                            }
                            bolIn = false;
                        }

                        if (!bolIn)
                        {
                            sw.Write(getCell(dc.DataType, "", dc.ColumnName));
                        }
                    }
                    sw.Write("</Row>");
                }

                sw.Write("\r\n</Table>\r\n</Worksheet>");
            }
        }
        return sw.ToString();
    }
    #endregion

    #region GetExcelXml
    private string GetExcelXml(DataTable dtInput)
    {
        string excelTemplate = getWorkbookTemplate();
        DataSet ds = new DataSet();
        ds.Tables.Add(dtInput.Copy());
        string worksheets = getWorksheets(ds);
        string excelXml = string.Format(excelTemplate, worksheets);
        return excelXml;
    }

    private string GetExcelXml(DataSet dsInput)
    {
        string excelTemplate = getWorkbookTemplate();
        string worksheets = getWorksheets(dsInput);
        string excelXml = string.Format(excelTemplate, worksheets);
        return excelXml;
    }

    private string GetExcelXml(DataTable[] dtInput)
    {
        string excelTemplate = getWorkbookTemplate();
        string worksheets = getWorksheets(dtInput);
        string excelXml = string.Format(excelTemplate, worksheets);
        return excelXml;
    }
    #endregion

    public void ToExcel(DataSet dsInput, string strFile)
    {
        try
        {
            string excelXml = GetExcelXml(dsInput);

            System.Xml.XmlDocument ss = new System.Xml.XmlDocument();
            ss.LoadXml(excelXml);
            ss.Save(strFile);
        }
        catch { }
    }

    public void ToExcel(DataTable dtInput, string strFile)
    {
        try
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dtInput.Copy());
            ToExcel(ds, strFile);
        }
        catch { }
    }

    public void ToExcel(DataTable[] dtInput, string strFile)
    {
        try
        {
            string excelXml = GetExcelXml(dtInput);

            System.Xml.XmlDocument ss = new System.Xml.XmlDocument();
            ss.LoadXml(excelXml);
            ss.Save(strFile);
        }
        catch { }
    }

}
#endregion

public static class cConverter
{
    public static DataTable ToDataTable<T>(this IEnumerable<T> items)
    {
        var tb = new DataTable(typeof(T).Name);
        PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (var prop in props)
        {
            tb.Columns.Add(prop.Name, prop.PropertyType);
        }

        foreach (var item in items)
        {
            var values = new object[props.Length];
            for (var i = 0; i < props.Length; i++)
            {
                values[i] = props[i].GetValue(item, null);
            }

            tb.Rows.Add(values);
        }
        return tb;
    }
}