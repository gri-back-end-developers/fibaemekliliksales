﻿#region Directives
using ModelLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Web.Script.Serialization;
using System.Xml;
#endregion

namespace FibaEmeklilikService
{
    public partial class Service1 : ServiceBase
    {
        Timer tmr;
        TimerCallback tmrCallBack;

        #region Service1
        public Service1()
        {
            InitializeComponent();
        }
        #endregion 

        #region OnStart
        protected override void OnStart(string[] args)
        {
            //ScratchWin();
            //ScratchWinPaycell();

            tmrCallBack = new TimerCallback(StartMethods);
            tmr = new Timer(tmrCallBack, null, 0, 10000);
        }
        #endregion

        #region StartMethods
        void StartMethods(Object state)
        {
            if (ConfigurationManager.AppSettings["ScratchWinRun"].ToString() == "on")
            {
                ScratchWin();

                ScratchWinPaycell();
            }
            SendMail();
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            tmr.Dispose();
        }
        #endregion

        #region SendMail
        void SendMail()
        {
            try
            {
                List<int> updateTime = ConfigurationManager.AppSettings["UpdateTime"].ToString().Split(':').Select(s => int.Parse(s)).ToList();

                int intHour = DateTime.Now.Hour;
                int intMin = DateTime.Now.Minute;

                if (intHour == updateTime[0] && intMin == updateTime[1])
                {
                    using (DBEntities db = new DBEntities())
                    {
                        string[] strEmails = ConfigurationManager.AppSettings["Mails"].ToString().Split(';');

                        DateTime time = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString());

                        var sales = db.vwSales.Where(w => w.dtRegisterDate >= time).ToList();
                        if (sales.Count > 0)
                        {
                            string strFile = ConfigurationManager.AppSettings["FilePath"] + "KpsLogs.xls";

                            DataTable dt = cConverter.ToDataTable<vwSales>(sales);
                            dt.Columns["strType"].ColumnName = "Proje/Ürün";
                            dt.Columns["strUserName"].ColumnName = "Kullanıcı Adı";
                            dt.Columns["strIP"].ColumnName = "IP";
                            dt.Columns["strTCKN"].ColumnName = "Kimlik No";
                            dt.Columns["strName"].ColumnName = "Ad";
                            dt.Columns["strSurname"].ColumnName = "Soyad";
                            dt.Columns["dtRegisterDate"].ColumnName = "Tarih";

                            ExcelExportSheet excel = new ExcelExportSheet(null);
                            excel.ToExcel(dt, strFile);

                            foreach (var item in strEmails)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    Email.SendEmail("Eacente ve Online Kps Logları", "", item.Trim(), new System.Net.Mail.Attachment(strFile));
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in strEmails)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    Email.SendEmail("Eacente ve Online Kps Logları", "Kayıt bulunamadı.", item.Trim());
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }
        #endregion

        #region ScratchWin
        void ScratchWin()
        {
            Logs logs = new Logs();
            int intScratchWinSaleID = 0;

            try
            {
                string[] applicationID = ConfigurationManager.AppSettings["smsApplicationID"].ToString().Split(',');

                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                {
                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                });

                using (DBEntities db = new DBEntities())
                {
                    for (int i = 0; i < applicationID.Length; i++)
                    {
                        string smsData = sms.MainReport(applicationID[i]);
                        //string smsData = "<Messages><Message><MsgID>1234568</MsgID><Date>xxx</Date><Number>905305518091</Number><Text>BASLA 24658240090 abc</Text></Message></Messages>";

                        if (!string.IsNullOrEmpty(smsData) && smsData.Length > 2)
                        {
                            List<cMessages> messages = Messages(smsData);

                            foreach (var message in messages)
                            {
                                try
                                {
                                    string strSmsMsg = "";
                                    string strMsg = "";
                                    tblScratchWin scratchWin = null;
                                    int intScratchWinID = 0;

                                    if (!string.IsNullOrEmpty(message.TextData.Activation))
                                    {
                                        scratchWin = db.tblScratchWin.Where(w => w.strActivationCode == message.TextData.Activation).FirstOrDefault();
                                        if (scratchWin != null)
                                        {
                                            if (scratchWin.bolIsUsed)
                                            {
                                                strSmsMsg = ConfigurationManager.AppSettings["UsedActivationMsg"];
                                                strMsg = "Aktivasyon kodu daha önceden kullanılmış!";
                                            }
                                            else if (!scratchWin.bolIsActive)
                                            {
                                                strSmsMsg = ConfigurationManager.AppSettings["PassiveActivationMsg"];
                                                strMsg = "Aktivasyon kodu kullanıma kapalı!";
                                            }
                                            else
                                            {
                                                intScratchWinID = scratchWin.intScratchWinID;
                                            }
                                        }
                                        else
                                        {
                                            strSmsMsg = ConfigurationManager.AppSettings["NotExistActivationMsg"];
                                            strMsg = "Aktivasyon kodu bulunamadı!";
                                        }
                                    }

                                    intScratchWinSaleID = logs.SalesLogInsert(intScratchWinID, message, strMsg);

                                    if (!string.IsNullOrEmpty(message.TextData.Tckn))
                                    {
                                        Kps kps = new Kps();
                                        cTcknInfo tcknInfo = kps.TCKNValid(message.TextData.Tckn);

                                        if (tcknInfo != null)
                                        {
                                            tcknInfo.strPhone = message.Number;

                                            logs.SalesLogUpdate_TcknInfo(intScratchWinSaleID, tcknInfo);

                                            if (intScratchWinID > 0)
                                            {
                                                Policy policy = new Policy();
                                                cPolicyData policyData = new cPolicyData();

                                                bool bolPaycell = string.IsNullOrEmpty(scratchWin.strPaycell) ? false : (scratchWin.strPaycell.ToLower() == "t");

                                                bool bolSuccess = policy.Create(intScratchWinSaleID, scratchWin.strTariffCode, bolPaycell, tcknInfo, ref policyData, ref strMsg);

                                                logs.SalesLogUpdate_Policy(intScratchWinSaleID, policyData, bolSuccess, strMsg);

                                                if (bolSuccess)
                                                {
                                                    //bool bolCrmSend = Email.SendCRM(ConfigurationManager.AppSettings["crmOwnerId"].ToString(), ConfigurationManager.AppSettings["crmOwnerId"].ToString(), ConfigurationManager.AppSettings["crmOwnerId"].ToString(), tcknInfo.strName, tcknInfo.strSurname, ConfigurationManager.AppSettings["crmOfferNo"], tcknInfo.strPhone, "Poliçe numarası : " + policyData.PoliceNo.ToString());
                                                    bool bolCrmSend = false;

                                                    scratchWin.bolIsUsed = true;
                                                    scratchWin.dtUsedDate = DateTime.Now;
                                                    db.SaveChanges();
                                                    
                                                    string strSmsToken = Utilities.RandomString(10).ToLower();

                                                    logs.SalesLogUpdate_TokenAndCrm(intScratchWinSaleID, strSmsToken, bolCrmSend);

                                                    if (!bolPaycell)
                                                    {
                                                        string strPdfDocumentLink = ConfigurationManager.AppSettings["PdfDocumentLink"].ToString();
                                                        string strPdfMessage = string.Format(ConfigurationManager.AppSettings["PdfDocumentMsg"].ToString(), db.tblPackets.First(f => f.intPacketID == policyData.PacketID).tblProducts.strName, strPdfDocumentLink);

                                                        sms.Send(strPdfMessage, message.Number);

                                                        Thread.Sleep(45000);

                                                        string strPdfPolicyLink = string.Format(ConfigurationManager.AppSettings["PdfPolicyLink"].ToString(), strSmsToken);

                                                        strSmsMsg = string.Format(ConfigurationManager.AppSettings["SuccessMsg"].ToString(), strPdfPolicyLink);
                                                    }
                                                    else
                                                    {
                                                        string strPayLink = string.Format(ConfigurationManager.AppSettings["PayLink"].ToString(), strSmsToken);

                                                        strSmsMsg = string.Format(ConfigurationManager.AppSettings["PayMsg"].ToString(), strPayLink);
                                                    }

                                                    CacheClear();
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(strMsg))
                                                    {
                                                        logs.SalesLogUpdate_Msg(intScratchWinSaleID, "Poliçe oluşturulamadı!");
                                                    }

                                                    strSmsMsg = ConfigurationManager.AppSettings["PolicyErrorMsg"];
                                                }
                                            }
                                        }
                                        else
                                        {
                                            logs.SalesLogUpdate_Msg(intScratchWinSaleID, "Hatalı kimlik numarası!");

                                            strSmsMsg = ConfigurationManager.AppSettings["ErrorTcknMsg"];
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(strSmsMsg))
                                    {
                                        sms.Send(strSmsMsg, message.Number);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logs.ServiceLog(intScratchWinSaleID, ex);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logs.ServiceLog(intScratchWinSaleID, ex);
            }
            finally
            {
                logs.Dispose();
            }
        }
        #endregion

        #region Messages
        private List<cMessages> Messages(string strXML)
        {
            string strKey = ConfigurationManager.AppSettings["smsKey"].ToString();

            List<cMessages> messages = new List<cMessages>();

            XmlDataDocument xmldoc = new XmlDataDocument();

            xmldoc.LoadXml(strXML);
            XmlNodeList xmlMessages = xmldoc.GetElementsByTagName("Messages");

            for (int i = 0; i < xmlMessages.Count; i++)
            {
                var xmlMessage = xmlMessages[i].SelectNodes("Message");
                for (int j = 0; j < xmlMessage.Count; j++)
                {
                    try
                    {
                        string strText = xmlMessage[j].ChildNodes.Item(3).InnerText.Trim().Replace("  ", " ");
                        string[] strTextData = strText.Split(' ');

                        cMessageText textData = new cMessageText();
                        if (strTextData.Length > 0)
                        {
                            textData.Prefix = Utilities.ConvertTurkish(strTextData[0].Trim().ToLower());
                            if (textData.Prefix == strKey)
                            {
                                if (strTextData.Length > 1 && strTextData.Length <= 3)
                                {
                                    textData.Tckn = strTextData.Length > 1 ? strTextData[1].Trim() : "";
                                    textData.Activation = strTextData.Length > 2 ? strTextData[2].Trim() : "";
                                }

                                messages.Add(new cMessages
                                {
                                    MsgID = xmlMessage[j].ChildNodes.Item(0).InnerText.Trim(),
                                    Date = Utilities.StringToDate(xmlMessage[j].ChildNodes.Item(1).InnerText.Trim()),
                                    Number = xmlMessage[j].ChildNodes.Item(2).InnerText.Trim(),
                                    Text = strText,
                                    TextData = textData
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs logs = new Logs();
                        logs.ServiceLog(0, ex);
                    }
                }
            }

            return messages;
        }
        #endregion

        #region CacheClear
        private void CacheClear()
        {
            try
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                wc.OpenRead(System.Configuration.ConfigurationManager.AppSettings["CacheLink"]);
            }
            catch { }
        }
        #endregion

        #region ScratchWinPaycell
        void ScratchWinPaycell()
        {
            Logs logs = new Logs();
            int intScratchWinSaleID = 0;

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    List<tblScratchWinPaycell> payCells = db.tblScratchWinPaycell.Where(w => !w.bolIsPaid.HasValue && !w.bolPassive.HasValue).ToList();

                    foreach (var payCell in payCells)
                    {
                        try
                        {
                            intScratchWinSaleID = payCell.intScratchWinSaleID;

                            cPaycellData paycellData = PaycellData(payCell.strTrackingId);
                            if (paycellData != null && paycellData.status != null && paycellData.status != "PENDING")
                            { 
                                payCell.strResultData = paycellData.paymentResult != null ? paycellData.paymentResult.ToString() : "";
                                payCell.dtUpdateDate = DateTime.Now;

                                if (paycellData.status == "SUCCESS")
                                {
                                    payCell.bolIsPaid = true;
                                    payCell.bolPassive = true;

                                    using (FIBAWS2.fibaws2Client service = new FIBAWS2.fibaws2Client())
                                    {
                                        var teklifpolicelesme = service.teklifpolicelesme(payCell.tblScratchWinSales.intPolicyID.Value, 1, "", "");

                                        if (string.IsNullOrEmpty(teklifpolicelesme.cevap))
                                        {
                                            payCell.tblScratchWinSales.intTeklifPolicyID = (int)teklifpolicelesme.policeno.Value;
                                            payCell.tblScratchWinSales.strTeklifMsg = "Teklif poliçeleşme başarılı";

                                            TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                                            {
                                                ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                                                Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                                                PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                                                Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                                                UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                                            });

                                            JavaScriptSerializer jss = new JavaScriptSerializer();
                                            cPolicyData policyData = jss.Deserialize<cPolicyData>(payCell.tblScratchWinSales.strPolicyData);

                                            string strPdfDocumentLink = ConfigurationManager.AppSettings["PdfDocumentLink"].ToString();
                                            string strPdfMessage = string.Format(ConfigurationManager.AppSettings["PdfDocumentMsg"].ToString(), db.tblPackets.First(f => f.intPacketID == policyData.PacketID).tblProducts.strName, strPdfDocumentLink);

                                            sms.Send(strPdfMessage, payCell.tblScratchWinSales.strPhone);

                                            Thread.Sleep(45000);

                                            string strSmsToken = payCell.tblScratchWinSales.strSmsToken;
                                            if (string.IsNullOrEmpty(strSmsToken))
                                            { 
                                                strSmsToken = Utilities.RandomString(10).ToLower();

                                                logs.SalesLogUpdate_TokenAndCrm(intScratchWinSaleID, strSmsToken, true); 
                                            }

                                            string strPdfPolicyLink = string.Format(ConfigurationManager.AppSettings["PdfPolicyLink"].ToString(), strSmsToken);
                                            string strSuccessMsg  = string.Format(ConfigurationManager.AppSettings["SuccessMsg"].ToString(), strPdfPolicyLink);

                                            sms.Send(strSuccessMsg, payCell.tblScratchWinSales.strPhone);
                                        }
                                        else
                                        {
                                            payCell.tblScratchWinSales.strTeklifMsg = teklifpolicelesme.cevap;
                                        }
                                    }
                                }
                                else
                                {
                                   // payCell.bolIsPaid = false;
                                }

                                db.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Email.SendEmail("Kazın kazan hata", payCell.tblScratchWinSales.strPolicyData, "selda.abat@fibaemeklilik.com.tr");

                            logs.ServiceLog(intScratchWinSaleID, ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logs.ServiceLog(intScratchWinSaleID, ex);
            }
            finally
            {
                logs.Dispose();
            }
        }

        cPaycellData PaycellData(string strTrackingId)
        {
            cPaycellData paycellData = new cPaycellData();
            try
            {
                string strUrl = string.Format(ConfigurationManager.AppSettings["PaycellStatusLink"].ToString(), strTrackingId);

                var request = WebRequest.Create(strUrl);
                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    request.ContentType = "application/json; charset=utf-8";

                    paycellData = JsonConvert.DeserializeObject<cPaycellData>(sr.ReadToEnd());
                }
            }
            catch { }

            return paycellData;
        }
        #endregion
    }
}
