﻿using System; 

namespace FibaEmeklilikService
{
    public class cMessages
    {
        public string MsgID { get; set; }
        public string Date { get; set; }
        public string Number { get; set; }
        public string Text { get; set; }
        public cMessageText TextData { get; set; }
    }

    public class cMessageText
    {
        public string Prefix { get; set; }
        public string Tckn { get; set; }
        public string Activation { get; set; } 
    }
}
