﻿using ModelLibrary;
using System;
using System.Linq;
using System.Web.Script.Serialization;

namespace FibaEmeklilikService
{
    public class Logs
    {
        DBEntities db;

        public Logs()
        {
            db = new DBEntities();
        }

        #region SalesLogInsert
        public int SalesLogInsert(int intScratchWinID, cMessages message, string strMsg)
        {
            if (db == null)
                db = new DBEntities();

            tblScratchWinSales winSale = new tblScratchWinSales
            {
                dtRegisterDate = DateTime.Now,
                bolSuccess = false,
                intScratchWinID = intScratchWinID,
                XmlDate = message.Date,
                strPhone = message.Number,
                XmlMsgID = message.MsgID,
                XmlText = message.Text,
                strMsg = strMsg
            };

            db.tblScratchWinSales.Add(winSale);
            db.SaveChanges();

            return winSale.intScratchWinSaleID;
        }
        #endregion

        #region SalesLogUpdate_TcknInfo
        public void SalesLogUpdate_TcknInfo(int intScratchWinSaleID, cTcknInfo tcknInfo)
        {
            if (db == null)
                db = new DBEntities();

            tblScratchWinSales scratchWinSale = db.tblScratchWinSales.FirstOrDefault(f => f.intScratchWinSaleID == intScratchWinSaleID);
            if (scratchWinSale != null)
            {
                scratchWinSale.strName = tcknInfo.strName;
                scratchWinSale.strSurname = tcknInfo.strSurname;
                scratchWinSale.strTCKN = cCrypto.EncryptText(tcknInfo.strTCKN);

                db.SaveChanges();
            }
        }
        #endregion

        #region SalesLogUpdate_Policy
        public void SalesLogUpdate_Policy(int intScratchWinSaleID, cPolicyData policyData, bool bolSuccess, string strMsg)
        {
            if (db == null)
                db = new DBEntities();

            tblScratchWinSales scratchWinSale = db.tblScratchWinSales.FirstOrDefault(f => f.intScratchWinSaleID == intScratchWinSaleID);
            if (scratchWinSale != null)
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();

                scratchWinSale.strPolicyData = jss.Serialize(policyData);
                scratchWinSale.intPolicyID = policyData.PoliceNo;
                scratchWinSale.bolSuccess = bolSuccess;
                scratchWinSale.strMsg = strMsg;
                scratchWinSale.intPacketID = policyData.PacketID;

                db.SaveChanges();
            }
        }
        #endregion

        #region SalesLogUpdate_Msg
        public void SalesLogUpdate_Msg(int intScratchWinSaleID, string strMsg)
        {
            if (db == null)
                db = new DBEntities();

            tblScratchWinSales scratchWinSale = db.tblScratchWinSales.FirstOrDefault(f => f.intScratchWinSaleID == intScratchWinSaleID);
            if (scratchWinSale != null)
            {
                scratchWinSale.strMsg = strMsg;

                db.SaveChanges();
            }
        }
        #endregion

        #region SalesLogUpdate_TokenAndCrm
        public void SalesLogUpdate_TokenAndCrm(int intScratchWinSaleID, string strSmsToken, bool bolCrmSend)
        {
            if (db == null)
                db = new DBEntities();

            tblScratchWinSales scratchWinSale = db.tblScratchWinSales.FirstOrDefault(f => f.intScratchWinSaleID == intScratchWinSaleID);
            if (scratchWinSale != null)
            {
                scratchWinSale.strSmsToken = strSmsToken;
                scratchWinSale.bolCrmSend = bolCrmSend;

                db.SaveChanges();
            }
        }
        #endregion

        #region WSLog
        public int WSLog(int intWSLogID, int intSaleLogID, string strSaleType, string strClassName, string strMethodType, string strJson, string strMethod)
        {
            if (db == null)
                db = new DBEntities();

            tblWSLogs wslog = new tblWSLogs
            {
                dtRegisterDate = DateTime.Now,
                intReleationWSLogID = intWSLogID,
                intSaleLogID = intSaleLogID,
                intStep = 0,
                intUserID = 0,
                strClassName = strClassName,
                strJson = strJson,
                strMethod = strMethod,
                strMethodType = strMethodType,
                strProject = "FibaService",
                strSaleType = strSaleType
            };

            db.tblWSLogs.Add(wslog);
            db.SaveChanges();

            return wslog.intWSLogID;
        }
        #endregion

        #region ServiceLog
        public void ServiceLog(int intScratchWinSaleID, Exception ex)
        {
            if (db == null)
                db = new DBEntities();
             
            db.tblScratchServiceLogs.Add(new tblScratchServiceLogs
            {
                dtRegisterDate = DateTime.Now,
                intScratchWinSaleID = intScratchWinSaleID,
                strException = ex.ToString()
            });
            db.SaveChanges(); 
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            db.Dispose();
        } 
        #endregion
    }
}
