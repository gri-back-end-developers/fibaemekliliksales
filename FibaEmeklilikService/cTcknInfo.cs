﻿using System;

namespace FibaEmeklilikService
{
    public class cTcknInfo
    {
        public string strTCKN { get; set; }
        public string strMaritalStatus { get; set; }
        public string strGender { get; set; }
        public string strFatherName { get; set; }
        public string strMotherName { get; set; }
        public string strBirthplace { get; set; }
        public string strBirthRegionCode { get; set; }
        public string strBirthDistrictCode { get; set; }
        public string strAdress { get; set; }
        public string strRegionCode { get; set; }
        public string strDistrictCode { get; set; }
        public string strCountryCode { get; set; }
        public string strPhone { get; set; }
        public string strName { get; set; }
        public string strSurname { get; set; }
        public DateTime? dtBirthDate { get; set; }
    }
}
