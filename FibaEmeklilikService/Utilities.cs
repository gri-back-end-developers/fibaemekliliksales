using System;
using System.Text;

namespace FibaEmeklilikService
{
    public class Utilities
    {
        #region NullFixLong
        public static long NullFixLong(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return long.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixInt
        public static int NullFixInt(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return int.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixByte
        public static byte NullFixByte(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return byte.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixShort
        public static short NullFixShort(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return short.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixDate
        public static DateTime NullFixDate(string Value)
        {
            if (Value == "" || Value == null)
            {
                return DateTime.Now;
            }
            try
            {
                return DateTime.Parse(Value);
            }
            catch
            {
                return DateTime.Now;
            }
        }
        #endregion

        #region NullFixString
        public static string NullFixString(object Value)
        {
            if (Value == null)
            {
                Value = "";
            }
            try
            {
                return Value.ToString();
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region NullFixDouble
        public static double NullFixDouble(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return double.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixBool
        public static bool NullFixBool(string Value)
        {
            if (Value == null || Value == "0")
            {
                return false;
            }
            else if (Value == "1")
            {
                return true;
            }
            try
            {
                return bool.Parse(Value);
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region NullFixFloat
        public static float NullFixFloat(string strValue)
        {
            try
            {
                return float.Parse(strValue.ToString());
            }

            catch
            {
                return 0.0F;
            }
        }
        #endregion

        #region NullFixDecimal
        public static decimal NullFixDecimal(string strValue)
        {
            try
            {
                return decimal.Parse(strValue.ToString());
            }

            catch
            {
                return 0;
            }
        }
        #endregion

        #region StringToDate
        public static string StringToDate(string strValue)
        {
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    strValue = strValue.Substring(0, 2) + "." + strValue.Substring(2, 2) + "." + strValue.Substring(4, 4) + " " + strValue.Substring(8, 2) + ":" + strValue.Substring(10, 2) + ":" + strValue.Substring(12, 2);
                }
                catch { }
            }

            return strValue;
        }
        #endregion

        #region RandomString
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));

                builder.Append(ch);
            }

            return ConvertTurkish(builder.ToString().ToLower());
        }
        #endregion

        #region ConvertTurkish
        public static string ConvertTurkish(string input)
        {
            input = input.Replace("�", "c");
            input = input.Replace("�", "C");
            input = input.Replace("�", "I");
            input = input.Replace("�", "i");
            input = input.Replace("�", "O");
            input = input.Replace("�", "o");
            input = input.Replace("�", "U");
            input = input.Replace("�", "u");
            input = input.Replace("�", "G");
            input = input.Replace("�", "g");
            input = input.Replace("�", "S");
            input = input.Replace("�", "s");

            return input;
        }
        #endregion
    }
}