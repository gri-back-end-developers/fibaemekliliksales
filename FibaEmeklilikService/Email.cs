#region Directives
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail; 
#endregion

namespace FibaEmeklilikService
{
    public class Email
    {
        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody)
        {
            try
            {
                MailAddress adressFrom = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), "Eacente Servis");
                MailAddress adressTo = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString());
                MailMessage mailMessage = new MailMessage(adressFrom, adressTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());

                server.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody, string strTo, Attachment attachment = null)
        {
            try
            {
                MailAddress adressFrom = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), "Eacente Servis");
                MailAddress adressTo = new MailAddress(strTo);
                MailMessage mailMessage = new MailMessage(adressFrom, adressTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                if (attachment != null)
                {
                    mailMessage.Attachments.Add(attachment);
                }

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());
                server.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody, string strInfo, string strTo)
        {
            try
            {
                MailAddress adressFrom = new MailAddress(strInfo, "Eacente Servis");
                MailAddress adressTo = new MailAddress(strTo);
                MailMessage mailMessage = new MailMessage(adressFrom, adressTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());
                server.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SendCRM
        public static bool SendCRM(string strOwnerId, string strQueueId, string strPhoneCallType, string strCustomerName, string strCustomerLastName, string strOfferNo, string strPhoneNumber, string strDescription)
        {
            try
            {
                if (!string.IsNullOrEmpty(strPhoneNumber))
                {
                    strPhoneNumber = strPhoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "");
                    if (strPhoneNumber.Length == 11)
                    {
                        strPhoneNumber = strPhoneNumber.Remove(0, 1);
                    }
                }

                using (CrmWS.FibaEmeklilikService crm = new CrmWS.FibaEmeklilikService())
                {
                    var request = new CrmWS.CreateOutBoundCallRequest
                    {
                        HeaderData = new CrmWS.RequestHeader
                        {
                            Password = ConfigurationManager.AppSettings["crmPassword"].ToString(),
                            Username = ConfigurationManager.AppSettings["crmUsername"].ToString()
                        },
                        CustomerLastName = strCustomerLastName,
                        CustomerName = strCustomerName,
                        OfferNo = strOfferNo,
                        OwnerId = strOwnerId,
                        OwnerType = "team",
                        PhoneCallType = strPhoneCallType,
                        PhoneNumber = strPhoneNumber.Remove(0, 1),
                        QueueId = strQueueId,
                        Description = strDescription
                    };

                    var result = crm.CreateOutBoundCall(request);
                    if (result.ResponseCode == "200")
                    {
                        return true;
                    }
                }
            }
            catch { }

            return false;
        }
        #endregion
    }
}