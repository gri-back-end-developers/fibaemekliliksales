﻿using System; 

namespace FibaEmeklilikService
{
    public class cPolicyData
    {
        public int PacketID { get; set; }
        public int PoliceNo { get; set; }
        public decimal? ToplamPirim { get; set; }
        public decimal? IlkPrim { get; set; }
        public string TeklifDurum { get; set; }
        public decimal? IlkPrimKomisyon { get; set; }
        public decimal? ToplamKomisyon { get; set; }
        public string KrediNo { get; set; } 
        public int intCrediTime { get; set; }
    }
}
