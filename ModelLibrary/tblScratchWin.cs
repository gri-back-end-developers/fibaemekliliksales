//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblScratchWin
    {
        public tblScratchWin()
        {
            this.tblScratchWinSales = new HashSet<tblScratchWinSales>();
        }
    
        public int intScratchWinID { get; set; }
        public string strBarcode { get; set; }
        public string strActivationCode { get; set; }
        public string strRefNo { get; set; }
        public string strTariffCode { get; set; }
        public string strPacketInfo { get; set; }
        public string strChannel { get; set; }
        public string strSeller { get; set; }
        public string strPaycell { get; set; }
        public bool bolIsUsed { get; set; }
        public Nullable<System.DateTime> dtUsedDate { get; set; }
        public Nullable<int> intScratchWinExcelID { get; set; }
        public bool bolIsActive { get; set; }
        public int intInsUserID { get; set; }
        public Nullable<int> intUpdUserID { get; set; }
        public System.DateTime dtRegisterDate { get; set; }
        public Nullable<System.DateTime> dtUpdateDate { get; set; }
    
        public virtual tblScratchWinExcels tblScratchWinExcels { get; set; }
        public virtual ICollection<tblScratchWinSales> tblScratchWinSales { get; set; }
    }
}
