//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblLogs
    {
        public int intLogID { get; set; }
        public byte intPageID { get; set; }
        public int intUserID { get; set; }
        public int intID { get; set; }
        public byte intLogTypeID { get; set; }
        public string strIP { get; set; }
        public System.DateTime dtRegisterDate { get; set; }
    
        public virtual tblLogTypes tblLogTypes { get; set; }
        public virtual tblPages tblPages { get; set; }
        public virtual tblUsers tblUsers { get; set; }
    }
}
