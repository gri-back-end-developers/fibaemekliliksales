//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUserTransLogs
    {
        public int ID { get; set; }
        public string strTransaction { get; set; }
        public int intUserID { get; set; }
    
        public virtual tblUsers tblUsers { get; set; }
    }
}
