//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Test.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCallcenters
    {
        public tblCallcenters()
        {
            this.tblCallcenterCredits = new HashSet<tblCallcenterCredits>();
            this.tblCredits = new HashSet<tblCredits>();
        }
    
        public int intCallcenterID { get; set; }
        public string strName { get; set; }
        public string strEmail { get; set; }
        public string strBankNo { get; set; }
        public string strRegisterNo { get; set; }
        public string strCallcenterCode { get; set; }
        public string strTradeName { get; set; }
        public string strPhone { get; set; }
        public string strFax { get; set; }
        public string strAddress { get; set; }
        public bool bolInfoForm { get; set; }
        public bool bolBesSaleFinishedButton { get; set; }
        public byte intSaleTypeID { get; set; }
        public string strPublishKey { get; set; }
        public string strInformationForm { get; set; }
        public string strInformationFormBesPlan { get; set; }
        public string strInformationFormCredit { get; set; }
        public decimal flKKDF { get; set; }
        public decimal flBSMV { get; set; }
        public bool bolCancel { get; set; }
        public bool bolIsActive { get; set; }
        public bool bolIsDelete { get; set; }
        public Nullable<System.DateTime> dtUpdateDate { get; set; }
        public System.DateTime dtRegisterDate { get; set; }
    
        public virtual ICollection<tblCallcenterCredits> tblCallcenterCredits { get; set; }
        public virtual ICollection<tblCredits> tblCredits { get; set; }
    }
}
