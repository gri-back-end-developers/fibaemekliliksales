﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebServiceOKS.Classes;
using WebServiceOKS.OksService;

namespace WebServiceOKS
{
    [ServiceContract]
    public interface IOksWS
    {
        [OperationContract]
        TypOkserroroutUser cancelfile(decimal? pFileid, string pCancelexp, string pOnlyerror);

        [OperationContract]
        WscontractsearchoutUser getcontractsearch(decimal? pCompanypid, string pGroupid, decimal? pParticipantidentityno, string pParticipantname, string pParticipantsurname, decimal? pParticipantpid, decimal? pPolid, string pContractstatus, string pIscancelation, string pGrouptype, string pIsoks, string pBranchcode, decimal? pPaymentday, decimal? pRecordstart, decimal? pRecordcount, decimal? pDatamasking);

        [OperationContract]
        string getfile(decimal? pFileid);

        [OperationContract]
        WsfonsoutUser getfonbazdeg(WssozfonadetinUser pInput);

        [OperationContract]
        TypOksdetailoutUser getoksdetaillist(decimal? pFileid);

        [OperationContract]
        TypOksgruplistoutUser getoksgruplist(string pTcno);

        [OperationContract]
        TypOksmasteroutUser getoksmaster(decimal? pFileid);

        [OperationContract]
        TypOksmasterlistoutUser getoksmasterlist(TypOksmasterlistinUser pQuery);

        [OperationContract]
        WsfondagsoutUser getsirfondeg(WssozfonadetinUser pInput);

        [OperationContract]
        WsfondagsoutUser getsirfondegyuzde(WssozfonadetinUser pInput);

        [OperationContract]
        WssozfonadetsoutUser getsozfonadet(WssozfonadetinUser pInput);

        [OperationContract]
        WssozkpoutsoutUser getsozkp(WssozfonadetinUser pInput);

        [OperationContract]
        WsyetkioutUser isyetkili(string pTckn);

        [OperationContract]
        TypOksprocessfileoutUser processfile(decimal? pFileid);

        [OperationContract]
        TypOkssetfileoutUser setfile(TypOkssetfileinUser pSetfileinput);

        [OperationContract]
        TypOkserroroutUser setoksdetail(decimal? pFileid, decimal? pDetailid, TypOksuretimUser pUretim);

        [OperationContract]
        TypOkserroroutUser setoksreppassword(TypOkskurumsalparolaUser pOkspsswrd);
    }
}
