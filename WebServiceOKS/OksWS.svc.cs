﻿#region Namespaces
using ModelLibrary;
using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using WebServiceOKS.Classes;
using WebServiceOKS.OksService;
#endregion

namespace WebServiceOKS
{
    public class FibaWS : IOksWS
    {
        #region Member Variables
        OKSWS oksWs;
        #endregion

        #region ValidService
        ServiceUser ValidService()
        {
            ServiceUser serviceUser = null;

            var ctx = WebOperationContext.Current;

            string strKey = ctx.IncomingRequest.Headers["key"];
            if (string.IsNullOrEmpty(strKey))
            {
                throw new Exception("Erişim yetkiniz bulunmamaktadır!")
                {
                    Source = "valid"
                };
            }
            else
            {
                using (DBEntities db = new DBEntities())
                {
                    bool bolValid = false;

                    var ServiceHashKey = db.tblServiceKey.FirstOrDefault();
                    if (ServiceHashKey != null)
                    {
                        var users = db.tblServiceUsers.Where(w => w.bolIsActive && !w.bolIsDelete).ToList();
                        foreach (var item in users)
                        {
                            string strUserKey = cCrypto.Create(item.strPassword, ServiceHashKey.strValue);
                            if (strKey == strUserKey)
                            {
                                bolValid = true;

                                serviceUser = new ServiceUser
                                {
                                    intServiceUserID = item.intServiceUserID,
                                    strAgencyID = item.strAgencyID
                                };

                                break;
                            }
                        }
                    }

                    if (!bolValid)
                    {
                        throw new Exception("Erişim yetkiniz bulunmamaktadır!")
                        {
                            Source = "valid"
                        };
                    }
                }
            }

            return serviceUser;
        }
        #endregion
         
        #region cancelfile
        public TypOkserroroutUser cancelfile(decimal? pFileid, string pCancelexp, string pOnlyerror)
        {
            string strMethod = "cancelfile";
            TypOkserroroutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.cancelfile(pFileid, pCancelexp, pOnlyerror);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getcontractsearch
		public WscontractsearchoutUser getcontractsearch(decimal? pCompanypid, string pGroupid, decimal? pParticipantidentityno, string pParticipantname, string pParticipantsurname, decimal? pParticipantpid, decimal? pPolid, string pContractstatus, string pIscancelation, string pGrouptype, string pIsoks, string pBranchcode, decimal? pPaymentday, decimal? pRecordstart, decimal? pRecordcount, decimal? pDatamasking)
        {
            string strMethod = "getcontractsearch";
            WscontractsearchoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getcontractsearch(pCompanypid, pGroupid, pParticipantidentityno, pParticipantname,
                         pParticipantsurname, pParticipantpid, pPolid, pContractstatus, pIscancelation, pGrouptype, pIsoks, pBranchcode, pPaymentday, pRecordstart, pRecordcount, pDatamasking);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getfile
        public string getfile(decimal? pFileid)
        {
            string strMethod = "getfile";
            string data = "";
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getfile(pFileid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getfonbazdeg
        public WsfonsoutUser getfonbazdeg(WssozfonadetinUser pInput)
        {
            string strMethod = "getfonbazdeg";
            WsfonsoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getfonbazdeg(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getoksdetaillist
        public TypOksdetailoutUser getoksdetaillist(decimal? pFileid)
        {
            string strMethod = "getoksdetaillist";
            TypOksdetailoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getoksdetaillist(pFileid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getoksgruplist
        public TypOksgruplistoutUser getoksgruplist(string pTcno)
        {
            string strMethod = "getoksgruplist";
            TypOksgruplistoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getoksgruplist(pTcno);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getoksmaster
        public TypOksmasteroutUser getoksmaster(decimal? pFileid)
        {
            string strMethod = "getoksmaster";
            TypOksmasteroutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getoksmaster(pFileid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getoksmasterlist
        public TypOksmasterlistoutUser getoksmasterlist(TypOksmasterlistinUser pQuery)
        {
            string strMethod = "getoksmasterlist";
            TypOksmasterlistoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getoksmasterlist(pQuery);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getsirfondeg
        public WsfondagsoutUser getsirfondeg(WssozfonadetinUser pInput)
        {
            string strMethod = "getsirfondeg";
            WsfondagsoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getsirfondeg(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getsirfondegyuzde
        public WsfondagsoutUser getsirfondegyuzde(WssozfonadetinUser pInput)
        {
            string strMethod = "getsirfondegyuzde";
            WsfondagsoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getsirfondegyuzde(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getsozfonadet
        public WssozfonadetsoutUser getsozfonadet(WssozfonadetinUser pInput)
        {
            string strMethod = "getsozfonadet";
            WssozfonadetsoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getsozfonadet(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region getsozkp
        public WssozkpoutsoutUser getsozkp(WssozfonadetinUser pInput)
        {
            string strMethod = "getsozkp";
            WssozkpoutsoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.getsozkp(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region isyetkili
        public WsyetkioutUser isyetkili(string pTckn)
        {
            string strMethod = "isyetkili";
            WsyetkioutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.isyetkili(pTckn);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region processfile
        public TypOksprocessfileoutUser processfile(decimal? pFileid)
        {
            string strMethod = "processfile";
            TypOksprocessfileoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.processfile(pFileid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region setfile
        public TypOkssetfileoutUser setfile(TypOkssetfileinUser pSetfileinput)
        {
            string strMethod = "setfile";
            TypOkssetfileoutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.setfile(pSetfileinput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region setoksdetail
        public TypOkserroroutUser setoksdetail(decimal? pFileid, decimal? pDetailid, TypOksuretimUser pUretim)
        {
            string strMethod = "setoksdetail";
            TypOkserroroutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.setoksdetail(pFileid, pDetailid, pUretim);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 

        #region setoksreppassword
        public TypOkserroroutUser setoksreppassword(TypOkskurumsalparolaUser pOkspsswrd)
        {
            string strMethod = "setoksreppassword";
            TypOkserroroutUser data = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                serviceUser = ValidService();

                using (oksWs = new OKSWS())
                {
                    data = oksWs.setoksreppassword(pOkspsswrd);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(strMethod, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(strMethod, serviceUser.intServiceUserID, "");

            return data;
        }
        #endregion 
    }
}
