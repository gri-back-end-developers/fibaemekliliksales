﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 

namespace WebServiceOKS.Classes
{
    public class Logs
    {
        #region Write
        public static int Write(string strMethod, int intServiceUserID, string strException)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblServiceOKSLogs serviceLog = new tblServiceOKSLogs
                     {
                         dtRegisterDate = DateTime.Now,
                         strMethod = strMethod,
                         intServiceUserID = intServiceUserID,
                         strException = strException
                     };

                    db.tblServiceOKSLogs.Add(serviceLog);
                    db.SaveChanges();

                    return serviceLog.intServiceOKSLogID;
                }
            }
            catch { }

            return 0;
        } 
        #endregion

        #region Error
        public static int Error(Exception ex)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblExceptions exceptions = new tblExceptions()
                    { 
                        intPageID = 0,
                        strException = ex.ToString(),
                        dtRegisterDate = DateTime.Now
                    };

                    db.tblExceptions.Add(exceptions);
                    db.SaveChanges();

                }
            }
            catch { } 

            return 0;
        }
        #endregion
    }
}