var validNavigation = false;
$(function () {
    window.onbeforeunload = function () {
        if (!validNavigation) {
            SessionCloseTime();
        }
    };

    $(document).bind('keypress', function (e) {
        if (e.keyCode == 116) {
            validNavigation = true;
        }
    });

    $("a").bind("click", function () {
        validNavigation = true;
    });
     
    $("form").bind("submit", function () {
        validNavigation = true;
    });

    $("input[type=submit]").bind("click", function () {
        validNavigation = true;
    });

    $("input[type=button]").bind("click", function () {
        validNavigation = true;
    }); 

    $("button[type=submit]").bind("click", function () {
        validNavigation = true;
    }); 
});

function SessionCloseTime() {
    $.ajax({
        type: "POST",
        url: "/Main/SessionCloseTime",
        cache: false,
        headers: { "cache-control": "no-cache" }
    });
};