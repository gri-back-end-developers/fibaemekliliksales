﻿using System.Web.Mvc;

namespace FibaEmeklilikSales.Areas.Credit
{
    public class CreditAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Credit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Credit_default",
                "Credit/{controller}/{action}/{token}",
                new { action = "Index", token = UrlParameter.Optional }
            );
        }
    }
}