﻿using FibaEmeklilikSales.Areas.Credit.Classes;
using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Areas.Credit.Controllers
{
    public class SaleController : Controller
    {
        #region Index
        public ActionResult Index(string token)
        {
            SaleInfo saleInfo = null;

            if (!string.IsNullOrEmpty(token))
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblCreditSaleLogs saleLog = db.tblCreditSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intCreditSaleInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var saleLogInfo = db.tblCreditSaleLogInfo.Where(w => w.intCreditSaleInfoID == intCreditSaleInfoID).FirstOrDefault();
                        if (saleLogInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View(saleInfo);
                        }

                        if (saleLogInfo.bolCreditApprove)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else if (!String.IsNullOrEmpty(saleLogInfo.strPolicyData))
                        {
                            saleInfo = new SaleInfo();
                            saleInfo.strHealtAnswer = saleLogInfo.intQuestionType.HasValue ? (saleLogInfo.intQuestionType.Value == 0 ? "Hayır" : "Evet") : "Evet";

                            FibaEmeklilikSales.Classes.Credits.cPolicyData policy = Newtonsoft.Json.JsonConvert.DeserializeObject<FibaEmeklilikSales.Classes.Credits.cPolicyData>(saleLogInfo.strPolicyData);

                            var saleLogPoliceler = saleLog.tblCreditPacketPolicy.First(f => !string.IsNullOrEmpty(f.strPolicyID));

                            saleInfo.strCreditAmount = saleLogInfo.flCreditAmount.HasValue ? saleLogInfo.flCreditAmount.ToString() : "";
                            saleInfo.strCreditInterest = saleLogInfo.flInterest.HasValue ? saleLogInfo.flInterest.ToString() : "";
                            saleInfo.strCreditMonth = saleLogInfo.intCreditMonth.HasValue ? saleLogInfo.intCreditMonth.ToString() : "";
                            saleInfo.strKkdf = saleLogInfo.flKKDF.HasValue ? saleLogInfo.flKKDF.ToString() : "";
                            saleInfo.strBsmv = saleLogInfo.flBSMV.HasValue ? saleLogInfo.flBSMV.ToString() : "";

                            var user = db.tblUsers.Where(w => w.intUserID == saleLog.intUserID).First();

                            using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                            {
                                var policesorgu = fibaws.getpolicesorgu(new FIBAWS.FibawsPolicesorguinrecUser
                                {
                                    acenteno = user.tblCallcenters.strCallcenterCode,
                                    polid = policy.PoliceNo
                                });

                                if (policesorgu != null && policesorgu.pollist != null && policesorgu.pollist.Count > 0)
                                {
                                    saleInfo.strStartDate = policesorgu.pollist.First().polbastar;
                                    saleInfo.strEndDate = policesorgu.pollist.First().polbittar;
                                }
                            }

                            var credit = saleLog.tblCredits;
                            if (credit != null)
                            {
                                var creditPacket = credit.tblCreditPackets.FirstOrDefault(f => f.intCreditPacketID == saleLogInfo.intCreditPacketID);
                                if (creditPacket != null)
                                {
                                    saleInfo.strPacketName = creditPacket.strName;
                                    saleInfo.strCrediCurrencyType = creditPacket.strCrediCurrencyType;

                                    if (creditPacket.tblCreditAssurances.FirstOrDefault(f => !f.bolIsDelete) != null)
                                        saleInfo.strAssuranceName = creditPacket.tblCreditAssurances.First(f => !f.bolIsDelete).strName;
                                }
                                saleInfo.strProductName = credit.strName;
                                saleInfo.strQuestion = credit.strQuestion;

                                cCreditCard cardInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<cCreditCard>(cCrypto.DecryptDES(saleLog.strCreditCardInfo));

                                int intPaymentMethodID = Utilities.NullFixInt(cardInfo.type);
                                var paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                                decimal tutar = intPaymentMethodID == 3 ? policy.IlkPrim.Value : policy.ToplamPirim.Value;

                                saleInfo.strPrim = Utilities.Price(tutar) + " TL";
                                saleInfo.strInstallment = paymentMethod.strName;
                            }

                            saleInfo.paymentPlans = saleLog.tblCreditSalePaymentPlans.ToList();

                            saleInfo.intPolID = policy.PoliceNo.ToString();
                            saleInfo.strCallcenterName = user.tblCallcenters.strName;
                            saleInfo.strCreditType = saleLog.tblCreditTypes.strTypeName;
                            saleInfo.strName = saleLogInfo.strName;
                            saleInfo.strSurname = saleLogInfo.strSurname;
                            saleInfo.strTCKN = saleLogInfo.strTCKN;
                            saleInfo.strPhone = saleLogInfo.strMobile;
                            saleInfo.strEmail = saleLogInfo.strEmail;
                            saleInfo.strAddress = saleLogInfo.strAddress;

                            FIBAWS.fibawsClient fibawsClient = new FIBAWS.fibawsClient();
                            var odemeBilgi = fibawsClient.getodemebilgirec(Utilities.NullFixDecimal(saleInfo.intPolID));
                            if (odemeBilgi != null)
                            {
                                saleInfo.strCardNo = odemeBilgi.kartno;
                            }
                        }
                    }
                }
            }

            return View(saleInfo);
        }
        #endregion

        #region Tanzim

        #region Get
        [HttpGet]
        public ActionResult Tanzim(string token, bool bolPost = false)
        {
            string callcenterID = "";
            string strNameSurname = "";

            if (!string.IsNullOrEmpty(token) && !bolPost)
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblCreditSaleLogs saleLog = db.tblCreditSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intCreditSaleInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var saleLogInfo = db.tblCreditSaleLogInfo.Where(w => w.intCreditSaleInfoID == intCreditSaleInfoID).FirstOrDefault();
                        if (saleLogInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View();
                        }

                        if (saleLogInfo.bolCreditApprove)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else
                        {
                            var user = db.tblUsers.Where(w => w.intUserID == saleLog.intUserID).First();

                            callcenterID = user.intCallcenterID.ToString();
                            strNameSurname = saleLogInfo.strName + " " + saleLogInfo.strSurname;
                        }
                    }
                }
            }

            ViewBag.callcenterID = callcenterID;
            ViewBag.name = strNameSurname;

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult Tanzim(string token)
        {
            int intLogID = 0;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    string strToken = token.Substring(0, 8);

                    using (DBEntities db = new DBEntities())
                    {
                        tblCreditSaleLogs saleLog = db.tblCreditSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                        if (saleLog != null)
                        {
                            intLogID = saleLog.intLogID;

                            int intCreditSaleInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                            var saleLogInfo = db.tblCreditSaleLogInfo.Where(w => w.intCreditSaleInfoID == intCreditSaleInfoID).First();

                            if (saleLogInfo.bolCreditApprove)
                            {
                                ViewBag.Message = "Poliçe tanzim edilmiş!";
                            }
                            else
                            {
                                saleLogInfo.kvkkMetni = "evet";
                                saleLogInfo.mesafeliSozlesme = "evet";
                                saleLogInfo.bilgilendirmeFormu = "evet";

                                BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client();

                                JavaScriptSerializer java = new JavaScriptSerializer();

                                var salePolicy = java.Deserialize<FibaEmeklilikSales.Classes.Credits.cPolicyData>(saleLogInfo.strPolicyData);

                                int intWSLogID = Logs.WSLog(0, saleLog.intUserID, intLogID, "krediteminatı", "Tanzim", "Input", "{\"tkl\" : \"" + salePolicy.PoliceNo + "\", \"insCan\" : \"1\", \"pKredino2\": \"" + salePolicy.KrediNo + "\", \"krd\" : \"" + salePolicy.KrediNo + "\" }", "teklifpolicelesme");

                                var teklifpolicelesme = service.teklifpolicelesme(salePolicy.PoliceNo, 1, salePolicy.KrediNo, salePolicy.KrediNo);

                                Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "krediteminatı", "Tanzim", "Output", java.Serialize(teklifpolicelesme), "teklifpolicelesme");

                                if (teklifpolicelesme != null && teklifpolicelesme.policeno != null)
                                {
                                    saleLogInfo.strPolicyData2 = java.Serialize(teklifpolicelesme);

                                    saleLogInfo.bolCreditApprove = true;

                                    cCreditCard cardInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<cCreditCard>(cCrypto.DecryptDES(saleLog.strCreditCardInfo));

                                    int intPaymentMethodID = Utilities.NullFixInt(cardInfo.type);

                                    tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                                    decimal? tutar = intPaymentMethodID == 3 ? teklifpolicelesme.ilkprim : teklifpolicelesme.toplamprim;

                                    BankaWebV2.bankaweb_v2Client service2 = new BankaWebV2.bankaweb_v2Client();
                                    BankaWebV2.BankawebV2SanalposinUser pInput = new BankaWebV2.BankawebV2SanalposinUser
                                    {
                                        cvv = 0,
                                        expmonth = cardInfo.month,
                                        expyear = cardInfo.year,
                                        kartno = cardInfo.card.Replace(" ", "").Trim(),
                                        polid = teklifpolicelesme.policeno,
                                        taksitadedi = paymentMethod.intInstallment,
                                        tutar = tutar,
                                        user = null,
                                        vadelist = new BankaWebV2.Emkblibsposvadetyl {
                                            new BankaWebV2.EmkblibsposvadetypUser
                                            {
                                                tutar = tutar,
                                                vadetar = DateTime.Now.ToString("ddMMyyyy")
                                            }
                                        }
                                    };

                                    intWSLogID = Logs.WSLog(0, saleLog.intUserID, saleLog.intLogID, "krediteminatı", "Tanzim", "Input", "", "spostaksitli");

                                    var spostaksitli = service2.spostaksitli(pInput);

                                    Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "krediteminatı", "Tanzim", "Output", java.Serialize(spostaksitli), "spostaksitli");

                                    saleLogInfo.bolSuccessPay = spostaksitli.issuccess == "T";
                                    saleLogInfo.strPayMessage = spostaksitli.aciklama;

                                    string strPolicy = cCrypto.EncryptDES(teklifpolicelesme.policeno.Value.ToString());

                                    ViewBag.Message = "";
                                    ViewBag.Policy = strPolicy;

                                    saleLog.strCreditCardInfo = "";

                                    if (saleLogInfo.bolSuccessPay)
                                    {
                                        string strLink = ConfigurationManager.AppSettings["SaleUrl"] + "/pdf/credit/" + strToken;

                                        bool bolSms = false;
                                        bool bolEmail = false;

                                        if (saleLogInfo.InfoFormSendType == "sms")
                                        {
                                            bolSms = SmsSend(saleLogInfo.strMobile, strLink, saleLogInfo.strName + " " + saleLogInfo.strSurname);
                                            if (!bolSms)
                                            {
                                                bolEmail = MailSend(saleLogInfo.strEmail, strLink, saleLogInfo.strName + " " + saleLogInfo.strSurname);
                                            }
                                        }
                                        else if (saleLogInfo.InfoFormSendType == "mail")
                                        {
                                            bolEmail = MailSend(saleLogInfo.strEmail, strLink, saleLogInfo.strName + " " + saleLogInfo.strSurname);
                                            if (!bolEmail)
                                            {
                                                bolSms = SmsSend(saleLogInfo.strMobile, strLink, saleLogInfo.strName + " " + saleLogInfo.strSurname);
                                            }
                                        }

                                        saleLogInfo.bolFinishMail = bolEmail;
                                        saleLogInfo.bolFinishSms = bolSms;
                                        saleLogInfo.dtFinishDate = DateTime.Now;
                                        
                                    }
                                    else
                                    {
                                        var policeCancel = PoliceCancel(teklifpolicelesme.policeno.Value, salePolicy.KrediNo, saleLog.intUserID);
                                        if (policeCancel.cevapkod > 0)
                                        {
                                            saleLogInfo.strPayMessage += " - İptal edildi";
                                        }

                                        ViewBag.Message = "Ödeme başarısız!";
                                    }
                                }
                                else
                                {
                                    ViewBag.Message = "Poliçe tanzim edilemedi!";
                                }

                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            ViewBag.Message = "Hatalı giriş!";
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "Hatalı giriş!";
                }
            }
            catch (Exception ex)
            {
                int intExceptionID = Logs.Error(ex); 
            }

            return Tanzim(token, true);
        }

        private bool SmsSend(string strPhone, string strLink, string strNameSurname)
        {
            try
            {
                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                {
                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                });

                string smsSend = sms.Send("Sayın " + strNameSurname + ", poliçenize " + strLink + " linkinden erişebilirsiniz. Fibaemeklilik ile bir ömür dileriz. Fibaemeklilik 444 82 88 Mersis No: 396059952300012", strPhone);

                return smsSend.Contains("ID");
            }
            catch { }

            return false;
        }

        private bool MailSend(string strEmail, string strLink, string strNameSurname)
        {
            string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
            strHtml = strHtml.Replace("#adsoyad#", strNameSurname);
            strHtml = strHtml.Replace("#mesaj#", "<p>Poliçenize <a href=\"" + strLink + "\">" + strLink + "</a> linkinden erişebilirsiniz.</p><p>İyi günler dileriz.</p><p>FİBA EMEKLİLİK VE HAYAT A.Ş.<br/>www.fibaemeklilik.com.tr<br/>444 82 88</p>");

            return Email.SendEmail("Fiba Emeklilik – Kredi Hayat Sigortası Bilgilendirme Formları Hk.", strHtml, strEmail);
        }
        #endregion

        #region PoliceCancel
        BankaWebV2.BankawebV2PoliptrecUser PoliceCancel(decimal PoliceNo, string strKrediNo, int intUserID)
        {
            BankaWebV2.BankawebV2PoliptrecUser poluser = new BankaWebV2.BankawebV2PoliptrecUser();
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                BankaWebV2.BankawebV2PoliptinUser poliptinUser = new BankaWebV2.BankawebV2PoliptinUser();
                poliptinUser.iptnedenkod = 9;
                poliptinUser.ipttar = (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                poliptinUser.polid = PoliceNo;
                poliptinUser.kredino = strKrediNo;

                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, intUserID, 0, "krediteminatı", "PoliceCancel", "Input", java.Serialize(poliptinUser), "poliptal");

                poluser = service.poliptal("HYSG", poliptinUser);

                Logs.WSLog(intWSLogID, intUserID, 0, "krediteminatı", "PoliceCancel", "Output", java.Serialize(poluser), "poliptal");

                service.Close();
                service.Abort();
            }

            return poluser;
        }
        #endregion

        #endregion
    }
}