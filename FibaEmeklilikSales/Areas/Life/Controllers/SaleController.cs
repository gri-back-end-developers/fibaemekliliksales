﻿using FibaEmeklilikSales.Areas.Life.Classes;
using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Areas.Life.Controllers
{
    public class SaleController : Controller
    {
        #region Index
        public ActionResult Index(string token)
        {
            SaleInfo saleInfo = null;

            if (!string.IsNullOrEmpty(token))
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblSaleLogs saleLog = db.tblSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intSaleLogID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var saleLogInfo = db.tblSaleLogInfo.Where(w => w.intID == intSaleLogID).FirstOrDefault();
                        if (saleLogInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View(saleInfo);
                        }

                        if (saleLog.tblSaleLogPoliceler.Count(c => c.strService == "teklifpolicelesme") > 0)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else if (saleLogInfo.bolLifeApprove)
                        {
                            ViewBag.Message = "Tanzim isteği gönderilmiş!";
                        }
                        else
                        {
                            saleInfo = new SaleInfo();
                            saleInfo.strHealtAnswer = saleLog.strHealthAnswer;

                            var saleLogPoliceler = saleLog.tblSaleLogPoliceler.First(f => f.strService == "policelesme");

                            saleInfo.strBsmv = saleLogPoliceler.Bsmv;

                            using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                            {
                                var policesorgu = fibaws.getpolicesorgu(new FIBAWS.FibawsPolicesorguinrecUser
                                {
                                    acenteno = saleLog.tblUsers.tblCallcenters.strCallcenterCode,
                                    polid = Utilities.NullFixDecimal(saleLogPoliceler.PoliceNo)
                                });

                                if (policesorgu != null && policesorgu.pollist != null && policesorgu.pollist.Count > 0)
                                {
                                    saleInfo.strStartDate = policesorgu.pollist.First().polbastar;
                                    saleInfo.strEndDate = policesorgu.pollist.First().polbittar;
                                }
                            }

                            var packet = db.tblPackets.Where(w => w.intPacketID == saleLogPoliceler.intPacketID).FirstOrDefault();
                            if (packet != null)
                            {
                                saleInfo.strPacketName = packet.strName;
                                saleInfo.strProductName = packet.tblProducts.strName;
                                saleInfo.strCrediCurrencyType = packet.strCrediCurrencyType;
                                saleInfo.strQuestion = packet.tblProducts.strQuestion;

                                cCreditCard cardInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<cCreditCard>(cCrypto.DecryptDES(saleLog.strCreditCardInfo));

                                int intPaymentMethodID = Utilities.NullFixInt(cardInfo.type);
                                var paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                                decimal tutar = packet.tblProducts.bolFirstBonus ? (intPaymentMethodID == 2 ? (Utilities.NullFixDecimal(saleLogPoliceler.ToplamPirim) / (packet.intCrediTime / 12)) : Utilities.NullFixDecimal(saleLogPoliceler.IlkPrim)) : (intPaymentMethodID == 3 ? Utilities.NullFixDecimal(saleLogPoliceler.IlkPrim) : Utilities.NullFixDecimal(saleLogPoliceler.ToplamPirim));

                                saleInfo.strPrim = Utilities.Price(tutar) + " TL";
                                saleInfo.strInstallment = paymentMethod.strName;

                                saleInfo.assurances = packet.tblAssurances.ToList();
                            }

                            saleInfo.intPolID = saleLogPoliceler.PoliceNo;
                            saleInfo.strCallcenterName = saleLog.tblUsers.tblCallcenters.strName;

                            tblSaleLogInfo saleLogInfo_1 = saleLog.tblSaleLogInfo.Where(w => !w.bolDifferentPaid).First();

                            saleInfo.strName = saleLogInfo_1.ad;
                            saleInfo.strSurname = saleLogInfo_1.soyad;
                            saleInfo.strTCKN = saleLogInfo_1.tckn;
                            saleInfo.strPhone = saleLogInfo_1.iletisimno;
                            saleInfo.strEmail = saleLogInfo_1.email;
                            saleInfo.strAddress = saleLogInfo_1.adres;

                            if (saleLog.tblSaleLogInfo.Count(c => c.bolDifferentPaid) > 0)
                            {
                                tblSaleLogInfo saleLogInfoDiff = saleLog.tblSaleLogInfo.Where(w => w.bolDifferentPaid).First();

                                saleInfo.strDiffrerentTCKN = saleLogInfoDiff.tckn;
                                saleInfo.strDiffrerentName = saleLogInfoDiff.ad;
                                saleInfo.strDiffrerentSurname = saleLogInfoDiff.soyad;
                            }

                            FIBAWS.fibawsClient fibawsClient = new FIBAWS.fibawsClient();
                            var odemeBilgi = fibawsClient.getodemebilgirec(Utilities.NullFixDecimal(saleInfo.intPolID));
                            if (odemeBilgi != null)
                            {
                                saleInfo.strCardNo = odemeBilgi.kartno;
                            }
                        }
                    }
                }
            }

            return View(saleInfo);
        }
        #endregion

        #region Tanzim

        #region Get
        [HttpGet]
        public ActionResult Tanzim(string token, bool bolPost = false)
        {
            string callcenterID = "";
            string strNameSurname = "";

            if (!string.IsNullOrEmpty(token) && !bolPost)
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblSaleLogs saleLog = db.tblSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var saleInfo = db.tblSaleLogInfo.Where(w => w.intID == intID).FirstOrDefault();
                        if (saleInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View();
                        }

                        if (saleLog.tblSaleLogPoliceler.Count(c => c.strService == "teklifpolicelesme") > 0)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else if (saleInfo.bolLifeApprove)
                        {
                            ViewBag.Message = "Tanzim isteği gönderilmiş!";
                        }
                        else
                        {
                            callcenterID = saleLog.tblUsers.intCallcenterID.ToString();
                            strNameSurname = saleInfo.ad + " " + saleInfo.soyad;
                        }
                    }
                }
            }

            ViewBag.callcenterID = callcenterID;
            ViewBag.name = strNameSurname;

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult Tanzim(string token)
        {
            int inLogID = 0;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    string strToken = token.Substring(0, 8);

                    using (DBEntities db = new DBEntities())
                    {
                        tblSaleLogs saleLog = db.tblSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                        if (saleLog != null)
                        {
                            inLogID = saleLog.intLogID;

                            int intID = Utilities.NullFixInt(token.Replace(strToken, ""));

                            var saleInfo = db.tblSaleLogInfo.Where(w => w.intID == intID).First();

                            if (saleLog.tblSaleLogPoliceler.Count(c => c.strService == "teklifpolicelesme") > 0)
                            {
                                ViewBag.Message = "Poliçe tanzim edilmiş!";
                            }
                            else if (saleInfo.bolLifeApprove)
                            {
                                ViewBag.Message = "Tanzim isteği gönderilmiş!";
                            }
                            else
                            {
                                bool bolNext = true;

                                if (saleLog.tblSaleLogInfo.Count(c => c.bolDifferentPaid) > 0)
                                {
                                    if (saleLog.tblSaleLogInfo.Count(c => c.bolLifeApprove) == 0)
                                    {
                                        bolNext = false;
                                    }
                                }

                                saleInfo.kvkkMetni = "evet";
                                saleInfo.mesafeliSozlesme = "evet";
                                saleInfo.bilgilendirmeFormu = "evet";

                                if (!bolNext)
                                {
                                    saleInfo.bolLifeApprove = true;

                                    ViewBag.Policy = "1";
                                    ViewBag.Message = "Tanzim isteği gönderildi.";
                                }
                                else
                                {
                                    BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client();

                                    JavaScriptSerializer javaSer = new JavaScriptSerializer();

                                    var salePolicy = saleLog.tblSaleLogPoliceler.Where(w => w.strService == "policelesme").First();

                                    int intWSLogID = Logs.WSLog(0, saleLog.intUserID, saleLog.intLogID, "hayat", "Tanzim", "Input", "policeno : " + salePolicy.PoliceNo, "teklifpolicelesme");

                                    var police = service.teklifpolicelesme(Utilities.NullFixDecimal(salePolicy.PoliceNo), 1, "", "");

                                    Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "hayat", "Tanzim", "Output", javaSer.Serialize(police), "teklifpolicelesme");

                                    if (police != null && police.policeno != null)
                                    {
                                        saleInfo.bolLifeApprove = true;

                                        var packet = db.tblPackets.Where(w => w.intPacketID == salePolicy.intPacketID).First();

                                        SaleLogPoliceler teklifpolicelesme = new SaleLogPoliceler
                                        {
                                            IlkPrim = police.ilkprim.HasValue ? police.ilkprim.Value : 0,
                                            intPacketID = salePolicy.intPacketID,
                                            ToplamPirim = police.toplamprim.HasValue ? police.toplamprim.Value : 0,
                                            PoliceNo = police.policeno.HasValue ? police.policeno.Value : 0,
                                            IlkPrimKomisyon = police.ilkprimkomisyon.HasValue ? police.ilkprimkomisyon.Value : 0,
                                            KrediNo = police.kredino,
                                            TeklifDurum = police.teklifdurum,
                                            ToplamKomisyon = police.toplamkomisyon.HasValue ? police.toplamkomisyon.Value : 0,
                                            intPoliceTypeID = (byte)Enums.PoliceType.TeklifPolicelesme,
                                            bolCancel = false,
                                            strService = "teklifpolicelesme",
                                            bolNew = true,
                                            strYenileme = salePolicy.strYenileme,
                                            intCrediTime = packet.intCrediTime
                                        };

                                        cCreditCard cardInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<cCreditCard>(cCrypto.DecryptDES(saleLog.strCreditCardInfo));

                                        int intPaymentMethodID = Utilities.NullFixInt(cardInfo.type);

                                        tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                                        decimal? tutar = packet.tblProducts.bolFirstBonus ? (intPaymentMethodID == 2 ? (police.toplamprim / (packet.intCrediTime / 12)) : police.ilkprim) : (intPaymentMethodID == 3 ? police.ilkprim : police.toplamprim);

                                        BankaWebV2.bankaweb_v2Client service2 = new BankaWebV2.bankaweb_v2Client();
                                        BankaWebV2.BankawebV2SanalposinUser pInput = new BankaWebV2.BankawebV2SanalposinUser
                                        {
                                            cvv = 0,
                                            expmonth = cardInfo.month,
                                            expyear = cardInfo.year,
                                            kartno = cardInfo.card.Replace(" ", "").Trim(),
                                            polid = police.policeno,
                                            taksitadedi = paymentMethod.intInstallment,
                                            tutar = tutar,
                                            user = null,
                                            vadelist = new BankaWebV2.Emkblibsposvadetyl
                                                {
                                                    new BankaWebV2.EmkblibsposvadetypUser
                                                    {
                                                        tutar = tutar,
                                                        vadetar = DateTime.Now.ToString("ddMMyyyy")
                                                    }
                                                }
                                        };

                                        intWSLogID = Logs.WSLog(0, saleLog.intUserID, saleLog.intLogID, "hayat", "Tanzim", "Input", "", "spostaksitli");

                                        var spostaksitli = service2.spostaksitli(pInput);

                                        Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "hayat", "Tanzim", "Output", javaSer.Serialize(spostaksitli), "spostaksitli");

                                        Logs.BesSaleLogPay(saleLog.intLogID, spostaksitli.issuccess, spostaksitli.aciklama);

                                        teklifpolicelesme.bolPay = spostaksitli.issuccess == "T";
                                        teklifpolicelesme.strPayMessage = spostaksitli.aciklama;

                                        string strPolicy = cCrypto.EncryptDES(salePolicy.PoliceNo);

                                        ViewBag.Message = "";
                                        ViewBag.Policy = strPolicy;

                                        saleLog.strCreditCardInfo = "";

                                        if (teklifpolicelesme.bolPay)
                                        {
                                            string strLink = ConfigurationManager.AppSettings["SaleUrl"] + "/pdf/life/" + strToken;

                                            bool bolSms = false;
                                            bool bolEmail = false;

                                            var saleInfoPdf = db.tblSaleLogInfo.Where(w => w.intLogID == inLogID && !w.bolDifferentPaid).First();

                                            if (saleInfoPdf.InfoFormSendType == "sms")
                                            {
                                                bolSms = SmsSend(saleInfoPdf.iletisimno, strLink, saleInfoPdf.ad + " " + saleInfoPdf.soyad);
                                                if (!bolSms)
                                                {
                                                    bolEmail = MailSend(saleInfoPdf.email, strLink, saleInfoPdf.ad + " " + saleInfoPdf.soyad);
                                                }
                                            }
                                            else if (saleInfoPdf.InfoFormSendType == "mail")
                                            {
                                                bolEmail = MailSend(saleInfoPdf.email, strLink, saleInfoPdf.ad + " " + saleInfoPdf.soyad);
                                                if (!bolEmail)
                                                {
                                                    bolSms = SmsSend(saleInfoPdf.iletisimno, strLink, saleInfoPdf.ad + " " + saleInfoPdf.soyad);
                                                }
                                            }

                                            saleInfoPdf.bolFinishMail = bolEmail;
                                            saleInfoPdf.bolFinishSms = bolSms;
                                            saleInfo.dtFinishDate = DateTime.Now;

                                            Logs.SaleLog(saleLog.intLogID, false, Enums.LogType.PaySuccess, "", "", "", "", new List<SaleLogPoliceler> { teklifpolicelesme });
                                        }
                                        else
                                        {
                                            var policeCancel = PoliceCancel(police.policeno.Value, saleLog.intUserID);
                                            if (policeCancel.cevapkod > 0)
                                            {
                                                teklifpolicelesme.strMessage = "İptal edildi";
                                                teklifpolicelesme.bolCancel = true;
                                                teklifpolicelesme.strService = "poliptal";

                                                saleInfo.bolLifeApprove = false;
                                            }

                                            Logs.SaleLog(saleLog.intLogID, false, Enums.LogType.PayUnSuccess, "", "", "", "", new List<SaleLogPoliceler> { teklifpolicelesme });

                                            ViewBag.Message = "Ödeme başarısız!";
                                        }
                                    }
                                    else
                                    {
                                        ViewBag.Message = "Poliçe tanzim edilemedi!";
                                    }
                                }

                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            ViewBag.Message = "Hatalı giriş!";
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "Hatalı giriş!";
                }
            }
            catch (Exception ex)
            {
                int intExceptionID = Logs.Error(ex);

                Logs.BesSaleLogEx(inLogID, intExceptionID, "Bes tanzim hata oluştu.");
            }

            return Tanzim(token, true);
        }

        private bool SmsSend(string strPhone, string strLink, string strNameSurname)
        {
            try
            {
                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                {
                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                });

                string smsSend = sms.Send("Sayın " + strNameSurname + ", poliçenize " + strLink + " linkinden erişebilirsiniz. Fibaemeklilik ile bir ömür dileriz. Fibaemeklilik 444 82 88 Mersis No: 396059952300012", strPhone);

                return smsSend.Contains("ID");
            }
            catch { }

            return false;
        }

        private bool MailSend(string strEmail, string strLink, string strNameSurname)
        {
            string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
            strHtml = strHtml.Replace("#adsoyad#", strNameSurname);
            strHtml = strHtml.Replace("#mesaj#", "<p>Poliçenize <a href=\"" + strLink + "\">" + strLink + "</a> linkinden erişebilirsiniz.</p><p>İyi günler dileriz.</p><p>FİBA EMEKLİLİK VE HAYAT A.Ş.<br/>www.fibaemeklilik.com.tr<br/>444 82 88</p>");

            return Email.SendEmail("Fiba Emeklilik – Hayat Sigortası Bilgilendirme Formları Hk.", strHtml, strEmail);
        }
        #endregion

        #region PoliceCancel
        BankaWebV2.BankawebV2PoliptrecUser PoliceCancel(decimal PoliceNo, int intUserID)
        {
            BankaWebV2.BankawebV2PoliptrecUser poluser = new BankaWebV2.BankawebV2PoliptrecUser();
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                BankaWebV2.BankawebV2PoliptinUser poliptinUser = new BankaWebV2.BankawebV2PoliptinUser();
                poliptinUser.iptnedenkod = 7;
                poliptinUser.ipttar = (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                poliptinUser.polid = PoliceNo;

                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, intUserID, 0, "hayat", "PoliceCancel", "Input", java.Serialize(poliptinUser), "poliptal");

                poluser = service.poliptal("1", poliptinUser);

                Logs.WSLog(intWSLogID, intUserID, 0, "hayat", "PoliceCancel", "Output", java.Serialize(poluser), "poliptal");

                service.Close();
                service.Abort();
            }

            return poluser;
        }
        #endregion

        #endregion
    }
}