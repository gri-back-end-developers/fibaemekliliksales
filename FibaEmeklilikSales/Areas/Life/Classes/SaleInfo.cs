﻿using System.Collections.Generic;

namespace FibaEmeklilikSales.Areas.Life.Classes
{
    public class SaleInfo
    {
        public string strProductName { get; set; }
        public string strPacketName { get; set; }
        public string intPolID { get; set; }
        public string strTCKN { get; set; }
        public string strName { get; set; }
        public string strSurname { get; set; }
        public string strDiffrerentTCKN { get; set; }
        public string strDiffrerentName { get; set; }
        public string strDiffrerentSurname { get; set; }
        public string strCardNo { get; set; }
        public string strPhone { get; set; }
        public string strEmail { get; set; }
        public string strAddress { get; set; }
        public string strQuestion { get; set; }
        public string strHealtAnswer { get; set; }
        public string strCrediCurrencyType { get; set; }
        public string strPrim { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public string strInstallment { get; set; }
        public string strBsmv { get; set; }
        public string strCallcenterName { get; set; }
        public List<ModelLibrary.tblAssurances> assurances { get; set; }
    }
}