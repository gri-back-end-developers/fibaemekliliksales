﻿using System.Web.Mvc;

namespace FibaEmeklilikSales.Areas.Life
{
    public class LifeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Life";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Life_default",
                "Life/{controller}/{action}/{token}",
                new { action = "Index", token = UrlParameter.Optional }
            );
        }
    }
}