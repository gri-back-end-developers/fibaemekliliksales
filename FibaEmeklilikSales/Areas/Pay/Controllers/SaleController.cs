﻿using FibaEmeklilikSales.Areas.Pay.Classes;
using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Mvc;

namespace FibaEmeklilikSales.Areas.Pay.Controllers
{
    public class SaleController : Controller
    {
        public ActionResult Start()
        {
            using (DBEntities db = new DBEntities())
            {
                string strSmsToken = Request.Url.Segments[Request.Url.Segments.Length - 1];

                tblScratchWinSales scratchWinSales = db.tblScratchWinSales.Where(w => w.strSmsToken == strSmsToken).First();
                try
                {
                    tblScratchWinPaycell scratchWinPaycellIsHave = db.tblScratchWinPaycell.Where(w => w.intScratchWinSaleID == scratchWinSales.intScratchWinSaleID && !w.bolPassive.HasValue).FirstOrDefault();
                    if (scratchWinPaycellIsHave == null || !scratchWinPaycellIsHave.bolIsPaid.HasValue)
                    {
                        SaleLogPoliceler saleLog = Newtonsoft.Json.JsonConvert.DeserializeObject<SaleLogPoliceler>(scratchWinSales.strPolicyData);

                        if (scratchWinPaycellIsHave != null)
                        {
                            scratchWinPaycellIsHave.bolPassive = true;
                            scratchWinPaycellIsHave.dtUpdateDate = DateTime.Now;
                        }

                        tblScratchWinPaycell scratchWinPaycell = new tblScratchWinPaycell();
                        scratchWinPaycell.dtRegisterDate = DateTime.Now;

                        scratchWinPaycell.intScratchWinSaleID = scratchWinSales.intScratchWinSaleID;

                        string merchantCode = ConfigurationManager.AppSettings["Paycell_merchantCode"];
                        string terminalCode = ConfigurationManager.AppSettings["Paycelll_terminalCode"];
                        string secureCode = ConfigurationManager.AppSettings["Paycell_secureCode"];
                        string hostAccount = ConfigurationManager.AppSettings["Paycell_hostAccount"];
                        string paymentReferenceNumber = Guid.NewGuid().ToString().ToLower();
                        string currency = "99";
                        string paymentSecurity = "THREED_SECURE";
                        string amount = saleLog.ToplamPirim.Value.ToString().Replace(",", "").Replace(".", "");

                        scratchWinPaycell.strPaymnetReferenceNumber = paymentReferenceNumber;
                        scratchWinPaycell.strAmount = amount;

                        string securityData = Sha256Cipher(secureCode + terminalCode);

                        string hashData = Sha256Cipher(paymentReferenceNumber + terminalCode + amount + currency + paymentSecurity + hostAccount + securityData);

                        scratchWinPaycell.strHashData = hashData;

                        using (HttpClient client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Add("Accept", "application/json");

                            string strTransactionId = DateTime.Now.Ticks.ToString();
                            scratchWinPaycell.strTransactionId = strTransactionId;

                            string data = "{ \"requestHeader\": { \"merchant\": { \"merchantCode\": \"" + merchantCode + "\", \"terminalCode\": \"" + terminalCode + "\" }, \"transactionInfo\": { \"transactionDateTime\": \"" + DateTime.Now.ToString("yyyyMMddHHmmssSSS") + "\", \"transactionId\": \"" + strTransactionId + "\" } }, \"hashData\": \"" + hashData + "\", \"hostAccount\": \"" + hostAccount + "\", \"language\": \"tr\", \"payment\": { \"amount\": \"" + amount + "\", \"currency\": \"99\", \"paymentReferenceNumber\": \"" + paymentReferenceNumber + "\", \"paymentSecurity\": \"" + paymentSecurity + "\" }, \"returnUrl\": \"" + ConfigurationManager.AppSettings["siteUrl"] + "/pay/status/success\", \"cancelOperationUrl\": \"" + ConfigurationManager.AppSettings["siteUrl"] + "/pay/status/policyerror\" }";

                            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

                            var sessionInitResponse = client.PostAsync(ConfigurationManager.AppSettings["Paycell_SessionInit"], content).Result;

                            SessionInit sessionInit = sessionInitResponse.Content.ReadAsAsync<SessionInit>(new[] { new System.Net.Http.Formatting.JsonMediaTypeFormatter() }).Result;

                            if (sessionInit != null && sessionInit.statusCode == "0")
                            {
                                scratchWinPaycell.strTrackingId = sessionInit.trackingId;
                                scratchWinPaycell.strStatusCode = sessionInit.statusCode;
                                scratchWinPaycell.strMessage = sessionInit.message;

                                db.tblScratchWinPaycell.Add(scratchWinPaycell);
                                db.SaveChanges();

                                return Redirect(ConfigurationManager.AppSettings["Paycell_Tracking"] + "/" + sessionInit.trackingId);
                            }
                            else
                            {
                                scratchWinPaycell.strMessage = string.IsNullOrEmpty(sessionInit.message) ? "SessionInit is null" : sessionInit.message;

                                db.tblScratchWinPaycell.Add(scratchWinPaycell);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Error(ex);
                }
            }

            return Redirect("/pay/status/error");
        }

        public static string Sha256Cipher(string input)
        {
            System.Security.Cryptography.SHA256 sha = new System.Security.Cryptography.SHA256Managed();
            byte[] data = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] digiest = sha.ComputeHash(data);

            return Convert.ToBase64String(digiest);
        }
    }
}