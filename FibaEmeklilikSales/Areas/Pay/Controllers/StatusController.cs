﻿using FibaEmeklilikSales.Classes;
using ModelLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FibaEmeklilikSales.Areas.Pay.Controllers
{
    public class StatusController : Controller
    {
        public ActionResult Success()
        {
            return View();
        }
        
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Timeout()
        {
            return View();
        }
    }
}