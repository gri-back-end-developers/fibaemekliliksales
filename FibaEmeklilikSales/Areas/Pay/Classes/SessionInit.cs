﻿namespace FibaEmeklilikSales.Areas.Pay.Classes
{
    public class SessionInit
    {
        public string trackingId { get; set; }
        public string statusCode { get; set; }
        public string message { get; set; }
    }
}