﻿namespace FibaEmeklilikSales.Areas.Pay.Classes
{
    public class PaymentNotification
    {
        public string TrackingId { get; set; }
        public string PaymentMethodType { get; set; }
        public string PaymnetReferenceNumber { get; set; }
        public bool IsPaid { get; set; }
        public string TrnsactionDate { get; set; }
        public string Amount { get; set; }
    }
}