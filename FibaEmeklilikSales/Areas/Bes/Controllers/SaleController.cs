﻿using FibaEmeklilikSales.Areas.Bes.Classes;
using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Areas.Bes.Controllers
{
    public class SaleController : Controller
    {
        #region Index
        public ActionResult Index(string token)
        {
            SaleInfo saleInfo = null;

            if (!string.IsNullOrEmpty(token))
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblBesSaleLogs saleLog = db.tblBesSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intBesSaleLogInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var besSaleInfo = db.tblBesSaleLogInfo.Where(w => w.intID == intBesSaleLogInfoID).FirstOrDefault();
                        if (besSaleInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View(saleInfo);
                        }

                        if (saleLog.tblBesSaleLogPoliceler.Count(c => c.strMethod == "settanzim") > 0)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else if (besSaleInfo.bolBesApprove)
                        {
                            ViewBag.Message = "Tanzim isteği gönderilmiş!";
                        }
                        else
                        {
                            saleInfo = new SaleInfo();

                            saleInfo.strInterMediaryName = saleLog.strInterMediaryName;
                            saleInfo.strPlanName = saleLog.tblBesPlans.strPlanName;
                            saleInfo.strComment = saleLog.tblBesPlans.strComment;

                            saleInfo.intPolID = saleLog.tblBesSaleLogPoliceler.First(f => f.strMethod == "setbasvuru").intPolID;

                            tblBesSaleLogInfo besSaleLogInfo = saleLog.tblBesSaleLogInfo.OrderBy(o => o.intID).First();

                            saleInfo.strName = besSaleLogInfo.adi;
                            saleInfo.strSurname = besSaleLogInfo.soyadi;
                            saleInfo.strTCKN = besSaleLogInfo.TCKN;

                            if (saleLog.bolDifferentPaid)
                            {
                                tblBesSaleLogInfo besSaleLogInfoDiff = saleLog.tblBesSaleLogInfo.OrderByDescending(o => o.intID).First();

                                saleInfo.strDiffrerentTCKN = besSaleLogInfoDiff.TCKN;
                                saleInfo.strDiffrerentName = besSaleLogInfoDiff.adi;
                                saleInfo.strDiffrerentSurname = besSaleLogInfoDiff.soyadi;
                            }

                            tblBesSaleLogOrder besSaleLogOrder = saleLog.tblBesSaleLogOrder.First();

                            saleInfo.strFirstDueDate = besSaleLogOrder.FirstDueDate;
                            saleInfo.flExtraContAmount = Utilities.Price(besSaleLogOrder.flExtraContAmount);
                            saleInfo.flInitilalContAmount = Utilities.Price(besSaleLogOrder.flInitilalContAmount);
                            saleInfo.flOrderlyContAmount = Utilities.Price(besSaleLogOrder.flOrderlyContAmount);

                            switch (besSaleLogOrder.intOrderlyContAmountPeriod.Value)
                            {
                                case 12:
                                    saleInfo.strOrderlyContAmountPeriod = "Aylık";
                                    break;
                                case 4:
                                    saleInfo.strOrderlyContAmountPeriod = "3 Aylık";
                                    break;
                                case 2:
                                    saleInfo.strOrderlyContAmountPeriod = "6 Aylık";
                                    break;
                                case 1:
                                    saleInfo.strOrderlyContAmountPeriod = "Yıllık";
                                    break;
                            }

                            saleInfo.fonList = new List<FonList>();

                            JavaScriptSerializer jss = new JavaScriptSerializer();

                            List<FonList> fonList = jss.Deserialize<List<FonList>>(besSaleLogOrder.FonList);
                            if (fonList != null && fonList.Count > 0)
                            {
                                FibaBES.FibaBesWebServiceClient service = new FibaBES.FibaBesWebServiceClient();
                                var fonlistService = service.getfonbytarife(saleLog.tblBesPlans.strPlanCode);

                                if (fonlistService != null && fonlistService.fonlar != null && fonlistService.fonlar.Count > 0)
                                {
                                    foreach (var fon in fonList)
                                    {
                                        if (Utilities.NullFixDecimal(fon.Oran) > 0 && fonlistService.fonlar.Count(c => c.fonkod == fon.FonKod) > 0)
                                        {
                                            fon.Fonaciklama = fonlistService.fonlar.First(f => f.fonkod == fon.FonKod).fonaciklama;

                                            saleInfo.fonList.Add(fon);
                                        }
                                    }
                                }
                            }

                            var policy = saleLog.tblBesSaleLogPoliceler.FirstOrDefault();
                            if (policy != null)
                            {
                                FIBAWS.fibawsClient fibawsClient = new FIBAWS.fibawsClient();
                                var odemeBilgi = fibawsClient.getodemebilgirec(Utilities.NullFixDecimal(policy.intPolID));
                                if (odemeBilgi != null)
                                {
                                    saleInfo.strCardNo = odemeBilgi.kartno;
                                }
                            }
                        }
                    }
                }
            }

            return View(saleInfo);
        }
        #endregion

        #region Tanzim

        #region Get
        [HttpGet]
        public ActionResult Tanzim(string token, bool bolPost = false)
        {
            string callcenterID = "";
            if (!string.IsNullOrEmpty(token) && !bolPost)
            {
                string strToken = token.Substring(0, 8);

                using (DBEntities db = new DBEntities())
                {
                    tblBesSaleLogs saleLog = db.tblBesSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                    if (saleLog != null)
                    {
                        int intBesSaleLogInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                        var besSaleInfo = db.tblBesSaleLogInfo.Where(w => w.intID == intBesSaleLogInfoID).FirstOrDefault();
                        if (besSaleInfo == null)
                        {
                            ViewBag.Message = "Eksik veya hatalı link!";
                            return View();
                        }

                        if (saleLog.tblBesSaleLogPoliceler.Count(c => c.strMethod == "settanzim") > 0)
                        {
                            ViewBag.Message = "Poliçe tanzim edilmiş!";
                        }
                        else if (besSaleInfo.bolBesApprove)
                        {
                            ViewBag.Message = "Tanzim isteği gönderilmiş!";
                        }
                        else
                        {
                            tblBesSaleLogInfo saleLogInfo = saleLog.tblBesSaleLogInfo.First();

                            callcenterID = cCrypto.EncryptDES(saleLogInfo.tblBesSaleLogs.tblUsers.intCallcenterID.ToString());
                        }
                    }
                }
            }

            ViewBag.callcenterID = callcenterID;

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult Tanzim(string token)
        {
            int inLogID = 0;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    string strToken = token.Substring(0, 8);

                    using (DBEntities db = new DBEntities())
                    {
                        tblBesSaleLogs saleLog = db.tblBesSaleLogs.Where(w => w.InfoFormToken == strToken).FirstOrDefault();
                        if (saleLog != null)
                        {
                            inLogID = saleLog.intLogID;

                            int intBesSaleLogInfoID = Utilities.NullFixInt(token.Replace(strToken, ""));

                            var besSaleInfo = db.tblBesSaleLogInfo.Where(w => w.intID == intBesSaleLogInfoID).First();

                            if (saleLog.tblBesSaleLogPoliceler.Count(c => c.strMethod == "settanzim") > 0)
                            {
                                ViewBag.Message = "Poliçe tanzim edilmiş!";
                            }
                            else if (besSaleInfo.bolBesApprove)
                            {
                                ViewBag.Message = "Tanzim isteği gönderilmiş!";
                            }
                            else
                            {
                                bool bolNext = true;

                                if (saleLog.bolDifferentPaid)
                                {
                                    if (saleLog.tblBesSaleLogInfo.Count(c => c.bolBesApprove) == 0)
                                    {
                                        bolNext = false;
                                    }
                                }

                                besSaleInfo.bolBesApprove = true;

                                if (!bolNext)
                                {
                                    ViewBag.Policy = "1";
                                    ViewBag.Message = "Tanzim isteği gönderildi.";
                                }
                                else
                                {
                                    FibaBES.EmkbasvurulibuserinputtypUser pUserbilgi = new FibaBES.EmkbasvurulibuserinputtypUser
                                    {
                                        password = ConfigurationManager.AppSettings["BesWsPss"],
                                        kullanicikodu = ConfigurationManager.AppSettings["BesWsUserCode"],
                                        bankano = null,
                                        username = ConfigurationManager.AppSettings["BesWsUserName"],
                                        subeno = Utilities.NullFixDecimal(saleLog.tblUsers.tblCallcenters.strBankNo)
                                    };

                                    JavaScriptSerializer javaSer = new JavaScriptSerializer();

                                    int intWSLogID = Logs.WSLog(0, saleLog.intUserID, saleLog.intLogID, "bes", "New", "Input", javaSer.Serialize(pUserbilgi), "tanzim");

                                    FibaBES.FibaBesWebServiceClient service = new FibaBES.FibaBesWebServiceClient();

                                    var tanzim = service.settanzim(Utilities.NullFixDecimal(saleLog.tblBesSaleLogPoliceler.Last().intPolID), pUserbilgi);
                                    if (tanzim != null && tanzim.polid != null)
                                    {
                                        Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "bes", "New", "Output", javaSer.Serialize(tanzim), "tanzim");

                                        BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();

                                        Logs.BesSaleLogPolice(true, saleLog.intLogID, "settanzim", tanzim.polid.Value.ToString(), tanzim.sonuc.hataaciklama);

                                        decimal tutar = 0;
                                        if (saleLog.tblBesPlans.bolInitialCont)
                                        {
                                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount;
                                        }
                                        else if (saleLog.tblBesSaleLogOrder.First().flInitilalContAmount > 0)
                                        {
                                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount + saleLog.tblBesSaleLogOrder.First().flInitilalContAmount;
                                        }
                                        else
                                        {
                                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount;
                                        }

                                        cCreditCard cardInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<cCreditCard>(cCrypto.DecryptDES(saleLog.strCreditCardInfo));

                                        BankaWebV2.bankaweb_v2Client service2 = new BankaWebV2.bankaweb_v2Client();
                                        BankaWebV2.BankawebV2SanalposinUser pInput = new BankaWebV2.BankawebV2SanalposinUser
                                        {
                                            cvv = cardInfo.cvv,
                                            expmonth = cardInfo.month,
                                            expyear = cardInfo.year,
                                            kartno = cardInfo.card,
                                            polid = tanzim.polid,
                                            taksitadedi = 1,
                                            tutar = tutar,
                                            user = "HYSG",
                                            vadelist = new BankaWebV2.Emkblibsposvadetyl
                                            {
                                                new BankaWebV2.EmkblibsposvadetypUser
                                                {
                                                    tutar = tutar,
                                                    vadetar = DateTime.Now.ToString("ddMMyyyy")
                                                }
                                            }
                                        };

                                        intWSLogID = Logs.WSLog(0, saleLog.intUserID, saleLog.intLogID, "bes", "New", "Input", "", "spostaksitli");

                                        var spostaksitli = service2.spostaksitli(pInput);

                                        Logs.WSLog(intWSLogID, saleLog.intUserID, saleLog.intLogID, "bes", "New", "Output", javaSer.Serialize(spostaksitli), "spostaksitli");

                                        Logs.BesSaleLogPay(saleLog.intLogID, spostaksitli.issuccess, spostaksitli.aciklama);

                                        string strPolicy = cCrypto.EncryptDES(tanzim.polid.ToString());

                                        ViewBag.Message = "";
                                        ViewBag.Policy = strPolicy;

                                        saleLog.strCreditCardInfo = "";

                                        db.SaveChanges();

                                        string strLink = ConfigurationManager.AppSettings["SaleUrl"] + "/pdf/bes/" + strToken;
                                        
                                        var infoPdf = db.tblBesSaleLogInfo.Where(w => w.intLogID == inLogID && !w.DifferentPaid).First();

                                        bool bolSms = false;
                                        bool bolEmail = false;

                                        if (infoPdf.InfoFormSendType == "sms")
                                        {
                                            bolSms = SmsSend(infoPdf.iletisimno, strLink, infoPdf.adi + " " + infoPdf.soyadi);
                                            if (!bolSms)
                                            {
                                                bolEmail = MailSend(infoPdf.email, strLink, infoPdf.adi + " " + infoPdf.soyadi);
                                            }
                                        }
                                        else if (infoPdf.InfoFormSendType == "mail")
                                        {
                                            bolEmail = MailSend(infoPdf.email, strLink, infoPdf.adi + " " + infoPdf.soyadi);
                                            if (!bolEmail)
                                            {
                                                bolSms = SmsSend(infoPdf.iletisimno, strLink, infoPdf.adi + " " + infoPdf.soyadi);
                                            }
                                        }

                                        Logs.BesLinkLog(saleLog.intLogID, besSaleInfo.TCKN, besSaleInfo.iletisimno, besSaleInfo.email, tanzim.polid.ToString(), strLink, bolSms, bolEmail);
                                    }
                                    else
                                    {
                                        ViewBag.Message = "Poliçe tanzim edilemedi!";
                                    }
                                }

                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            ViewBag.Message = "Hatalı giriş!";
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "Hatalı giriş!";
                }
            }
            catch (Exception ex)
            {
                int intExceptionID = Logs.Error(ex);

                Logs.BesSaleLogEx(inLogID, intExceptionID, "Bes tanzim hata oluştu.");
            }

            return Tanzim(token, true);
        }

        private bool SmsSend(string strPhone, string strLink, string strNameSurname)
        {
            try
            {
                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                {
                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                });

                string smsSend = sms.Send("Sayın " + strNameSurname + ", bireysel emeklilik sözleşmenize " + strLink + " linkinden erişebilirsiniz. Fibaemeklilik ile bir ömür dileriz. Fibaemeklilik 444 82 88 Mersis No: 396059952300012", strPhone);

                return smsSend.Contains("ID");
            }
            catch { }

            return false;
        }

        private bool MailSend(string strEmail, string strLink, string strNameSurname)
        {
            string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
            strHtml = strHtml.Replace("#adsoyad#", strNameSurname);
            strHtml = strHtml.Replace("#mesaj#", "<p>Bireysel emeklilik sözleşmenize <a href=\"" + strLink + "\">" + strLink + "</a> linkinden erişebilirsiniz.</p><p>İyi günler dileriz.</p><p>FİBA EMEKLİLİK VE HAYAT A.Ş.<br/>www.fibaemeklilik.com.tr<br/>444 82 88</p>");

            return Email.SendEmail("Fiba Emeklilik – Bireysel Emeklilik Sözleşmesi Bilgilendirme Formları Hk.", strHtml, strEmail);
        }
        #endregion

        #endregion
    }
}