﻿namespace FibaEmeklilikSales.Areas.Bes.Classes
{
    public class FonList
    {
        public string Fonaciklama { get; set; }
        public string Oran { get; set; }
        public string FonKod { get; set; }
    }
}