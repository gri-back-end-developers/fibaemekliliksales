﻿using System;
using System.Collections.Generic; 

namespace FibaEmeklilikSales.Areas.Bes.Classes
{
    public class SaleInfo
    {
        public string strPlanName { get; set; }
        public string strComment { get; set; }
        public string intPolID { get; set; }
        public string strTCKN { get; set; }
        public string strName { get; set; }
        public string strSurname { get; set; }
        public string strDiffrerentTCKN { get; set; }
        public string strDiffrerentName { get; set; }
        public string strDiffrerentSurname { get; set; }
        public string flOrderlyContAmount { get; set; }
        public string flInitilalContAmount { get; set; }
        public string flExtraContAmount { get; set; }
        public string strOrderlyContAmountPeriod { get; set; }
        public string strFirstDueDate { get; set; }
        public string strCardNo { get; set; }
        public string strInterMediaryName { get; set; }
        public List<FonList> fonList { get; set; }
    }
}