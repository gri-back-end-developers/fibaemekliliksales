﻿using System.Web.Mvc;

namespace FibaEmeklilikSales.Areas.Bes
{
    public class BesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Bes";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Bes_default",
                "Bes/{controller}/{action}/{token}",
                new { action = "Index", token = UrlParameter.Optional }
            );
        }
    }
}