﻿using FibaEmeklilikSales.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace FibaEmeklilikSales
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error()
        {
        }

        protected void Application_EndRequest()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Error500"] == "on")
            {
                if (Context.Response.StatusCode == 404 || Context.Response.StatusCode == 500)
                {
                    Response.Clear();

                    var rd = new RouteData();
                    rd.Values["controller"] = "Error";
                    rd.Values["action"] = Context.Response.StatusCode == 404 ? "status404" : "status500";

                    IController c = new ErrorController();
                    c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
                }
            }
        }
    }
}