#region Directives
using FibaEmeklilikSales.CrmWS;
using ModelLibrary;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Linq;
#endregion

namespace FibaEmeklilikSales.Classes
{
    public class Email
    {
        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString());
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());

                server.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                return false;
            }
        }
        #endregion

        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody, string strTo)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), strTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());
                server.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                return false;
            }
        }
        #endregion

        #region SendCRM
        public static bool SendCRM(string strOwnerId, string strQueueId, string strPhoneCallType, string strCustomerName, string strCustomerLastName, string strOfferNo, string strPhoneNumber, string strDescription, string strSubject)
        {
            try
            {
                if (!string.IsNullOrEmpty(strPhoneNumber))
                {
                    strPhoneNumber = strPhoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "");
                    if (strPhoneNumber.Length == 11)
                    {
                        strPhoneNumber = strPhoneNumber.Remove(0, 1);
                    }
                }

                using (FibaEmeklilikService crm = new FibaEmeklilikService())
                {
                    var request = new CreateOutBoundCallRequest
                    {
                        HeaderData = new RequestHeader
                        {
                            Password = ConfigurationManager.AppSettings["crmPassword"].ToString(),
                            Username = ConfigurationManager.AppSettings["crmUsername"].ToString()
                        },
                        CustomerLastName = strCustomerLastName,
                        CustomerName = strCustomerName,
                        OfferNo = string.IsNullOrEmpty(strOfferNo) ? strSubject : strOfferNo,
                        OwnerId = strOwnerId,
                        OwnerType = "team",
                        PhoneCallType = strPhoneCallType,
                        PhoneNumber = strPhoneNumber,
                        QueueId = strQueueId,
                        Description = strDescription
                    };

                    //System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //string ss = jss.Serialize(request);

                    var result = crm.CreateOutBoundCall(request);
                    if (result.ResponseCode == "200")
                    {
                        Logs.Error(new Exception("SendCrm Success PolID [" + strOfferNo + "] : " + result.ResponseMessage));
                        return true;
                    }
                    else
                    {
                        Logs.Error(new Exception("SendCrm Response PolID [" + strOfferNo + "] : " + result.ResponseMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(new Exception("SendCrm Ex PolID [" + strOfferNo + "] : " + ex.ToString()));
            }

            return false;
        }
        #endregion

        #region SendBes
        public static void SendBes(tblBesSaleLogs saleLog, string strSubject)
        {
            var besInfo = saleLog.tblBesSaleLogInfo.First();

            //string strTemplate = Utilities.GetFileText(Server.MapPath("~/htmls/MailIssued.html"));
            //strTemplate = strTemplate.Replace("#adsoyad#", besInfo.adi + " " + besInfo.soyadi);
            //strTemplate = strTemplate.Replace("#tel#", besInfo.iletisimno);
            //strTemplate = strTemplate.Replace("#ceptel#", "");

            var saleOrder = saleLog.tblBesSaleLogOrder.FirstOrDefault();
            if (saleOrder != null)
            {
                string strDetail = "";
                strDetail += "<p><b>S�zle�me No :</b>" + saleLog.tblBesSaleLogPoliceler.Last().intPolID + "</p>";
                strDetail += "<p><b>�r�n :</b>" + saleLog.tblBesPlans.strPlanName + "</p>";
                strDetail += "<p><b>D�zenli Katk� Pay� Tutar� :</b>" + Utilities.Price(saleOrder.flOrderlyContAmount) + " TL</p>";
                strDetail += "<p><b>Ba�lang�� Katk� Pay� Tutar� :</b>" + Utilities.Price(saleOrder.flInitilalContAmount) + " TL</p>";
                strDetail += "<p><b>Ek Katk� Pay� Tutar� :</b>" + Utilities.Price(saleOrder.flExtraContAmount) + " TL</p>";
                string strPeriod = "";
                switch (saleOrder.intOrderlyContAmountPeriod)
                {
                    case 12:
                        strPeriod = "Ayl�k";
                        break;
                    case 4:
                        strPeriod = "3 Ayl�k";
                        break;
                    case 2:
                        strPeriod = "6 Ayl�k";
                        break;
                    case 1:
                        strPeriod = "Y�ll�k";
                        break;
                }
                strDetail += "<p><b>D�zenli Katk� Pay� �deme Periyodu :</b>" + strPeriod + "</p>";
                strDetail += "<p><b>�lk Vade Tarihi :</b>" + saleOrder.FirstDueDate + "</p>";

                //strTemplate = strTemplate.Replace("#detay#", strDetail);

                //List<string> emails = ConfigurationManager.AppSettings["BesInsuedMail"].Split(';').ToList();
                //foreach (var item in emails)
                //{
                //    Email.SendEmail("Teklifi fibaemeklili�e g�nder arac�l���yla g�nderilmi�tir", strTemplate, item);
                //}
                Email.SendCRM(ConfigurationManager.AppSettings["crmTeklifGonderOwnerId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderQueueId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderPhoneCallType"].ToString(), besInfo.adi, besInfo.soyadi, saleLog.tblBesSaleLogPoliceler.Last().intPolID, besInfo.iletisimno, strDetail.Replace("<p><b>", Environment.NewLine).Replace("</b>", "").Replace("</p>", ""), strSubject);
            }
        }
        #endregion
    }
}