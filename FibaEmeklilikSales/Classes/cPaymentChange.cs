﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class cPaymentChange
    {
        public List<cPaymentChangeValues> banks { get; set; }
        public List<cPaymentChangeValues> branch { get; set; }
    }

    public class cPaymentChangeValues
    {
        public string strName { get; set; }
        public string strValue { get; set; }
    }
}