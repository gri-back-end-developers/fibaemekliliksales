﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class Caches
    {
        #region ScratchWins
        public static List<tblScratchWin> ScratchWins
        {
            get
            {
                try
                {
                    var scratchWins = (List<tblScratchWin>)HttpContext.Current.Cache["ScratchWins"];

                    return scratchWins == null ? new List<tblScratchWin>() : scratchWins;
                }
                catch
                {
                    return new List<tblScratchWin>();
                }
            }
            set
            {
                System.Web.HttpContext.Current.Cache.Add("ScratchWins", value, null, DateTime.Now.AddDays(1), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null); 
            }
        }
        #endregion

        #region RemoveScratchWins
        public static void RemoveScratchWins()
        {
            System.Web.HttpContext.Current.Cache.Remove("ScratchWins");
        }
        #endregion
    }
}