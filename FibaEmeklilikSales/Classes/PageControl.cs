﻿using ModelLibrary;
using System.Linq;
using System.Web; 

namespace FibaEmeklilikSales.Classes
{
    public class PageControl
    {
        private static bool _bolPage = true;
        private static string _strPage = "";
        private static string _strDomainType = "";

        public static string DomainType
        {
            get
            {
                if (string.IsNullOrEmpty(_strDomainType))
                    return HttpContext.Current.Request.Url.Segments[HttpContext.Current.Request.Url.Segments.Length - 1].ToLower();
                else
                    return _strDomainType;
            }
        }

        static string Path
        {
            get
            {
                string strUrl = HttpContext.Current.Request.Url.AbsolutePath;
                switch (DomainType)
                {
                    case "new":
                    case "update":
                    case "delete":
                        strUrl = "/" + (string.IsNullOrEmpty(_strPage) ? HttpContext.Current.Request.Url.Segments[1].Replace("/", "") : _strPage) + "/List";
                        break;
                    case "products":
                    case "besplans":
                    case "credits":
                    case "fibabankproducts":
                        strUrl = "/main/index";
                        break;
                }

                return strUrl;
            }
        }

        public static bool GetDetail(bool bolPage = true, string strPage = "", string strDomainType = "")
        {
            _bolPage = bolPage;
            _strPage = strPage;
            _strDomainType = strDomainType;

            return Get;
        }

        public static bool Get
        {
            get
            {
                bool bolResponse = false;
                try
                {
                    var userPage = Sessions.CurrentUser.tblUserAuth.Where(w => w.tblPages.strUrl.ToLower() == Path.ToLower()).FirstOrDefault();
                    if (userPage == null)
                    {
                        DBEntities db = new DBEntities();
                        var subPage = db.tblPages.Where(w => w.strUrl.ToLower() == Path.ToLower() && w.intSubPageID > 0).FirstOrDefault();
                        if (subPage != null)
                        {
                            var page = db.tblPages.Where(w => w.intPageID == subPage.intSubPageID).First();
                            userPage = Sessions.CurrentUser.tblUserAuth.Where(w => w.tblPages.strUrl.ToLower() == page.strUrl.ToLower()).FirstOrDefault();
                        }

                        if (userPage == null && _bolPage)
                        { 
                            Sessions.LastPage = HttpContext.Current.Request.Url.ToString();
                            HttpContext.Current.Response.Redirect("/Default/Login");
                        }
                    }

                    switch (DomainType)
                    {
                        case "list":
                        case "cancel":
                        case "policyview":
                        case "index":
                        case "products":
                        case "besplans":
                        case "credits":
                        case "fibabankproducts":
                            if (userPage.tblPages.strPageName == "Raporlar")
                            {
                                bolResponse = UserFilter.PagePermision(userPage);
                            }
                            else
                            {
                                bolResponse = userPage.bolView;
                            }
                            break;
                        case "new":
                            bolResponse = userPage.bolInsert;
                            break;
                        case "update":
                            bolResponse = userPage.bolUpdate;
                            break;
                        case "delete":
                            bolResponse = userPage.bolDelete;
                            break;
                    }

                    if (!bolResponse && _bolPage)
                    {
                        var firstPage = Sessions.CurrentUser.tblUserAuth.Where(w => w.bolView).OrderBy(o => o.tblPages.intOrder).FirstOrDefault();

                        if (DomainType == "cancel")
                        {
                            if (firstPage != null)
                                Sessions.Message = "<script>top.location.href = '" + firstPage.tblPages.strUrl + "';</script>";
                            else
                                Sessions.Message = "<script>top.location.href = '/Default/Login';</script>";
                        }
                        else
                        {
                            if (firstPage != null)
                                HttpContext.Current.Response.Redirect(firstPage.tblPages.strUrl);
                            else
                                HttpContext.Current.Response.Redirect("/Default/Login", false);
                        }
                    }
                }
                catch
                {
                    try
                    {
                        if (_bolPage)
                        {
                            Sessions.LastPage = HttpContext.Current.Request.Url.ToString();
                            if (DomainType == "cancel")
                                Sessions.Message = "<script>top.location.href = '/Default/Login';</script>";
                            else
                                HttpContext.Current.Response.Redirect("/Default/Login", false);
                        }
                    }
                    catch { }
                }

                _bolPage = true;
                _strPage = "";
                _strDomainType = "";

                return bolResponse;
            }
        }

        public static byte PageID
        {
            get
            {
                byte PageID = 0;
                try
                {
                    var userPage = Sessions.CurrentUser.tblUserAuth.Where(w => w.tblPages.strUrl.ToLower() == Path.ToLower()).FirstOrDefault();
                    if (userPage != null)
                    {
                        return userPage.intPageID;
                    }
                }
                catch { }

                return PageID;
            }
        }
    }
}