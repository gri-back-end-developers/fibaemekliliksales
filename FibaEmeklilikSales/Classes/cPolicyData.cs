﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class cPolicySearch
    {
        public List<cPolicySearchData> PolicySearchData { get; set; }
        public int intCount { get; set; }
        public int intTotalCount { get; set; }
        public HtmlString strMsg { get; set; }
        public string strSearch { get; set; } 
        public string strSearchType { get; set; }
        public string strStatu { get; set; }
        public int intPage { get; set; }
        public bool bolPaymentChange { get; set; }

        public cPolicySearch()
        {
            strMsg = new HtmlString("");
            intCount = 0;
            intTotalCount = 0;
            intPage = 1;
            PolicySearchData = null;
        }
    }

    public class cPolicySearchData
    {
        public string PolID { get; set; }
        public string PoliceNo { get; set; }
        public string Durum { get; set; }
        public string Brans { get; set; }
        public string SigortaliKatilimciAdSoyad { get; set; }
        public string KimlikNo { get; set; }
        public string OdeyenAdSoyad { get; set; }
        public string PlanTarifeNo { get; set; }
        public string BitisTarihi { get; set; }
        public DateTime BaslamaTarihi { get; set; }
        public string Pid { get; set; }
    }

    #region LifeDetail
    public class cPolicyLifeDetail
    {
        public cPolicyLifeDetailGeneralInfo GeneralInfo { get; set; }
        public List<cPolicyLifeDetailCollectionInfo> CollectionInfo { get; set; }
        public cPolicyDetailMusteri Musteri { get; set; }

        public cPolicyLifeDetail()
        {
            GeneralInfo = new cPolicyLifeDetailGeneralInfo();
        }
    }

    public class cPolicyLifeDetailGeneralInfo
    {
        public string PoliceNumarasi { get; set; }
        public string Sigortali { get; set; }
        public string BaslangicTarihi { get; set; }
        public string BitisTarihi { get; set; }
        public string Durum { get; set; }
        public string UrunAdi { get; set; }
        public string SigortaEttiren { get; set; }
        public string SigortaliMusteri { get; set; }
        public cPolicyLifeDetailAssurance Teminat { get; set; }
        public cPolicyLifeDetailPaymentInfo ÖdemeBilgileri { get; set; }
    }

    public class cPolicyLifeDetailAssurance
    {
        public List<cPolicyLifeDetailAssuranceDetail> Teminatlar { get; set; }
        public string ToplamPrim { get; set; }
    }

    public class cPolicyLifeDetailAssuranceDetail
    {
        public string Teminat { get; set; }
        public string Prim { get; set; }
        public string Tutar { get; set; }
    }

    public class cPolicyLifeDetailPaymentInfo
    {
        public string OdemeTipi { get; set; }
        public string BankaAdi { get; set; }
        public string SonKullanmaTarihi { get; set; }
        public string IbanNo { get; set; }
        public string YillikTaksitSayisi { get; set; }
        public string SubeNo { get; set; }
        public string KartNo { get; set; }
        public string HesapNo { get; set; }
        public string OdemeGunu { get; set; }
    }

    public class cPolicyLifeDetailCollectionInfo
    {
        public string MakbuzNo { get; set; }
        public string VadeTarihi { get; set; }
        public string Tahsilat { get; set; }
        public string VadeTutar { get; set; }
        public string Makbuz { get; set; }
        public string IntikalTarihi  { get; set; }
        public string DKTutari { get; set; }
        public string TahsilatTarihi  { get; set; }
    } 
    #endregion

    #region BesDetail
    public class cPolicyBesDetail
    {
        public cPolicyBesDetailGeneralInfo GeneralInfo { get; set; }
        public List<cPolicyBesDetailFundInfo> FundInfo { get; set; }
        public cPolicyBesDetailInvestInfo InvestInfp { get; set; }
        public cPolicyBesDetailTahsilatInfo Tahsilat { get; set; }
        public cPolicyDetailMusteri Musteri { get; set; }
    }

    public class cPolicyBesDetailGeneralInfo
    {
        public string GrupAdi { get; set; }
        public string GrupTipi { get; set; }
        public string Odeyen { get; set; }
        public string BesGirisTarihi { get; set; }
        public string YururlukTarihi { get; set; }
        public string KatkiPayiOdemeDonemi { get; set; }
        public string EmeklilikTarihi { get; set; }
        public string ToplamGirişAidatıTutari { get; set; }
        public string YillikKatkiPayiTutari { get; set; }
        public string KrediKartiHesap { get; set; }
        public string Devirmi { get; set; }
        public bool HesapBildirimCetveli { get; set; }
        public List<cPolicyBesDetailLehdarInfo> Lehdar { get; set; }
    }

    public class cPolicyBesDetailLehdarInfo
    {
        public string Adi { get; set; }
        public string LehtarTipi { get; set; }
        public string Oran { get; set; }
        public string Yakınlık { get; set; }
        public string Cins { get; set; }
    }

    public class cPolicyBesDetailFundInfo
    {
        public string FonKodu { get; set; }
        public string FonAdi { get; set; }
        public string KatkiPayiFonDagilimi { get; set; }
    }

    public class cPolicyBesDetailInvestInfo
    {
        public List<cPolicyBesDetailInvestInfoTable> Table { get; set; }
        public string ProvisyondaBekleyenveYatirimaYonlendirilemeyenTLTutar { get; set; }
        public string TLTahsilati { get; set; }
        public string ToplamDKTahsilati { get; set; }
        public string TLBirikimDevletKatkisiDahil { get; set; }
        public string TLBirikimDevletKatkisiHaric { get; set; }
        public string ToplamDKBirikim { get; set; }
        public string DKHakedisTutari { get; set; }
        public string OncekiSirkettekiBirikimTutari { get; set; }
    }

    public class cPolicyBesDetailInvestInfoTable
    {
        public string FonAdi { get; set; }
        public string BirimFiyati { get; set; }
        public string PayAdedi { get; set; }
        public string Tutar { get; set; }
        public string Oran { get; set; }
    }

    public class cPolicyBesDetailTahsilatInfo
    {
        public List<cPolicyLifeDetailCollectionInfo> CollectionInfo { get; set; }
        public string TahsilatDurdurmaTalepTarihi { get; set; }
        public string TahsilatDurdurmaBaslangicTarihi { get; set; }
        public string TahsilatDurdurmaBitisTarihi { get; set; }
        public bool OdemeLink { get; set; }
    }

    public class cPolicyDetailMusteri
    { 
        public string Email { get; set; }
        public string Adres { get; set; }
        public string Tel { get; set; }
        public string Tckn { get; set; }
    }

    public class cPolicyDetailInfoSend
    {
        public string Email { get; set; } 
        public string Phone { get; set; }
        public string SendType { get; set; }
    }
    #endregion
}