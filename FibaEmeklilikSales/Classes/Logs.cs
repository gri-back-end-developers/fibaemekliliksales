﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Configuration;
using ModelLibrary;

namespace FibaEmeklilikSales.Classes
{
    public class Logs
    {
        #region Login
        public static void Login(int intUserID)
        {
            DBEntities db = new DBEntities();
            try
            {
                db.tblUserLoginLog.Add(new tblUserLoginLog()
                {
                    intUserID = intUserID,
                    dtLoginDate = DateTime.Now,
                    strIP = HttpContext.Current.Request.UserHostAddress
                });
                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region Log
        public static void Log(Enums.LogType logType, int intID)
        {
            DBEntities db = new DBEntities();
            try
            {
                var intPageID = PageControl.PageID;

                if (intPageID > 0)
                {
                    db.tblLogs.Add(new tblLogs()
                    {
                        intPageID = intPageID,
                        intID = intID,
                        intUserID = Sessions.CurrentUser.intUserID,
                        dtRegisterDate = DateTime.Now,
                        strIP = HttpContext.Current.Request.UserHostAddress,
                        intLogTypeID = (byte)logType
                    });
                    db.SaveChanges();
                }
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region Cancel
        public static void Cancel(decimal intPolice)
        {
            DBEntities db = new DBEntities();
            try
            {
                db.tblCancelLogs.Add(new tblCancelLogs()
                {
                    intUserID = Sessions.CurrentUser.intUserID,
                    intPolice = intPolice,
                    dtRegisterDate = DateTime.Now
                });
                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region SaleLog
        public static void SaleLog(int intLogID, bool bolInsert, Enums.LogType logType, string strPacketIDs, string strExistMessage, string strTCKN, string strIdentityType, List<SaleLogPoliceler> pol = null, int intExceptionID = 0, bool bolValidCreditCard = false, bool bolDifferentPaid = false)
        {
            DBEntities db = new DBEntities();
            try
            {
                if (bolInsert)
                {
                    tblSaleLogs saleLogs = new tblSaleLogs();
                    saleLogs.intUserID = Sessions.CurrentUser.intUserID;
                    saleLogs.strPacketIDs = strPacketIDs;
                    saleLogs.dtRegisterDate = DateTime.Now;
                    saleLogs.intLogTypeID = (byte)logType;
                    saleLogs.strHealthAnswer = "Hayır";

                    db.tblSaleLogs.Add(saleLogs);
                    db.SaveChanges();

                    intLogID = saleLogs.intLogID;

                    Cookies.SaleLog = saleLogs.intLogID;
                    Sessions.SaleLog = saleLogs.intLogID;
                }
                else if (intExceptionID == 0)
                {
                    var log = db.tblSaleLogs.Where(w => w.intLogID == intLogID).First();

                    if (logType == Enums.LogType.ProductSelected)
                        log.strPacketIDs = strPacketIDs;

                    if (logType == Enums.LogType.CustomerPolicyInfo)
                        log.strExistMessage = strExistMessage;

                    log.intLogTypeID = (byte)logType;

                    //if (logType == Enums.LogType.CustomerInfo && QueryStrings.DifferentPaid == "")
                    //    log.bolDifferentPaid = bolDifferentPaid;

                    tblSaleLogInfo info = new tblSaleLogInfo();

                    if (logType == Enums.LogType.TCKNInput)
                    {
                        var saleLogCtrl = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && w.bolDifferentPaid == bolDifferentPaid).FirstOrDefault();
                        if (saleLogCtrl == null)
                        {
                            db.tblSaleLogInfo.Add(new tblSaleLogInfo
                            {
                                bolDifferentPaid = bolDifferentPaid,
                                intLogID = intLogID,
                                tckn = strTCKN,
                                kimliktipi = strIdentityType
                            });
                        }
                        else
                        {
                            saleLogCtrl.tckn = strTCKN;
                            saleLogCtrl.kimliktipi = strIdentityType;
                        }
                    }

                    if (pol != null && pol.Count > 0 && !bolValidCreditCard)
                    {
                        foreach (var item in pol)
                        {
                            tblSaleLogPoliceler policeler = new tblSaleLogPoliceler();
                            policeler.intLogID = intLogID;
                            policeler.intPacketID = item.intPacketID;
                            policeler.PoliceNo = item.PoliceNo.ToString();
                            policeler.IlkPrim = item.IlkPrim.ToString();
                            policeler.ToplamPirim = item.ToplamPirim.ToString();
                            policeler.IlkPrimKomisyon = item.IlkPrimKomisyon.ToString();
                            policeler.KrediNo = item.KrediNo;
                            policeler.TeklifDurum = item.TeklifDurum;
                            policeler.ToplamKomisyon = item.ToplamKomisyon.ToString();
                            policeler.intPoliceTypeID = item.intPoliceTypeID;
                            policeler.strMessage = item.strMessage;
                            policeler.bolCancel = item.bolCancel;
                            policeler.bolPay = item.bolPay;
                            policeler.strPayMessage = item.strPayMessage;
                            policeler.dtRegisterDate = DateTime.Now;
                            policeler.strService = item.strService;
                            policeler.bolNew = item.bolNew;
                            policeler.strYenileme = item.strYenileme;
                            policeler.Bsmv = item.bsmv.HasValue ? item.bsmv.Value.ToString() : "";

                            db.tblSaleLogPoliceler.Add(policeler);
                        }
                    }
                    else
                    {
                        log.bolValidCreditCard = true;
                    }
                }

                if (logType == Enums.LogType.UW || logType == Enums.LogType.ErrorSOR)
                {
                    if (HttpContext.Current.Request.Form["InfoFormSendType"] != null)
                    {
                        foreach (var saleinfo in db.tblSaleLogInfo.Where(w => w.intLogID == intLogID).ToList())
                        {
                            saleinfo.InfoFormSendType = HttpContext.Current.Request.Form["InfoFormSendType"];

                            saleinfo.tblSaleLogs.strCreditCardInfo = cCrypto.EncryptDES(Newtonsoft.Json.JsonConvert.SerializeObject(new cCreditCard
                            {
                                month = Sessions.CreditCard.month,
                                year = Sessions.CreditCard.year,
                                card = Sessions.CreditCard.card,
                                type = Sessions.CreditCard.type
                            }));
                        }
                        db.SaveChanges();
                    }
                }

                if (logType == Enums.LogType.LinkSend)
                {
                    ProductSaleMessageSend(intLogID, false);
                }

                tblSaleLogDetails logDetail = new tblSaleLogDetails();
                logDetail.intLogID = intLogID;
                logDetail.dtRegisterDate = DateTime.Now;
                logDetail.intLogTypeID = (byte)logType;
                logDetail.intExceptionID = intExceptionID;
                logDetail.strSaleType = "product";

                db.tblSaleLogDetails.Add(logDetail);

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region Error
        public static int Error(Exception ex)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblExceptions exceptions = new tblExceptions()
                {
                    intPageID = PageControl.PageID,
                    strException = ex.ToString(),
                    dtRegisterDate = DateTime.Now
                };

                db.tblExceptions.Add(exceptions);
                db.SaveChanges();

                return exceptions.intExceptionID;
            }
            catch { }

            db.Dispose();

            return 0;
        }
        #endregion

        #region SaleLogInfo
        public static void SaleLogInfo(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                bool bolDifferentPaid = QueryStrings.DifferentPaid != "";

                tblSaleLogInfo info = db.tblSaleLogInfo.FirstOrDefault(c => c.intLogID == intLogID && c.bolDifferentPaid == bolDifferentPaid);
                bool bolNew = false;

                if (info == null)
                {
                    bolNew = true;
                    info.intLogID = intLogID;
                    info.bolDifferentPaid = bolDifferentPaid;
                }

                info.ad = HttpContext.Current.Request.Form["txt_ad"];
                info.soyad = HttpContext.Current.Request.Form["txt_soyad"];
                info.adres = HttpContext.Current.Request.Form["adres"];
                info.email = HttpContext.Current.Request.Form["email"];
                info.ilcekod = HttpContext.Current.Request.Form["ilcekod"];
                info.iletisimno = HttpContext.Current.Request.Form["iletisimno"];
                info.iletisimtip = HttpContext.Current.Request.Form["iletisimtip"];
                info.ilkod = HttpContext.Current.Request.Form["ilkod"];
                info.ulkekod = HttpContext.Current.Request.Form["ulkekod"];
                info.uyruk = HttpContext.Current.Request.Form["uyruk"];
                info.anneadi = info.uyruk == "TR" ? "" : HttpContext.Current.Request.Form["anneadi"];
                info.medenidurum = info.uyruk == "TR" ? "" : HttpContext.Current.Request.Form["medenidurum"];
                info.adrestip = "E";

                if (bolNew)
                {
                    db.tblSaleLogInfo.Add(info);
                }

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLog
        public static void BesSaleLog(bool bolInsert, Enums.LogType logType, int intBesPlanID, int intExceptionID = 0)
        {
            DBEntities db = new DBEntities();
            try
            {
                int intSalePage = QueryStrings.SalePage;

                int intLogID = 0;
                if (bolInsert)
                {
                    tblBesSaleLogs saleLogs = new tblBesSaleLogs();
                    saleLogs.intUserID = Sessions.CurrentUser.intUserID;
                    saleLogs.intBesPlanID = intBesPlanID;
                    saleLogs.dtRegisterDate = DateTime.Now;
                    saleLogs.intLogTypeID = (byte)logType;

                    db.tblBesSaleLogs.Add(saleLogs);
                    db.SaveChanges();

                    intLogID = saleLogs.intLogID;

                    Cookies.BesSaleLog = saleLogs.intLogID;
                    Sessions.BesSaleLog = saleLogs.intLogID;
                }
                else if (intExceptionID == 0)
                {
                    intLogID = Sessions.BesSaleLog;

                    var log = db.tblBesSaleLogs.Where(w => w.intLogID == intLogID).First();

                    if (intSalePage == 1)
                    {
                        string strInterMediary = ConfigurationManager.AppSettings["InterMediary"];
                        if (Sessions.CurrentUser.bolMediator && !string.IsNullOrEmpty(Sessions.CurrentUser.strMediatorID))
                        {
                            strInterMediary = Sessions.CurrentUser.strMediatorID + "|" + Sessions.CurrentUser.strMediatorName;
                        }

                        log.strInterMediaryCode = strInterMediary.Split('|')[0];
                        log.strInterMediaryName = strInterMediary.Split('|')[1];
                    }
                    else if (intSalePage == 6)
                    {
                        log.bolDifferentPaid = HttpContext.Current.Request.Form["DifferentPaid"] == "on" || HttpContext.Current.Request.Form["DifferentPaid"] == "checked" || HttpContext.Current.Request.Form["DifferentPaid"] == "true";
                    }

                    db.SaveChanges();
                }

                tblSaleLogDetails logDetail = new tblSaleLogDetails();
                logDetail.intBesLogID = intLogID;
                logDetail.dtRegisterDate = DateTime.Now;
                logDetail.intLogTypeID = (byte)logType;
                logDetail.intExceptionID = intExceptionID;
                logDetail.strSaleType = "bes";
                db.tblSaleLogDetails.Add(logDetail);

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogInfo
        public static void BesSaleLogInfo(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                int intSalePage = QueryStrings.SalePage;

                if (intSalePage == 2)
                {
                    Sessions.TCKN = HttpContext.Current.Request.Form["txtTCKN"];

                    tblBesSaleLogInfo info = new tblBesSaleLogInfo();
                    info.intLogID = intLogID;
                    info.TCKN = HttpContext.Current.Request.Form["txtTCKN"];
                    info.dtRegisterDate = DateTime.Now;

                    db.tblBesSaleLogInfo.Add(info);
                }
                else if (intSalePage == 3)
                {
                    string strTCKN = Sessions.TCKN;

                    var info = db.tblBesSaleLogInfo.Where(w => w.intLogID == intLogID && w.TCKN == strTCKN).First();

                    info.uyruk = HttpContext.Current.Request.Form["uyruk"];
                    info.adi = HttpContext.Current.Request.Form["adi"];
                    info.anneadi = HttpContext.Current.Request.Form["anneAdi"];
                    info.babaadi = HttpContext.Current.Request.Form["babaAdi"];
                    info.cinsiyet = HttpContext.Current.Request.Form["cinsiyeti"];
                    info.dogumTarihi = HttpContext.Current.Request.Form["dogumTarihi"];
                    info.dogumYeri = HttpContext.Current.Request.Form["dogumYeri"];
                    info.medenidurum = info.uyruk == "TR" ? (HttpContext.Current.Request.Form["medeniHali"] == "EVLİ" ? "E" : "B") : HttpContext.Current.Request.Form["medeniHali"];
                    info.soyadi = HttpContext.Current.Request.Form["soyadi"];
                    info.adres = HttpContext.Current.Request.Form["adres"];
                    info.email = HttpContext.Current.Request.Form["email"];
                    info.ilcekod = HttpContext.Current.Request.Form["ilcekod"];
                    info.iletisimno = HttpContext.Current.Request.Form["iletisimno"];
                    info.ilkod = HttpContext.Current.Request.Form["ilkod"];
                    info.ulkekod = HttpContext.Current.Request.Form["ulkekod"];
                    info.DifferentPaid = info.tblBesSaleLogs.bolDifferentPaid;
                }

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogOrder
        public static void BesSaleLogOrder(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                int intSalePage = QueryStrings.SalePage;

                tblBesSaleLogOrder order = db.tblBesSaleLogOrder.Where(w => w.intLogID == intLogID).FirstOrDefault();
                bool bolNew = false;

                if (order == null)
                {
                    order = new tblBesSaleLogOrder();

                    bolNew = true;
                    order.intLogID = intLogID;
                    order.dtResgisterDate = DateTime.Now;
                }

                if (intSalePage == 4)
                {
                    order.flExtraContAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flExtraContAmount"]);
                    order.flInitilalContAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flInitilalContAmount"]);
                    order.flOrderlyContAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flOrderlyContAmount"]);
                    order.FirstDueDate = HttpContext.Current.Request.Form["FirstDueDate"];
                    order.intOrderlyContAmountPeriod = Utilities.NullFixByte(HttpContext.Current.Request.Form["intOrderlyContAmountPeriod"]);
                }
                else if (intSalePage == 5)
                {
                    List<cFonList> fonList = new List<cFonList>();

                    int intFonListCount = Utilities.NullFixInt(HttpContext.Current.Request.Form["FonListCount"]);
                    for (int i = 0; i < intFonListCount; i++)
                    {
                        string strOran = HttpContext.Current.Request.Form["fonstandartoran" + i];

                        fonList.Add(new cFonList
                        {
                            FonKod = HttpContext.Current.Request.Form["fonkod" + i],
                            Oran = string.IsNullOrEmpty(strOran) ? "0" : strOran
                        });
                    }

                    JavaScriptSerializer javaSer = new JavaScriptSerializer();
                    order.FonList = javaSer.Serialize(fonList);
                }
                else if (intSalePage == 6)
                {
                    order.InitialDifferentPaid = HttpContext.Current.Request.Form["InitialDifferentPaid"] == "on" || HttpContext.Current.Request.Form["InitialDifferentPaid"] == "checked" || HttpContext.Current.Request.Form["InitialDifferentPaid"] == "true";
                }
                else if (intSalePage == 7)
                {
                    order.AccountNo = HttpContext.Current.Request.Form["hesapno"];
                    order.intBranchesID = Utilities.NullFixInt(HttpContext.Current.Request.Form["subeno"]);
                }

                if (bolNew)
                {
                    db.tblBesSaleLogOrder.Add(order);
                }

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogPolice
        public static void BesSaleLogPolice(bool bolBesPage, int intLogID, string strMethod, string intPolID, string strMsg)
        {
            DBEntities db = new DBEntities();
            try
            {
                string strToken = Utilities.RandomString(8);

                foreach (var info in db.tblBesSaleLogInfo.Where(w => w.intLogID == intLogID).ToList())
                {
                    if (!bolBesPage)
                    {
                        bool bolInfoFormMail = false;
                        bool bolInfoFormSms = false;

                        info.InfoFormSendType = HttpContext.Current.Request.Form["InfoFormSendType"];

                        info.tblBesSaleLogs.InfoFormToken = strToken;

                        info.tblBesSaleLogs.strCreditCardInfo = cCrypto.EncryptDES(Newtonsoft.Json.JsonConvert.SerializeObject(new cCreditCard
                        {
                            cvv = Sessions.CreditCard.cvv,
                            month = Sessions.CreditCard.month,
                            year = Sessions.CreditCard.year,
                            card = Sessions.CreditCard.card
                        }));

                        string strLink = ConfigurationManager.AppSettings["MesafeSatisUrl"] + "/bes/sale/index/" + strToken + info.intID;
                        if (info.InfoFormSendType == "sms")
                        {
                            bolInfoFormSms = SmsSend(info.iletisimno, strLink, info.adi + " " + info.soyadi, "bes");

                            if (!bolInfoFormSms)
                            {
                                bolInfoFormMail = MailSend(info.email, strLink, info.adi + " " + info.soyadi, "bes");
                            }
                        }
                        else if (info.InfoFormSendType == "mail")
                        {
                            bolInfoFormMail = MailSend(info.email, strLink, info.adi + " " + info.soyadi, "bes");

                            if (!bolInfoFormMail)
                            {
                                bolInfoFormSms = SmsSend(info.iletisimno, strLink, info.adi + " " + info.soyadi, "bes");
                            }
                        }

                        info.bolInfoFormMail = bolInfoFormMail;
                        info.bolInfoFormSms = bolInfoFormSms;
                    }
                    else
                    {
                        info.kvkkMetni = "evet";
                        info.mesafeliSozlesme = "evet";
                        info.bilgilendirmeFormu = "evet";
                    }
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            try
            {
                tblBesSaleLogPoliceler police = new tblBesSaleLogPoliceler();

                police.intLogID = intLogID;
                police.dtRegisterDate = DateTime.Now;
                police.strMethod = strMethod;
                police.intPolID = intPolID;
                police.strMsg = strMsg;
                police.bolSendMail = false;

                db.tblBesSaleLogPoliceler.Add(police);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogPoliceSendMail
        public static void BesSaleLogPoliceSendMail(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                var police = db.tblBesSaleLogPoliceler.Where(w => w.intLogID == intLogID).First();
                police.bolSendMail = true;

                //string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\BesMail.html");
                //strHtml = strHtml.Replace("#aciklama#", police.tblBesSaleLogs.tblBesPlans.strComment);
                //strHtml = strHtml.Replace("#police#", police.intPolID);
                //strHtml = strHtml.Replace("#kullanici#", Sessions.CurrentUser.strNameSurname);

                //List<string> emails = ConfigurationManager.AppSettings["BesMail"].Split(';').ToList();

                //foreach (var item in emails)
                //{
                //Email.SendEmail("BES E-Acente Satışa Dönüştürme", strHtml, item);
                //Email.SendCRM(ConfigurationManager.AppSettings["crmBESEcenteSatisaDonusturmeOwnerId"].ToString(), ConfigurationManager.AppSettings["crmBESEcenteSatisaDonusturmeQueueId"].ToString(), ConfigurationManager.AppSettings["crmBESEcenteSatisaDonusturmePhoneCallType"].ToString(), police.tblBesSaleLogs.tblBesSaleLogInfo.First().adi, police.tblBesSaleLogs.tblBesSaleLogInfo.First().soyadi, police.intPolID, police.tblBesSaleLogs.tblBesSaleLogInfo.First().iletisimno, police.tblBesSaleLogs.tblBesPlans.strComment, "BES E-Acente Satışa Dönüştürme");
                //}

                Email.SendBes(police.tblBesSaleLogs, "Sözleşme tanzimi yapamasın seçeneği ile gönderilmiştir");

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogEx
        public static void BesSaleLogEx(int intLogID, int intExceptionID, string strMsg)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblBesSaleLogEx ex = new tblBesSaleLogEx();

                ex.intLogID = intLogID;
                ex.dtRegisterDate = DateTime.Now;
                ex.strMsg = strMsg;
                ex.intExceptionID = intExceptionID;

                db.tblBesSaleLogEx.Add(ex);
                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region BesSaleLogPay
        public static void BesSaleLogPay(int intLogID, string issuccess, string aciklama)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblBesSaleLogPay pay = new tblBesSaleLogPay();

                pay.intLogID = intLogID;
                pay.dtRegisterDate = DateTime.Now;
                pay.aciklama = aciklama;
                pay.issuccess = issuccess;

                db.tblBesSaleLogPay.Add(pay);
                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region CreditSaleLog
        public static int CreditSaleLog(int intCreditID)
        {
            DBEntities db = new DBEntities();
            int intLogID = 0;

            try
            {
                tblCreditSaleLogs saleLogs = new tblCreditSaleLogs();
                saleLogs.intUserID = Sessions.CurrentUser.intUserID;
                saleLogs.intCreditID = intCreditID;
                saleLogs.dtRegisterDate = DateTime.Now;
                saleLogs.intCreditTypeID = QueryStrings.CreditType;

                db.tblCreditSaleLogs.Add(saleLogs);
                db.SaveChanges();

                intLogID = saleLogs.intLogID;

                db.SaveChanges();

                Cookies.CreditSaleLog = saleLogs.intLogID;
                Sessions.CreditSaleLog = saleLogs.intLogID;
            }
            catch { }

            db.Dispose();

            return intLogID;
        }
        #endregion

        #region CreditSaleLogDetail
        public static void CreditSaleLogDetail(int intLogID, Enums.LogType logType, int intExceptionID = 0)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblSaleLogDetails logDetail = new tblSaleLogDetails();
                logDetail.intCreditLogID = intLogID;
                logDetail.dtRegisterDate = DateTime.Now;
                logDetail.intLogTypeID = (byte)logType;
                logDetail.intExceptionID = intExceptionID;
                logDetail.strSaleType = "credit";
                db.tblSaleLogDetails.Add(logDetail);

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region CreditSaleLogInfo
        public static void CreditSaleLogInfo(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                int intSalePage = QueryStrings.SalePage;

                if (intSalePage == 1)
                {
                    var existInfo = db.tblCreditSaleLogInfo.Where(w => w.intLogID == intLogID).FirstOrDefault();
                    if (existInfo == null)
                    {
                        tblCreditSaleLogInfo info = new tblCreditSaleLogInfo();
                        info.intLogID = intLogID;
                        info.strTCKN = HttpContext.Current.Request.Form["txtTCKN"];
                        info.dtRegisterDate = DateTime.Now;

                        db.tblCreditSaleLogInfo.Add(info);
                    }
                    else
                    {
                        existInfo.strTCKN = HttpContext.Current.Request.Form["txtTCKN"];
                    }
                }
                else if (intSalePage == 2 || intSalePage == 3 || intSalePage == 4 || intSalePage == 5 || intSalePage == 6 || intSalePage == 7)
                {
                    var info = db.tblCreditSaleLogInfo.Where(w => w.intLogID == intLogID).First();
                    if (intSalePage == 2)
                    {
                        info.strCountryCode = HttpContext.Current.Request.Form["uyruk"];
                        info.strName = HttpContext.Current.Request.Form["adi"];
                        info.strMotherName = HttpContext.Current.Request.Form["anneAdi"];
                        info.strFatherName = HttpContext.Current.Request.Form["babaAdi"];
                        info.strGender = HttpContext.Current.Request.Form["cinsiyeti"];
                        info.strBirthDate = HttpContext.Current.Request.Form["dogumTarihi"];
                        info.strBirthplace = HttpContext.Current.Request.Form["dogumYeri"];
                        info.strMaritalStatus = info.strCountryCode == "TR" ? (HttpContext.Current.Request.Form["medeniHali"] == "EVLİ" ? "E" : "B") : HttpContext.Current.Request.Form["medeniHali"];
                        info.strSurname = HttpContext.Current.Request.Form["soyadi"];
                        info.strAddress = HttpContext.Current.Request.Form["adres"];
                    }
                    else if (intSalePage == 3)
                    {
                        info.flCreditAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flCreditAmount"]);
                        info.flInterest = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flInterest"]);
                        info.intCreditMonth = Utilities.NullFixInt(HttpContext.Current.Request.Form["intCreditMonth"]);
                        info.flKKDF = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flKKDF"]);
                        info.flBSMV = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flBSMV"]);
                    }
                    else if (intSalePage == 4)
                    {
                        info.intCreditPacketID = Utilities.NullFixInt(HttpContext.Current.Request.Form["selectedPacket"]);
                    }
                    else if (intSalePage == 5)
                    {
                        info.strAddress = HttpContext.Current.Request.Form["adres"];
                        info.strEmail = HttpContext.Current.Request.Form["email"];
                        info.strMobile = HttpContext.Current.Request.Form["cepno"];
                        info.strPhone = HttpContext.Current.Request.Form["iletisimno"];
                        info.strCountryCode = HttpContext.Current.Request.Form["ulkekodu"];
                        info.strRegionCode = HttpContext.Current.Request.Form["ilkod"];
                        info.strDistrictCode = HttpContext.Current.Request.Form["ilcekod"];
                        info.bolInfoForm = Utilities.NullFixBool(HttpContext.Current.Request.Form["hdInfoForm"]);
                    }
                    else if (intSalePage == 6)
                    {
                        info.intQuestionType = Utilities.NullFixByte(HttpContext.Current.Request.Form["question"]);
                    }
                    else if (intSalePage == 7)
                    {
                        info.intBankCityID = Utilities.NullFixShort(HttpContext.Current.Request.Form["intBankCityID"]);
                        info.intBankID = Utilities.NullFixShort(HttpContext.Current.Request.Form["intBankID"]);
                        info.intBankBranchID = Utilities.NullFixShort(HttpContext.Current.Request.Form["intBankBranchID"]);
                    }
                }

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region CreditSalePaymentPlan
        public static void CreditSalePaymentPlan(int intLogID)
        {
            DBEntities db = new DBEntities();
            try
            {
                var creditSalePaymentPlans = db.tblCreditSalePaymentPlans.Where(w => w.intLogID == intLogID).ToList();
                foreach (var item in creditSalePaymentPlans)
                {
                    item.flInstallAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flInstallAmount"]);
                    item.flRemainingAmount = Utilities.NullFixDecimal(HttpContext.Current.Request.Form["flRemainingAmount" + item.intInstallment]);
                }

                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region WSLog
        public static int WSLog(int intWSLogID, int intUserID, int intSaleLogID, string strSaleType, string strClassName, string strMethodType, string strJson, string strMethod)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblWSLogs wslog = new tblWSLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intReleationWSLogID = intWSLogID,
                    intSaleLogID = intSaleLogID,
                    intStep = QueryStrings.SalePage,
                    intUserID = intUserID,
                    strClassName = strClassName,
                    strJson = strJson,
                    strMethod = strMethod,
                    strMethodType = strMethodType,
                    strProject = "eacente",
                    strSaleType = strSaleType
                };

                db.tblWSLogs.Add(wslog);
                db.SaveChanges();

                intWSLogID = wslog.intWSLogID;
            }
            catch { }

            db.Dispose();

            return intWSLogID;
        }
        #endregion

        #region BesLinkLog
        public static void BesLinkLog(int intBesSaleLogID, string strTckn, string strPhone, string strEmail, string strPolID, string strLink, bool bolSms, bool bolEmail)
        {
            DBEntities db = new DBEntities();
            try
            {
                db.tblBesSaleLinkLogs.Add(new tblBesSaleLinkLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intBesSaleLogID = intBesSaleLogID,
                    strEmail = strEmail,
                    strPhone = strPhone,
                    strPolID = strPolID,
                    strLink = strLink,
                    strTckn = strTckn,
                    bolEmail = bolEmail,
                    bolSms = bolSms
                });
                db.SaveChanges();
            }
            catch { }

            db.Dispose();
        }
        #endregion

        #region ProductSaleMessageSend
        public static void ProductSaleMessageSend(int intLogID, bool bolSearchPage)
        {
            string strToken = Utilities.RandomString(8);

            DBEntities db = new DBEntities();
            foreach (var saleinfo in db.tblSaleLogInfo.Where(w => w.intLogID == intLogID).ToList())
            {
                bool bolInfoFormMail = false;
                bool bolInfoFormSms = false;

                if (!bolSearchPage)
                {
                    saleinfo.InfoFormSendType = HttpContext.Current.Request.Form["InfoFormSendType"];

                    saleinfo.tblSaleLogs.strCreditCardInfo = cCrypto.EncryptDES(Newtonsoft.Json.JsonConvert.SerializeObject(new cCreditCard
                    {
                        month = Sessions.CreditCard.month,
                        year = Sessions.CreditCard.year,
                        card = Sessions.CreditCard.card,
                        type = Sessions.CreditCard.type
                    }));
                }

                saleinfo.tblSaleLogs.InfoFormToken = strToken;

                string strLink = ConfigurationManager.AppSettings["HayatSatisUrl"] + "/life/sale/index/" + strToken + saleinfo.intID;
                if (saleinfo.InfoFormSendType == "sms")
                {
                    bolInfoFormSms = SmsSend(saleinfo.iletisimno, strLink, saleinfo.ad + " " + saleinfo.soyad, "life");

                    if (!bolInfoFormSms)
                    {
                        bolInfoFormMail = MailSend(saleinfo.email, strLink, saleinfo.ad + " " + saleinfo.soyad, "life");
                    }
                }
                else if (saleinfo.InfoFormSendType == "mail")
                {
                    bolInfoFormMail = MailSend(saleinfo.email, strLink, saleinfo.ad + " " + saleinfo.soyad, "life");

                    if (!bolInfoFormMail)
                    {
                        bolInfoFormSms = SmsSend(saleinfo.iletisimno, strLink, saleinfo.ad + " " + saleinfo.soyad, "life");
                    }
                }

                saleinfo.bolInfoFormMail = bolInfoFormMail;
                saleinfo.bolInfoFormSms = bolInfoFormSms;
            }
            db.SaveChanges();
        }
        #endregion

        #region CreditSaleMessageSend
        public static void CreditSaleMessageSend(int intLogID, bool bolSearchPage)
        {
            string strToken = Utilities.RandomString(8);

            DBEntities db = new DBEntities();

            var saleinfo = db.tblCreditSaleLogInfo.Where(w => w.intLogID == intLogID).First();

            bool bolInfoFormMail = false;
            bool bolInfoFormSms = false;

            if (!bolSearchPage)
            {
                saleinfo.InfoFormSendType = HttpContext.Current.Request.Form["InfoFormSendType"];

                saleinfo.tblCreditSaleLogs.strCreditCardInfo = cCrypto.EncryptDES(Newtonsoft.Json.JsonConvert.SerializeObject(new cCreditCard
                {
                    month = HttpContext.Current.Request.Form["kartay"],
                    year = HttpContext.Current.Request.Form["kartyil"],
                    card = HttpContext.Current.Request.Form["kartno"].Replace(" ", "").Trim(),
                    type = HttpContext.Current.Request.Form["paymentMethods"]
                }));
            }

            saleinfo.tblCreditSaleLogs.InfoFormToken = strToken;

            string strLink = ConfigurationManager.AppSettings["KrediHayatSatisUrl"] + "/credit/sale/index/" + strToken + saleinfo.intCreditSaleInfoID;
            if (saleinfo.InfoFormSendType == "sms")
            {
                bolInfoFormSms = SmsSend(saleinfo.strMobile, strLink, saleinfo.strName + " " + saleinfo.strSurname, "credit");

                if (!bolInfoFormSms)
                {
                    bolInfoFormMail = MailSend(saleinfo.strEmail, strLink, saleinfo.strName + " " + saleinfo.strSurname, "credit");
                }
            }
            else if (saleinfo.InfoFormSendType == "mail")
            {
                bolInfoFormMail = MailSend(saleinfo.strEmail, strLink, saleinfo.strName + " " + saleinfo.strSurname, "credit");

                if (!bolInfoFormMail)
                {
                    bolInfoFormSms = SmsSend(saleinfo.strMobile, strLink, saleinfo.strName + " " + saleinfo.strSurname, "credit");
                }
            }

            saleinfo.bolInfoFormMail = bolInfoFormMail;
            saleinfo.bolInfoFormSms = bolInfoFormSms;

            db.SaveChanges();
        }
        #endregion
        
        #region SmsSend
        private static bool SmsSend(string strPhone, string strLink, string strNameSurname, string strSaleType)
        {
            try
            {
                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                {
                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                });

                string strMsg = "";
                if (strSaleType == "bes")
                {
                    strMsg = "Fibaemeklilik Bireysel Emeklilik Sözleşmesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için ";
                }
                else if (strSaleType == "life")
                {
                    strMsg = "Fibaemeklilik Hayat Poliçesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için ";
                }
                else if (strSaleType == "credit")
                {
                    strMsg = "Fibaemeklilik Kredi Hayat Poliçesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için ";
                }

                string smsSend = sms.Send("Sayın " + strNameSurname + ",  " + strMsg + strLink + " lütfen tıklayınız. Fibaemeklilik 444 82 88 Mersis No : 0396059952300012", strPhone);

                return smsSend.Contains("ID");
            }
            catch { }

            return false;
        }
        #endregion

        #region MailSend
        private static bool MailSend(string strEmail, string strLink, string strNameSurname, string strSaleType)
        {
            string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
            strHtml = strHtml.Replace("#adsoyad#", strNameSurname);

            string strMsg = "";
            string strTitle = "";
            if (strSaleType == "bes")
            {
                strMsg = "Fiba Emeklilik Hayat Sigortası Sözleşmesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için";
                strTitle = "Hayat Sigortası Sözleşmesi Bilgilendirme Formları Hk.";
            }
            else if (strSaleType == "life")
            {
                strMsg = "Fiba Emeklilik Hayat Sigortası Poliçesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için";
                strTitle = "Hayat Sigortası Poliçesi Bilgilendirme Formları Hk.";
            }
            else if (strSaleType == "credit")
            {
                strMsg = "Fiba Emeklilik Kredi Hayat Sigortası Poliçesinin Giriş Bilgi ve Teklif Formlarına ulaşmak ve onaylamak için";
                strTitle = "Kredi Hayat Sigortası Poliçesi Bilgilendirme Formları Hk.";
            }

            strHtml = strHtml.Replace("#mesaj#", "<p>" + strMsg + " <a href=\"" + strLink + "\">" + strLink + "</a> lütfen tıklayınız.</p><p>Fibaemeklilik ile bir ömür dileriz.</p><p>Saygılarımızla,<br/>FİBA EMEKLİLİK VE HAYAT A.Ş.<br/>www.fibaemeklilik.com.tr<br/>444 82 88<br/>Mersis No : 0396059952300012</p>");

            return Email.SendEmail("Fiba Emeklilik – " + strTitle, strHtml, strEmail);
        }
        #endregion

        #region PhoneLogCount
        public static int PhoneLogCount(string strTckn, string strPhone)
        {
            using (DBEntities db = new DBEntities())
            {
                int intSaleInfoLogCount = 0;
                int intCreditInfoLogCount = 0;

                var saleInfoLogs = db.tblSaleLogInfo.Where(w => !w.bolDifferentPaid && w.iletisimno == strPhone && w.tblSaleLogs.tblSaleLogPoliceler.Count(c => c.strService == "teklifpolicelesme") > 0 && w.tblSaleLogs.tblSaleLogPoliceler.Count(c => c.bolCancel) == 0).ToList();
                if (saleInfoLogs.Count(c => c.tckn == strTckn) == 0)
                {
                    intSaleInfoLogCount = saleInfoLogs.Count;
                }
                
                var creditInfoLogs = db.tblCreditSaleLogInfo.Where(w => w.strMobile == strPhone && !string.IsNullOrEmpty(w.strPolicyData2) && !w.strPayMessage.Contains("İptal")).ToList();
                if (creditInfoLogs.Count(c => c.strTCKN == strTckn) == 0)
                {
                    intCreditInfoLogCount = creditInfoLogs.Count;
                }

                return intSaleInfoLogCount + intCreditInfoLogCount;
            }
        } 
        #endregion
    }
}