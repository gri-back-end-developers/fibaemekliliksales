#region Directives
using FibaEmeklilikSales.Classes.Credits;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
#endregion

namespace FibaEmeklilikSales.Classes
{
    public class Cookies
    {
        #region RemoveAll
        public static void RemoveAll()
        {
            string[] cookies = { "SaleLog", "saleinfo", "BesSaleLog", "bessaleinfo", "CreditSaleLog", "creditsaleinfo", "popupnotification" };

            foreach (var item in cookies)
            {
                HttpContext.Current.Response.Cookies.Remove(item);
                HttpContext.Current.Response.Cookies[item].Expires = DateTime.Now.AddDays(-1);
            }
        }
        #endregion

        #region SaleLog
        public static int SaleLog
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["SaleLog"].Value;
                    if (objValue == null)
                    {
                        return 0;
                    }
                    return Utilities.NullFixInt(objValue.ToString());
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("SaleLog");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region SaleInfo
        public static string SaleInfo
        {
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("saleinfo");
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddMinutes(60);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region BesSaleLog
        public static int BesSaleLog
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["BesSaleLog"].Value;
                    if (objValue == null)
                    {
                        return 0;
                    }
                    return Utilities.NullFixInt(objValue.ToString());
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("BesSaleLog");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region BesSaleInfo
        public static string BesSaleInfo
        {
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("bessaleinfo");
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddMinutes(60);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region CreditSaleLog
        public static int CreditSaleLog
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["CreditSaleLog"].Value;
                    if (objValue == null)
                    {
                        return 0;
                    }
                    return Utilities.NullFixInt(objValue.ToString());
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("CreditSaleLog");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region CreditSaleInfo
        public static string CreditSaleInfo
        {
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("creditsaleinfo");
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddMinutes(60);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region SelectedPacket
        public static cSelectedPacket SelectedPacket
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["selectedPacket"].Value;
                    if (objValue == null)
                    {
                        return null;
                    }

                    JavaScriptSerializer java = new JavaScriptSerializer();

                    string strVal = HttpUtility.UrlDecode(objValue.ToString());

                    return java.Deserialize<cSelectedPacket>(strVal);
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region PopupNotification
        public static string PopupNotification
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["popupnotification"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region RemovePopupNotification
        public static void RemovePopupNotification()
        {
            HttpContext.Current.Response.Cookies.Remove("popupnotification");
            HttpContext.Current.Response.Cookies["popupnotification"].Expires = DateTime.Now.AddDays(-1);
        }
        #endregion
    }
}