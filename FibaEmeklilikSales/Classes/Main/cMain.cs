﻿using ModelLibrary;
using System.Collections.Generic;

namespace FibaEmeklilikSales.Classes.Main
{ 
    public class cMain
    {
        public bool bolProducts { get; set; }
        public bool bolPersonalProducts { get; set; }
        public bool bolBesPlans { get; set; }
        public bool bolCredit { get; set; }
        public bool bolFibabankProduct { get; set; }
        public List<tblNotifications> notifications { get; set; }
    }
}