﻿using ModelLibrary;
using System.Collections.Generic;

namespace FibaEmeklilikSales.Classes.Main
{
    public class cCredits
    {
        public List<tblCredits> credits { get; set; }
        public List<tblCreditTypes> creditTypes { get; set; }
        public int intCount { get; set; }
    }
}