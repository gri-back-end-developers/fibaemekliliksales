﻿using ModelLibrary;
using System.Collections.Generic;

namespace FibaEmeklilikSales.Classes.Main
{ 
    public class cBesPlans
    {
        public List<tblBesPlans> besPlans { get; set; }
        public int intCount { get; set; }
    }
}