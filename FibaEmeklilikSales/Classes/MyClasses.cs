﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class UserAuth
    {
        public List<UserAuthDetail> tblUserAuth { get; set; }
    }

    public class UserAuthDetail
    {
        public byte pageid { get; set; }
        public bool bolView { get; set; }
        public bool bolInsert { get; set; }
        public bool bolUpdate { get; set; }
        public bool bolDelete { get; set; }
    }

    public class cProductTable
    {
        public string strColumn { get; set; }
        public List<cProductTableValues> Values { get; set; }
    }

    public class cProductTableValues
    {
        public string Value { get; set; }
    }

    public class cSelectedPacket
    {
        public int id { get; set; }
        public string totalPremium { get; set; }
        public string firstPremium { get; set; } 
    }

    public class cReportPermisions
    {
        public Enums.UserType UserType { get; set; }
        public bool bolPage { get; set; }
        public bool bolExcel { get; set; }
    }
}