﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Classes
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string strPage = HttpContext.Current.Request.Url.AbsolutePath.ToLower();

            if (!strPage.Contains("/productsale/new") && !strPage.Contains("/besplansale/new") && !strPage.Contains("/creditsale/new"))
            {
                int intLogID = Sessions.SaleLog;
                if (intLogID > 0)
                {
                    DBEntities db = new DBEntities();

                    var saleLogPoliceler = db.tblSaleLogPoliceler.Where(w => w.intLogID == intLogID && w.intPoliceTypeID == (byte)Enums.PoliceType.TeklifPolicelesme && !w.bolPay && string.IsNullOrEmpty(w.strPayMessage)).ToList();
                    if (saleLogPoliceler.Count > 0)
                    {
                        foreach (var item in saleLogPoliceler)
                        {
                            var result = PoliceCancel(Utilities.NullFixDecimal(item.PoliceNo));
                            if (result.cevapkod > 0)
                            {
                                item.strMessage = "İptal edildi";
                                item.bolCancel = true;
                            }
                        }
                        db.SaveChanges();
                    }
                }

                Cookies.SaleLog = 0;
                Sessions.SaleLog = 0;
            }

            if (Sessions.IP != Utilities.ClientIP)
            {
                Sessions.CurrentUser = null;
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.RemoveAll();
                HttpContext.Current.Session.Abandon();
            }

            if (HttpContext.Current.Session["CurrentUser"] == null)
            {
                Sessions.LastPage = HttpContext.Current.Request.Url.ToString().Split('?')[0];

                HttpContext.Current.Response.Redirect("/Default/Login");
            }
        }

        #region PoliceCancel
        BankaWebV2.BankawebV2PoliptrecUser PoliceCancel(decimal PoliceNo)
        {
            BankaWebV2.BankawebV2PoliptrecUser poluser = new BankaWebV2.BankawebV2PoliptrecUser();
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                BankaWebV2.BankawebV2PoliptinUser poliptinUser = new BankaWebV2.BankawebV2PoliptinUser();
                poliptinUser.iptnedenkod = 7;
                poliptinUser.ipttar = (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                poliptinUser.polid = PoliceNo;

                poluser = service.poliptal("1", poliptinUser); 
            }

            return poluser;
        }
        #endregion

        #region PagePermision
        public static bool PagePermision(tblUserAuth userAuth)
        {
            bool bolIsView = false;
            try
            {
                if (userAuth.tblPages.strUrl == "/Report/List" && !Sessions.CurrentUser.tblUserTypes.bolPowerAdmin && (Enums.UserType)Sessions.CurrentUser.intUserTypeID != Enums.UserType.ReportUser)
                {
                    if (!string.IsNullOrEmpty(userAuth.tblUsers.tblCallcenters.strReportPermisions))
                    {
                        JavaScriptSerializer jss = new JavaScriptSerializer();
                        var reportPermision = jss.Deserialize<List<cReportPermisions>>(userAuth.tblUsers.tblCallcenters.strReportPermisions);

                        bolIsView = reportPermision.First(f => f.UserType == (Enums.UserType)Sessions.CurrentUser.intUserTypeID).bolPage;
                    }
                }
                else if (userAuth.tblPages.strUrl == "/ProductSale/Cancel" && !Sessions.CurrentUser.tblUserTypes.bolPowerAdmin && (Enums.UserType)Sessions.CurrentUser.intUserTypeID != Enums.UserType.PowerAdmin)
                {
                    bolIsView = userAuth.tblUsers.tblCallcenters.bolCancel;
                }
                else
                {
                    bolIsView = userAuth.bolView && userAuth.tblPages.bolIsView;
                }
            }
            catch { }

            return bolIsView;
        }
        #endregion
    }
}