﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class cBannerTbl
    {
        public int intBannerID { get; set; }
        public int intWebSaleLogID { get; set; }
        public tblWebSaleLogs tblWebSaleLogs { get; set; }
        public string strReferringSite { get; set; }
        public DateTime dtRegisterDate { get; set; }
        public bool bolCallcenter { get; set; }
        public string strKey { get; set; }
        public string strUrl { get; set; }
    }

    public class cBannerData
    {
        public string strKey { get; set; }
        public string strTypeName { get; set; }
        public string strName { get; set; }
        public string strReferringSite { get; set; }
        public string strPolicy { get; set; }
        public string strPrice { get; set; }
        public Enums.BannerStatus status { get; set; }
        public string strDate { get; set; }
        public string strUrl { get; set; }
    }

    public class cBannerList
    {
        public List<cBannerData> GetData(int intBannerID, int intCallcenterID, string strSaleType, Enums.BannerStatus status, string strSearch)
        {
            List<cBannerData> _data = new List<cBannerData>();

            DBEntities db = new DBEntities();

            List<cBannerTbl> bannerLogs = db.tblBannerLogs.Where(w => !w.tblBanners.bolIsDelete).Select(s => new cBannerTbl
            {
                intBannerID = s.intBannerID,
                intWebSaleLogID = s.intWebSaleLogID,
                tblWebSaleLogs = s.tblWebSaleLogs,
                strReferringSite = s.strReferringSite,
                dtRegisterDate = s.dtRegisterDate,
                strKey = s.strKey,
                strUrl = s.strUrl
            }).ToList();

            if (intBannerID > 0)
            {
                bannerLogs = bannerLogs.Where(w => w.intBannerID == intBannerID).ToList();
            }

            if (intCallcenterID == 0)
            {
                if (Sessions.CurrentUser.intUserTypeID == (byte)Enums.UserType.Admin)
                {
                    intCallcenterID = Sessions.CurrentUser.intCallcenterID;
                }
            }

            if (intCallcenterID > 0)
            {
                foreach (var item in bannerLogs)
                {
                    if (item.intWebSaleLogID > 0)
                    {
                        if (item.tblWebSaleLogs.intBesPlanID > 0)
                        {
                            var callcenter = db.tblCallcenterBesPlans.Where(w => w.intCallcenterID == intCallcenterID && w.intBesPlanID == item.tblWebSaleLogs.intBesPlanID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                            if (callcenter != null)
                            {
                                item.bolCallcenter = true;
                            }
                        }
                        else if (item.tblWebSaleLogs.intCreditPacketID > 0)
                        {
                            var callcenter = db.tblCallcenterCredits.Where(w => w.intCallcenterID == intCallcenterID && w.intCreditID == item.tblWebSaleLogs.intCreditPacketID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0 && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                            if (callcenter != null)
                            {
                                item.bolCallcenter = true;
                            }
                        }
                        else if (item.tblWebSaleLogs.intPacketID > 0)
                        {
                            var callcenter = db.tblCallcenterProducts.Where(w => w.intCallcenterID == intCallcenterID && w.intProductID == item.tblWebSaleLogs.tblPackets.tblProducts.intProductID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0 && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                            if (callcenter != null)
                            {
                                item.bolCallcenter = true;
                            }
                        }
                    }
                }

                bannerLogs = bannerLogs.Where(w => w.bolCallcenter).ToList();
            }

            if (!string.IsNullOrEmpty(strSaleType))
            {
                if (strSaleType == "bes")
                {
                    bannerLogs = bannerLogs.Where(w => w.tblWebSaleLogs != null && w.tblWebSaleLogs.intBesPlanID > 0).ToList();
                }
                else if (strSaleType == "product")
                {
                    bannerLogs = bannerLogs.Where(w => w.tblWebSaleLogs != null && w.tblWebSaleLogs.intPacketID > 0).ToList();
                }
                else if (strSaleType == "credit")
                {
                    bannerLogs = bannerLogs.Where(w => w.tblWebSaleLogs != null && w.tblWebSaleLogs.intCreditPacketID > 0).ToList();
                }
            }

            foreach (var item in bannerLogs)
            {
                cBannerData bannerList = new cBannerData();

                bannerList.strKey = item.strKey;
                bannerList.strUrl = item.strUrl;

                bool bolCard = false;
                bool bolContact = false;
                byte intQuestionType = 0;
                bool bolApproved = false;

                if (item.intWebSaleLogID > 0)
                {
                    if (item.tblWebSaleLogs.intBesPlanID > 0)
                    {
                        if (item.tblWebSaleLogs.tblWebSaleBesPlanInfo.Count > 0)
                        {
                            bolCard = item.tblWebSaleLogs.tblWebSaleBesPlanInfo.First().strPaymentOption == "card";
                            bolContact = item.tblWebSaleLogs.tblWebSaleBesPlanInfo.First().strPaymentOption == "contact";
                        }
                        bannerList.strTypeName = "Bes";
                    }
                    else if (item.tblWebSaleLogs.intCreditPacketID > 0)
                    {
                        if (item.tblWebSaleLogs.tblWebSaleCreditInfo.Count > 0)
                        {
                            bolCard = item.tblWebSaleLogs.tblWebSaleCreditInfo.First().strPaymentOption == "card";
                            bolContact = item.tblWebSaleLogs.tblWebSaleCreditInfo.First().strPaymentOption == "contact";
                            if (item.tblWebSaleLogs.tblWebSaleCreditInfo.First().intQuestionType.HasValue)
                            {
                                intQuestionType = item.tblWebSaleLogs.tblWebSaleCreditInfo.First().intQuestionType.Value;
                            }
                        }
                        bannerList.strTypeName = "Kredi Teminatı";
                    }
                    else if (item.tblWebSaleLogs.intPacketID > 0)
                    {
                        if (item.tblWebSaleLogs.tblWebSaleInfo.Count > 0)
                        {
                            bolCard = item.tblWebSaleLogs.tblWebSaleInfo.First().strPaymentOption == "card";
                            bolContact = item.tblWebSaleLogs.tblWebSaleInfo.First().strPaymentOption == "contact";
                            if (item.tblWebSaleLogs.tblWebSaleInfo.First().intQuestionType.HasValue)
                            {
                                intQuestionType = item.tblWebSaleLogs.tblWebSaleInfo.First().intQuestionType.Value;
                            }
                        }
                        bannerList.strTypeName = "Hayat";
                    }

                    if (bolCard)
                    {
                        bolApproved = db.tblWebSalePay.Count(c => c.intWebSaleLogID == item.intWebSaleLogID && c.bolApproved) > 0;
                    }
                }

                if (item.intWebSaleLogID > 0)
                {
                    if (item.tblWebSaleLogs.intBesPlanID > 0)
                    {
                        bannerList.strName = item.tblWebSaleLogs.tblBesPlans.strPlanName;
                    }
                    else if (item.tblWebSaleLogs.intCreditPacketID > 0)
                    {
                        bannerList.strName = item.tblWebSaleLogs.tblCreditPackets.tblCredits.strName;
                    }
                    else if (item.tblWebSaleLogs.intPacketID > 0)
                    {
                        bannerList.strName = item.tblWebSaleLogs.tblPackets.tblProducts.strName;
                    }
                }

                bannerList.strReferringSite = item.strReferringSite;

                if (bolApproved)
                {
                    bannerList.strPolicy = item.tblWebSaleLogs.tblWebSalePay.Where(w => w.bolApproved).First().intPolicy.ToString();
                }

                if (item.intWebSaleLogID > 0)
                {
                    if (item.tblWebSaleLogs.intBesPlanID > 0)
                    {
                        if (bolCard)
                        {
                            if (bolApproved)
                            {
                                bannerList.status = Enums.BannerStatus.paysuccess;
                            }
                            else
                            {
                                bannerList.status = Enums.BannerStatus.payunsuccess;
                            }
                        }
                        else if (bolContact)
                        {
                            bannerList.status = Enums.BannerStatus.contact;
                        }
                        else
                        {
                            bannerList.status = Enums.BannerStatus.notstep;
                        }
                    }
                    else if (item.tblWebSaleLogs.intCreditPacketID > 0)
                    {
                        if (bolCard)
                        {
                            if (bolApproved)
                            {
                                bannerList.status = Enums.BannerStatus.paysuccess;
                            }
                            else
                            {
                                bannerList.status = Enums.BannerStatus.payunsuccess;
                            }
                        }
                        else if (intQuestionType == 1)
                        {
                            bannerList.status = Enums.BannerStatus.questioncontact;
                        }
                        else if (bolContact)
                        {
                            bannerList.status = Enums.BannerStatus.contact;
                        }
                        else
                        {
                            bannerList.status = Enums.BannerStatus.notstep;
                        }
                    }
                    else if (item.tblWebSaleLogs.intPacketID > 0)
                    {
                        if (bolCard)
                        {
                            if (bolApproved)
                            {
                                bannerList.status = Enums.BannerStatus.paysuccess;
                            }
                            else
                            {
                                bannerList.status = Enums.BannerStatus.payunsuccess;
                            }
                        }
                        else if (intQuestionType == 1)
                        {
                            bannerList.status = Enums.BannerStatus.questioncontact;
                        }
                        else if (bolContact)
                        {
                            bannerList.status = Enums.BannerStatus.contact;
                        }
                        else
                        {
                            bannerList.status = Enums.BannerStatus.notstep;
                        }
                    }
                    else
                    {
                        bannerList.status = Enums.BannerStatus.sitelogin;
                    }
                }
                else
                {
                    bannerList.status = Enums.BannerStatus.sitelogin;
                }

                if (bolApproved)
                {
                    bannerList.strPrice = item.tblWebSaleLogs.tblWebSalePay.Where(w => w.bolApproved).First().flPrice + " TL";
                }

                bannerList.strDate = item.dtRegisterDate.ToString();

                if (status != 0)
                {
                    if (status == bannerList.status)
                    {
                        _data.Add(bannerList);
                    }
                }
                else
                {
                    _data.Add(bannerList);
                }
            }

            if (!string.IsNullOrEmpty(strSearch))
            {
                _data = _data.Where(w => (string.IsNullOrEmpty(w.strKey) ? false : w.strKey.ToLower().Contains(strSearch.ToLower())) || (string.IsNullOrEmpty(w.strPolicy) ? false : w.strPolicy.ToLower().Contains(strSearch.ToLower()))).ToList();
            }

            return _data;
        }

        public static string StatusName(Enums.BannerStatus bannerStatus)
        {
            switch (bannerStatus)
            {
                case Enums.BannerStatus.paysuccess:
                    return "Ödeme gerçekleşdi";
                case Enums.BannerStatus.payunsuccess:
                    return "Ödeme gerçekleşmedi";
                case Enums.BannerStatus.contact:
                    return "FİBA Beni Arasın";
                case Enums.BannerStatus.questioncontact:
                    return "Emin değilim, FİBA beni arasın";
                case Enums.BannerStatus.notstep:
                    return "Adımlar tamamlanmadı";
                case Enums.BannerStatus.sitelogin:
                    return "Satın Alma Başlatılmadı";
                default:
                    return "";
            }
        }
    }
}