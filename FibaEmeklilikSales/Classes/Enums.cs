﻿namespace FibaEmeklilikSales.Classes
{
    public class Enums
    {
        public enum LogType
        {
            Add = 1,
            Update = 2,
            Delete = 3,
            ProductSelected = 4,
            TCKNInput = 5,
            CustomerPolicyInfo = 6,
            PayView = 7,
            PaySuccess = 8,
            PayUnSuccess = 9,
            InterMediary = 10,
            CustomerInfo = 11,
            CreditInfo = 12,
            PremiumInfo = 13,
            Sales = 14,
            ContactInfo = 15,
            HealtCheck = 16,
            ExcelUpload = 17,
            LinkSend = 18,
            UW = 19,
            ErrorSOR = 20
        }

        public enum UserType : byte
        {
            Admin = 1,
            Normal = 2,
            PowerAdmin = 3,
            SpecialSale = 4,
            ReportUser = 5
        }

        public enum PoliceType : byte
        {
            Policelesme_NKT = 1,
            Policelesme_KRD = 2,
            TeklifPolicelesme = 3
        }

        public enum BannerStatus : byte
        {
            paysuccess = 1,
            payunsuccess = 2,
            contact = 3,
            questioncontact = 4,
            notstep = 5,
            sitelogin = 6
        }
    }
}