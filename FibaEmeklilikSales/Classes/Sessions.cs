﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using FibaEmeklilikSales.Classes.Credits;
using System.Web.Script.Serialization;
using System.Data;
using ModelLibrary;

namespace FibaEmeklilikSales.Classes
{
    public class Sessions
    {
        #region CurrentUser
        public static tblUsers CurrentUser
        {
            get
            {
                try
                {
                    var user = (tblUsers)HttpContext.Current.Session["CurrentUser"];

                    return user == null ? new tblUsers() : user;
                }
                catch
                {
                    return new tblUsers();
                }
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }
        #endregion

        #region LastPage
        public static string LastPage
        {
            get
            {
                if (HttpContext.Current.Session["LastPage"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["LastPage"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["LastPage"] = value;
            }
        }
        #endregion

        #region Message
        public static string Message
        {
            get
            {
                if (HttpContext.Current.Session["Message"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Message"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion

        #region Msg
        public static string Msg
        {
            get
            {
                if (HttpContext.Current.Session["Msg"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Msg"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Msg"] = value;
            }
        }
        #endregion

        #region SaleLog
        public static int SaleLog
        {
            get
            {
                if (HttpContext.Current.Session["SaleLog"] == null)
                {
                    return Cookies.SaleLog;
                }
                else
                {
                    return int.Parse(HttpContext.Current.Session["SaleLog"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["SaleLog"] = value;
            }
        }
        #endregion

        #region CharCrypto
        public static string CharCrypto
        {
            get
            {
                if (HttpContext.Current.Session["CharCrypto"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["CharCrypto"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["CharCrypto"] = value;
            }
        }
        #endregion

        #region Message2
        public static string Message2
        {
            get
            {
                if (HttpContext.Current.Session["Message2"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Message2"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Message2"] = value;
            }
        }
        #endregion

        #region Message3
        public static string Message3
        {
            get
            {
                if (HttpContext.Current.Session["Message3"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Message3"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Message3"] = value;
            }
        }
        #endregion

        #region KrediNo
        public static string KrediNo
        {
            get
            {
                if (HttpContext.Current.Session["KrediNo"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["KrediNo"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["KrediNo"] = value;
            }
        }
        #endregion

        #region SaleLogPoliceler
        public static List<SaleLogPoliceler> SaleLogPoliceler
        {
            get
            {
                if (HttpContext.Current.Session["SaleLogPoliceler"] == null)
                {
                    return null;
                }
                else
                {
                    return (List<SaleLogPoliceler>)HttpContext.Current.Session["SaleLogPoliceler"];
                }
            }
            set
            {
                HttpContext.Current.Session["SaleLogPoliceler"] = value;
            }
        }
        #endregion

        #region ProductTable
        public static List<cProductTable> ProductTable
        {
            get
            {
                if (HttpContext.Current.Session["ProductTable"] == null)
                {
                    return null;
                }
                else
                {
                    return (List<cProductTable>)HttpContext.Current.Session["ProductTable"];
                }
            }
            set
            {
                HttpContext.Current.Session["ProductTable"] = value;
            }
        }
        #endregion

        #region ProductTableDelete
        public static bool ProductTableDelete
        {
            get
            {
                if (HttpContext.Current.Session["ProductTableDelete"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)HttpContext.Current.Session["ProductTableDelete"];
                }
            }
            set
            {
                HttpContext.Current.Session["ProductTableDelete"] = value;
            }
        }
        #endregion

        #region IP
        public static string IP
        {
            get
            {
                if (HttpContext.Current.Session["IP"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["IP"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["IP"] = value;
            }
        }
        #endregion

        #region TCKN
        public static string TCKN
        {
            get
            {
                if (HttpContext.Current.Session["TCKN"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["TCKN"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["TCKN"] = value;
            }
        }
        #endregion

        #region CreditCard
        public static cCreditCard CreditCard
        {
            get
            {
                try
                {
                    return (cCreditCard)HttpContext.Current.Session["CreditCard"];
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["CreditCard"] = value;
            }
        }
        #endregion

        #region BesSaleLog
        public static int BesSaleLog
        {
            get
            {
                if (HttpContext.Current.Session["BesSaleLog"] == null)
                {
                    return Cookies.BesSaleLog;
                }
                else
                {
                    return int.Parse(HttpContext.Current.Session["BesSaleLog"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["BesSaleLog"] = value;
            }
        }
        #endregion

        #region CreditSaleLog
        public static int CreditSaleLog
        {
            get
            {
                if (HttpContext.Current.Session["CreditSaleLog"] == null)
                {
                    return Cookies.CreditSaleLog;
                }
                else
                {
                    return int.Parse(HttpContext.Current.Session["CreditSaleLog"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["CreditSaleLog"] = value;
            }
        }
        #endregion

        #region CreditSessionKey
        public static string CreditSessionKey
        {
            get
            {
                if (HttpContext.Current.Session["CreditSessionKey"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["CreditSessionKey"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["CreditSessionKey"] = value;
            }
        }
        #endregion

        #region CreditSessionKeys
        public static List<cCreditSessionKeys> CreditSessionKeys
        {
            get
            {
                List<cCreditSessionKeys> sessionKeys = new List<cCreditSessionKeys>();
                try
                {
                    object objValue = HttpContext.Current.Session["CreditSessionKeys"];
                    if (objValue == null)
                        return null;

                    JavaScriptSerializer java = new JavaScriptSerializer();
                    sessionKeys = java.Deserialize<List<cCreditSessionKeys>>(objValue.ToString());
                }
                catch { }

                return sessionKeys;
            }
            set
            {
                JavaScriptSerializer java = new JavaScriptSerializer();
                HttpContext.Current.Session["CreditSessionKeys"] = java.Serialize(value);
            }
        }
        #endregion 

        #region PolicyList
        public static FibaEmeklilikSales.FIBAWS.FibawspolicesorguoutrecAPol PolicyList
        {
            get
            {
                if (HttpContext.Current.Session["PolicyList"] == null)
                {
                    return null;
                }
                else
                {
                    return (FibaEmeklilikSales.FIBAWS.FibawspolicesorguoutrecAPol)HttpContext.Current.Session["PolicyList"];
                }
            }
            set
            {
                HttpContext.Current.Session["PolicyList"] = value;
            }
        }
        #endregion

        #region ReportData
        public static DataTable ReportData
        {
            get
            {
                try
                {
                    return (DataTable)HttpContext.Current.Session["ReportData"]; 
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["ReportData"] = value;
            }
        }
        #endregion
    }
}