﻿using ModelLibrary;
using System.Collections.Generic;

namespace FibaEmeklilikSales.Classes
{
    public class cNotification
    {
        public List<tblNotifications> notifications { get; set; }
        public tblNotifications notification { get; set; }
        public List<tblNotificationGroups> notificationGroups { get; set; }
        public int intCount { get; set; }
    }

    public class cNotificationGroup
    {
        public List<tblNotificationGroups> notificationGroups { get; set; }
        public tblNotificationGroups notificationGroup { get; set; }
        public List<tblCallcenters> callcenters { get; set; } 
        public int intCount { get; set; }
    } 
}