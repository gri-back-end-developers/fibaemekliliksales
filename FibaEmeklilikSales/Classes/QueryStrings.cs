#region Directives
using System;
using System.Collections.Generic;
using System.Web;
#endregion

namespace FibaEmeklilikSales.Classes
{
    public class QueryStrings
    {
        #region trans
        public static string trans
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["trans"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region page
        public static int page
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["page"];
                    if (value == null)
                    {
                        value = 0;
                    }

                    return int.Parse(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region date1
        public static string date1
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["date1"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region date2
        public static string date2
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["date2"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region search
        public static string search
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["txtsearch"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString().Trim();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region search2
        public static string search2
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["txtsearch2"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString().Trim();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region ID
        public static int ID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["i"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region ProductID
        public static int ProductID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["pi"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region CallcenterID
        public static int CallcenterID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["ci"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region SalePage
        public static byte SalePage
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["sp"];
                    if (value == null)
                    {
                        value = 0;
                    }

                    return byte.Parse(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region police
        public static string police
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["police"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region PacketIDs
        public static List<int> PacketIDs
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["packets"];
                    if (value == null)
                    {
                        return new List<int>();
                    }
                    else
                    {
                        List<int> list = new List<int>();
                        var strPackets = value.ToString().Split(',');
                        foreach (var item in strPackets)
                        {
                            list.Add(Utilities.NullFixInt(cCrypto.DecryptText(item)));
                        }

                        return list;
                    }
                }
                catch
                {
                    return new List<int>();
                }
            }
        }
        #endregion

        #region PacketID
        public static int PacketID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["pa"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region BesPlanID
        public static int BesPlanID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["bp"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region CreditID
        public static int CreditID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["cr"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region BannerID
        public static int BannerID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["banner"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixInt(value.ToString());
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region CreditType
        public static byte CreditType
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["ctpye"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixByte(value.ToString());
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region Type
        public static string Type
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["type"];
                    if (value == null)
                    {
                        return "";
                    }
                    else
                    {
                        return value.ToString();
                    }
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion 

        #region SearchType
        public static string SearchType
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["searchtype"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region PolID
        public static decimal PolID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["polid"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixDecimal(value.ToString());
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region Pid
        public static decimal Pid
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["pid"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixDecimal(value.ToString());
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region BankCityID
        public static short BankCityID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["bankCity"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixShort(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region BankID
        public static short BankID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["bank"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixShort(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region NotificationGroupID
        public static short NotificationGroupID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["ngid"];
                    if (value == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return Utilities.NullFixShort(cCrypto.DecryptText(value.ToString()));
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region DifferentPaid
        public static string DifferentPaid
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["diffpay"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region PersonalProduct
        public static bool PersonalProduct
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["personal"];
                    if (value == null)
                    {
                        value = false;
                    }
                    return value.ToString() == "1";
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion
    }
}