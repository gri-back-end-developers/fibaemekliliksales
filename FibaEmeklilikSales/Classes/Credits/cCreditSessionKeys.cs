﻿namespace FibaEmeklilikSales.Classes.Credits
{
    public class cCreditSessionKeys
    {
        public string strCreditID { get; set; }
        public int intCreditPacketID { get; set; }
    }
}