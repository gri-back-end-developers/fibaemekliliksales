﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes.Credits
{
    public class cKpsData
    {
        public cBireySorgu birey { get; set; }
        public cAcikAdres adres { get; set; }
        public List<tblMapping> ulkeler { get; set; }
        public List<tblMapping> iller { get; set; }
        public List<tblMapping> ilceler { get; set; }
        public string path { get; set; }
        public string script { get; set; }
        public string redirect { get; set; }

        public cKpsData()
        {
            birey = null;
            adres = null;
            ulkeler = null;
            iller = null;
            path = "";
            script = "";
            redirect = "";
        }

        public void Set(int intLogID, string strTCKN, int intCreditID)
        {
            FibaEmeklilikSales.AileBireySorgu.KpsBireySorguServiceClient aileService = new FibaEmeklilikSales.AileBireySorgu.KpsBireySorguServiceClient();
            DBEntities db = new DBEntities();

            this.birey = new cBireySorgu();
            this.adres = new cAcikAdres();

            try
            {
                string strBirthRegionCode = "";
                string strBirthDistrictCode = "";
                string strDistrictCode = "";
                string strRegionCode = "";

                this.ulkeler = db.tblMapping.Where(w => w.strType == "UYR" && !string.IsNullOrEmpty(w.strName.Trim())).OrderBy(o => o.strName).ToList();
                this.iller = db.tblMapping.Where(w => w.strType == "ILK" && !string.IsNullOrEmpty(w.strName.Trim())).OrderBy(o => o.strName).ToList();
                this.ilceler = db.tblMapping.Where(w => w.strType == "ILC" && !string.IsNullOrEmpty(w.strName.Trim())).OrderBy(o => o.strName).ToList();

                var kpsBireySorgu = new FibaEmeklilikSales.AileBireySorgu.kisiBilgisiType();
                try
                {
                    kpsBireySorgu = aileService.kpsBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTCKN),
                        kimlikNoSpecified = true
                    });

                    var birthRegionCode = iller.Where(w => w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                    if (birthRegionCode != null)
                    {
                        strBirthRegionCode = birthRegionCode.strCode.Trim();
                    }

                    var birthDistrict = ilceler.Where(w => w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                    if (birthDistrict != null)
                    {
                        strBirthDistrictCode = birthDistrict.strCode.Trim();
                    }

                    if (strBirthRegionCode == "" && strBirthDistrictCode != "")
                    {
                        strBirthRegionCode = strBirthDistrictCode.Substring(0, 3);
                    }

                    if (strBirthRegionCode == "")
                    {
                        strBirthRegionCode = "99";
                    }

                    if (strBirthRegionCode != "" && strBirthDistrictCode == "")
                    {
                        strBirthDistrictCode = "99900";
                    }
                }
                catch { }

                if (kpsBireySorgu.tckimlikNo == 0)
                {
                    var kpsYabanciSorgu = new FibaEmeklilikSales.AileBireySorgu.yabanciKisiBilgisiType();
                    try
                    {
                        kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });
                    }
                    catch { }

                    if (kpsYabanciSorgu.tckimlikNo > 0)
                    {
                        var uyr = this.ulkeler.Where(w => w.strCode == kpsYabanciSorgu.uyrukKod.ToString()).FirstOrDefault();

                        this.birey.adi = kpsYabanciSorgu.adi;
                        this.birey.uyrukAd = uyr != null ? uyr.strCountryCode : kpsYabanciSorgu.uyrukAd;
                        this.birey.babaAdi = kpsYabanciSorgu.babaAdi;
                        this.birey.cinsiyeti = kpsYabanciSorgu.cinsiyeti;
                        this.birey.dogumTarihi = kpsYabanciSorgu.dogumTarihi.ToShortDateString();
                        this.birey.soyadi = kpsYabanciSorgu.soyadi;
                        this.birey.olum = kpsBireySorgu.olumTarihi == null || kpsBireySorgu.olumTarihi.Year > 1000;

                        strBirthRegionCode = "99";
                        strBirthDistrictCode = "99900";
                        strDistrictCode = "99900";
                        strRegionCode = "99";
                    }
                    else
                    {
                        Sessions.Message2 = "<script>$(function () {  $.cookie('creditsaleinfo', '0', { path: '/' }); Msg(2, 'Kimlik No Hatalı!'); });</script>";

                        this.path = "/CreditSale/New?cr=" + HttpContext.Current.Request.QueryString["cr"];
                    }
                }
                else
                {
                    this.birey.adi = kpsBireySorgu.adi;
                    this.birey.anneAdi = kpsBireySorgu.anneAdi;
                    this.birey.babaAdi = kpsBireySorgu.babaAdi;
                    this.birey.cinsiyeti = kpsBireySorgu.cinsiyeti;
                    this.birey.dogumTarihi = kpsBireySorgu.dogumTarihi.ToShortDateString();
                    this.birey.dogumYeri = kpsBireySorgu.dogumYeri;
                    this.birey.medeniHali = kpsBireySorgu.medeniHali;
                    this.birey.soyadi = kpsBireySorgu.soyadi;
                    this.birey.uyrukAd = "TR";
                    this.birey.olum = kpsBireySorgu.olumTarihi == null || kpsBireySorgu.olumTarihi.Year > 1000;
                }

                if (birey.olum)
                {
                    this.script = "<script>Msg(2, 'Kimlik No Hatalı!');</script>";
                }

                if (birey.uyrukAd == "TR")
                {
                    var kpsTCKisiAcikAdresSorgu = aileService.kpsTCKisiAcikAdresSorgu(new FibaEmeklilikSales.AileBireySorgu.kisiAdresSorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTCKN),
                        kimlikNoSpecified = true
                    });

                    if (kpsTCKisiAcikAdresSorgu.kimlikNo > 0)
                    {
                        this.adres.acikAdres = kpsTCKisiAcikAdresSorgu.acikAdres;
                        this.adres.ilceKodu = kpsTCKisiAcikAdresSorgu.ilceKodu;
                        this.adres.ilKodu = kpsTCKisiAcikAdresSorgu.ilKodu;

                        strDistrictCode = kpsTCKisiAcikAdresSorgu.ilceKodu;
                        strRegionCode = kpsTCKisiAcikAdresSorgu.ilKodu;
                    }
                }
                else
                {
                    var kpsYabanciAcikAdresSorgu = aileService.kpsYabanciKisiAcikAdresSorgu(new FibaEmeklilikSales.AileBireySorgu.kisiAdresSorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTCKN),
                        kimlikNoSpecified = true
                    });

                    if (kpsYabanciAcikAdresSorgu.kimlikNo > 0)
                    {
                        this.adres.acikAdres = kpsYabanciAcikAdresSorgu.acikAdres;
                        this.adres.ilceKodu = "99900";
                        this.adres.ilKodu = "99";
                    }
                }

                var saleInfo = db.tblCreditSaleLogInfo.Where(w => w.intLogID == intLogID).FirstOrDefault();
                if (saleInfo != null)
                {
                    saleInfo.strBirthRegionCode = strBirthRegionCode;
                    saleInfo.strBirthDistrictCode = strBirthDistrictCode;
                    saleInfo.strDistrictCode = strDistrictCode;
                    saleInfo.strRegionCode = strRegionCode;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logs.CreditSaleLogDetail(intLogID, Enums.LogType.PayView, Logs.Error(ex));

                Sessions.Message2 = "<script>$(function () {  $.cookie('creditsaleinfo', '0', { path: '/' }); Msg(2, 'Kimlik No Hatalı!'); });</script>";

                this.path = "/CreditSale/New?cr=" + HttpContext.Current.Request.QueryString["cr"];
                this.redirect = "1";
            }
            finally
            {
                aileService.Close();
                db.Dispose();
            }
        }
    }
}