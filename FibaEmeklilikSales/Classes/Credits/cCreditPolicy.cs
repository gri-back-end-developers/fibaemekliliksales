﻿namespace FibaEmeklilikSales.Classes.Credits
{
    public class cCreditPolicy
    {
        public string strPolicyID { get; set; }
        public decimal flFirstPremium { get; set; }
        public decimal flTotalPremium { get; set; }
    }
}