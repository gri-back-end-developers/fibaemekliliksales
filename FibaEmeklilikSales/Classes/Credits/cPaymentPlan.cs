﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes.Credits
{
    public class cPaymentPlan
    {
        PaymentPlanRequest _paymentPlan;

        public cPaymentPlan(PaymentPlanRequest paymentPlan)
        {
            _paymentPlan = paymentPlan;
        }

        public List<PaymentPlanResponse> ToList()
        {
            List<PaymentPlanResponse> _response = new List<PaymentPlanResponse>();

            CalculatePaymentValues calculateValues = SetCalculateValues();

            for (int i = 1; i < _paymentPlan.period + 1; i++)
            {
                PaymentPlanResponse paymentPlanResponse = new PaymentPlanResponse();

                #region ReCalculate
                calculateValues.interest = CalculateService.calculateAmountInterest(_paymentPlan.remainingAmount, _paymentPlan.interest, 100);
                calculateValues.bsmv = CalculateService.calculateBSMV(calculateValues.interest, _paymentPlan.BSMV, 100);
                calculateValues.kkdf = CalculateService.calculateKKDF(calculateValues.interest, _paymentPlan.KKDF, 100);
                #endregion

                double totalCurrency = CalculateService.calculateTotalCurrency(calculateValues.installAmount, calculateValues.interest, calculateValues.kkdf, calculateValues.bsmv);
                paymentPlanResponse.totalCurrency = totalCurrency.ToString("0.00");

                _paymentPlan.remainingAmount =  CalculateService.calculateCreditPaymentAmount(_paymentPlan.remainingAmount, totalCurrency);

                paymentPlanResponse.installAmount =  calculateValues.installAmount.ToString("0.00");
                paymentPlanResponse.remainingAmount = _paymentPlan.remainingAmount.ToString("0.00");
                paymentPlanResponse.bsmv = calculateValues.bsmv.ToString("0.00");
                paymentPlanResponse.kkdf = calculateValues.kkdf.ToString("0.00");
                paymentPlanResponse.interest = calculateValues.interest.ToString("0.00");
                paymentPlanResponse.installment = i;

                _response.Add(paymentPlanResponse);
            }

            return _response; 
        }

        private CalculatePaymentValues SetCalculateValues()
        {
            int percentValue = 100;

            CalculatePaymentValues calculateValues = new CalculatePaymentValues();
            calculateValues.kkdf = CalculateService.calculateKKDF(_paymentPlan.interest, _paymentPlan.KKDF, percentValue);
            calculateValues.bsmv = CalculateService.calculateBSMV(_paymentPlan.interest, _paymentPlan.BSMV, percentValue);
            calculateValues.interest = CalculateService.calculateInterestAverage(calculateValues.kkdf, calculateValues.bsmv, _paymentPlan.interest, percentValue);
            calculateValues.installAmount = CalculateService.calculatePMT(calculateValues.interest, _paymentPlan.period, _paymentPlan.remainingAmount);

            return calculateValues;
        }
    }

    public class CalculateService
    {
        public static Double calculateBSMV(double interest, double bsmv, int percentValue)
        {
            Double result = interest * bsmv / percentValue;

            return result;
        }

        public static Double calculateKKDF(double interest, double kkdf, int percentValue)
        {
            Double result = interest * kkdf / percentValue;

            return result;
        }

        public static Double calculateInterestAverage(double kkdf, double bsmv, double interest, double percentValue)
        {
            Double result = (kkdf + bsmv + interest) / percentValue;

            return result;
        }

        public static Double calculatePMT(double rate, int period, double amount)
        {
            return Financial.Pmt(rate, period, -amount, 0, 0);
        }

        public static Double calculateTotalCurrency(double creditAmout, double interest, double kkdf, double bsmv)
        {
            Double result = creditAmout - (interest + kkdf + bsmv);

            return result;
        }

        public static Double calculateCreditPaymentAmount(double creditAmount, double mainCurrency)
        {
            Double result = creditAmount - mainCurrency;

            return result;
        }

        public static Double calculateAmountInterest(double creditAmount, double interest, double percentValue)
        {
            Double result = creditAmount / percentValue * interest;

            return result;
        }
         
        public static string RoundUp(double deger, int basamak)
        {
            string[] value = deger.ToString().Split(',');
            string calculatedValue;
            calculatedValue = Convert.ToDouble(value[0]) + ".";

            for (int i = 0; i < value[1].Length; i++)
            {
                if (i < 3)
                {
                    calculatedValue += value[1][i];
                }
            }

            return calculatedValue;
        }

        public static double RoundUpNext(double deger, int basamak)
        {
            double sonuc;
            double asd = Math.Pow(10, basamak);

            if (((deger - Convert.ToInt32(deger))) > 1 / asd)
            {
                sonuc = Convert.ToInt32(deger) + (Math.Floor((deger - Convert.ToInt32(deger)) * asd) + 1) / asd;
            }
            else
            {
                sonuc = deger;
            } 

            return sonuc;
        } 
    }

    public class CalculatePaymentValues
    {
        public double kkdf { get; set; }
        public double bsmv { get; set; }
        public double interest { get; set; }
        public double installAmount { get; set; }
    }

    public class PaymentPlanRequest
    { 
        public double interest { get; set; }
        public double remainingAmount { get; set; }
        public double BSMV { get; set; }
        public double KKDF { get; set; }
        public int period { get; set; }
    }

    public class PaymentPlanResponse
    {
        public int installment { get; set; }
        public string installAmount { get; set; }
        public string bsmv { get; set; }
        public string kkdf { get; set; }
        public string remainingAmount { get; set; }
        public string interest { get; set; }
        public string totalCurrency { get; set; }
    }
}