﻿using System.Collections.Generic;

namespace FibaEmeklilikSales.Classes.Credits
{
    public class cCreditPackets
    {
        public int id { get; set; }
        public string name { get; set; }
        public string strComment { get; set; }
        public List<cCreditAssurances> assurances { get; set; }
        public cCreditPolicy policy { get; set; }
    }
}