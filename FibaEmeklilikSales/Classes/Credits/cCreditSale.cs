﻿using ModelLibrary;
using System;
using System.Collections;
using System.Collections.Generic; 

namespace FibaEmeklilikSales.Classes.Credits
{
    public class cCreditSale
    {
        public tblCreditSaleLogs saleLog { get; set; }
        public tblCreditTypes creditTypes { get; set; }
        public string strTCKN { get; set; }
        public string flKKDF { get; set; }
        public string flBSMV { get; set; }
        public int intMaxTime { get; set; }
        public decimal intMaxPrice { get; set; }
        public List<PaymentPlanResponse> paymentPlanList { get; set; }
        public List<cCreditPackets> packets { get; set; }
        public bool bolInfoForm { get; set; }
        public List<tblBanks> banks { get; set; }
        public List<tblPaymentMethods> paymentMethods { get; set; } 
    } 
}