﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace FibaEmeklilikSales.Classes
{
    public class cReport
    {
        public List<tblReports> reports { get; set; }
        public DataTable data { get; set; }
        public List<cReportColumns> columns { get; set; }
        public int intCount { get; set; }
        public bool bolExcel { get; set; }

        public static void CopyDB()
        {
            using (DBEntities db = new DBEntities())
            {
                var files = Directory.GetFiles(Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"]).ToList();
                if (files.Count > 0)
                {
                    foreach (var item in files)
                    {
                        db.tblReports.Add(new tblReports
                        {
                            bolIsActive = true,
                            dtRegisterDate = System.IO.File.GetLastWriteTime(item),
                            strFile = Path.GetFileName(item),
                            strName = Path.GetFileName(item).Replace(Path.GetExtension(item), ""),
                            intCallcenterType = 0
                        });
                        db.SaveChanges();
                    }
                }
            }
        }
    }

    public class cReportColumns
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}