﻿using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Classes
{
    public class JsonSerializer
    {
        public static string Serialize<T>(T t)
        {
            JavaScriptSerializer javaSer = new JavaScriptSerializer();
            return javaSer.Serialize(t);
        }

        public static T Deserialize<T>(string jsonString)
        {
            JavaScriptSerializer javaSer = new JavaScriptSerializer();
            return javaSer.Deserialize<T>(jsonString);
        }
    }
}