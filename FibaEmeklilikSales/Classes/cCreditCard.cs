﻿namespace FibaEmeklilikSales.Classes
{
    public class cCreditCard
    {
        public string month { get; set; }
        public string year { get; set; }
        public string card { get; set; }
        public decimal cvv { get; set; }
        public string type { get; set; }
    }
}