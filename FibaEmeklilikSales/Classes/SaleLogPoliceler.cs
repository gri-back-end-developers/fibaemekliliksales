﻿namespace FibaEmeklilikSales.Classes
{
    public class SaleLogPoliceler
    {
        public int intPacketID { get; set; }
        public decimal? PoliceNo { get; set; }
        public decimal? ToplamPirim { get; set; }
        public decimal? IlkPrim { get; set; }
        public string TeklifDurum { get; set; }
        public decimal? IlkPrimKomisyon { get; set; }
        public decimal? ToplamKomisyon { get; set; }
        public string KrediNo { get; set; }
        public byte intPoliceTypeID { get; set; }
        public bool bolCancel { get; set; }
        public string strMessage { get; set; }
        public bool bolPay { get; set; }
        public string strPayMessage { get; set; }
        public string strService { get; set; }
        public bool bolNew { get; set; }
        public string strYenileme { get; set; }
        public int intCrediTime { get; set; }
        public decimal? bsmv { get; set; }
    }
}