﻿namespace FibaEmeklilikSales.Classes
{
    public class cBireySorgu
    {
        public string adi { get; set; }
        public string soyadi { get; set; }
        public string anneAdi { get; set; }
        public string babaAdi { get; set; }
        public string cinsiyeti { get; set; }
        public string dogumYeri { get; set; }
        public string dogumTarihi { get; set; }
        public string medeniHali { get; set; }
        public string uyrukAd { get; set; }
        public bool olum { get; set; }
    }
}