﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using ModelLibrary; 

namespace FibaEmeklilikSales.Controllers
{
    public class SharedController : Controller
    {
        [UserFilter]
        public ActionResult _Layout()
        {
            try
            {
                DBEntities db = new DBEntities();
                
                bool bolOpen = Sessions.CurrentUser.bolPssChange;
                if (!bolOpen)
                {
                    var tblUserLogs = db.tblUserPssLogs.Where(w => w.intUserID == Sessions.CurrentUser.intUserID).OrderBy(o => o.dtLoginDate).ToList();

                    if (tblUserLogs.Count > 0)
                    {
                        TimeSpan time = DateTime.Now.Subtract(tblUserLogs[tblUserLogs.Count - 1].dtLoginDate);
                        if (time.Days > 30)
                        {
                            bolOpen = true;
                        }
                    }
                }

                if (bolOpen)
                {
                    ViewBag.PssChange = new HtmlString("<script>$('#modalPssChange').modal('show');</script>");
                }
            }
            catch { }
            return View();
        }

        public ActionResult Close()
        {
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();

            return Redirect("/Default/Login");
        }
    }
}
