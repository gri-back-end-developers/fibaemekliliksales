﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BankCityController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intBankCityID = QueryStrings.ID;
                    if (intBankCityID > 0)
                    {
                        var bankcity = db.tblBankCity.Where(w => w.intBankCityID == intBankCityID).FirstOrDefault();
                        if (bankcity != null)
                        {
                            db.Database.ExecuteSqlCommand("DELETE FROM tblBankCities WHERE intBankCityID = {0}", intBankCityID);
                            db.SaveChanges();
                             
                            bankcity.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intBankCityID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şehir silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Şehir bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Şehir bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankCity");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strCity = formValues["strCity"];

                    if (db.tblBankCity.Count(c => c.strCity == strCity) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı şehir adı sistemde mevcut!");
                        return RedirectToAction("New", "BankCity");
                    }

                    tblBankCity bankcity = new tblBankCity();
                    bankcity.strCity = strCity;
                    bankcity.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bankcity.dtRegisterDate = DateTime.Now;

                    db.tblBankCity.Add(bankcity);
                    db.SaveChanges();

                    SetCities(bankcity.intBankCityID);

                    Logs.Log(Enums.LogType.Add, bankcity.intBankCityID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şehir eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankCity");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intBankCityID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    string strCity = formValues["strCity"];

                    if (db.tblBankCity.Count(c => c.intBankCityID != intBankCityID && c.strCity == strCity) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı şehir adı sistemde mevcut!");
                        return RedirectToAction("Update", "BankCity", new { i = Request.QueryString["i"] });
                    }

                    var bankcity = db.tblBankCity.Where(w => w.intBankCityID == intBankCityID).FirstOrDefault();
                    bankcity.strCity = strCity;
                    bankcity.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bankcity.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    SetCities(bankcity.intBankCityID);

                    Logs.Log(Enums.LogType.Update, intBankCityID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şehir güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankCity");
        }
        #endregion

        #region SetCities
        private void SetCities(short intBankCityID)
        {
            db.Database.ExecuteSqlCommand("DELETE FROM tblBankCities WHERE intBankCityID = {0}", intBankCityID);
            db.SaveChanges();

            var banks = string.IsNullOrEmpty(Request.Form["hdBanks"]) ? new List<string>() : Request.Form["hdBanks"].Split(',').ToList();
            foreach (var item in banks)
            {
                tblBankCities bankCities = new tblBankCities();
                bankCities.intBankCityID = intBankCityID;
                bankCities.intBankID = Utilities.NullFixShort(cCrypto.DecryptText(item));

                db.tblBankCities.Add(bankCities);
            }
            db.SaveChanges();
        }
        #endregion
    }
}
