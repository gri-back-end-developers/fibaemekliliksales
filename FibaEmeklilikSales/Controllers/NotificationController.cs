﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using System.Configuration;
using System.IO;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class NotificationController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            cNotification notification = new cNotification();

            using (db = new DBEntities())
            {
                List<tblNotifications> notifications = new List<tblNotifications>();

                if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                {
                    notifications.AddRange(db.tblNotifications.Where(w => !w.bolIsDelete).OrderByDescending(o => o.dtStartDate).ToList());
                }

                string strNotificationType = Request.QueryString["ntype"];
                if (!string.IsNullOrEmpty(strNotificationType))
                {
                    notifications = notifications.Where(w => w.strNotificationType == strNotificationType).ToList();
                }

                short intNotificationGroupID = QueryStrings.NotificationGroupID;
                if (intNotificationGroupID > 0)
                {
                    notifications = notifications.Where(w => w.intNotificationGroupID == intNotificationGroupID).ToList();
                }

                string startdate = Request.QueryString["startdate"];
                if (!string.IsNullOrEmpty(startdate))
                {
                    DateTime dtStartDate = Utilities.NullFixDate(startdate);
                    notifications = notifications.Where(w => w.dtStartDate >= dtStartDate).ToList();
                }

                string finishdate = Request.QueryString["finishdate"];
                if (!string.IsNullOrEmpty(finishdate))
                {
                    DateTime dtFinishDate = Utilities.NullFixDate(startdate);
                    notifications = notifications.Where(w => w.dtFinishDate <= dtFinishDate).ToList();
                }

                string strSearch = QueryStrings.search;
                if (!string.IsNullOrEmpty(strSearch))
                {
                    notifications = notifications.Where(w => w.strTitle.ToLower().Contains(strSearch.ToLower())).ToList();
                }

                int intPageSize = 20;
                notification.intCount = (notifications.Count / intPageSize) + (notifications.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                notifications = notifications.Skip(intPage * intPageSize).Take(intPageSize).ToList();

                notification.notifications = notifications;
                notification.notificationGroups = db.tblNotificationGroups.Where(w => !w.bolIsDelete).OrderBy(o => o.strGroupName).ToList();
            }

            return View(notification);
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    int intNotificationID = QueryStrings.ID;
                    if (intNotificationID > 0)
                    {
                        using (db = new DBEntities())
                        {
                            var notification = db.tblNotifications.Where(w => w.intNotificationID == intNotificationID).FirstOrDefault();
                            if (notification != null)
                            {
                                notification.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intNotificationID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Duyuru silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Duyuru bulunamadı!");
                            }
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Duyuru bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Notification");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New(cNotification notification = null)
        {
            if (notification.notification == null)
            {
                notification.notification = new tblNotifications();

                using (db = new DBEntities())
                {
                    notification.notificationGroups = db.tblNotificationGroups.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strGroupName).ToList();
                }
            }

            return View(notification);
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(HttpPostedFileBase file = null)
        {
            try
            {
                var formValues = Request.Form;
                ViewBag.strNotificationType = formValues["strNotificationType"];
                ViewBag.strTitle = formValues["strTitle"];
                ViewBag.dtStartDate = formValues["dtStartDate"];
                ViewBag.dtFinishDate = formValues["dtFinishDate"];
                ViewBag.intNotificationGroupID = formValues["intNotificationGroupID"];
                ViewBag.strSpot = formValues["strSpot"];
                ViewBag.strText = formValues["strText"];
                ViewBag.bolIsActive = formValues["bolIsActive"];
                ViewBag.bolAutoPopup = formValues["bolAutoPopup"];
                ViewBag.intOrder = formValues["intOrder"];

                string strFile = "";
                if (file != null)
                {
                    strFile = SaveFile(file, "NotificationFile", ConfigurationManager.AppSettings["ImageExt"] + "," + ConfigurationManager.AppSettings["FileExt"]);
                    if (string.IsNullOrEmpty(strFile))
                    {
                        if (!string.IsNullOrEmpty(formValues["hd_file_Delete"]))
                            strFile = "";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(formValues["hd_file_Delete"]))
                        strFile = "";
                }
                ViewBag.file = strFile;

                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (!Validate(strFile))
                    {
                        return New(new cNotification());
                    }

                    using (db = new DBEntities())
                    {
                        string strTitle = formValues["strTitle"];

                        if (db.tblNotifications.Count(c => c.strTitle == strTitle && !c.bolIsDelete) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı duyuru başlığı sistemde mevcut!");
                            return New(new cNotification());
                        }

                        bool bolAutoPopup = formValues["bolAutoPopup"] == "on" || formValues["bolAutoPopup"] == "checked" || formValues["bolAutoPopup"] == "true";
                        DateTime dtFinishDate = Utilities.NullFixDate(formValues["dtFinishDate"]);
                        DateTime dtStartDate = Utilities.NullFixDate(formValues["dtStartDate"]);

                        if (bolAutoPopup)
                        {
                            if (db.tblNotifications.Count(c => c.dtStartDate <= dtStartDate && c.dtFinishDate >= dtFinishDate && c.bolAutoPopup && c.bolIsActive && !c.bolIsDelete) > 0)
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Seçtiğiniz tarih aralığında Otomatik Popup ayarlı duyuru sistemde mevcut!");
                                return New(new cNotification());
                            }
                        }

                        tblNotifications notification = new tblNotifications()
                        {
                            strTitle = strTitle,
                            bolIsDelete = false,
                            dtFinishDate = dtFinishDate,
                            dtStartDate = dtStartDate,
                            intNotificationGroupID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intNotificationGroupID"])),
                            intOrder = Utilities.NullFixShort(formValues["intOrder"]),
                            strFile = strFile,
                            strNotificationType = formValues["strNotificationType"],
                            strSpot = formValues["strSpot"],
                            strText = formValues["strText"],
                            bolAutoPopup = bolAutoPopup,
                            bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true",
                            dtRegisterDate = DateTime.Now
                        };

                        db.tblNotifications.Add(notification);
                        db.SaveChanges();

                        Logs.Log(Enums.LogType.Add, notification.intNotificationID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Duyuru eklendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Notification");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            cNotification notification = new cNotification();

            using (db = new DBEntities())
            {
                var _data = new tblNotifications();
                int intNotificationID = QueryStrings.ID;

                if (PageControl.DomainType == "update" && intNotificationID > 0)
                {
                    _data = db.tblNotifications.Where(w => w.intNotificationID == intNotificationID && !w.bolIsDelete).FirstOrDefault();

                    if (_data == null)
                    {
                        _data = new tblNotifications();
                    }
                }

                notification.notification = _data;
                notification.notificationGroups = db.tblNotificationGroups.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strGroupName).ToList();
            }

            return View("New", notification);
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(HttpPostedFileBase file = null)
        {
            try
            {
                var formValues = Request.Form;
                ViewBag.strNotificationType = formValues["strNotificationType"];
                ViewBag.strTitle = formValues["strTitle"];
                ViewBag.dtStartDate = formValues["dtStartDate"];
                ViewBag.dtFinishDate = formValues["dtFinishDate"];
                ViewBag.intNotificationGroupID = formValues["intNotificationGroupID"];
                ViewBag.strSpot = formValues["strSpot"];
                ViewBag.strText = formValues["strText"];
                ViewBag.bolIsActive = formValues["bolIsActive"];
                ViewBag.bolAutoPopup = formValues["bolAutoPopup"];
                ViewBag.intOrder = formValues["intOrder"];

                string strFile = formValues["hd_file"];
                if (file != null)
                {
                    strFile = SaveFile(file, "NotificationFile", ConfigurationManager.AppSettings["ImageExt"] + "," + ConfigurationManager.AppSettings["FileExt"]);
                    if (string.IsNullOrEmpty(strFile))
                    {
                        if (!string.IsNullOrEmpty(formValues["hd_file_Delete"]))
                            strFile = "";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(formValues["hd_file_Delete"]))
                        strFile = "";
                }
                ViewBag.file = strFile;

                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (!Validate(strFile))
                    {
                        return Update();
                    }

                    using (db = new DBEntities())
                    {
                        int intNotificationID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                        string strTitle = formValues["strTitle"];

                        if (db.tblNotifications.Count(c => c.intNotificationID != intNotificationID && c.strTitle == strTitle && !c.bolIsDelete) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı duyuru başlığı sistemde mevcut!");
                            return Update();
                        }

                        bool bolAutoPopup = formValues["bolAutoPopup"] == "on" || formValues["bolAutoPopup"] == "checked" || formValues["bolAutoPopup"] == "true";
                        DateTime dtFinishDate = Utilities.NullFixDate(formValues["dtFinishDate"]);
                        DateTime dtStartDate = Utilities.NullFixDate(formValues["dtStartDate"]);

                        if (bolAutoPopup)
                        {
                            if (db.tblNotifications.Count(c => c.intNotificationID != intNotificationID && c.dtStartDate <= dtStartDate && c.dtFinishDate >= dtFinishDate && c.bolAutoPopup && c.bolIsActive && !c.bolIsDelete) > 0)
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Seçtiğiniz tarih aralığında Otomatik Popup ayarlı duyuru sistemde mevcut!");
                                return Update();
                            }
                        }

                        var notification = db.tblNotifications.Where(w => w.intNotificationID == intNotificationID).First();
                        notification.strTitle = strTitle;
                        notification.dtFinishDate = dtFinishDate;
                        notification.dtStartDate = dtStartDate;
                        notification.intNotificationGroupID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intNotificationGroupID"]));
                        notification.intOrder = Utilities.NullFixShort(formValues["intOrder"]);
                        notification.strNotificationType = formValues["strNotificationType"];
                        notification.strSpot = formValues["strSpot"];
                        notification.strText = formValues["strText"];
                        notification.bolAutoPopup = bolAutoPopup;
                        notification.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                        notification.dtUpdateDate = DateTime.Now;
                        notification.strFile = strFile;

                        db.SaveChanges();

                        Logs.Log(Enums.LogType.Update, intNotificationID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Duyuru güncellendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Notification");
        }
        #endregion

        #region SaveFile
        private string SaveFile(HttpPostedFileBase file, string strType, string strExtList)
        {
            bool bolEx = false;
            var ExtList = strExtList.Split(',').ToList();
            foreach (var item in ExtList)
            {
                if (Path.GetExtension(file.FileName).ToLower() == "." + item)
                {
                    bolEx = true;
                    break;
                }
            }

            if (bolEx)
            {
                string strImage = Utilities.RandomString(20) + Path.GetExtension(file.FileName);
                file.SaveAs(Utilities.AppPathServer + ConfigurationManager.AppSettings[strType] + strImage);

                return strImage;
            }

            return "";
        }
        #endregion

        #region Validate
        bool Validate(string strFile = null)
        {
            var formValues = Request.Form;

            if (string.IsNullOrEmpty(formValues["dtStartDate"]))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlangıç tarihi seçiniz!");
                return false;
            }

            if (string.IsNullOrEmpty(formValues["dtFinishDate"]))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Bitiş tarihi seçiniz!");
                return false;
            }

            string strTitle = formValues["strTitle"];
            if (strTitle.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            if (string.IsNullOrEmpty(formValues["intNotificationGroupID"]))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Dağıtım listesi seçiniz!");
                return false;
            }

            if (formValues["strNotificationType"] == "file" && string.IsNullOrEmpty(strFile))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Dosya ekleyiniz!");
                return false;
            }

            return true;
        }
        #endregion

        #region GetNotificationGroups
        public JsonResult GetNotificationGroups()
        {
            using (db = new DBEntities())
            {
                var notificationGroups = db.tblNotificationGroups.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strGroupName).Select(s => new
                {
                    s.intNotificationGroupID,
                    s.strGroupName
                }).ToList();

                return Json(notificationGroups.Select(s => new
                {
                    intNotificationGroupID = cCrypto.EncryptText(s.intNotificationGroupID.ToString()),
                    s.strGroupName
                }), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region GetDetail
        public JsonResult GetDetail(string id)
        {
            if (Session["CurrentUser"] != null)
            {
                using (db = new DBEntities())
                {
                    int intNotificationID = Utilities.NullFixInt(cCrypto.DecryptText(id));

                    var notification = db.tblNotifications.Where(w => w.bolIsActive && !w.bolIsDelete && w.intNotificationID == intNotificationID && w.strNotificationType == "normal" && w.tblNotificationGroups.tblNotificationGroupCallenters.Count(c => c.intCallcenterID == Sessions.CurrentUser.intCallcenterID) > 0).Select(s => new
                    {
                        s.strTitle,
                        s.strText,
                        s.dtStartDate,
                        s.dtFinishDate
                    }).FirstOrDefault();

                    if (notification != null)
                    {
                        return Json(new
                        {
                            strTitle = notification.strTitle,
                            strText = notification.strText,
                            strDate = (notification.dtStartDate.HasValue && notification.dtFinishDate.HasValue ? notification.dtStartDate.Value.ToShortDateString() + " - " + notification.dtFinishDate.Value.ToShortDateString() : "")
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
