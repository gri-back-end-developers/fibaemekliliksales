﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using System.Text;
using System.Net;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class ProductSaleController : Controller
    {
        DBEntities db;

        #region PolicyView
        [HttpGet]
        public ActionResult PolicyView()
        {
            return View("Cancel");
        }
        #endregion

        #region Cancel
        [HttpGet]
        public ActionResult Cancel()
        {
            return View();
        }
        #endregion

        #region Cancel
        [HttpPost]
        public ActionResult Cancel(string str = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    string strPoliceler = Request.Form["hdPoliceler"];
                    if (!string.IsNullOrEmpty(strPoliceler))
                    {
                        var strPolicelerSp = strPoliceler.Split(',');

                        string strMsg = "";
                        foreach (var item in strPolicelerSp)
                        {
                            var result = PoliceCancel(Utilities.NullFixDecimal(item));
                            if (result.cevapkod == 0)
                            {
                                strMsg += "<p><b>" + item + " : </b>" + (result.cevap.Split('|').Length > 0 ? result.cevap.Split('|')[1] : "Police bulunamadı veya bir hata oluştu!") + "</p>";
                            }
                            else
                            {
                                strMsg += "<p><b>" + item + " :</b> İptal edildi.</p>";
                                Logs.Cancel(Utilities.NullFixDecimal(item));
                            }
                        }

                        Sessions.Message2 = strMsg;
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { Msg(3, 'Poliçe seçiniz!'); });</script>";
                    }
                }
                else
                {
                    Sessions.Message = "<script>$(function () { Msg(3, 'Yetkiniz bulunmamaktadır.'); });</script>";
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = "<script>$(function () { Msg(4, 'Bir hata oluştu! Tekrar deneyiniz.'); });</script>";
            }

            db.Dispose();

            return RedirectToAction("Cancel", "ProductSale", new { txtSearch = Request.Form["txtSearch"] });
        }
        #endregion

        #region CancelRedirect
        [HttpPost]
        public ActionResult CancelRedirect()
        {
            return Redirect(Request.Form["hdUserPage"] + "?txtSearch=" + cCrypto.EncryptText(Request.Form["txtSearch"]));
        }
        #endregion

        #region PoliceCancel
        BankaWebV2.BankawebV2PoliptrecUser PoliceCancel(decimal PoliceNo)
        {
            BankaWebV2.BankawebV2PoliptrecUser poluser = new BankaWebV2.BankawebV2PoliptrecUser();
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                BankaWebV2.BankawebV2PoliptinUser poliptinUser = new BankaWebV2.BankawebV2PoliptinUser();
                poliptinUser.iptnedenkod = 7;
                poliptinUser.ipttar = (DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day : DateTime.Now.Day.ToString()) + (DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                poliptinUser.polid = PoliceNo;

                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, 0, "hayat", "PoliceCancel", "Input", java.Serialize(poliptinUser), "poliptal");

                poluser = service.poliptal("1", poliptinUser);

                Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, 0, "hayat", "PoliceCancel", "Output", java.Serialize(poluser), "poliptal");

                service.Close();
                service.Abort();
            }

            return poluser;
        }
        #endregion

        #region New
        [HttpGet]
        [UserFilter]
        public ActionResult New()
        {
            var intLogID = Sessions.SaleLog;
            var intSalePage = QueryStrings.SalePage;

            if (intSalePage == 5)
            {
                using (db = new DBEntities())
                {
                    SendIssuedMail("kartodeme", intLogID, db, "[" + Sessions.CurrentUser.tblCallcenters.strName + "] Eacente Satış - Sözleşme linki gönderilemediği için gönderilmiştir", ConfigurationManager.AppSettings["MailForValidCreditCard"].Split(';').ToList());
                }

                Logs.SaleLog(intLogID, false, Enums.LogType.PaySuccess, "", "", "", "", null, 0, true);

                StringBuilder sb = new StringBuilder();
                sb.Append("<p>Değerli Müşterimiz,</p>");
                sb.Append("<p>Talebiniz tarafımıza ulaşmıştır, en kısa sürede geri dönüş yapılacaktır.</p>");
                sb.Append("<p>Teşekkür eder, mutlu ve sağlıklı günler dileriz.</p>");
                sb.Append("<p>Fiba Emeklilik ve Hayat A.Ş.</p>");

                Sessions.Message = "<script>$(function () { Msg(1, '" + sb.ToString() + "'); });</script>";
                return Redirect("/main/index");
            }

            string strPacketIDs = "";
            foreach (var item in QueryStrings.PacketIDs)
            {
                strPacketIDs += "," + item.ToString();
            }
            strPacketIDs = strPacketIDs.Remove(0, 1);

            if (intLogID > 0 && strPacketIDs != "" && Request.QueryString["new"] == null && QueryStrings.DifferentPaid == "")
            {
                using (db = new DBEntities())
                {
                    if (db.tblSaleLogs.Count(c => c.strPacketIDs == strPacketIDs && c.intLogID == intLogID) == 0)
                    {
                        Sessions.Message = "<script>$(function () { Msg(2, 'Hayat sigortası ürünü için yeni bir satış başlattığınız için sonlandırdı!'); });</script>";
                        return Redirect("/main/index");
                    }
                }
            }

            if (intSalePage == 0)
            {
                if (intLogID == 0 || (Request.QueryString["new"] != null && QueryStrings.DifferentPaid == ""))
                {
                    Logs.SaleLog(intLogID, true, Enums.LogType.ProductSelected, strPacketIDs, "", "", null);
                }
            }
            else if (intSalePage == 2)
            {
                Logs.SaleLog(intLogID, false, Enums.LogType.CustomerPolicyInfo, strPacketIDs, "Evet", "", null);
            }

            return View();
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string str = "")
        {
            db = new DBEntities();
            JavaScriptSerializer java = new JavaScriptSerializer();

            bool bolEx = false;
            var intSalePage = QueryStrings.SalePage;
            if (intSalePage == 1)
            {
                Logs.SaleLog(Sessions.SaleLog, false, Enums.LogType.TCKNInput, "", "Hayır", Request.Form["txtTCKN"], Request.Form["identitytype"], null, 0, false, QueryStrings.DifferentPaid != "");
            }
            else if (intSalePage == 3 || intSalePage == 4)
            {
                List<SaleLogPoliceler> teklifPoliceler = new List<SaleLogPoliceler>();
                string strMsg = "";
                try
                {
                    int intLogID = Sessions.SaleLog;
                    var saleLog = db.tblSaleLogs.Where(w => w.intLogID == intLogID).First();

                    bool bolValidCreditCard = Request.Form["bolValidCreditCard"] == "on" || Request.Form["bolValidCreditCard"] == "checked" || Request.Form["bolValidCreditCard"] == "true";

                    List<SaleLogPoliceler> policeler = new List<SaleLogPoliceler>();

                    bool bolDifferentPaid = Request.Form["differentPaid"] == "on" || Request.Form["differentPaid"] == "checked" || Request.Form["differentPaid"] == "true";

                    if (intSalePage == 3)
                    {
                        Logs.SaleLog(intLogID, false, Enums.LogType.CustomerInfo, "", "", "", "", null, 0, bolValidCreditCard, bolDifferentPaid);

                        Logs.SaleLogInfo(intLogID);

                        var strTckn = saleLog.tblSaleLogInfo.First(f => !f.bolDifferentPaid).tckn;

                        if (Logs.PhoneLogCount(strTckn, Request.Form["iletisimno"]) >= 3)
                        {
                            Sessions.Message = "<script>$(function () { Msg(2, 'Bir telefon numarası en fazla 3 kisiye kaydedilebilir.'); });</script>";

                            return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = 2 });
                        }
                    }

                    bool bolUW = false;
                    bool bolSOR = false;

                    if (!bolValidCreditCard)
                    {
                        if (!bolDifferentPaid)
                        {
                            if (intSalePage == 3)
                            {
                                PrimResult(intLogID, saleLog, ref policeler, ref strMsg);
                            }
                            else
                            {
                                CreatePolice(intLogID, saleLog, ref policeler, ref strMsg, ref bolUW, ref bolSOR);
                            }
                        }
                    }
                    else
                    {
                        CreatePolice(intLogID, saleLog, ref policeler, ref strMsg, ref bolUW, ref bolSOR);
                    }

                    Logs.SaleLog(intLogID, false, Enums.LogType.CustomerInfo, "", "", "", "", policeler, 0, bolValidCreditCard, bolDifferentPaid);

                    if (intSalePage == 3)
                    {
                        if (bolDifferentPaid)
                        {
                            return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), diffpay = "1" });
                        }
                    }
                    else
                    {
                        if (!bolValidCreditCard)
                        {
                            //poliçeden kredi kartı hatası varsa
                            if (strMsg.Contains("GetKKSanalPosBankaNo"))
                            {
                                Sessions.Message = "<script>$(function () { Msg(2, '" + (strMsg.Split('>').Length > 1 ? strMsg.Split('>')[1] : strMsg) + "'); $('#btnMyModalClose').click(); });</script>";

                                return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = 3 });
                            }
                            else if (bolUW)
                            {
                                Logs.SaleLog(intLogID, false, Enums.LogType.UW, "", "", "", "");

                                Sessions.Message = "<script>$(function () { Finished(1, 'Teklif almış olduğununuz sigorta poliçesi Tıbbi değerlendirmeye düşmüş olup, riskkabul@fibaemeklilik.com.tr mail adresine mail atınız.'); });</script>";

                                return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = 4 });
                            }
                            else if (bolSOR)
                            {
                                Logs.SaleLog(intLogID, false, Enums.LogType.ErrorSOR, "", "", "", "");

                                Sessions.Message = "<script>$(function () { Finished(1, 'Teklif almış olduğununuz sigorta poliçesi Tıbbi değerlendirmeye düşmüş olup, riskkabul@fibaemeklilik.com.tr mail adresine mail atınız.'); });</script>";

                                return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = 4 });
                            }
                        }

                        if (Sessions.CurrentUser.tblCallcenters.bolProductIssued)
                        {
                            if (policeler.Count > 0 || bolValidCreditCard)
                            {
                                StringBuilder sb = new StringBuilder();
                                if (!bolValidCreditCard)
                                {
                                    Logs.SaleLog(intLogID, false, Enums.LogType.LinkSend, "", "", "", "");

                                    db = new DBEntities();
                                    var saleLog_2 = db.tblSaleLogs.Where(w => w.intLogID == intLogID).First();

                                    bool bolSendButton = false;

                                    sb.Append("<p>" + policeler[0].PoliceNo + " No lu teklifiniz oluşturulmuştur.</p>");
                                    if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormMail && !saleLog_2.tblSaleLogInfo.First().bolInfoFormSms)
                                    {
                                        sb.Append("<p>Tanzim linki müşteriye e-posta veya sms ile gönderilemedi. Teklifi fiba emekliliğe gönderiniz.</p>");
                                        sb.Append("<p><a href=\"/ProductSale/New?packets=" + Request.QueryString["packets"] + "&sp=5\" class=\"btn btn-success floatRight\">Teklifi Fibaemekliliğe Gönder</a></p>");
                                        bolSendButton = true;
                                    }
                                    else
                                    {
                                        var saleInfo = saleLog_2.tblSaleLogInfo.First();
                                        if (saleInfo.InfoFormSendType == "sms")
                                        {
                                            if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormSms)
                                            {
                                                sb.Append("<p>Tanzim linki sms olarak gönderilemedi. E-posta adresine link gönderildi.</p>");
                                            }
                                            else
                                            {
                                                sb.Append("<p>Tanzim linki müşteriye sms olarak gönderildi.</p>");
                                            }
                                        }
                                        else
                                        {
                                            if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormMail)
                                            {
                                                sb.Append("<p>Tanzim linki e-posta olarak gönderilemedi. Telefon numarasına sms linki gönderildi.</p>");
                                            }
                                            else
                                            {
                                                sb.Append("<p>Tanzim linki müşteriye e-posta olarak gönderildi.</p>");
                                            }
                                        }
                                    }

                                    if (saleLog_2.tblSaleLogInfo.Count == 2)
                                    {
                                        if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormMail && !saleLog_2.tblSaleLogInfo.Last().bolInfoFormSms)
                                        {
                                            sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta veya sms ile gönderilemedi. Teklifi fiba emekliliğe gönderiniz.</p>");
                                            if (!bolSendButton)
                                            {
                                                sb.Append("<p><a href=\"/ProductSale/New?packets=" + Request.QueryString["packets"] + "&sp=5\" class=\"btn btn-success floatRight\">Teklifi Fibaemekliliğe Gönder</a></p>");
                                            }
                                        }
                                        else
                                        {
                                            var saleInfo = saleLog_2.tblSaleLogInfo.Last();
                                            if (saleInfo.InfoFormSendType == "sms")
                                            {
                                                if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormSms)
                                                {
                                                    sb.Append("<p>Tanzim linki ödeyen farklı kişiye sms olarak gönderilemedi. E-posta adresine link gönderildi.</p>");
                                                }
                                                else
                                                {
                                                    sb.Append("<p>Tanzim linki ödeyen farklı kişiye sms olarak gönderildi.</p>");
                                                }
                                            }
                                            else
                                            {
                                                if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormMail)
                                                {
                                                    sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta olarak gönderilemedi. Telefon numarasına sms linki gönderildi.</p>");
                                                }
                                                else
                                                {
                                                    sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta olarak gönderildi.</p>");
                                                }
                                            }
                                        }
                                    }

                                    if (bolSendButton)
                                    {
                                        Sessions.Message3 = sb.ToString();
                                    }
                                    else
                                    {
                                        Sessions.Message = "<script>$(function () { Finished(1, '" + sb.ToString() + "'); });</script>";
                                    }
                                }
                                else
                                {
                                    SendIssuedMail("kartodeme", intLogID, db, "[" + Sessions.CurrentUser.tblCallcenters.strName + "] Eacente Satış - Kredi kartı ile ödeme yapmadan gönderilmiştir", ConfigurationManager.AppSettings["MailForValidCreditCard"].Split(';').ToList());

                                    Logs.SaleLog(Sessions.SaleLog, false, Enums.LogType.PaySuccess, "", "", "", "", null, 0, bolValidCreditCard);

                                    sb.Append("<p>Değerli Müşterimiz,</p>");
                                    sb.Append("<p>Talebiniz tarafımıza ulaşmıştır, en kısa sürede geri dönüş yapılacaktır.</p>");
                                    sb.Append("<p>Teşekkür eder, mutlu ve sağlıklı günler dileriz.</p>");
                                    sb.Append("<p>Fiba Emeklilik ve Hayat A.Ş.</p>");

                                    Sessions.Message = "<script>$(function () { Finished(1, '" + sb.ToString() + "'); });</script>";
                                }
                            }
                            else
                            {
                                Logs.SaleLog(intLogID, false, Enums.LogType.PayView, "", "", "", "");

                                var saleLogPolicelerCancel = db.tblSaleLogPoliceler.Where(w => w.intLogID == intLogID && w.strService == "policelesme" && w.bolNew).ToList();
                                foreach (var item in saleLogPolicelerCancel)
                                {
                                    item.bolNew = false;
                                }
                                db.SaveChanges();

                                Sessions.Message = "<script>$(function () { Finished(0, '<p>Kredi kartından provizyon alımı yapılamadı lütfen yeni kredi kartı bilgileri giriniz. (" + teklifPoliceler.First().strPayMessage + ")</p>'); });</script>";
                            }
                        }
                        else
                        {
                            SendIssuedMail("teklif", intLogID, db, "Teklifi fibaemekliliğe gönder aracılığıyla gönderilmiştir", ConfigurationManager.AppSettings["InsuedMail"].Split(';').ToList());

                            Logs.SaleLog(intLogID, false, Enums.LogType.PaySuccess, "", "", "", "");

                            string strIssuedMsg = "";
                            foreach (var item in policeler)
                            {
                                strIssuedMsg += item.PoliceNo + " No’lu, ";
                            }
                            strIssuedMsg = strIssuedMsg.Remove(strIssuedMsg.Length - 2, 2);

                            strIssuedMsg = "<p>" + strIssuedMsg + " No’lu teklifiniz oluşturulmuştur. Müşteri Hizmetlerimiz tarafından müşteri aranarak poliçeniz tanzim edilecektir.</p>";

                            Sessions.Message = "<script>$(function () { Finished(1, '" + strIssuedMsg + "'); });</script>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    bolEx = true;
                    Logs.SaleLog(Sessions.SaleLog, false, Enums.LogType.PayView, "", "", "", "", null, Logs.Error(ex));

                    Sessions.Message = "<script>$(function () { Msg(2, '" + (strMsg == "" ? "Bir hata oluştu! Poliçe oluşturulamadı." : strMsg) + "'); $('#btnMyModalClose').click(); });</script>";
                }
            }

            db.Dispose();

            if (!bolEx)
            {
                if (QueryStrings.DifferentPaid == "")
                {
                    return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = intSalePage });
                }
                else
                {
                    return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]), sp = intSalePage, diffpay = "1" });
                }
            }
            else
            {
                Sessions.SaleLog = 0;
                Cookies.SaleLog = 0;

                Cookies.SaleInfo = "0";

                return RedirectToAction("New", "ProductSale", new { packets = HttpUtility.HtmlDecode(Request.QueryString["packets"]) });
            }
        }

        #region SendIssuedMail
        private void SendIssuedMail(string strType, int intLogID, DBEntities db, string strTitle, List<string> emails)
        {
            var saleInfo = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID).First();

            //string strTemplate = Utilities.GetFileText(Server.MapPath("~/htmls/MailIssued.html"));
            //strTemplate = strTemplate.Replace("#baslik#", strTitle);
            //strTemplate = strTemplate.Replace("#adsoyad#", saleInfo.ad + " " + saleInfo.soyad);
            //strTemplate = strTemplate.Replace("#tel#", saleInfo.iletisimno);
            //strTemplate = strTemplate.Replace("#ceptel#", "");

            string strDetail = "";

            var strPacketIDs = saleInfo.tblSaleLogs.strPacketIDs.Split(',').Select(s => new { intPacketID = int.Parse(s) });
            foreach (var item in strPacketIDs)
            {
                var packet = db.tblPackets.Where(w => w.intPacketID == item.intPacketID && !w.bolIsDelete).FirstOrDefault();
                if (packet != null)
                {
                    strDetail += "<p><b>Ürün / Paket :</b>" + packet.tblProducts.strName + " / " + packet.strName + "</p>";
                }
            }

            var salePoliceler = saleInfo.tblSaleLogs.tblSaleLogPoliceler.Where(w => w.strService == "policelesme").FirstOrDefault();
            if (salePoliceler != null)
            {
                strDetail += "<p><b>Poliçe Numarası :</b>" + salePoliceler.PoliceNo + "</p>";
                strDetail += "<p><b>Toplam Prim :</b>" + salePoliceler.ToplamPirim + " TL</p>";

                //strTemplate = strTemplate.Replace("#detay#", strDetail);

                //foreach (var item in emails)
                //{
                //Email.SendEmail(strTitle, strTemplate, item);
                if (strType == "teklif")
                {
                    Email.SendCRM(ConfigurationManager.AppSettings["crmTeklifGonderOwnerId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderQueueId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderPhoneCallType"].ToString(), saleInfo.ad, saleInfo.soyad, salePoliceler.PoliceNo, saleInfo.iletisimno, strDetail.Replace("<p><b>", Environment.NewLine).Replace("</b>", "").Replace("</p>", ""), strTitle);
                }
                else
                {
                    Email.SendCRM(ConfigurationManager.AppSettings["crmKrediKartOdemeYapmadanOwnerId"].ToString(), ConfigurationManager.AppSettings["crmKrediKartOdemeYapmadanQueueId"].ToString(), ConfigurationManager.AppSettings["crmKrediKartOdemeYapmadanPhoneCallType"].ToString(), saleInfo.ad, saleInfo.soyad, salePoliceler.PoliceNo, saleInfo.iletisimno, strDetail.Replace("<p><b>", Environment.NewLine).Replace("</b>", "").Replace("</p>", ""), strTitle);
                }
                //}
            }
        }
        #endregion

        #endregion

        #region PrimResult
        void PrimResult(int intLogID, tblSaleLogs saleLog, ref List<SaleLogPoliceler> policeler, ref string strMsg)
        {
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                try
                {
                    int i = 0;
                    var packets = saleLog.strPacketIDs.Split(',').Select(s => int.Parse(s)).ToList();
                    foreach (var item in packets)
                    {
                        var packet = db.tblPackets.Where(w => w.intPacketID == item && !w.bolIsDelete).FirstOrDefault();
                        if (packet != null)
                        {
                            BankaWebV2.BankawebV2PrimsiminUser pInput = new BankaWebV2.BankawebV2PrimsiminUser();

                            #region ozluk
                            AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient();

                            if (Request.Form["uyruk"] == "TR")
                            {
                                var kpsBireySorgu = aileService.kpsBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                {
                                    cacheStyle = 1,
                                    maxAge = 180,
                                    kimlikNo = Utilities.NullFixLong(saleLog.tblSaleLogInfo.First(f => !f.bolDifferentPaid).tckn),
                                    kimlikNoSpecified = true
                                });

                                pInput.ad = kpsBireySorgu.adi;
                                pInput.soyad = kpsBireySorgu.soyadi;
                                pInput.cins = kpsBireySorgu.cinsiyeti;
                                pInput.dogtar = kpsBireySorgu.dogumTarihi.ToString("ddMMyyyy");
                            }
                            else
                            {
                                var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                {
                                    cacheStyle = 1,
                                    maxAge = 180,
                                    kimlikNo = Utilities.NullFixLong(saleLog.tblSaleLogInfo.First(f => !f.bolDifferentPaid).tckn),
                                    kimlikNoSpecified = true
                                });

                                pInput.ad = kpsYabanciSorgu.adi;
                                pInput.soyad = kpsYabanciSorgu.soyadi;
                                pInput.cins = kpsYabanciSorgu.cinsiyeti;
                                pInput.dogtar = kpsYabanciSorgu.dogumTarihi.ToString("ddMMyyyy");
                            }
                            #endregion

                            pInput.aylikazal = "F";
                            pInput.odeperiyod = packet.strPremiumPaymentPeriod;
                            pInput.sure = packet.intTime >= 12 ? (packet.intTime / 12) : packet.intTime;
                            pInput.doviz = "TL";
                            pInput.kreditip = packet.strIntegrationTariffCode;

                            #region kredi
                            BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                            foreach (var assurance in packet.tblAssurances.Where(w => !w.bolIsDelete).ToList())
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    bedel = assurance.flPrice,
                                    tmntkod = assurance.strCode,
                                    yil = assurance.intYear
                                });
                            }

                            pInput.yillikkreditaksit = yillikkreditaksit;
                            if (yillikkreditaksit.Count > 0)
                            {
                                pInput.tmnttutar = yillikkreditaksit.First().bedel;
                            }
                            #endregion

                            JavaScriptSerializer java = new JavaScriptSerializer();
                            int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, intLogID, "hayat", "PrimResult", "Input", java.Serialize(pInput), "getprimsimresult");

                            var primsimresult = service.getprimsimresult(pInput);

                            Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, intLogID, "hayat", "PrimResult", "Output", java.Serialize(primsimresult), "getprimsimresult");

                            if (primsimresult != null && primsimresult.cevap.Contains("OK"))
                            {
                                policeler.Add(new SaleLogPoliceler
                                {
                                    IlkPrim = primsimresult.prim.Value,
                                    intPacketID = packet.intPacketID,
                                    ToplamPirim = primsimresult.prim.Value,
                                    intCrediTime = packet.intCrediTime,
                                    IlkPrimKomisyon = primsimresult.komisyon.Value,
                                    ToplamKomisyon = primsimresult.komisyon.Value,
                                    intPoliceTypeID = (byte)Enums.PoliceType.Policelesme_NKT,
                                    bolCancel = false,
                                    strService = "getprimsimresult",
                                    bolNew = true
                                });
                            }
                            else if (!string.IsNullOrEmpty(primsimresult.cevap))
                            {
                                strMsg = primsimresult.cevap.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                                throw new Exception(strMsg);
                            }
                        }

                        i++;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    service.Close();
                    service.Abort();
                }
            }
        }
        #endregion

        #region CreatePolice
        void CreatePolice(int intLogID, tblSaleLogs saleLog, ref List<SaleLogPoliceler> policeler, ref string strMsg, ref bool bolUW, ref bool bolSOR)
        {
            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                try
                {
                    int i = 0;
                    var packets = saleLog.strPacketIDs.Split(',').Select(s => int.Parse(s)).ToList();
                    foreach (var item in packets)
                    {
                        var packet = db.tblPackets.Where(w => w.intPacketID == item && !w.bolIsDelete).FirstOrDefault();
                        if (packet != null)
                        {
                            BankaWebV2.BankawebV2PolreqUser user = new BankaWebV2.BankawebV2PolreqUser();

                            #region adres 
                            var saleLogInfo = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && !w.bolDifferentPaid).First();
                            user.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                            {
                                adres = saleLogInfo.adres,
                                adrestip = saleLogInfo.adrestip,
                                email = saleLogInfo.email,
                                ilcekod = saleLogInfo.ilcekod,
                                iletisimno = saleLogInfo.iletisimno,
                                iletisimtip = saleLogInfo.iletisimtip,
                                ilkod = saleLogInfo.ilkod,
                                ulkekod = saleLogInfo.ulkekod
                            };
                            #endregion

                            #region ozluk
                            AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient();

                            string strUyruk = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && !w.bolDifferentPaid).First().uyruk;
                            if (strUyruk == "TR")
                            {
                                var kpsBireySorgu = aileService.kpsBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                {
                                    cacheStyle = 1,
                                    maxAge = 180,
                                    kimlikNo = Utilities.NullFixLong(saleLog.tblSaleLogInfo.First(f => !f.bolDifferentPaid).tckn),
                                    kimlikNoSpecified = true
                                });

                                string dogumil = "";
                                var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                                if (il != null)
                                    dogumil = il.strCode.Trim();

                                string dogumilce = "";
                                var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                                if (ilce != null)
                                    dogumilce = ilce.strCode.Trim();

                                if (dogumil == "" && dogumilce != "")
                                {
                                    dogumil = dogumilce.Substring(0, 3);
                                }

                                if (dogumil == "")
                                {
                                    dogumil = "99";
                                }

                                if (dogumil != "" && dogumilce == "")
                                {
                                    dogumilce = "99900";
                                }

                                user.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                                {
                                    ad = kpsBireySorgu.adi,
                                    anneadi = kpsBireySorgu.anneAdi,
                                    babaad = kpsBireySorgu.babaAdi,
                                    cinsiyet = kpsBireySorgu.cinsiyeti,
                                    dogumil = dogumil.Trim(),
                                    dogumilce = dogumilce.Trim(),
                                    dogumtarih = kpsBireySorgu.dogumTarihi,
                                    medenidurum = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B",
                                    soyad = kpsBireySorgu.soyadi,
                                    tckimlikno = kpsBireySorgu.tckimlikNo.ToString(),
                                    musteritip = "G",
                                    meslekkod = "065",
                                    uyruk = strUyruk
                                };
                            }
                            else
                            {
                                var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                {
                                    cacheStyle = 1,
                                    maxAge = 180,
                                    kimlikNo = Utilities.NullFixLong(saleLog.tblSaleLogInfo.First(f => !f.bolDifferentPaid).tckn),
                                    kimlikNoSpecified = true
                                });

                                user.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                                {
                                    ad = kpsYabanciSorgu.adi,
                                    anneadi = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && !w.bolDifferentPaid).First().anneadi,
                                    babaad = kpsYabanciSorgu.babaAdi,
                                    cinsiyet = kpsYabanciSorgu.cinsiyeti,
                                    dogumil = "99",
                                    dogumilce = "99900",
                                    dogumtarih = kpsYabanciSorgu.dogumTarihi,
                                    medenidurum = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && !w.bolDifferentPaid).First().medenidurum,
                                    soyad = kpsYabanciSorgu.soyadi,
                                    tckimlikno = kpsYabanciSorgu.tckimlikNo.ToString(),
                                    musteritip = "G",
                                    meslekkod = "065",
                                    uyruk = strUyruk
                                };
                            }
                            #endregion

                            #region araci
                            user.araci = new BankaWebV2.Bankawebv2polreqAracireqUser
                            {
                                bankasube = Utilities.NullFixDecimal(Sessions.CurrentUser.tblCallcenters.strBankNo),
                                sicilno = Sessions.CurrentUser.tblCallcenters.strRegisterNo
                            };
                            #endregion

                            #region kredi
                            BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                            foreach (var assurance in packet.tblAssurances.Where(w => !w.bolIsDelete).ToList())
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    bedel = assurance.flPrice,
                                    tmntkod = assurance.strCode,
                                    yil = assurance.intYear
                                });
                            }

                            string strKrediNo = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();

                            user.kredi = new BankaWebV2.Bankawebv2polreqKredireqUser
                            {
                                kredisure = packet.intCrediTime,
                                kredidoviztip = packet.strCrediCurrencyType,
                                kreditaksittip = "S",
                                kredidonustutar = packet.flCrediReturnPrice,
                                yillikkreditaksit = yillikkreditaksit,
                                kreditutar = 1,
                                kreditip = packet.strIntegrationTariffCode,
                                sure = packet.intTime,
                                kredino = "x" + (Utilities.NullFixLong(strKrediNo) + i)
                            };
                            #endregion

                            #region odeme
                            bool bolValidCreditCard = Request.Form["bolValidCreditCard"] == "on" || Request.Form["bolValidCreditCard"] == "checked" || Request.Form["bolValidCreditCard"] == "true";
                            if (bolValidCreditCard)
                            {
                                user.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                                {
                                    odemetip = "NKT",
                                    dovizkod = "TL",
                                    primodemeperiyod = packet.strPremiumPaymentPeriod,
                                    primodemesure = packet.intPremiumPaymentTime
                                };
                            }
                            else
                            {
                                int intPaymentMethodID = Utilities.NullFixInt(Request.Form["paymentMethods"]);

                                tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                                user.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                                {
                                    primodemeperiyod = paymentMethod.strPeriod,
                                    primodemesure = paymentMethod.intInstallment,
                                    odemetip = "KRD",
                                    dovizkod = "TL",
                                    kartno = Request.Form["kartno"].Replace(" ", "").Trim(),
                                    sonkullanmatarih = Request.Form["kartay"] + "20" + Request.Form["kartyil"]
                                };

                                Sessions.CreditCard = new cCreditCard
                                {
                                    card = Request.Form["kartno"].Replace(" ", "").Trim(),
                                    month = Request.Form["kartay"],
                                    year = Request.Form["kartyil"],
                                    type = intPaymentMethodID.ToString()
                                };
                            }
                            #endregion

                            #region yenileme
                            string strYenileme = "";
                            if (packet.tblProducts.bolIsRefresh)
                            {
                                int intRefresh = Utilities.NullFixInt(Request.Form["hdrefresh" + packet.tblProducts.intProductID]);
                                if (intRefresh > 0)
                                {
                                    user.yenileme = "T";
                                    user.yenilemesayisi = intRefresh;

                                    strYenileme = "T - " + intRefresh;
                                }
                                else
                                {
                                    user.yenileme = "F";
                                    strYenileme = "F";
                                }
                            }
                            #endregion

                            #region OdeyenFarkli
                            var saleLogInfoDiff = db.tblSaleLogInfo.Where(w => w.intLogID == intLogID && w.bolDifferentPaid).FirstOrDefault();
                            if (saleLogInfoDiff != null)
                            {
                                #region odeyen
                                if (saleLogInfoDiff.kimliktipi == "tckn")
                                {
                                    if (saleLogInfoDiff.uyruk == "TR")
                                    {
                                        var kpsBireySorgu = aileService.kpsBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                        {
                                            cacheStyle = 1,
                                            maxAge = 180,
                                            kimlikNo = Utilities.NullFixLong(saleLogInfoDiff.tckn),
                                            kimlikNoSpecified = true
                                        });

                                        string dogumil = "";
                                        var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                                        if (il != null)
                                            dogumil = il.strCode.Trim();

                                        string dogumilce = "";
                                        var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                                        if (ilce != null)
                                            dogumilce = ilce.strCode.Trim();

                                        if (dogumil == "" && dogumilce != "")
                                        {
                                            dogumil = dogumilce.Substring(0, 3);
                                        }

                                        if (dogumil == "")
                                        {
                                            dogumil = "99";
                                        }

                                        if (dogumil != "" && dogumilce == "")
                                        {
                                            dogumilce = "99900";
                                        }

                                        user.odeyen = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                                        {
                                            ad = kpsBireySorgu.adi,
                                            anneadi = kpsBireySorgu.anneAdi,
                                            babaad = kpsBireySorgu.babaAdi,
                                            cinsiyet = kpsBireySorgu.cinsiyeti,
                                            dogumil = dogumil.Trim(),
                                            dogumilce = dogumilce.Trim(),
                                            dogumtarih = kpsBireySorgu.dogumTarihi,
                                            medenidurum = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B",
                                            soyad = kpsBireySorgu.soyadi,
                                            tckimlikno = kpsBireySorgu.tckimlikNo.ToString(),
                                            musteritip = "G",//T
                                            meslekkod = "065",
                                            uyruk = strUyruk
                                        };
                                    }
                                    else
                                    {
                                        var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new FibaEmeklilikSales.AileBireySorgu.bireySorguInputType()
                                        {
                                            cacheStyle = 1,
                                            maxAge = 180,
                                            kimlikNo = Utilities.NullFixLong(saleLogInfoDiff.tckn),
                                            kimlikNoSpecified = true
                                        });

                                        user.odeyen = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                                        {
                                            ad = kpsYabanciSorgu.adi,
                                            anneadi = saleLogInfoDiff.anneadi,
                                            babaad = kpsYabanciSorgu.babaAdi,
                                            cinsiyet = kpsYabanciSorgu.cinsiyeti,
                                            dogumil = "99",
                                            dogumilce = "99900",
                                            dogumtarih = kpsYabanciSorgu.dogumTarihi,
                                            medenidurum = saleLogInfoDiff.medenidurum,
                                            soyad = kpsYabanciSorgu.soyadi,
                                            tckimlikno = kpsYabanciSorgu.tckimlikNo.ToString(),
                                            musteritip = "G",
                                            meslekkod = "065",
                                            uyruk = strUyruk
                                        };
                                    }
                                }
                                else if (saleLogInfoDiff.kimliktipi == "vkn")
                                {
                                    user.odeyen = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                                    {
                                        dogumil = "99",
                                        dogumilce = "99900",
                                        musteritip = "T",
                                        meslekkod = "065",
                                        uyruk = saleLogInfoDiff.uyruk,
                                        sirket = saleLogInfoDiff.soyad + " " + saleLogInfoDiff.ad,
                                        vergino = saleLogInfoDiff.tckn
                                    };
                                }
                                #endregion

                                user.odeyenadres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                                {
                                    adres = saleLogInfoDiff.adres,
                                    adrestip = saleLogInfoDiff.adrestip,
                                    email = saleLogInfoDiff.email,
                                    ilcekod = saleLogInfoDiff.ilcekod.Trim(),
                                    iletisimno = saleLogInfoDiff.iletisimno,
                                    iletisimtip = saleLogInfoDiff.iletisimtip,
                                    ilkod = saleLogInfoDiff.ilkod.Trim(),
                                    ulkekod = saleLogInfoDiff.ulkekod
                                };
                            }
                            #endregion

                            JavaScriptSerializer java = new JavaScriptSerializer();
                            int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, intLogID, "hayat", "New", "Input", java.Serialize(user), "policelesme");

                            var police = service.policelesme(user, "T");

                            Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, intLogID, "hayat", "New", "Output", java.Serialize(police), "policelesme");

                            if (police.cevap == null)
                            {
                                if (police.teklifdurum == "UW")
                                {
                                    bolUW = true;
                                }
                                else if (police.teklifdurum == "SOR")
                                {
                                    bolSOR = true;
                                }

                                policeler.Add(new SaleLogPoliceler
                                {
                                    IlkPrim = police.ilkprim.Value,
                                    intPacketID = packet.intPacketID,
                                    ToplamPirim = police.toplamprim.Value,
                                    intCrediTime = packet.intCrediTime,
                                    PoliceNo = police.policeno.Value,
                                    IlkPrimKomisyon = police.ilkprimkomisyon.Value,
                                    KrediNo = police.kredino,
                                    TeklifDurum = police.teklifdurum,
                                    ToplamKomisyon = police.toplamkomisyon.Value,
                                    intPoliceTypeID = bolValidCreditCard ? (byte)Enums.PoliceType.Policelesme_NKT : (byte)Enums.PoliceType.Policelesme_KRD,
                                    bolCancel = false,
                                    strService = "policelesme",
                                    bolNew = true,
                                    strYenileme = strYenileme,
                                    bsmv = police.bsmv
                                });
                            }
                            else if (police.cevap.Contains("Err|"))
                            {
                                strMsg = police.cevap.Split('|')[1].Trim();
                                strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                                //Kredi kartı ile ilgili bir mesaj dönerse hataya düşmemesi gerekiyor
                                if (!strMsg.Contains("GetKKSanalPosBankaNo"))
                                    throw new Exception(strMsg);
                            }
                        }

                        i++;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    service.Close();
                    service.Abort();
                }
            }
        }
        #endregion

        #region Info
        [HttpGet]
        public ActionResult Info()
        {
            return View();
        }
        #endregion

        #region Counties
        public JsonResult Counties(string id)
        {
            db = new DBEntities();

            var ilceler = db.tblMapping.Where(w => w.strType == "ILC" && !string.IsNullOrEmpty(w.strName.Trim()) && w.strCode.Substring(0, 3) == id).OrderBy(o => o.strName).Select(s => new
            {
                s.strCode,
                s.strName
            }).ToList();

            db.Dispose();

            return Json(ilceler, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region InfoForm
        [HttpGet]
        public ActionResult InfoForm()
        {
            return View();
        }
        #endregion
    }
}
