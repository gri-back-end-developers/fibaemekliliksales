﻿#region Directives
using FibaEmeklilikSales.Classes;
using FibaEmeklilikSales.FIBAWS;
using FibaEmeklilikSales.INTBESWS;
using jCryption;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endregion

namespace FibaEmeklilikSales.Controllers
{
    public class PolicyController : Controller
    {
        string strCallcenterCode = Sessions.CurrentUser != null && Sessions.CurrentUser.tblCallcenters != null ? Sessions.CurrentUser.tblCallcenters.strCallcenterCode : "";
        //string strCallcenterCode = "E_2313";

        #region List
        [HttpGet]
        [UserFilter]
        public ActionResult List(string strSearchType = "", string strStatu = "", string strSearch = "", string strPage = "1", string strType = "", string strButtonType = "")
        {
            cPolicySearch policyData = new cPolicySearch();
            policyData.strSearch = strSearch;
            policyData.strSearchType = strSearchType;
            policyData.strStatu = strStatu;
            policyData.intPage = Utilities.NullFixInt(strPage);
            policyData.bolPaymentChange = Sessions.CurrentUser.tblCallcenters.bolPolicyBesPaymentChange;

            if (!string.IsNullOrEmpty(strSearchType) && !string.IsNullOrEmpty(strSearch))
            {
                if (strSearchType == "5" && strSearch.Length == 11)
                {
                    policyData.strSearch = strSearch.Substring(0, 3) + "******" + strSearch.Substring(9, 2);
                }
            }

            try
            {
                if (!string.IsNullOrEmpty(strSearchType))
                {
                    strSearch = strSearch.Trim();

                    using (fibawsClient service = new fibawsClient())
                    {
                        FibawsPolicesorguoutrecUser policesorgu = new FibawsPolicesorguoutrecUser();
                        FibawspolicesorguoutrecAPol pollist = !string.IsNullOrEmpty(strType) ? Sessions.PolicyList : null;
                        if (pollist == null)
                        {
                            switch (strSearchType)
                            {
                                case "1":
                                case "2":
                                    if (Utilities.NullFixDecimal(strSearch) > 0)
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            polid = Utilities.NullFixDecimal(strSearch),
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "3":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            policeno = strSearch,
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "4":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            katilimciadsoyad = strSearch.ToUpper(),
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "5":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            kimlikno = strSearch,
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "6":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            odeyenadsoyad = strSearch.ToUpper(),
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "7":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            polbastar = strSearch.Replace(".", "/"),
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "8":
                                    if (!string.IsNullOrEmpty(strSearch))
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            polbittar = strSearch.Replace(".", "/"),
                                            acenteno = strCallcenterCode
                                        });
                                    }
                                    break;
                                case "9":
                                    policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                    {
                                        acenteno = strCallcenterCode,
                                        statu = "II"
                                    });
                                    break;
                                case "10":
                                    policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                    {
                                        acenteno = strCallcenterCode,
                                        statu = "MM"
                                    });
                                    break;
                                case "11":
                                    policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                    {
                                        acenteno = strCallcenterCode,
                                        branskod = "E"
                                    });
                                    break;
                                case "12":
                                    if (strStatu == "")
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            acenteno = strCallcenterCode,
                                            branskod = "H"
                                        });
                                    }
                                    else
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            acenteno = strCallcenterCode,
                                            branskod = "H",
                                            statu = strStatu
                                        });
                                    }
                                    break;
                                case "13":
                                    if (strStatu == "")
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            acenteno = strCallcenterCode,
                                            branskod = "K"
                                        });
                                    }
                                    else
                                    {
                                        policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                        {
                                            acenteno = strCallcenterCode,
                                            branskod = "K",
                                            statu = strStatu
                                        });
                                    }
                                    break;
                            }

                            pollist = policesorgu.pollist;
                        }

                        if (pollist != null)
                        {
                            policyData.PolicySearchData = new List<cPolicySearchData>();
                            policyData.intTotalCount = pollist.Count;

                            foreach (var item in pollist)
                            {
                                if (item.pid != null || item.polid != null)
                                {
                                    string PolID = item.polid.HasValue ? item.polid.Value.ToString() : "";

                                    if (policyData.PolicySearchData.Count(c => c.PolID == PolID) == 0)
                                    {
                                        policyData.PolicySearchData.Add(new cPolicySearchData
                                        {
                                            PlanTarifeNo = item.tarifeno,
                                            BitisTarihi = item.polbittar,
                                            BaslamaTarihi = Utilities.NullFixDate(item.polbastar),
                                            Brans = item.branskod,
                                            Durum = item.statu,
                                            OdeyenAdSoyad = item.odeyenadsoyad,
                                            KimlikNo = item.kimlikno.Length == 11 ? item.kimlikno.Substring(0, 3) + "******" + item.kimlikno.Substring(9, 2) : item.kimlikno,
                                            PoliceNo = item.policeno,
                                            PolID = PolID,
                                            SigortaliKatilimciAdSoyad = item.katilimciadsoyad,
                                            Pid = item.pid.HasValue ? item.pid.Value.ToString() : ""
                                        });
                                    }
                                }
                            }

                            policyData.PolicySearchData = policyData.PolicySearchData.OrderByDescending(o => o.BaslamaTarihi).ToList();

                            if (policyData.PolicySearchData.Count > 0)
                            {
                                if (strButtonType == "excel")
                                {
                                    DataTable dtExcel = new DataTable();
                                    dtExcel.Columns.Add("Police_Sozlesme_No");
                                    dtExcel.Columns.Add("Police_Numarasi");
                                    dtExcel.Columns.Add("Durum");
                                    dtExcel.Columns.Add("Brans");
                                    dtExcel.Columns.Add("Sigortali_Katilimci_Ad_Soyad");
                                    dtExcel.Columns.Add("Kimlik_No");
                                    dtExcel.Columns.Add("Odeyen_Sigorta_Ettiren_Ad_Soyad");
                                    dtExcel.Columns.Add("Plan_Tarife_No");
                                    dtExcel.Columns.Add("Bitis_Tarihi");
                                    dtExcel.Columns.Add("Baslama_Tarihi");

                                    foreach (var item in policyData.PolicySearchData)
                                    {
                                        DataRow dr = dtExcel.NewRow();
                                        dr[0] = item.PolID;
                                        dr[1] = item.PoliceNo;
                                        dr[2] = item.Durum;
                                        dr[3] = (item.Brans == "E" ? "Bes" : "Hayat");
                                        dr[4] = item.SigortaliKatilimciAdSoyad;
                                        dr[5] = item.KimlikNo;
                                        dr[6] = item.OdeyenAdSoyad;
                                        dr[7] = item.PlanTarifeNo;
                                        dr[8] = item.BitisTarihi;
                                        dr[9] = item.BaslamaTarihi.ToShortDateString();

                                        dtExcel.Rows.Add(dr);
                                    }

                                    ExcelExportSheet excel = new ExcelExportSheet();
                                    excel.ToExcel(dtExcel, "Police_Sozlesme.xls", this.Response, "");
                                }

                                int intPageSize = 20;
                                policyData.intCount = (policyData.PolicySearchData.Count / intPageSize) + (policyData.PolicySearchData.Count % intPageSize > 0 ? 1 : 0);

                                int intPage = Utilities.NullFixInt(strPage);
                                intPage = intPage > 0 ? intPage - 1 : intPage;

                                if (policyData.intCount > 1 && string.IsNullOrEmpty(strType))
                                {
                                    Sessions.PolicyList = pollist;
                                }

                                policyData.PolicySearchData = policyData.PolicySearchData.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                            }
                            else
                            {
                                policyData.strMsg = Utilities.Msg(Utilities.MsgType.Warning, "Poliçe / Sözleşme bulunamadı!");
                            }
                        }
                        else
                        {
                            policyData.strMsg = Utilities.Msg(Utilities.MsgType.Warning, "Poliçe / Sözleşme bulunamadı!");
                        }

                        service.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);

                policyData.strMsg = Utilities.Msg(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deyiniz.");
            }

            return View(policyData);
        }

        [HttpPost]
        [UserFilter]
        public ActionResult List()
        {
            return List(Request.Form["searchtype"], Request.Form["status"], Request.Form["txtSearch"], Request.Form["hdPage"], Request.Form["hdType"], Request.Form["hdButtonType"]);
        }
        #endregion

        #region LifeDetail

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult LifeDetail()
        {
            cPolicyLifeDetail policyLifeDetail = new cPolicyLifeDetail();
            if (Session["CurrentUser"] == null)
            {
                return View(policyLifeDetail);
            }

            try
            {
                using (fibawsClient service = new fibawsClient())
                {
                    string strType = QueryStrings.Type;
                    if (strType == "genel")
                    {
                        FibawsPolicebilgirecUser policebilgirec = null;
                        try
                        {
                            policebilgirec = service.getpolicebilgirec(QueryStrings.police.ToString());

                            FibawsPolicesorguoutrecUser policesorgu = null;
                            try
                            {
                                policesorgu = service.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                {
                                    pid = QueryStrings.Pid,
                                    acenteno = strCallcenterCode
                                });
                            }
                            catch (Exception ex)
                            {
                                Logs.Error(ex);
                            }

                            if (policebilgirec != null)
                            {
                                policyLifeDetail.GeneralInfo = new cPolicyLifeDetailGeneralInfo()
                                {
                                    BaslangicTarihi = policebilgirec.polbastar,
                                    BitisTarihi = policebilgirec.polbittar,
                                    Durum = policebilgirec.statu,
                                    PoliceNumarasi = policebilgirec.policeno,
                                    Sigortali = policesorgu != null ? (policesorgu.pollist.Count > 0 ? policesorgu.pollist[0].katilimciadsoyad : "") : "",
                                    SigortaEttiren = policebilgirec.sigettiren,
                                    SigortaliMusteri = policebilgirec.musteri,
                                    UrunAdi = policebilgirec.urunad
                                };

                                if (policebilgirec.teminatlar != null)
                                {
                                    if (policebilgirec.teminatlar.Count > 0)
                                    {
                                        policyLifeDetail.GeneralInfo.Teminat = new cPolicyLifeDetailAssurance();
                                        policyLifeDetail.GeneralInfo.Teminat.ToplamPrim = Utilities.Price(policebilgirec.teminatlar.Sum(s => s.teminatprimi.Value));

                                        policyLifeDetail.GeneralInfo.Teminat.Teminatlar = new List<cPolicyLifeDetailAssuranceDetail>();

                                        foreach (var item in policebilgirec.teminatlar)
                                        {
                                            policyLifeDetail.GeneralInfo.Teminat.Teminatlar.Add(new cPolicyLifeDetailAssuranceDetail
                                            {
                                                Prim = item.teminatprimi.HasValue ? Utilities.Price(item.teminatprimi.Value) : "",
                                                Teminat = item.teminat,
                                                Tutar = item.teminatmiktar.HasValue ? Utilities.Price(item.teminatmiktar.Value) : ""
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.Error(ex);
                        }

                        try
                        {
                            FibawsOdemebilgirecUser odemebilgirec = service.getodemebilgirec(QueryStrings.PolID);
                            if (odemebilgirec != null)
                            {
                                policyLifeDetail.GeneralInfo.ÖdemeBilgileri = new cPolicyLifeDetailPaymentInfo
                                {
                                    BankaAdi = odemebilgirec.bankaadi,
                                    HesapNo = odemebilgirec.hesapno,
                                    IbanNo = odemebilgirec.ibanno,
                                    KartNo = odemebilgirec.kartno,
                                    OdemeGunu = odemebilgirec.odemegunu.HasValue ? odemebilgirec.odemegunu.Value.ToString() : "",
                                    OdemeTipi = odemebilgirec.odetip,
                                    SonKullanmaTarihi = odemebilgirec.sonkultar,
                                    SubeNo = odemebilgirec.subeno,
                                    YillikTaksitSayisi = odemebilgirec.odedonem.HasValue ? odemebilgirec.odedonem.Value.ToString() : ""
                                };
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.Error(ex);
                        }
                    }
                    else if (strType == "tahsilat")
                    {
                        using (NTBESClient serviceIntBes = new NTBESClient())
                        {
                            var vadebilgilerirec = serviceIntBes.getvadebilgilerirec(QueryStrings.PolID);
                            if (vadebilgilerirec != null && vadebilgilerirec.AVade != null && vadebilgilerirec.AVade.Count > 0)
                            {
                                policyLifeDetail.CollectionInfo = new List<cPolicyLifeDetailCollectionInfo>();

                                foreach (var item in vadebilgilerirec.AVade.Where(w => w.borctip == "Katkı Payı" || w.borctip == "Ek Yatırım").ToList())
                                {
                                    policyLifeDetail.CollectionInfo.Add(new cPolicyLifeDetailCollectionInfo
                                    {
                                        Makbuz = "/policy/pdf?type=hayattahsilatmakbuz&makbuzno=" + item.makbuzno,
                                        MakbuzNo = item.makbuzno.HasValue ? item.makbuzno.Value.ToString() : "",
                                        Tahsilat = item.odenentutar.HasValue ? Utilities.Price(item.odenentutar.Value) : "",
                                        VadeTarihi = item.vadetar,
                                        VadeTutar = item.vadetutar.HasValue ? Utilities.Price(item.vadetutar.Value) : "",
                                        IntikalTarihi = item.intiktar,
                                        TahsilatTarihi = item.odevadetar
                                    });
                                }
                            }
                        }
                    }
                    else if (strType == "musteri")
                    {
                        var kisibilgirec = service.getkisibilgirec(QueryStrings.Pid);
                        if (kisibilgirec != null)
                        {
                            policyLifeDetail.Musteri = new cPolicyDetailMusteri();
                            policyLifeDetail.Musteri.Adres = kisibilgirec.kimlikevadres;
                            policyLifeDetail.Musteri.Email = "";
                            policyLifeDetail.Musteri.Tel = kisibilgirec.kimlikceptel;
                            policyLifeDetail.Musteri.Tckn = kisibilgirec.kimlikno;
                        }
                    }

                    service.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(policyLifeDetail);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LifeDetail(string tel, string email)
        {
            try
            {
                using (NTBESClient service = new NTBESClient())
                {
                    var adresdegisikligi = service.setadresdegisikligi(new IntbesAdresdegtypeUser
                    {
                        //emailbilgi = email,
                        pid = QueryStrings.Pid,
                        telbilgi = new IntbesadresdegtypeTelrectypeUser
                        {
                            eid = "E",
                            telno = tel,
                            teltip = "G",
                            pid = QueryStrings.Pid
                        }
                    });

                    if (adresdegisikligi.sonuc == "BAŞARILI")
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Info, "Müşteri bilgileri güncellendi.");
                    }
                    else
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Bir hata oluştu!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return Redirect("/Policy/LifeDetail?police=" + QueryStrings.police + "&pid=" + QueryStrings.Pid + "&polid=" + QueryStrings.PolID + "&type=musteri");
        }
        #endregion

        #endregion

        #region BesDetail

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult BesDetail()
        {
            cPolicyBesDetail policyBesDetail = new cPolicyBesDetail();
            if (Session["CurrentUser"] == null)
            {
                return View(policyBesDetail);
            }

            try
            {
                using (NTBESClient serviceIntBes = new NTBESClient())
                {
                    using (fibawsClient serviceFibaws = new fibawsClient())
                    {
                        string strType = QueryStrings.Type;

                        #region Genel
                        if (strType == "genel")
                        {
                            try
                            {
                                var policebilgirec = serviceIntBes.getpolicebilgirec(QueryStrings.Pid);

                                if (policebilgirec != null)
                                {
                                    if (policebilgirec.ASoz != null)
                                    {
                                        var aSoz = policebilgirec.ASoz.Where(w => w.polid == QueryStrings.PolID).FirstOrDefault();
                                        if (aSoz != null)
                                        {
                                            if (aSoz.sozlesmedetay != null)
                                            {
                                                if (aSoz.sozlesmedetay.Count > 0)
                                                {
                                                    policyBesDetail.GeneralInfo = new cPolicyBesDetailGeneralInfo()
                                                    {
                                                        BesGirisTarihi = aSoz.sozlesmedetay[0].besgirtar,
                                                        EmeklilikTarihi = aSoz.sozlesmedetay[0].emekliliktar,
                                                        GrupAdi = aSoz.sozlesmedetay[0].grupadi,
                                                        GrupTipi = aSoz.sozlesmedetay[0].gruptip,
                                                        Devirmi = aSoz.sozlesmedetay[0].devirmi,
                                                        KatkiPayiOdemeDonemi = aSoz.sozlesmedetay[0].katpayodedonem,
                                                        KrediKartiHesap = aSoz.sozlesmedetay[0].kredikart,
                                                        ToplamGirişAidatıTutari = aSoz.sozlesmedetay[0].topgiraidat.HasValue ? Utilities.Price(aSoz.sozlesmedetay[0].topgiraidat.Value) : "",
                                                        YillikKatkiPayiTutari = aSoz.sozlesmedetay[0].katpaytut.HasValue ? Utilities.Price(aSoz.sozlesmedetay[0].katpaytut.Value) : "",
                                                        YururlukTarihi = aSoz.sozlesmedetay[0].yururluktar,
                                                        HesapBildirimCetveli = (aSoz.sozlesmedetay[0].gruptip == "İGES" && aSoz.sozlesmedetay[0].devirmi == "X")
                                                            || (aSoz.sozlesmedetay[0].gruptip == "İGES" && string.IsNullOrEmpty(aSoz.sozlesmedetay[0].devirmi))
                                                            || (string.IsNullOrEmpty(aSoz.sozlesmedetay[0].gruptip) && aSoz.sozlesmedetay[0].devirmi == "X")
                                                            || (string.IsNullOrEmpty(aSoz.sozlesmedetay[0].gruptip) && string.IsNullOrEmpty(aSoz.sozlesmedetay[0].devirmi)
                                                            || (aSoz.sozlesmedetay[0].gruptip == "GBB" && aSoz.sozlesmedetay[0].devirmi == "X")
                                                            || (aSoz.sozlesmedetay[0].gruptip == "GBB" && string.IsNullOrEmpty(aSoz.sozlesmedetay[0].devirmi))
                                                            || (string.IsNullOrEmpty(aSoz.sozlesmedetay[0].gruptip) && aSoz.sozlesmedetay[0].devirmi == "P"))
                                                    };

                                                    if (aSoz.sozlesmedetay[0].lehdarbilgi != null)
                                                    {
                                                        if (aSoz.sozlesmedetay[0].lehdarbilgi.Count > 0)
                                                        {
                                                            policyBesDetail.GeneralInfo.Lehdar = new List<cPolicyBesDetailLehdarInfo>();
                                                            foreach (var item in aSoz.sozlesmedetay[0].lehdarbilgi)
                                                            {
                                                                policyBesDetail.GeneralInfo.Lehdar.Add(new cPolicyBesDetailLehdarInfo
                                                                {
                                                                    Adi = item.adsoyad,
                                                                    Cins = item.cins,
                                                                    LehtarTipi = item.lehtartip,
                                                                    Oran = item.oran.HasValue ? item.oran.Value.ToString() : "",
                                                                    Yakınlık = item.yakinlik
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        try
                                        {
                                            FibawsPolicesorguoutrecUser policesorgu = serviceFibaws.getpolicesorgu(new FibaEmeklilikSales.FIBAWS.FibawsPolicesorguinrecUser
                                            {
                                                pid = QueryStrings.Pid,
                                                acenteno = strCallcenterCode
                                            });

                                            if (policesorgu != null)
                                            {
                                                if (policesorgu.pollist != null)
                                                {
                                                    if (policesorgu.pollist.Count > 0)
                                                    {
                                                        if (policyBesDetail.GeneralInfo == null)
                                                        {
                                                            policyBesDetail.GeneralInfo = new cPolicyBesDetailGeneralInfo();
                                                        }

                                                        policyBesDetail.GeneralInfo.Odeyen = policesorgu.pollist[0].odeyenadsoyad;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logs.Error(ex);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logs.Error(ex);
                            }
                        }
                        #endregion
                        #region Fon
                        else if (strType == "fon")
                        {
                            var sozfonbilgileri = serviceIntBes.getsozfonbilgileri(QueryStrings.PolID);
                            if (sozfonbilgileri != null)
                            {
                                if (sozfonbilgileri.AFonlist != null)
                                {
                                    if (sozfonbilgileri.AFonlist.Count > 0)
                                    {
                                        policyBesDetail.FundInfo = new List<cPolicyBesDetailFundInfo>();

                                        foreach (var item in sozfonbilgileri.AFonlist)
                                        {
                                            policyBesDetail.FundInfo.Add(new cPolicyBesDetailFundInfo
                                            {
                                                FonAdi = item.fonad,
                                                FonKodu = item.fonkod,
                                                KatkiPayiFonDagilimi = item.oran.HasValue ? (item.oran.Value * 100).ToString() : ""
                                            });
                                        }

                                        foreach (var item in policyBesDetail.FundInfo)
                                        {
                                            if (item.KatkiPayiFonDagilimi.Contains(","))
                                            {
                                                item.KatkiPayiFonDagilimi = item.KatkiPayiFonDagilimi.Split(',')[0];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Tahsilat
                        else if (strType == "tahsilat")
                        {
                            var vadebilgilerirec = serviceIntBes.getvadebilgilerirec(QueryStrings.PolID);
                            if (vadebilgilerirec != null && vadebilgilerirec.AVade != null && vadebilgilerirec.AVade.Count > 0)
                            {
                                policyBesDetail.Tahsilat = new cPolicyBesDetailTahsilatInfo();
                                policyBesDetail.Tahsilat.CollectionInfo = new List<cPolicyLifeDetailCollectionInfo>();

                                foreach (var item in vadebilgilerirec.AVade.Where(w => w.borctip == "Katkı Payı" || w.borctip == "Ek Yatırım").ToList())
                                {
                                    policyBesDetail.Tahsilat.CollectionInfo.Add(new cPolicyLifeDetailCollectionInfo
                                    {
                                        Makbuz = "/policy/pdf?type=bestahsilatmakbuz&makbuzno=" + item.makbuzno,
                                        MakbuzNo = item.makbuzno.HasValue ? item.makbuzno.Value.ToString() : "",
                                        Tahsilat = item.odenentutar.HasValue ? Utilities.Price(item.odenentutar.Value) : "",
                                        VadeTarihi = item.vadetar,
                                        VadeTutar = item.vadetutar.HasValue ? Utilities.Price(item.vadetutar.Value) : "",
                                        IntikalTarihi = item.intiktar,
                                        DKTutari = item.odenentutardk.HasValue ? Utilities.Price(item.odenentutardk.Value) : "",
                                        TahsilatTarihi = item.odevadetar
                                    });

                                    if (!policyBesDetail.Tahsilat.OdemeLink)
                                    {
                                        if (Convert.ToDateTime(item.vadetar) < Convert.ToDateTime(DateTime.Now.ToShortDateString()) && item.vadetutar > 0)
                                        {
                                            policyBesDetail.Tahsilat.OdemeLink = true;
                                        }
                                    }
                                }
                            }

                            var policebilgirec = serviceIntBes.getpolicebilgirec(QueryStrings.Pid);
                            if (policebilgirec != null)
                            {
                                if (policebilgirec.ASoz != null)
                                {
                                    var aSoz = policebilgirec.ASoz.Where(w => w.polid == QueryStrings.PolID).FirstOrDefault();
                                    if (aSoz != null)
                                    {
                                        if (aSoz.sozlesmedetay != null)
                                        {
                                            if (aSoz.sozlesmedetay.Count > 0)
                                            {
                                                if (policyBesDetail.Tahsilat == null)
                                                {
                                                    policyBesDetail.Tahsilat = new cPolicyBesDetailTahsilatInfo();
                                                }

                                                policyBesDetail.Tahsilat.TahsilatDurdurmaBaslangicTarihi = aSoz.sozlesmedetay[0].tahsdurbastar;
                                                policyBesDetail.Tahsilat.TahsilatDurdurmaBitisTarihi = aSoz.sozlesmedetay[0].tahsdurbittar;
                                                policyBesDetail.Tahsilat.TahsilatDurdurmaTalepTarihi = aSoz.sozlesmedetay[0].tahsdurtaleptar;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Yatırım
                        else if (strType == "yatirim")
                        {
                            var yatirimdetay = serviceIntBes.getyatirimdetay(QueryStrings.PolID);
                            if (yatirimdetay != null)
                            {
                                if (yatirimdetay.AYatirim != null)
                                {
                                    if (yatirimdetay.AYatirim.Count > 0)
                                    {
                                        policyBesDetail.InvestInfp = new cPolicyBesDetailInvestInfo();
                                        policyBesDetail.InvestInfp.Table = new List<cPolicyBesDetailInvestInfoTable>();

                                        foreach (var item in yatirimdetay.AYatirim)
                                        {
                                            policyBesDetail.InvestInfp.Table.Add(new cPolicyBesDetailInvestInfoTable
                                            {
                                                FonAdi = item.fonad,
                                                BirimFiyati = item.fonbirimfiyat.HasValue ? item.fonbirimfiyat.Value.ToString().Replace(".", ",") : "",
                                                Oran = item.fonoran.HasValue ? item.fonoran.Value.ToString() : "",
                                                PayAdedi = item.fonpayadet.HasValue ? item.fonpayadet.Value.ToString() : "",
                                                Tutar = item.fontutar.HasValue ? Utilities.Price(item.fontutar.Value) : ""
                                            });
                                        }
                                    }
                                }
                            }

                            var yatirimyontutar = serviceIntBes.getyatirimyontutar(QueryStrings.PolID);
                            if (yatirimyontutar != null)
                            {
                                if (policyBesDetail.InvestInfp == null)
                                {
                                    policyBesDetail.InvestInfp = new cPolicyBesDetailInvestInfo();
                                }

                                policyBesDetail.InvestInfp.DKHakedisTutari = yatirimyontutar.hakedistutdk.HasValue ? Utilities.Price(yatirimyontutar.hakedistutdk.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.OncekiSirkettekiBirikimTutari = yatirimyontutar.brkoncesitutar.HasValue ? Utilities.Price(yatirimyontutar.brkoncesitutar.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.ProvisyondaBekleyenveYatirimaYonlendirilemeyenTLTutar = yatirimyontutar.yatyonlenmeyentutar.HasValue ? Utilities.Price(yatirimyontutar.yatyonlenmeyentutar.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.TLBirikimDevletKatkisiDahil = yatirimyontutar.toplambirikimdk.HasValue ? Utilities.Price(yatirimyontutar.toplambirikimdk.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.TLBirikimDevletKatkisiHaric = yatirimyontutar.birikim.HasValue ? Utilities.Price(yatirimyontutar.birikim.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.TLTahsilati = yatirimyontutar.tahsilat.HasValue ? Utilities.Price(yatirimyontutar.tahsilat.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.ToplamDKBirikim = yatirimyontutar.birikimdk.HasValue ? Utilities.Price(yatirimyontutar.birikimdk.Value) + " TL" : "";
                                policyBesDetail.InvestInfp.ToplamDKTahsilati = yatirimyontutar.tahsilatdk.HasValue ? Utilities.Price(yatirimyontutar.tahsilatdk.Value) + " TL" : "";
                            }
                        }
                        #endregion
                        #region Musteri
                        else if (strType == "musteri")
                        {
                            var kisibilgirec = serviceFibaws.getkisibilgirec(QueryStrings.Pid);
                            if (kisibilgirec != null)
                            {
                                policyBesDetail.Musteri = new cPolicyDetailMusteri();
                                policyBesDetail.Musteri.Adres = kisibilgirec.kimlikevadres;
                                policyBesDetail.Musteri.Email = "";
                                policyBesDetail.Musteri.Tel = kisibilgirec.kimlikceptel;
                                policyBesDetail.Musteri.Tckn = kisibilgirec.kimlikno;
                            }
                        }
                        #endregion
                        serviceFibaws.Close();
                    }
                    serviceIntBes.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(policyBesDetail);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BesDetail(string tel, string email)
        {
            try
            {
                using (NTBESClient service = new NTBESClient())
                {
                    var adresdegisikligi = service.setadresdegisikligi(new IntbesAdresdegtypeUser
                    {
                        //emailbilgi = email,
                        pid = QueryStrings.Pid,
                        telbilgi = new IntbesadresdegtypeTelrectypeUser
                        {
                            eid = "E",
                            telno = tel,
                            teltip = "G",
                            pid = QueryStrings.Pid
                        }
                    });

                    if (adresdegisikligi.sonuc == "BAŞARILI")
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Info, "Müşteri bilgileri güncellendi.");
                    }
                    else
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Bir hata oluştu!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return Redirect("/Policy/BesDetail?pid=" + QueryStrings.Pid + "&polid=" + QueryStrings.PolID + "&type=musteri");
        }
        #endregion

        #endregion

        #region Pdf
        public ActionResult Pdf()
        {
            string strUrl = "";
            string strType = QueryStrings.Type;
            string strFileName = "";

            if (strType == "hayattahsilatmakbuz" || strType == "bestahsilatmakbuz")
            {
                strFileName = "makbuz";
                string strReport = "makbuzbas_hyt";
                if (strType == "bestahsilatmakbuz")
                {
                    strReport = "makbuzbas_bes";
                }

                strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=" + strReport + "&p_makbuzno=" + Request.QueryString["makbuzno"] + "&cmdkey=fibahayat&t=" + DateTime.Now.Ticks + "&server=" + ConfigurationManager.AppSettings["SalePdfserver"];
            }
            else if (strType == "list")
            {
                if (Request.QueryString["branskod"] == "E")
                {
                    strFileName = "sozlesme";
                    strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=sozlesme_bes_matbaa.rdf&desname=" + Request.QueryString["polid"] + DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") + ".pdf&server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + Request.QueryString["polid"];
                }
                else
                {
                    strFileName = "police";
                    strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&report=hytpoliceweb.rdf&desformat=PDF&destype=CACHE&p_polid=" + Request.QueryString["polid"] + "&cmdkey=fibahayat";
                }
            }
            else if (strType == "hesapbildirimcetveli")
            {
                string strGrupTip = Request.QueryString["gruptipi"];
                string strDevirmi = Request.QueryString["devirmi"];
                string strReport = "";
                strFileName = "hesapbildirimcetveli";

                if (strGrupTip == "İGES" && strDevirmi == "X")
                {
                    strReport = "hesapekstre_iges_aktarim.rdf";
                }
                else if (strGrupTip == "İGES" && string.IsNullOrEmpty(strDevirmi))
                {
                    strReport = "hesapekstre_iges_yeni.rdf";
                }
                else if (string.IsNullOrEmpty(strGrupTip) && strDevirmi == "X")
                {
                    strReport = "hesapekstre_ferdi_aktarim.rdf";
                }
                else if (string.IsNullOrEmpty(strGrupTip) && string.IsNullOrEmpty(strDevirmi))
                {
                    strReport = "hesapekstre_ferdi_yeni.rdf";
                }
                else if (strGrupTip == "GBB" && (string.IsNullOrEmpty(strDevirmi) || strDevirmi == "X"))
                {
                    strReport = "hesapekstre_ferdi_yeni.rdf";
                }
                else if (string.IsNullOrEmpty(strGrupTip) && strDevirmi == "P")
                {
                    strReport = "hesapekstre_ferdi_aktarim.rdf";
                }

                strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=" + strReport + "&desname=" + Request.QueryString["polid"] + DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") + ".pdf&server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + Request.QueryString["polid"];
            }

            Response.Clear();
            Response.ClearContent();
            Response.ContentEncoding = Encoding.GetEncoding("windows-1254");
            Response.Charset = "windows-1254";
            Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName + "_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
            Response.ContentType = "application/pdf";
            Response.Expires = 0;
            Response.Write(UrlGet(strUrl, "GET"));

            return View();
        }

        string UrlGet(string Url, string Method)
        {
            string result = "";
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
                myRequest.Method = Method;
                myRequest.ContentType = "application/pdf";

                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                Stream encodingStream = myResponse.GetResponseStream();
                StreamReader read = new StreamReader(encodingStream, Encoding.GetEncoding("utf-8"));
                result = read.ReadToEnd();
                myResponse.Close();
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                throw ex;
            }

            return result;
        }
        #endregion

        #region PaymentChange

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult PaymentChange()
        {
            cPaymentChange paymentChange = new cPaymentChange();
            paymentChange.banks = new List<cPaymentChangeValues>();
            paymentChange.branch = new List<cPaymentChangeValues>();

            if (Request.QueryString["getPublicKey"] == null)
            {
                if (Session["CurrentUser"] == null)
                {
                    return View(paymentChange);
                }

                if (string.IsNullOrEmpty(ViewBag.odemetip))
                {
                    ViewBag.odemetip = "kart";
                }

                paymentChange.banks = GetBanks(ViewBag.odemetip);

                if (!string.IsNullOrEmpty(ViewBag.bankano) && paymentChange.banks.Count > 0)
                {
                    paymentChange.branch = GetBranches(ViewBag.bankano);
                }
            }

            return View(paymentChange);
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult PaymentChange(string odemetip)
        {
            if (string.IsNullOrEmpty(odemetip))
            {
                return View();
            }

            ViewBag.odemetip = odemetip;
            ViewBag.polid = Request.Form["polid"];
            ViewBag.bankano = Request.Form["bankano"];
            ViewBag.subeno = Request.Form["subeno"];
            ViewBag.ad = Request.Form["ad"];
            ViewBag.soyad = Request.Form["soyad"];
            ViewBag.hesapgun = Request.Form["hesapgun"];
            ViewBag.kartno = Request.Form["kartno"];
            ViewBag.karttip = Request.Form["karttip"];
            ViewBag.cvv = Request.Form["cvv"];
            ViewBag.kartay = Request.Form["kartay"];
            ViewBag.kartyil = Request.Form["kartyil"];
            ViewBag.hesapno = Request.Form["hesapno"];
            ViewBag.hesapadi = Request.Form["hesapadi"];

            #region Validate
            string strMessage = "";
            if (string.IsNullOrEmpty(Request.Form["bankano"]))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Banka adı seçiniz!");

                return PaymentChange();
            }

            if (odemetip == "kart")
            {
                if (string.IsNullOrEmpty(Request.Form["ad"])
                    || string.IsNullOrEmpty(Request.Form["soyad"])
                        || string.IsNullOrEmpty(Request.Form["karttip"])
                            || string.IsNullOrEmpty(Request.Form["cvv"])
                                || string.IsNullOrEmpty(Request.Form["kartno"]))
                {
                    if (string.IsNullOrEmpty(Request.Form["ad"]))
                        strMessage = "Kart sahibinin adını giriniz!";
                    else if (string.IsNullOrEmpty(Request.Form["soyad"]))
                        strMessage = "Kart sahibinin soyadını giriniz!";
                    else if (string.IsNullOrEmpty(Request.Form["kartno"]))
                        strMessage = "Kart numarasını giriniz!";
                    else if (string.IsNullOrEmpty(Request.Form["karttip"]))
                        strMessage = "Kart tipi seçiniz!";
                    else if (string.IsNullOrEmpty(Request.Form["cvv"]))
                        strMessage = "Güvenlik kodunu giriniz!";
                }
            }
            else if (odemetip == "hesap")
            {
                if (string.IsNullOrEmpty(Request.Form["hesapno"]) || string.IsNullOrEmpty(Request.Form["hesapadi"]) || string.IsNullOrEmpty(Request.Form["subeno"]))
                {
                    if (string.IsNullOrEmpty(Request.Form["subeno"]))
                        strMessage = "Şube adını seçiniz!";
                    else if (string.IsNullOrEmpty(Request.Form["hesapno"]))
                        strMessage = "Hesap numarasını giriniz!";
                    else if (string.IsNullOrEmpty(Request.Form["hesapadi"]))
                        strMessage = "Hesap adını giriniz!";
                }
            }

            if (strMessage != "")
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, strMessage);

                return PaymentChange();
            }
            #endregion

            using (NTBESClient serviceIntBes = new NTBESClient())
            {
                IntbesPerkartrecUser pPerkart = null;
                IntbesPerhesaprecUser pPerhesap = null;

                if (odemetip == "kart")
                {
                    pPerkart = new IntbesPerkartrecUser
                    {
                        adi = Request.Form["ad"],
                        bankano = Utilities.NullFixDecimal(Request.Form["bankano"]),
                        cvv = Utilities.NullFixDecimal(Request.Form["cvv"]),
                        hesapgun = Utilities.NullFixDecimal(Request.Form["hesapgun"]),
                        kartno = Request.Form["kartno"],
                        karttip = Request.Form["karttip"],
                        sonkultar = Request.Form["kartay"] + Request.Form["kartyil"],
                        soyadi = Request.Form["soyad"]
                    };
                }
                else if (odemetip == "hesap")
                {
                    pPerhesap = new IntbesPerhesaprecUser
                    {
                        bankano = Utilities.NullFixDecimal(Request.Form["bankano"]),
                        ekno = "",
                        hesapadi = Request.Form["hesapadi"],
                        hesapno = Request.Form["hesapno"],
                        subeno = Utilities.NullFixDecimal(Request.Form["subeno"])
                    };
                }

                JavaScriptSerializer java = new JavaScriptSerializer();

                odemetip = odemetip == "kart" ? "K" : "O";
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, 0, "policesorgulama", "PaymentChange", "Input", "{ \"pPolid\" : \"" + Request.Form["polid"] + "\", \"pOdetip\" : \"" + odemetip + "\", \"pPerkart\": " + java.Serialize(pPerkart) + ", \"pPerhesap\" : " + java.Serialize(pPerhesap) + " }", "getodemearacdeg");

                var odemearacdeg = serviceIntBes.getodemearacdeg(Utilities.NullFixDecimal(Request.Form["polid"]), odemetip, pPerkart, pPerhesap);

                Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, 0, "policesorgulama", "PaymentChange", "Output", java.Serialize(odemearacdeg), "getodemearacdeg");

                if (odemearacdeg.Contains("Tamamlandi"))
                {
                    ViewBag.tamamlandi = "1";
                }
                else
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Bir hata oluştu! (Mesaj: " + odemearacdeg + ")");
                }
            }

            return PaymentChange();
        }
        #endregion

        #region GetBanks
        List<cPaymentChangeValues> GetBanks(string odemetip)
        {
            List<cPaymentChangeValues> banks = new List<cPaymentChangeValues>();

            using (FibaBES.FibaBesWebServiceClient serviceFibaBes = new FibaBES.FibaBesWebServiceClient())
            {
                var bankalar = serviceFibaBes.getbankalar();
                if (bankalar != null)
                {
                    if (bankalar.result != null)
                    {
                        foreach (var item in bankalar.result)
                        {
                            banks.Add(new cPaymentChangeValues
                            {
                                strName = item.extraf2,
                                strValue = item.extraf1
                            });
                        }

                        banks = banks.OrderBy(o => o.strName).ToList();
                    }
                }
            }

            if (odemetip == "hesap" && banks.Count > 0)
            {
                banks = banks.Where(w => w.strName.ToLower().Contains("fibabanka")
                    || w.strName.ToLower().Contains("akbank")
                        || w.strName.ToLower().Contains("şekerbank")
                            || w.strName.ToLower().Contains("garanti")).ToList();
            }

            return banks;
        }
        #endregion

        #region GetBranches
        List<cPaymentChangeValues> GetBranches(string bankano)
        {
            List<cPaymentChangeValues> branches = new List<cPaymentChangeValues>();

            using (NTBESClient serviceIntBes = new NTBESClient())
            {
                decimal bankID = Utilities.NullFixDecimal(bankano);

                var subebilgileri = serviceIntBes.getsubebilgileri(bankID);
                if (subebilgileri != null)
                {
                    if (subebilgileri.subeler != null)
                    {
                        foreach (var item in subebilgileri.subeler)
                        {
                            if (item.subeno.HasValue)
                            {
                                branches.Add(new cPaymentChangeValues
                                {
                                    strName = item.subeadi,
                                    strValue = item.subeno.Value.ToString()
                                });
                            }
                        }

                        branches = branches.OrderBy(o => o.strName).ToList();
                    }
                }
            }

            return branches;
        }
        #endregion

        #region Banks
        public JsonResult Banks(string odemetip)
        {
            var banks = GetBanks(odemetip);

            return Json(banks, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BankBranches
        public JsonResult BankBranches(string bankano)
        {
            var branches = GetBranches(bankano);
            if (branches != null)
            {
                branches = branches.OrderBy(o => o.strName).ToList();
            }

            return Json(branches, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region CollectionInfoSend

        #region Get
        [HttpGet]
        public ActionResult CollectionInfoSend()
        {
            cPolicyDetailInfoSend info = new cPolicyDetailInfoSend();

            try
            {
                info.SendType = string.IsNullOrEmpty(ViewBag.sendType) ? "1" : ViewBag.sendType;

                using (fibawsClient serviceFibaws = new fibawsClient())
                {
                    var kisibilgirec = serviceFibaws.getkisibilgirec(QueryStrings.Pid);
                    if (kisibilgirec != null)
                    {
                        info.Email = string.IsNullOrEmpty(ViewBag.email) ? "" : ViewBag.email;
                        info.Phone = string.IsNullOrEmpty(ViewBag.phone) ? kisibilgirec.kimlikceptel : ViewBag.phone;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(info);
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult CollectionInfoSend(string sendType, string phone, string email)
        {
            ViewBag.sendType = sendType;
            ViewBag.phone = phone;
            ViewBag.email = email;

            bool bolSend = false;
            try
            {
                using (fibawsClient serviceFibaws = new fibawsClient())
                {
                    var kisibilgirec = serviceFibaws.getkisibilgirec(QueryStrings.Pid);
                    if (kisibilgirec != null)
                    {
                        string strNameSurname = kisibilgirec.kimlikad + " " + kisibilgirec.kimliksoyad;
                        string strTCKN = cCrypto.EncryptDES(kisibilgirec.kimlikno.Trim());

                        string strToken = Utilities.RandomString(10);
                        string strProjectLink = ConfigurationManager.AppSettings["TahsilatProjeLink"] + "/" + strToken;

                        if (sendType == "1")
                        {
                            TuratelSms.Service smsService = new TuratelSms.Service(new TuratelSms.Data
                            {
                                ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                                Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                                PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                                Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                                UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                            });

                            string lResponseStr = smsService.Send("Sayın " + strNameSurname + ", Fiba Emeklilik BireyseL Emeklilik Sözleşmenizde ödenmemiş vadeleriniz bulunmaktadır. Ödeme yapmak için " + strProjectLink + " tıklayınız.", phone);

                            if (lResponseStr.Contains("ID"))
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Info, "Sms gönderildi.");
                                bolSend = true;
                            }
                            else
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Sms gönderilemedi! Telefon numarasını kontrol ettikten sonra tekrar deneyiniz.");
                            }
                        }
                        else
                        {
                            string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
                            strHtml = strHtml.Replace("#adsoyad#", strNameSurname);
                            strHtml = strHtml.Replace("#mesaj#", "<a href=\"" + strProjectLink + "\">" + strProjectLink + "</a> linki tıklayarak ödemenizi gerçekleştirebilirsiniz.");

                            bool bolMail = Email.SendEmail("Ödenmemiş tahsilatınız bulunmaktadır", strHtml, email);
                            if (bolMail)
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Info, "E-posta gönderildi.");
                                bolSend = true;
                            }
                            else
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta gönderilemedi! E-posta adresini kontrol ettikten sonra tekrar deneyiniz.");
                            }
                        }

                        using (DBEntities db = new DBEntities())
                        {
                            db.tblCollectionSendLogs.Add(new tblCollectionSendLogs
                            {
                                bolIsSend = bolSend,
                                dtRegisterDate = DateTime.Now,
                                intPolID = (int)QueryStrings.PolID,
                                intUserID = Sessions.CurrentUser.intUserID,
                                strMail = sendType == "2" ? email : "",
                                strPhone = sendType == "1" ? phone : "",
                                strSendType = sendType,
                                strToken = strToken,
                                strTCKN = strTCKN,
                                intCallcenterID = Sessions.CurrentUser.intCallcenterID
                            });
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kimlik bilgileri alınamadı! Tekrar deneyiniz.");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            if (bolSend)
            {
                return Redirect("/Policy/BesDetail?pid=" + QueryStrings.Pid + "&polid=" + QueryStrings.PolID + "&type=tahsilat");
            }
            else
            {
                return CollectionInfoSend();
            }
        }
        #endregion

        #endregion

        #region SendMsg
        [HttpGet]
        public JsonResult SendMsg(string policeno)
        {
            Utilities.MsgType msgType = Utilities.MsgType.Success;
            StringBuilder sb = new StringBuilder();

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    string strSaleType = "";

                    if (db.tblSaleLogPoliceler.Count(c => c.PoliceNo == policeno) > 0)
                    {
                        strSaleType = "life";
                    }
                    else if (db.tblCreditPacketPolicy.Count(c => c.strPolicyID == policeno) > 0)
                    {
                        strSaleType = "credit";
                    }

                    if (strSaleType == "life")
                    {
                        var salePolice = db.tblSaleLogPoliceler.Where(w => w.PoliceNo == policeno && w.strService == "policelesme").FirstOrDefault();
                        if (salePolice != null)
                        {
                            if (db.tblSaleLogPoliceler.Count(c => c.intLogID == salePolice.intLogID && c.strService == "teklifpolicelesme") > 0)
                            {
                                msgType = Utilities.MsgType.Info;

                                sb.Append("Poliçe'nin satışı tamamlanmış.");
                            }
                            else
                            {
                                Logs.ProductSaleMessageSend(salePolice.intLogID, true);

                                var saleLog_2 = db.tblSaleLogs.Where(w => w.intLogID == salePolice.intLogID).First();

                                if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormMail && !saleLog_2.tblSaleLogInfo.First().bolInfoFormSms)
                                {
                                    msgType = Utilities.MsgType.Warning;

                                    sb.Append("<p>Tanzim linki müşteriye e-posta veya sms ile gönderilemedi.</p>");
                                }
                                else
                                {
                                    msgType = Utilities.MsgType.Success;

                                    var saleInfo = saleLog_2.tblSaleLogInfo.First();
                                    if (saleInfo.InfoFormSendType == "sms")
                                    {
                                        if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormSms)
                                        {
                                            sb.Append("<p>Tanzim linki sms olarak gönderilemedi. E-posta adresine link gönderildi.</p>");
                                        }
                                        else
                                        {
                                            sb.Append("<p>Tanzim linki müşteriye sms olarak gönderildi.");
                                        }
                                    }
                                    else
                                    {
                                        if (!saleLog_2.tblSaleLogInfo.First().bolInfoFormMail)
                                        {
                                            sb.Append("<p>Tanzim linki e-posta olarak gönderilemedi. Telefon numarasına sms linki gönderildi.</p>");
                                        }
                                        else
                                        {
                                            sb.Append("<p>Tanzim linki müşteriye e-posta olarak gönderildi.</p>");
                                        }
                                    }
                                }

                                if (saleLog_2.tblSaleLogInfo.Count == 2)
                                {
                                    if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormMail && !saleLog_2.tblSaleLogInfo.Last().bolInfoFormSms)
                                    {
                                        msgType = Utilities.MsgType.Warning;
                                        sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta veya sms ile gönderilemedi.</p>");
                                    }
                                    else
                                    {
                                        msgType = Utilities.MsgType.Success;

                                        var saleInfo = saleLog_2.tblSaleLogInfo.Last();
                                        if (saleInfo.InfoFormSendType == "sms")
                                        {
                                            if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormSms)
                                            {
                                                sb.Append("<p>Tanzim linki ödeyen farklı kişiye sms olarak gönderilemedi. E-posta adresine link gönderildi.</p>");
                                            }
                                            else
                                            {
                                                sb.Append("<p>Tanzim linki ödeyen farklı kişiye sms olarak gönderildi.</p>");
                                            }
                                        }
                                        else
                                        {
                                            if (!saleLog_2.tblSaleLogInfo.Last().bolInfoFormMail)
                                            {
                                                sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta olarak gönderilemedi. Telefon numarasına sms linki gönderildi.</p>");
                                            }
                                            else
                                            {
                                                sb.Append("<p>Tanzim linki ödeyen farklı kişiye e-posta olarak gönderildi.</p>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            msgType = Utilities.MsgType.Warning;
                            sb.Append("Sistemde poliçeye ait kayıt bulunamadı!");
                        }
                    }
                    else if (strSaleType == "credit")
                    {
                        var creditPolicy = db.tblCreditPacketPolicy.Where(w => w.strPolicyID == policeno).FirstOrDefault();
                        if (creditPolicy != null)
                        {
                            if (db.tblCreditSaleLogInfo.Count(c => c.intLogID == creditPolicy.intCreditSaleLogID && c.bolCreditApprove) > 0)
                            {
                                msgType = Utilities.MsgType.Info;

                                sb.Append("Poliçe'nin satışı tamamlanmış.");
                            }
                            else
                            {
                                Logs.CreditSaleMessageSend(creditPolicy.intCreditSaleLogID, true);

                                var saleLogInfo = db.tblCreditSaleLogInfo.Where(w => w.intLogID == creditPolicy.intCreditSaleLogID).First();

                                if (!saleLogInfo.bolInfoFormMail && !saleLogInfo.bolInfoFormSms)
                                {
                                    msgType = Utilities.MsgType.Warning;

                                    sb.Append("<p>Tanzim linki müşteriye e-posta veya sms ile gönderilemedi.</p>");
                                }
                                else
                                {
                                    msgType = Utilities.MsgType.Success;
                                    
                                    if (saleLogInfo.InfoFormSendType == "sms")
                                    {
                                        if (!saleLogInfo.bolInfoFormSms)
                                        {
                                            sb.Append("<p>Tanzim linki sms olarak gönderilemedi. E-posta adresine link gönderildi.</p>");
                                        }
                                        else
                                        {
                                            sb.Append("<p>Tanzim linki müşteriye sms olarak gönderildi.");
                                        }
                                    }
                                    else
                                    {
                                        if (!saleLogInfo.bolInfoFormMail)
                                        {
                                            sb.Append("<p>Tanzim linki e-posta olarak gönderilemedi. Telefon numarasına sms linki gönderildi.</p>");
                                        }
                                        else
                                        {
                                            sb.Append("<p>Tanzim linki müşteriye e-posta olarak gönderildi.</p>");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            msgType = Utilities.MsgType.Warning;
                            sb.Append("Sistemde poliçeye ait kayıt bulunamadı!");
                        }
                    }
                    else
                    {
                        msgType = Utilities.MsgType.Warning;
                        sb.Append("Sistemde poliçeye ait kayıt bulunamadı!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);

                msgType = Utilities.MsgType.Error;
                sb.Append("Bir hata oluştu!");
            } 

            return Json(new
            {
                type = ((byte)msgType).ToString(),
                msg = sb.ToString()
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
