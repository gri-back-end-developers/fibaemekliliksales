﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BesFundController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intBesFundID = QueryStrings.ID;
                    if (intBesFundID > 0)
                    {
                        var besFund = db.tblBesFunds.Where(w => w.intBesFundID == intBesFundID).FirstOrDefault();
                        if (besFund != null)
                        {
                            db.tblBesFunds.Remove(besFund);
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intBesFundID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Fon silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Fon bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Fon bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesFund");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();
            
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                     
                    string strCode = formValues["strCode"];

                    if (db.tblBesFunds.Count(c => c.strCode == strCode) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı fon kodu sistemde mevcut!");
                        return RedirectToAction("New", "BesFund");
                    }

                    tblBesFunds besFund = new tblBesFunds();
                    besFund.strCode = strCode;
                    besFund.strName = formValues["strName"];
                    besFund.flYearlyRate = Utilities.NullFixDecimal(formValues["flYearlyRate"]);
                    besFund.flDailyRate = formValues["flDailyRate"].Replace(",", ".");
                    besFund.flYearlyRateFTGK = formValues["flYearlyRateFTGK"].Replace(",", ".");
                    besFund.dtRegisterDate = DateTime.Now;

                    db.tblBesFunds.Add(besFund);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, besFund.intBesFundID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Fon eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesFund");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();
             
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intBesFundID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    string strCode = formValues["strCode"];

                    if (db.tblBesFunds.Count(c => c.intBesFundID != intBesFundID && c.strCode == strCode) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı fon kodu sistemde mevcut!");
                        return RedirectToAction("Update", "BesFund", new { i = Request.QueryString["i"] });
                    }

                    var besFund = db.tblBesFunds.Where(w => w.intBesFundID == intBesFundID).FirstOrDefault();
                    besFund.strCode = strCode;
                    besFund.strName = formValues["strName"];
                    besFund.flYearlyRate = Utilities.NullFixDecimal(formValues["flYearlyRate"]);
                    besFund.flDailyRate = formValues["flDailyRate"].Replace(".", ",");
                    besFund.flYearlyRateFTGK = formValues["flYearlyRateFTGK"].Replace(",", ".");
                    besFund.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, besFund.intBesFundID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Fon güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesFund");
        }
        #endregion
    }
}
