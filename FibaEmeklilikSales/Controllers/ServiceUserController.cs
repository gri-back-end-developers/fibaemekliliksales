﻿using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FibaEmeklilikSales.Controllers
{
    public class ServiceUserController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intServiceUserID = QueryStrings.ID;
                    if (intServiceUserID > 0)
                    {
                        var user = db.tblServiceUsers.Where(w => w.intServiceUserID == intServiceUserID).FirstOrDefault();
                        if (user != null)
                        {
                            user.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intServiceUserID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kullanıcı bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kullanıcı bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ServiceUser");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "ServiceUser");
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    tblServiceUsers user = new tblServiceUsers();
                    user.strNameSurname = formValues["strNameSurname"];
                    user.strEmail = formValues["strEmail"];
                    user.strPassword = formValues["strPassword"];
                    user.strAgencyID = formValues["strAgencyID"];
                    user.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    user.bolIsDelete = false;
                    user.dtRegisterDate = DateTime.Now;

                    db.tblServiceUsers.Add(user);
                    db.SaveChanges();

                    ServiceUserPermisions(user.intServiceUserID);

                    Logs.Log(Enums.LogType.Add, user.intServiceUserID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ServiceUser");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "ServiceUser", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intServiceUserID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    var user = db.tblServiceUsers.Where(w => w.intServiceUserID == intServiceUserID).FirstOrDefault();
                    user.strNameSurname = formValues["strNameSurname"];
                    user.strEmail = formValues["strEmail"]; ;
                    user.strPassword = formValues["strPassword"];
                    user.strAgencyID = formValues["strAgencyID"];
                    user.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    user.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    ServiceUserPermisions(intServiceUserID);

                    Logs.Log(Enums.LogType.Update, user.intServiceUserID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ServiceUser");
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strNameSurname = formValues["strNameSurname"];
            if (strNameSurname.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad/soyad giriniz!");
                return false;
            }

            string strEmail = formValues["strEmail"];
            if (strEmail.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta giriniz!");
                return false;
            }

            if (!Utilities.IsEmail(strEmail))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta  adresini hatalı giriniz!");
                return false;
            }
            return true;
        }
        #endregion

        #region ServiceUserPermisions
        private void ServiceUserPermisions(int intServiceUserID)
        {
            var formValues = Request.Form;

            db.Database.ExecuteSqlCommand("DELETE FROM tblServiceUserPermisions WHERE intServiceUserID = {0}", intServiceUserID);
            db.SaveChanges();

            var serviceMethods = string.IsNullOrEmpty(formValues["hdServiceMethods"]) ? new List<string>() : formValues["hdServiceMethods"].Split(',').ToList();
            foreach (var item in serviceMethods)
            {
                tblServiceUserPermisions serviceUserPermision = new tblServiceUserPermisions();
                serviceUserPermision.intServiceUserID = intServiceUserID;
                serviceUserPermision.intServiceMethodID = Utilities.NullFixByte(cCrypto.DecryptText(item));

                db.tblServiceUserPermisions.Add(serviceUserPermision);
            }
            db.SaveChanges(); 
        }
        #endregion
    }
}
