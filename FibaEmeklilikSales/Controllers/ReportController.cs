﻿using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Controllers
{
    public class ReportController : Controller
    {
        #region List
        [UserFilter]
        public ActionResult List()
        {
            cReport report = new cReport()
            {
                reports = new List<tblReports>(),
                columns = new List<cReportColumns>(),
                data = null,
                intCount = 0,
                bolExcel = false
            };

            if (Sessions.CurrentUser.tblCallcenters != null)
            {
                if (!string.IsNullOrEmpty(Sessions.CurrentUser.tblCallcenters.strReportPermisions))
                {
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    var reportPermision = jss.Deserialize<List<cReportPermisions>>(Sessions.CurrentUser.tblCallcenters.strReportPermisions);

                    report.bolExcel = reportPermision.First(f => f.UserType == (Enums.UserType)Sessions.CurrentUser.intUserTypeID).bolExcel;
                }
            }

            DBEntities db = new DBEntities();
            report.reports = db.tblReports.OrderByDescending(o => o.dtUpdateDate.HasValue ? o.dtUpdateDate.Value : o.dtRegisterDate).ToList();

            int intReportID = Utilities.NullFixInt(Request.QueryString["reportid"]);
            string strFile = "";

            if (intReportID == 0)
            {
                strFile = report.reports.FirstOrDefault() != null ? report.reports.First().strFile : "";
            }
            else
            {
                strFile = report.reports.Where(w => w.intReportID == intReportID).First().strFile;
            }

            if (!Sessions.CurrentUser.tblUserTypes.bolPowerAdmin && (Enums.UserType)Sessions.CurrentUser.intUserTypeID != Enums.UserType.ReportUser)
            {
                DataTable dtExcel = null;

                try
                {
                    report.reports = report.reports.Where(w => w.intCallcenterType != 0 ? w.intCallcenterType == Sessions.CurrentUser.tblCallcenters.intCallcenterType : true).ToList();

                    if (report.reports.Count == 0)
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Rapor bulunamadı!");
                        return View(report);
                    }

                    string strColumns = Request.QueryString["columns"];
                    string strOrder = Request.QueryString["order"];
                    string strOrderType = Request.QueryString["ordertype"];
                    string strSearch = QueryStrings.search;

                    DataTable dt = null;
                    if (Request.QueryString["page"] != null)
                    {
                        dt = Sessions.ReportData;
                    }
                    else
                    {
                        Sessions.ReportData = null;
                    }

                    if (dt == null)
                    {
                        cExcel excel = new cExcel();
                        dt = excel.Read(Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"] + strFile);
                    }

                    if (dt != null)
                    {
                        Sessions.ReportData = dt;

                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            if (dt.Columns[i].ColumnName.Trim().ToLower() != "acenteno")
                            {
                                string strColumnName = Utilities.SearchClear(dt.Columns[i].ColumnName).ToLower();
                                report.columns.Add(new cReportColumns
                                {
                                    id = strColumnName,
                                    name = dt.Columns[i].ColumnName
                                });

                                dt.Columns[i].ColumnName = strColumnName;
                            }
                        }

                        string strOrderText = "";
                        if (!string.IsNullOrEmpty(strOrder))
                        {
                            strOrderText = strOrder + " " + strOrderType;
                        }

                        try
                        {
                            dt = dt.Select("acenteno = '" + Sessions.CurrentUser.tblCallcenters.strCallcenterCode.Trim() + "'").CopyToDataTable();
                        }
                        catch
                        {
                            dt = null;
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Acenteno ya ait kayıt bulunamadı!");
                        }

                        if (dt == null)
                            return View(report);

                        try
                        {
                            dt.Columns.Remove("acenteno");
                        }
                        catch
                        { }

                        if (!string.IsNullOrEmpty(strSearch))
                        {
                            try
                            {
                                dt = dt.Select(strColumns + " like '%" + strSearch + "%'").CopyToDataTable();
                            }
                            catch
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kayıt bulunamadı!");
                                dt = null;
                            }
                        }

                        if (dt == null)
                            return View(report);

                        dtExcel = dt.Copy();

                        int intPageSize = 20;
                        report.intCount = (dt.Rows.Count / intPageSize) + (dt.Rows.Count % intPageSize > 0 ? 1 : 0);

                        int intPage = QueryStrings.page;
                        intPage = intPage > 0 ? intPage - 1 : intPage;

                        try
                        {
                            report.data = dt.Select("", strOrderText).Skip(intPage * intPageSize).Take(intPageSize).CopyToDataTable();
                        }
                        catch { }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Error(ex);
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu. Dosyada problem olabilir kontrol ediniz.");
                }

                if (report.data == null)
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kayıt bulunamadı!");
                }

                if (Request.QueryString["btntype"] == "excel" && string.IsNullOrEmpty(Sessions.Message) && dtExcel != null)
                {
                    try
                    {
                        for (int i = 0; i < dtExcel.Columns.Count; i++)
                        {
                            var column = report.columns.FirstOrDefault(f => f.id == dtExcel.Columns[i].ColumnName);
                            if (column != null)
                            {
                                dtExcel.Columns[i].ColumnName = column.name;
                            }
                        }

                        ExcelExportSheet excel = new ExcelExportSheet();
                        excel.ToExcel(dtExcel, strFile.Replace(".xlsx", ".xls"), this.Response, "");
                    }
                    catch (Exception ex)
                    {
                        Logs.Error(ex);
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu. Tekrar deneyiniz.");
                    }
                }
            }
            else
            {
                int intPageSize = 20;
                report.intCount = (report.reports.Count / intPageSize) + (report.reports.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                report.reports = report.reports.Skip(intPage * intPageSize).Take(intPageSize).ToList();
            }

            return View(report);
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    DBEntities db = new DBEntities();

                    int intReportID = QueryStrings.ID;
                    if (intReportID > 0)
                    {
                        var report = db.tblReports.Where(w => w.intReportID == intReportID).FirstOrDefault();
                        if (report != null)
                        {
                            try
                            {
                                string strPath = Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"];
                                System.IO.File.Delete(strPath + report.strFile);
                            }
                            catch { }

                            db.tblReports.Remove(report);
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intReportID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Rapor silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Rapor bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Rapor bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Report");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(HttpPostedFileBase file = null)
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    bool bolEx = false;
                    var ExtList = ("xls,xlsx").Split(',');
                    foreach (var item in ExtList)
                    {
                        if (Path.GetExtension(file.FileName).ToLower() == "." + item)
                        {
                            bolEx = true;
                            break;
                        }
                    }

                    if (bolEx)
                    {
                        using (DBEntities db = new DBEntities())
                        {
                            string strName = Request.Form["strName"];
                            if (db.tblReports.Count(c => c.strName == strName) > 0)
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı rapor adı sistemde mevcut!");
                                return RedirectToAction("New", "Report");
                            }

                            string strPath = Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"];
                            string strFileName = Utilities.Friendly(strName) + Path.GetExtension(file.FileName);

                            tblReports report = new tblReports
                            {
                                bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true",
                                dtRegisterDate = DateTime.Now,
                                strFile = strFileName,
                                strName = strName,
                                intCallcenterType = Utilities.NullFixByte(Request.Form["intCallcenterType"])
                            };

                            db.tblReports.Add(report);
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Add, report.intReportID);

                            file.SaveAs(strPath + strFileName);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Rapor eklendi.");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Dosya formatını hatalı seçtiniz!");
                        return RedirectToAction("New", "Report");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Report");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(HttpPostedFileBase file = null)
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (file != null)
                    {
                        bool bolEx = false;
                        var ExtList = ("xls,xlsx").Split(',');
                        foreach (var item in ExtList)
                        {
                            if (Path.GetExtension(file.FileName).ToLower() == "." + item)
                            {
                                bolEx = true;
                                break;
                            }
                        }

                        if (!bolEx)
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Dosya formatını hatalı seçtiniz!");
                            return RedirectToAction("Update", "Report", new { i = Request.QueryString["i"] });
                        }
                    }

                    short intReportID = Utilities.NullFixShort(cCrypto.DecryptText(Request.Form["hdID"]));
                    using (DBEntities db = new DBEntities())
                    {
                        string strName = Request.Form["strName"];
                        if (db.tblReports.Count(c => c.strName == strName && c.intReportID != intReportID) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı rapor adı sistemde mevcut!");
                            return RedirectToAction("Update", "Report", new { i = Request.QueryString["i"] });
                        }

                        var report = db.tblReports.Where(w => w.intReportID == intReportID).First();
                        report.bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true";
                        report.dtUpdateDate = DateTime.Now;
                        report.intCallcenterType = Utilities.NullFixByte(Request.Form["intCallcenterType"]);

                        string strFileName = report.strFile;
                        if (file != null)
                        {
                            string strPath = Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"];
                            strFileName = Utilities.Friendly(strName) + Path.GetExtension(file.FileName);

                            file.SaveAs(strPath + strFileName);
                        }
                        else if (report.strName != strName)
                        {
                            string strPath = Utilities.AppPathServer + ConfigurationManager.AppSettings["reportfile"];
                            strFileName = Utilities.Friendly(strName) + Path.GetExtension(report.strFile);

                            System.IO.File.Copy(strPath + report.strFile, strPath + strFileName);
                            System.IO.File.Delete(strPath + report.strFile);
                        }

                        report.strFile = strFileName;
                        report.strName = strName;

                        db.SaveChanges();

                        Logs.Log(Enums.LogType.Update, intReportID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Rapor güncellendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Report");
        }
        #endregion
    }
}
