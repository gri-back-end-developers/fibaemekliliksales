﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class PacketController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intPacketID = QueryStrings.ID;
                    if (intPacketID > 0)
                    {
                        var packet = db.tblPackets.Where(w => w.intPacketID == intPacketID).FirstOrDefault();
                        if (packet != null)
                        {
                            packet.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intPacketID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Paket bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Paket bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Packet", new { pi = Request.QueryString["pi"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "Packet", new { pi = Request.QueryString["pi"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    int intProductID = QueryStrings.ProductID;
                    string strName = formValues["strName"];

                    if (db.tblPackets.Count(c => !c.bolIsDelete && c.intProductID == intProductID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ürüne ait aynı paket başlığı sistemde mevcut!");
                        return RedirectToAction("New", "Packet", new { pi = Request.QueryString["pi"] });
                    }

                    tblPackets packet = new tblPackets();
                    packet.strName = formValues["strName"];
                    packet.strComment = formValues["strComment"];
                    packet.intProductID = intProductID;
                    packet.strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
                    packet.strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
                    packet.intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
                    packet.intTime = Utilities.NullFixInt(formValues["intTime"]);
                    packet.intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
                    packet.strCrediCurrencyType = formValues["strCrediCurrencyType"];
                    packet.flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
                    packet.bolIsDelete = false;
                    packet.dtRegisterDate = DateTime.Now;

                    db.tblPackets.Add(packet);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, packet.intPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Packet", new { pi = Request.QueryString["pi"] });
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Packet", new { pi = Request.QueryString["pi"], i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intPacketID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    int intProductID = QueryStrings.ProductID;
                    string strName = formValues["strName"];

                    if (db.tblPackets.Count(c => !c.bolIsDelete && c.intProductID == intProductID && c.intPacketID != intPacketID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ürüne ait aynı paket adı sistemde mevcut!");
                        return RedirectToAction("Update", "Packet", new { pi = Request.QueryString["pi"], i = Request.QueryString["i"] });
                    }

                    var packet = db.tblPackets.Where(w => w.intPacketID == intPacketID).FirstOrDefault();
                    packet.strName = strName;
                    packet.strComment = formValues["strComment"];
                    packet.strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
                    packet.strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
                    packet.intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
                    packet.intTime = Utilities.NullFixInt(formValues["intTime"]);
                    packet.intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
                    packet.strCrediCurrencyType = formValues["strCrediCurrencyType"];
                    packet.flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
                    packet.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, packet.intPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Packet", new { pi = Request.QueryString["pi"] });
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strName = formValues["strName"];
            if (strName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            string strComment = formValues["strComment"];
            if (strComment.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                return false;
            }

            string strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
            if (strIntegrationTariffCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Entegrasyon tarife kodu giriniz!");
                return false;
            }

            string strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
            if (strPremiumPaymentPeriod.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Prim ödeme periyodu giriniz!");
                return false;
            }

            int intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
            if (intPremiumPaymentTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Prim ödeme süresi giriniz!");
                return false;
            }

            int intTime = Utilities.NullFixInt(formValues["intTime"]);
            if (intTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Süre giriniz!");
                return false;
            }

            int intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
            if (intCrediTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi süresi giriniz!");
                return false;
            }

            string strCrediCurrencyType = formValues["strCrediCurrencyType"];
            if (strCrediCurrencyType.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi döviz tipini giriniz!");
                return false;
            }

            decimal flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
            if (flCrediReturnPrice == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi dönüş tutarı giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
