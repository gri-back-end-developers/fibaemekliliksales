﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using System.Web.Services; 
using System.Configuration;
using FibaEmeklilikSales.Classes.Main;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class MainController : Controller
    {
        #region Index
        [HttpGet]
        [UserFilter]
        public ActionResult Index()
        {
            cMain main = new cMain();
            main.notifications = new List<tblNotifications>();

            try
            {
                if (Session["sale"] != null)
                {
                    Session["sale"] = null;
                }

                int intCallcenterID = Sessions.CurrentUser.intCallcenterID;

                using (DBEntities db = new DBEntities())
                {
                    if (Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage)
                    {
                        main.bolProducts = db.tblProducts.Count(c => !c.bolPersonalProduct && !c.bolScratchWin && !c.bolIsDelete && c.bolIsActive && c.bolSpecialSale == Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage && c.tblPackets.Count(cp => !cp.bolIsDelete && cp.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0) > 0;
                    }
                    else if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                    {
                        main.bolProducts = db.tblProducts.Count(c => !c.bolPersonalProduct && !c.bolIsDelete && c.bolIsActive && c.tblPackets.Count(cp => !cp.bolIsDelete && cp.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0) > 0;

                        main.bolPersonalProducts = db.tblProducts.Count(c => c.bolPersonalProduct && !c.bolIsDelete && c.bolIsActive && c.tblPackets.Count(cp => !cp.bolIsDelete && cp.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0) > 0;

                        main.bolBesPlans = db.tblBesPlans.Count(c => !c.bolIsDelete && c.bolIsActive) > 0;

                        main.bolCredit = db.tblCredits.Count(c => !c.bolIsDelete && c.bolIsActive && c.tblCreditPackets.Count(cp => !cp.bolIsDelete && cp.tblCreditAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0) > 0;

                        main.bolFibabankProduct = db.tblFibabankProducts.Count(c => !c.bolIsDelete && c.bolIsActive) > 0; 
                    }
                    else
                    {
                        main.bolProducts = db.tblProducts.Count(c => !c.bolPersonalProduct && !c.bolScratchWin && !c.bolIsDelete && c.tblPackets.Count(cp => !cp.bolIsDelete && cp.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && c.bolIsActive && c.tblCallcenterProducts.Count(cp => cp.intProductID == c.intProductID && cp.intCallcenterID == intCallcenterID) > 0) > 0;

                        main.bolPersonalProducts = db.tblProducts.Count(c => c.bolPersonalProduct && !c.bolScratchWin && !c.bolIsDelete && c.tblPackets.Count(cp => !cp.bolIsDelete && cp.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && c.bolIsActive && c.tblCallcenterProducts.Count(cp => cp.intProductID == c.intProductID && cp.intCallcenterID == intCallcenterID) > 0) > 0;

                        main.bolBesPlans = db.tblBesPlans.Count(c => !c.bolIsDelete && c.bolIsActive && c.tblCallcenterBesPlans.Count(cb => cb.intBesPlanID == c.intBesPlanID && cb.intCallcenterID == intCallcenterID) > 0) > 0;

                        main.bolCredit = db.tblCredits.Count(c => !c.bolIsDelete && c.bolIsActive && c.tblCreditPackets.Count(cp => !cp.bolIsDelete && cp.tblCreditAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && c.tblCallcenterCredits.Count(cp => cp.intCreditID == c.intCreditID && cp.intCallcenterID == intCallcenterID) > 0) > 0;

                        main.bolFibabankProduct = db.tblFibabankProducts.Count(c => !c.bolIsDelete && c.bolIsActive && c.tblCallcenterFibabankProducts.Count(cb => cb.intFibabankProductID == c.intFibabankProductID && cb.intCallcenterID == intCallcenterID) > 0) > 0;

                        var notifications = db.tblNotifications.Where(w => w.tblNotificationGroups.tblNotificationGroupCallenters.Count(c => c.intCallcenterID == intCallcenterID) > 0 && w.bolIsActive && !w.bolIsDelete && w.dtStartDate <= DateTime.Now && w.dtFinishDate >= DateTime.Now).ToList();
                        
                        var notificationsByOrder = notifications.Where(w => w.intOrder > 0).OrderBy(o => o.intOrder).ToList(); 
                        main.notifications.AddRange(notificationsByOrder);

                        var notificationsByDate = notifications.Where(w => w.intOrder == 0).OrderByDescending(o => o.dtStartDate).ToList();
                        main.notifications.AddRange(notificationsByDate);                        
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(main);
        }
        #endregion

        #region BesPlans
        [HttpGet]
        [UserFilter]
        public ActionResult BesPlans()
        {
            cBesPlans main = new cBesPlans();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    List<tblBesPlans> besPlans = new List<tblBesPlans>();

                    bool bolPowerAdmin = Sessions.CurrentUser.tblUserTypes.bolPowerAdmin;
                    bool bolResponse = PageControl.Get;
                    if (bolResponse)
                    {
                        if (bolPowerAdmin)
                        {
                            besPlans.AddRange(db.tblBesPlans.Where(w => !w.bolIsDelete && w.bolIsActive).ToList());
                        }
                        else
                        {
                            besPlans.AddRange(db.tblBesPlans.Where(w => !w.bolIsDelete && w.bolIsActive && w.tblCallcenterBesPlans.Count(c => !c.bolSpecialSale && c.intBesPlanID == w.intBesPlanID && c.intCallcenterID == Sessions.CurrentUser.intCallcenterID) > 0).ToList());
                        }
                    }

                    int intPageSize = 20;
                    main.intCount = (besPlans.Count / intPageSize) + (besPlans.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    main.besPlans = besPlans.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(main);
        }
        #endregion

        #region Products
        [HttpGet]
        [UserFilter]
        public ActionResult Products()
        {
            cProducts main = new cProducts();

            try
            {
                DBEntities db = new DBEntities();
                List<tblProducts> products = new List<tblProducts>();

                bool bolPowerAdmin = Sessions.CurrentUser.tblUserTypes.bolPowerAdmin;
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin || Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage)
                    {
                        products.AddRange(db.tblProducts.Where(w => (Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage ? !w.bolScratchWin : true) && !w.bolIsDelete && w.bolIsActive && w.bolSpecialSale == Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage && w.tblPackets.Count(c => !c.bolIsDelete && c.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && w.bolPersonalProduct == QueryStrings.PersonalProduct).ToList());
                    }
                    else
                    {
                        products.AddRange(db.tblProducts.Where(w => !w.bolScratchWin && !w.bolIsDelete && w.tblPackets.Count(c => !c.bolIsDelete && c.tblAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && w.bolIsActive && w.tblCallcenterProducts.Count(c => !c.bolSpecialSale && c.intProductID == w.intProductID && c.intCallcenterID == Sessions.CurrentUser.intCallcenterID) > 0 && w.bolPersonalProduct == QueryStrings.PersonalProduct).ToList());
                    }
                }

                int intPageSize = 20;
                main.intCount = (products.Count / intPageSize) + (products.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                main.products = products.Skip(intPage * intPageSize).Take(intPageSize).ToList();
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(main);
        }
        #endregion

        #region Credits
        [HttpGet]
        [UserFilter]
        public ActionResult Credits()
        {
            cCredits main = new cCredits();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    List<tblCredits> credits = new List<tblCredits>();

                    bool bolResponse = PageControl.Get;
                    if (bolResponse)
                    {
                        if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                        {
                            credits.AddRange(db.tblCredits.Where(w => !w.bolIsDelete && w.bolIsActive && w.tblCreditPackets.Count(c => !c.bolIsDelete && c.tblCreditAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0).ToList());
                        }
                        else
                        {
                            credits.AddRange(db.tblCredits.Where(w => !w.bolIsDelete && w.tblCreditPackets.Count(c => !c.bolIsDelete && c.tblCreditAssurances.Count(ca => !ca.bolIsDelete) > 0) > 0 && w.bolIsActive && w.tblCallcenterCredits.Count(c => !c.bolSpecialSale && c.intCreditID == w.intCreditID && c.intCallcenterID == Sessions.CurrentUser.intCallcenterID) > 0).ToList());
                        }
                    }

                    int intPageSize = 20;
                    main.intCount = (credits.Count / intPageSize) + (credits.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    main.credits = credits.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                    main.creditTypes = db.tblCreditTypes.ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View(main);
        }
        #endregion

        #region FibabankProducts
        [HttpGet]
        [UserFilter]
        public ActionResult FibabankProducts()
        {
            return View();
        }
        #endregion

        #region ProductComment
        [HttpGet]
        public ActionResult ProductComment()
        {
            return View();
        }
        #endregion

        #region BesPlanComment
        [HttpGet]
        public ActionResult BesPlanComment()
        {
            return View();
        }
        #endregion

        #region CreditComment
        [HttpGet]
        public ActionResult CreditComment()
        {
            return View();
        }
        #endregion

        #region SessionCloseTime
        [WebMethod(EnableSession = true)]
        public void SessionCloseTime()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
        }
        #endregion

        #region SendForm
        public string SendForm(string packets, string namesurname, string tckn, string phone, string comment)
        {
            try
            {
                List<int> list = new List<int>();
                var strPackets = packets.Split(',');
                foreach (var item in strPackets)
                {
                    list.Add(Utilities.NullFixInt(cCrypto.DecryptText(item)));
                }

                string strHtml = "";

                using (DBEntities db = new DBEntities())
                {
                    foreach (var item in list)
                    {
                        var packet = db.tblPackets.Where(w => w.intPacketID == item).FirstOrDefault();
                        if (packet != null)
                        {
                            strHtml += "<p><strong>Ürün / Paket : </strong>" + packet.tblProducts.strName + " / " + packet.strName + "</p>";
                        }
                    }

                    strHtml += "<p><strong>Satış Kanalı : </strong>" + Sessions.CurrentUser.tblCallcenters.strName + "</p>";
                    strHtml += "<p><strong>Müşteri Ad Soyad : </strong>" + namesurname + "</p>";
                    strHtml += "<p><strong>Müşteri Tckn : </strong>" + tckn + "</p>";
                    strHtml += "<p><strong>Müşteri Telefon No : </strong>" + phone + "</p>";
                    strHtml += "<p><strong>Açıklama : </strong>" + comment + "</p>";

                    //string[] strEmails = ConfigurationManager.AppSettings["ProductYesAnswerMails"].Split(',');
                    //foreach (var item in strEmails)
                    //{
                    //Email.SendEmail("Eacentede Sağlık Sorunu Nedeniyle İşleme Devam Edilemedi", strHtml, item);
                 bool bolMailSend = Email.SendCRM(ConfigurationManager.AppSettings["crmSaglikSorunuOwnerId"].ToString(), ConfigurationManager.AppSettings["crmSaglikSorunuQueueId"].ToString(), ConfigurationManager.AppSettings["crmSaglikSorunuPhoneCallType"].ToString(), Utilities.NameSplit(namesurname, 1), Utilities.NameSplit(namesurname, 2), "", phone, strHtml.Replace("<p><strong>", Environment.NewLine).Replace("</strong>", "").Replace("</p>", ""), "Eacentede Sağlık Sorunu Nedeniyle İşleme Devam Edilemedi");
                    //}
                     
                    db.tblSaleLogHealthLogs.Add(new tblSaleLogHealthLogs
                    {
                        bolMailSend = bolMailSend,
                        dtRegisterDate = DateTime.Now,
                        strInfo = comment,
                        strIP = Utilities.ClientIP,
                        strNameSurname = namesurname,
                        strPhone = phone,
                        strTckn = tckn,
                        strHealthAnswer = "Evet"
                    });
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);

                return "0";
            }

            return "1";
        }
        #endregion
    }
}
