﻿using FibaEmeklilikSales.Classes; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BesPlanController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intBesPlanID = QueryStrings.ID;
                    if (intBesPlanID > 0)
                    {
                        var besPlan = db.tblBesPlans.Where(w => w.intBesPlanID == intBesPlanID).FirstOrDefault();
                        if (besPlan != null)
                        {
                            bool bolSaleLog = db.tblBesSaleLogs.Where(w => !w.tblUsers.bolIsDelete && !w.tblUsers.tblCallcenters.bolIsDelete && w.intBesPlanID == intBesPlanID).FirstOrDefault() == null;
                            bool bolWebSaleLog = db.tblWebSaleLogs.Where(w => w.intBesPlanID == intBesPlanID && w.bolIsFinished).FirstOrDefault() == null;

                            if (bolSaleLog && bolWebSaleLog)
                            {
                                besPlan.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intBesPlanID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Bes plan silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Bes plana ait satış işlemleri var!");
                            }
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Bes plan bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Bes plan bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesPlan", new { txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "", List<HttpPostedFileBase> files = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "BesPlan");
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strPlanName = formValues["strPlanName"];

                    if (db.tblBesPlans.Count(c => !c.bolIsDelete && c.strPlanName == strPlanName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı plan adı sistemde mevcut!");
                        return RedirectToAction("List", "BesPlan");
                    }

                    tblBesPlans besPlan = new tblBesPlans();
                    besPlan.intCallcenterID = Sessions.CurrentUser.intCallcenterID;
                    besPlan.bolInitialCont = formValues["bolInitialCont"] == "on" || formValues["bolInitialCont"] == "checked" || formValues["bolInitialCont"] == "true";
                    besPlan.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    besPlan.flInitialCont = Utilities.NullFixDecimal(formValues["flInitialCont"]);
                    besPlan.flMinAddContAmount = Utilities.NullFixDecimal(formValues["flMinAddContAmount"]);
                    besPlan.flMinContAmount = Utilities.NullFixDecimal(formValues["flMinContAmount"]);
                    besPlan.strComment = formValues["strComment"];
                    besPlan.strScript = formValues["strScript"];
                    besPlan.strPlanCode = formValues["strPlanCode"];
                    besPlan.strPlanName = formValues["strPlanName"];
                    besPlan.bolIsDelete = false;
                    besPlan.dtRegisterDate = DateTime.Now;
                    besPlan.bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";

                    if (Utilities.NullFixInt(formValues["intGroupID"]) > 0)
                    {
                        besPlan.intGroupID =  Utilities.NullFixInt(formValues["intGroupID"]);
                    }
                    else
                    {
                        besPlan.intGroupID = null;
                    }

                    db.tblBesPlans.Add(besPlan);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, besPlan.intBesPlanID);
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Plan eklendi.");

                    for (int i = files.Count - 1; i >= 0; i--)
                    {
                        if (files[i] == null)
                        {
                            files.RemoveAt(i);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hdFiles"]))
                            {
                                var hdFiles = formValues["hdFiles"].Split(',');
                                if (hdFiles.Count(c => c == files[i].FileName) == 0)
                                {
                                    files.RemoveAt(i);
                                }
                            }
                        }
                    }

                    if (files.Count > 0)
                    {
                        bool bolEx = false;
                        for (int i = 0; i < files.Count; i++)
                        {
                            if (Path.GetExtension(files[i].FileName) != ".pdf")
                            {
                                bolEx = true;
                                break;
                            }
                        }

                        if (!bolEx)
                        {
                            string strBesPlanFile = System.Configuration.ConfigurationManager.AppSettings["BesPlanFile"];

                            foreach (var item in files)
                            {
                                if (item != null)
                                {
                                    string strFileName = Utilities.RandomString(20) + Path.GetExtension(item.FileName);

                                    db.tblBesPlanFiles.Add(new tblBesPlanFiles
                                    {
                                        intBesPlanID = besPlan.intBesPlanID,
                                        strFile = item.FileName,
                                        strName = strFileName
                                    });

                                    item.SaveAs(Utilities.AppPathServer + strBesPlanFile + strFileName);
                                }
                            }
                            db.SaveChanges();
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Plan eklendi fakat dosyalar pdf uzantılı olmadığı için eklenmedi!");
                        }
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesPlan");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "", List<HttpPostedFileBase> files = null, HttpPostedFileBase imagetable = null, HttpPostedFileBase image = null, HttpPostedFileBase imagethumb = null, HttpPostedFileBase video = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "BesPlan", new { i = Request.QueryString["i"], type = Request.QueryString["type"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intBesPlanID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    string strPlanName = formValues["strPlanName"];

                    if (db.tblBesPlans.Count(c => !c.bolIsDelete && c.intBesPlanID != intBesPlanID && c.strPlanName == strPlanName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı plan adı sistemde mevcut!");
                        return RedirectToAction("Update", "BesPlan", new { i = Request.QueryString["i"], type = Request.QueryString["type"] });
                    }

                    var besPlan = db.tblBesPlans.Where(w => w.intBesPlanID == intBesPlanID).FirstOrDefault();

                    if (Request.QueryString["type"] == null)
                    {
                        besPlan.bolInitialCont = formValues["bolInitialCont"] == "on" || formValues["bolInitialCont"] == "checked" || formValues["bolInitialCont"] == "true";
                        besPlan.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                        besPlan.flInitialCont = Utilities.NullFixDecimal(formValues["flInitialCont"]);
                        besPlan.flMinAddContAmount = Utilities.NullFixDecimal(formValues["flMinAddContAmount"]);
                        besPlan.flMinContAmount = Utilities.NullFixDecimal(formValues["flMinContAmount"]);
                        besPlan.strComment = formValues["strComment"];
                        besPlan.strScript = formValues["strScript"];
                        besPlan.strPlanCode = formValues["strPlanCode"];
                        besPlan.strPlanName = formValues["strPlanName"];
                        besPlan.bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";

                        if (Utilities.NullFixInt(formValues["intGroupID"]) > 0)
                        {
                            besPlan.intGroupID = Utilities.NullFixInt(formValues["intGroupID"]);
                        }
                        else
                        {
                            besPlan.intGroupID = null;
                        }
                    }
                    else
                    {
                        besPlan.bolPublished = formValues["bolPublished"] == "on" || formValues["bolPublished"] == "checked" || formValues["bolPublished"] == "true";
                        besPlan.bolSlider = formValues["bolSlider"] == "on" || formValues["bolSlider"] == "checked" || formValues["bolSlider"] == "true";
                        besPlan.strPublishComment = formValues["strPublishComment"];
                        besPlan.strPublishSpot = formValues["strPublishSpot"];
                        besPlan.strDetailUrl = formValues["strDetailUrl"];
                        besPlan.strDtSub2 = formValues["strDtSub2"];

                        if (imagetable != null)
                        {
                            string strImage = SaveImage(imagetable, "BenPlanImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                besPlan.strImageTable = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                                {
                                    besPlan.strImageTable = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                            {
                                besPlan.strImageTable = "";
                            }
                        }

                        if (image != null)
                        {
                            string strImage = SaveImage(image, "BenPlanImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                besPlan.strImage = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                                {
                                    besPlan.strImage = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                            {
                                besPlan.strImage = "";
                            }
                        }

                        if (imagethumb != null)
                        {
                            string strImageThumb = SaveImage(imagethumb, "BenPlanImageThumb", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImageThumb))
                            {
                                besPlan.strImageThumb = strImageThumb;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                                {
                                    besPlan.strImageThumb = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                            {
                                besPlan.strImageThumb = "";
                            }
                        }

                        if (video != null)
                        {
                            string strVideo = SaveImage(video, "BenPlanVideo", ConfigurationManager.AppSettings["VideoExt"]);
                            if (!string.IsNullOrEmpty(strVideo))
                            {
                                besPlan.strVideo = strVideo;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                                {
                                    besPlan.strVideo = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                            {
                                besPlan.strVideo = "";
                            }
                        }
                    }

                    besPlan.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, besPlan.intBesPlanID);
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Plan güncellendi.");

                    if (files != null)
                    {
                        for (int i = files.Count - 1; i >= 0; i--)
                        {
                            if (files[i] == null)
                            {
                                files.RemoveAt(i);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hdFiles"]))
                                {
                                    var hdFiles = formValues["hdFiles"].Split(',');
                                    if (hdFiles.Count(c => c == files[i].FileName) == 0)
                                    {
                                        files.RemoveAt(i);
                                    }
                                }
                            }
                        }

                        if (files.Count > 0 || !string.IsNullOrEmpty(formValues["hdFileDelete"]))
                        {
                            string strBesPlanFile = System.Configuration.ConfigurationManager.AppSettings["BesPlanFile"];

                            if (files.Count == 0 && !string.IsNullOrEmpty(formValues["hdFileDelete"]))
                            {
                                var fileDelete = formValues["hdFileDelete"].Split(',');
                                foreach (var item in fileDelete)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        var besPlanFile = db.tblBesPlanFiles.Where(w => w.intBesPlanID == intBesPlanID && w.strFile == item).FirstOrDefault();
                                        if (besPlanFile != null)
                                        {
                                            db.tblBesPlanFiles.Remove(besPlanFile);
                                            db.SaveChanges();

                                            try
                                            {
                                                System.IO.File.Delete(Utilities.AppPathServer + strBesPlanFile + besPlanFile.strName);
                                            }
                                            catch { }
                                        }
                                    }
                                }
                            }
                            else if (files.Count > 0)
                            {
                                bool bolEx = false;
                                for (int i = 0; i < files.Count; i++)
                                {
                                    if (Path.GetExtension(files[i].FileName) != ".pdf")
                                    {
                                        bolEx = true;
                                        break;
                                    }
                                }

                                if (!bolEx)
                                {
                                    var besPlanfiles = db.tblBesPlanFiles.Where(w => w.intBesPlanID == intBesPlanID);

                                    foreach (var item in files)
                                    {
                                        if (item != null)
                                        {
                                            string strFileName = Utilities.RandomString(20) + Path.GetExtension(item.FileName);

                                            var besPlanFile = besPlanfiles.Where(w => w.strFile == item.FileName).FirstOrDefault();
                                            if (besPlanFile == null)
                                            {
                                                db.tblBesPlanFiles.Add(new tblBesPlanFiles
                                                {
                                                    intBesPlanID = besPlan.intBesPlanID,
                                                    strFile = item.FileName,
                                                    strName = strFileName
                                                });

                                                item.SaveAs(Utilities.AppPathServer + strBesPlanFile + strFileName);
                                            }
                                            else
                                            {
                                                besPlanFile.strName = strFileName;

                                                try
                                                {
                                                    System.IO.File.Delete(Utilities.AppPathServer + strBesPlanFile + besPlanFile.strName);
                                                }
                                                catch { }

                                                item.SaveAs(Utilities.AppPathServer + strBesPlanFile + strFileName);
                                            }
                                        }
                                    }
                                    db.SaveChanges();
                                }
                                else
                                {
                                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Plan güncellendi fakat dosyalar pdf uzantılı olmadığı için eklenmedi!");
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BesPlan");
        }
        #endregion

        #region SaveImage
        private string SaveImage(HttpPostedFileBase image, string strType, string strImageExtList)
        {
            bool bolEx = false;
            var ImageExtList = strImageExtList.Split(',').ToList();
            foreach (var item in ImageExtList)
            {
                if (Path.GetExtension(image.FileName).ToLower() == "." + item)
                {
                    bolEx = true;
                    break;
                }
            }

            if (bolEx)
            {
                string strImage = Utilities.RandomString(20) + Path.GetExtension(image.FileName);
                image.SaveAs(Utilities.AppPathServer + ConfigurationManager.AppSettings[strType] + strImage);

                return strImage;
            }

            return "";
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            if (Request.QueryString["type"] == null)
            {
                string strPlanName = formValues["strPlanName"];
                if (strPlanName.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Plan adı giriniz!");
                    return false;
                }

                string strPlanCode = formValues["strPlanCode"];
                if (strPlanCode.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Plan kodu giriniz!");
                    return false;
                }

                decimal flMinContAmount = Utilities.NullFixDecimal(formValues["flMinContAmount"]);
                if (flMinContAmount == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Asgari katkı payı tutarı giriniz!");
                    return false;
                }

                bool bolInitialCont = formValues["bolInitialCont"] == "on" || formValues["bolInitialCont"] == "checked" || formValues["bolInitialCont"] == "true";
                if (bolInitialCont)
                {
                    decimal flInitialCont = Utilities.NullFixDecimal(formValues["flInitialCont"]);
                    if (flInitialCont == 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Asgari başlangıç katkı payı tutarı giriniz!");
                        return false;
                    }
                }
            }
            else
            {
                string strPublishComment = formValues["strPublishComment"];
                if (strPublishComment.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region Video
        public ActionResult Video()
        {
            return View();
        }
        #endregion
    }
}
