﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BankController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intBankID = QueryStrings.ID;
                    if (intBankID > 0)
                    {
                        var bank = db.tblBanks.Where(w => w.intBankID == intBankID).FirstOrDefault();
                        if (bank != null)
                        {
                            db.Database.ExecuteSqlCommand("DELETE FROM tblBankCities WHERE intBankID = {0}", intBankID);
                            db.SaveChanges();

                            bank.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intBankID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Banka silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Banka bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Banka bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Bank");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strName = formValues["strName"];

                    if (db.tblBanks.Count(c => c.strName == strName && !c.bolIsDelete) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı banka adı sistemde mevcut!");
                        return RedirectToAction("New", "Bank");
                    }

                    tblBanks bank = new tblBanks();
                    bank.strName = strName;
                    bank.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bank.dtRegisterDate = DateTime.Now;

                    db.tblBanks.Add(bank);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, bank.intBankID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Banka eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Bank");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    short intBankID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["hdID"]));
                    string strName = formValues["strName"];

                    if (db.tblBanks.Count(c => c.intBankID != intBankID && c.strName == strName && !c.bolIsDelete) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı banka adı sistemde mevcut!");
                        return RedirectToAction("Update", "Bank", new { i = Request.QueryString["i"] });
                    }

                    var bank = db.tblBanks.Where(w => w.intBankID == intBankID).FirstOrDefault();
                    bank.strName = strName;
                    bank.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bank.dtUpdateDate = DateTime.Now;

                    db.SaveChanges(); 

                    Logs.Log(Enums.LogType.Update, intBankID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Banka güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Bank");
        }
        #endregion
    }
}
