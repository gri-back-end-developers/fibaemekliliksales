﻿using FibaEmeklilikSales.Classes; 
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using jCryption;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class DefaultController : Controller
    {
        #region ReportUser

        #region Get
        [HttpGet]
        public ActionResult ReportUser()
        {
            return View("Login", "");
        } 
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult ReportUser(String txtUserName, String txtPass)
        {
            if (string.IsNullOrEmpty(txtUserName) && string.IsNullOrEmpty(txtPass))
            {
                return View();
            }

            return Login(txtUserName, txtPass);
        } 
        #endregion

        #endregion

        #region Login Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Login(string returnUrl)
        {
            if (Request.QueryString["getPublicKey"] == null)
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                Cookies.RemoveAll();

                SessionIDManager manager = new SessionIDManager();
                string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                bool redirected = false;
                bool IsAdded = false;
                manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);
            }

            return View();
        }
        #endregion

        #region Login Set
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Login(String txtUserName, String txtPass)
        {
            if (string.IsNullOrEmpty(txtUserName) && string.IsNullOrEmpty(txtPass))
            {
                return View();
            }

            if (Request.Url.AbsolutePath != "/default/reportuser")
            {
                string strCaptcha = cCrypto.DecryptDES(Request.Form["hdCapctha"]);
                if (strCaptcha != Request.Form["txtCaptcha"])
                {
                    ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "Güvenlik kodunu hatalı girdiniz!");
                    return View();
                }
            }

            string strUserName = txtUserName;
            if (strUserName.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "Kullanıcı adınızı giriniz!");
                return View();
            }

            string strPass = txtPass;
            if (strPass.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "Şifrenizi giriniz!");
                return View();
            }

            try
            {
                DBEntities db = new DBEntities();

                string strCryptoPassword = cCrypto.EncryptDES(strPass.Trim());

                var user = db.tblUsers.SingleOrDefault(w => !w.bolIsDelete && w.bolIsActive && w.strUserName == strUserName.Trim() && w.strPassword == strCryptoPassword);
                bool bolNext = true;
                if (user != null)
                {
                    if (user.intCallcenterID > 0)
                    {
                        bolNext = !user.tblCallcenters.bolIsDelete && user.tblCallcenters.bolIsActive;
                    }
                }

                if (user != null && bolNext)
                {
                    var tblUPCL = db.tblUserPssChangeLogs.Where(w => w.intUserID == user.intUserID).OrderByDescending(o => o.dtRegisterDate).ToList();

                    int intCount = tblUPCL.Count;

                    Logs.Login(user.intUserID);

                    if (user.intLoginCount == 0)
                        user.bolPssChange = true;

                    user.intLoginCount = user.intLoginCount + 1;
                    db.SaveChanges();

                    foreach (var item in tblUPCL)
                    {
                        db.tblUserPssChangeLogs.Remove(item);
                        db.SaveChanges();
                    }

                    user.tblUserLoginLog = null;

                    Sessions.CurrentUser = user;
                    Sessions.IP = Utilities.ClientIP;

                    FormsAuthentication.SetAuthCookie(strUserName, false);

                    if ((Enums.UserType)user.intUserTypeID == Enums.UserType.ReportUser)
                    {
                        Response.Redirect("/report/list", false);
                    }
                    else
                    {
                        string strUrl = Sessions.LastPage;

                        Response.Redirect(string.IsNullOrEmpty(strUrl) ? "/main/index" : strUrl, false);
                    }
                }
                else
                {
                    user = db.tblUsers.SingleOrDefault(w => !w.bolIsDelete && w.bolIsActive && w.strUserName == strUserName.Trim());
                    bolNext = true;
                    if (user != null)
                    {
                        if (!user.tblUserTypes.bolPowerAdmin && !user.tblUserTypes.bolSpecialSalePage)
                        {
                            bolNext = !user.tblCallcenters.bolIsDelete && user.tblCallcenters.bolIsActive;
                        }
                    }

                    if (user != null && bolNext)
                    {
                        int intUserID = user.intUserID;
                        var tblUPCL = db.tblUserPssChangeLogs.Where(w => w.intUserID == intUserID).ToList();

                        int intCount = tblUPCL.Count;
                        if (intCount > 0)
                        {
                            foreach (var item in tblUPCL)
                            {
                                TimeSpan time = DateTime.Now.Subtract(item.dtRegisterDate);
                                int intMinute = time.Minutes + (time.Hours * 60) + ((time.Days * 24) * 60);
                                if (intMinute > 60)
                                {
                                    db.tblUserPssChangeLogs.Remove(item);
                                    intCount--;
                                }
                            }
                            db.SaveChanges();
                        }

                        tblUserPssChangeLogs newtblUserPssChangeLog = new tblUserPssChangeLogs();
                        newtblUserPssChangeLog.intUserID = intUserID;
                        newtblUserPssChangeLog.dtRegisterDate = DateTime.Now;

                        db.tblUserPssChangeLogs.Add(newtblUserPssChangeLog);
                        db.SaveChanges();

                        intCount++;

                        if (intCount >= 3)
                        {
                            Sessions.CurrentUser = null;
                            ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "3 kere Hatalı giriş yaptığınız için hesabınız bloklanmıştır!");
                        }
                        else
                        {
                            ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "Kullanıcı adı veya şifenizi hatalı girdiniz!");
                        }
                    }
                    else
                    {
                        ViewBag.Message = Utilities.Msg(Utilities.MsgType.Warning, "Kullanıcı adı veya şifenizi hatalı girdiniz!!");
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.Msg(Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
                Logs.Error(ex);
            }

            if (Request.Url.AbsolutePath != "/default/reportuser")
            {
                return View();
            }
            else
            {
                return View("Login", "");
            }
        }
        #endregion

        #region EmailSend
        public string EmailSend(string txtEmail)
        {
            try
            {
                DBEntities db = new DBEntities();

                var user = db.tblUsers.Where(w => w.strEmail == txtEmail.Trim() && w.bolIsActive == true).FirstOrDefault();
                if (user != null)
                {
                    string strHtml = Utilities.GetFileText(Utilities.AppPathServer + "Htmls\\EmailTemplate.html");
                    strHtml = strHtml.Replace("#adsoyad#", user.strNameSurname);
                    strHtml = strHtml.Replace("#mesaj#", "Şifre : " + cCrypto.DecryptDES(user.strPassword));

                    if (Email.SendEmail("FibaEmeklilikSales şifre hatırlatma", strHtml, user.strEmail))
                        return Utilities.MsgToString(Utilities.MsgType.Info, "Şifreniz e-posta adresinize gönderildi.");
                    else
                        return Utilities.MsgToString(Utilities.MsgType.Error, "Hatalı e-posta!");
                }
                else
                {
                    return Utilities.MsgToString(Utilities.MsgType.Warning, "Kayıtlı e-posta adresi bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                return Utilities.MsgToString(Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }
        }
        #endregion

        #region Message
        [HttpGet]
        [ValidateInput(false)]
        public string Message(int msgtype, string msg)
        {
            Sessions.Msg = "";
            return Utilities.MsgToString((Utilities.MsgType)msgtype, msg);
        }
        #endregion

        #region Capctha
        public ActionResult Capctha()
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            string a = cCrypto.DecryptDES(Request.QueryString["key"]);
            var captcha = string.Format("{0}", a);

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(100, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                int i, r, x, y;
                var pen = new Pen(Color.Yellow);
                for (i = 1; i < 10; i++)
                {
                    pen.Color = Color.FromArgb(
                    (rand.Next(0, 255)),
                    (rand.Next(0, 255)),
                    (rand.Next(0, 255)));

                    r = rand.Next(0, (100 / 3));
                    x = rand.Next(0, 100);
                    y = rand.Next(0, 30);

                    gfx.DrawEllipse(pen, x - r, y - r, r, r);
                }

                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 10, 3);

                Response.ContentType = "image/png";
                Response.Clear();
                Response.BufferOutput = true;
                bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);

                Response.Flush();

                return View();
            }
        }
        #endregion
    }
}
