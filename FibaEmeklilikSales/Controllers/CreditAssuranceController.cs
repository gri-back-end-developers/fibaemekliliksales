﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class CreditAssuranceController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intCreditAssuranceID = QueryStrings.ID;
                    if (intCreditAssuranceID > 0)
                    {
                        var assurance = db.tblCreditAssurances.Where(w => w.intCreditAssuranceID == intCreditAssuranceID).FirstOrDefault();
                        if (assurance != null)
                        {
                            bool bolExist = db.tblWebSaleLogs.Where(w => w.tblCreditPackets.tblCreditAssurances.Count(c => c.intCreditAssuranceID == intCreditAssuranceID) > 0).FirstOrDefault() != null;

                            if (!bolExist)
                            {
                                assurance.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intCreditAssuranceID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Teminata ait satış işlemleri var!");
                            }
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Teminat bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Teminat bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    int intCreditPacketID = QueryStrings.PacketID;
                    string strName = formValues["strName"];

                    if (db.tblCreditAssurances.Count(c => !c.bolIsDelete && c.intCreditPacketID == intCreditPacketID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Pakete ait aynı teminat başlığı sistemde mevcut!");
                        return RedirectToAction("New", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"] });
                    }

                    tblCreditAssurances assurances = new tblCreditAssurances();
                    assurances.intCreditPacketID = intCreditPacketID;
                    assurances.strName = formValues["strName"];
                    assurances.strCode = formValues["strCode"];
                    assurances.strCoefficientType = formValues["strCoefficientType"];
                    assurances.flCoefficient = Utilities.NullFixDecimal(formValues["flCoefficient"]);
                    assurances.flMaxAssurance = Utilities.NullFixDecimal(formValues["flMaxAssurance"]);
                    assurances.bolIsDelete = false;
                    assurances.dtRegisterDate = DateTime.Now;

                    db.tblCreditAssurances.Add(assurances);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, assurances.intCreditAssuranceID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"] });
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"], i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intCreditAssuranceID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    int intCreditPacketID = QueryStrings.PacketID;
                    string strName = formValues["strName"];

                    if (db.tblCreditAssurances.Count(c => !c.bolIsDelete && c.intCreditPacketID == intCreditPacketID && c.intCreditAssuranceID != intCreditAssuranceID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Pakete ait aynı teminat adı sistemde mevcut!");
                        return RedirectToAction("Update", "CreditAssurance", new { cr = Request.QueryString["cr"], i = Request.QueryString["i"] });
                    }

                    var assurances = db.tblCreditAssurances.Where(w => w.intCreditAssuranceID == intCreditAssuranceID).FirstOrDefault();
                    assurances.strName = strName;
                    assurances.strCode = formValues["strCode"];
                    assurances.strCoefficientType = formValues["strCoefficientType"];
                    assurances.flCoefficient = Utilities.NullFixDecimal(formValues["flCoefficient"]);
                    assurances.flMaxAssurance = Utilities.NullFixDecimal(formValues["flMaxAssurance"]);
                    assurances.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, assurances.intCreditPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditAssurance", new { cr = Request.QueryString["cr"], pa = Request.QueryString["pa"] });
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strName = formValues["strName"];
            if (strName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            string strCode = formValues["strCode"];
            if (strCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kod giriniz!");
                return false;
            }

            decimal flCoefficient = Utilities.NullFixDecimal(formValues["flCoefficient"]);
            if (flCoefficient == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Katsayı değerini giriniz!");
                return false;
            }

            decimal flMaxAssurance = Utilities.NullFixDecimal(formValues["flMaxAssurance"]);
            if (flMaxAssurance == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Max teminat giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
