﻿using FibaEmeklilikSales.Classes; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class FibabankProductController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intFibabankProductID = QueryStrings.ID;
                    if (intFibabankProductID > 0)
                    {
                        var besPlan = db.tblFibabankProducts.Where(w => w.intFibabankProductID == intFibabankProductID).FirstOrDefault();
                        if (besPlan != null)
                        {
                            besPlan.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intFibabankProductID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Ürün bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Ürün bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "FibabankProduct", new { txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "", HttpPostedFileBase image = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "FibabankProduct");
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strTitle = formValues["strTitle"];

                    if (db.tblFibabankProducts.Count(c => !c.bolIsDelete && c.strTitle == strTitle) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı başlık sistemde mevcut!");
                        return RedirectToAction("List", "BesPlan");
                    }

                    tblFibabankProducts fibabankProduct = new tblFibabankProducts();
                    fibabankProduct.intCallcenterID = Sessions.CurrentUser.intCallcenterID;
                    fibabankProduct.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    fibabankProduct.strTitle = strTitle;
                    fibabankProduct.strText = formValues["strText"];
                    fibabankProduct.strButton1Link = formValues["strButton1Link"];
                    fibabankProduct.strButton1Title = formValues["strButton1Title"];
                    fibabankProduct.strButton2Link = formValues["strButton2Link"];
                    fibabankProduct.strButton2Title = formValues["strButton2Title"];
                    fibabankProduct.bolIsDelete = false;
                    fibabankProduct.dtRegisterDate = DateTime.Now;

                    if (image != null)
                    {
                        string strImage = SaveImage(image, "FibabankProductImage", ConfigurationManager.AppSettings["ImageExt"]);
                        if (!string.IsNullOrEmpty(strImage))
                        {
                            fibabankProduct.strImage = strImage;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                            {
                                fibabankProduct.strImage = "";
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                        {
                            fibabankProduct.strImage = "";
                        }
                    }

                    db.tblFibabankProducts.Add(fibabankProduct);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, fibabankProduct.intFibabankProductID);
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "FibabankProduct");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "", HttpPostedFileBase image = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "FibabankProduct", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intFibabankProductID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    string strTitle = formValues["strTitle"];

                    if (db.tblFibabankProducts.Count(c => !c.bolIsDelete && c.intFibabankProductID != intFibabankProductID && c.strTitle == strTitle) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı başlık sistemde mevcut!");
                        return RedirectToAction("Update", "FibabankProduct", new { i = Request.QueryString["i"] });
                    }

                    var fibabankProduct = db.tblFibabankProducts.Where(w => w.intFibabankProductID == intFibabankProductID).FirstOrDefault();
                   
                    fibabankProduct.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    fibabankProduct.strTitle = strTitle;
                    fibabankProduct.strText = formValues["strText"];
                    fibabankProduct.strButton1Link = formValues["strButton1Link"];
                    fibabankProduct.strButton1Title = formValues["strButton1Title"];
                    fibabankProduct.strButton2Link = formValues["strButton2Link"];
                    fibabankProduct.strButton2Title = formValues["strButton2Title"];

                    if (image != null)
                    {
                        string strImage = SaveImage(image, "FibabankProductImage", ConfigurationManager.AppSettings["ImageExt"]);
                        if (!string.IsNullOrEmpty(strImage))
                        {
                            fibabankProduct.strImage = strImage;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                            {
                                fibabankProduct.strImage = "";
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                        {
                            fibabankProduct.strImage = "";
                        }
                    }

                    fibabankProduct.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, fibabankProduct.intFibabankProductID);
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün güncellendi."); 
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "FibabankProduct");
        }
        #endregion

        #region SaveImage
        private string SaveImage(HttpPostedFileBase image, string strType, string strImageExtList)
        {
            bool bolEx = false;
            var ImageExtList = strImageExtList.Split(',').ToList();
            foreach (var item in ImageExtList)
            {
                if (Path.GetExtension(image.FileName).ToLower() == "." + item)
                {
                    bolEx = true;
                    break;
                }
            }

            if (bolEx)
            {
                string strImage = Utilities.RandomString(20) + Path.GetExtension(image.FileName);
                image.SaveAs(Utilities.AppPathServer + ConfigurationManager.AppSettings[strType] + strImage);

                return strImage;
            }

            return "";
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strTitle = formValues["strTitle"];
            if (strTitle.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            string strText = formValues["strText"];
            if (strText.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                return false;
            }

            string strButton1Link = formValues["strButton1Link"];
            if (strButton1Link.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Buton1 link giriniz!");
                return false;
            }

            string strButton1Title = formValues["strButton1Title"];
            if (strButton1Title.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Buton1 isim giriniz!");
                return false;
            }

            string strButton2Link = formValues["strButton2Link"];
            if (strButton2Link.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Buton2 link giriniz!");
                return false;
            }

            string strButton2Title = formValues["strButton2Title"];
            if (strButton2Title.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Buton2 isim giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
