﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using System.Text;
using System.IO;
using System.Configuration;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class CreditController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intCreditID = QueryStrings.ID;
                    if (intCreditID > 0)
                    {
                        var credit = db.tblCredits.Where(w => w.intCreditID == intCreditID).FirstOrDefault();
                        if (credit != null)
                        {
                            bool bolWebSaleLog = db.tblWebSaleLogs.Where(w => w.tblCreditPackets.intCreditID == intCreditID && w.bolIsFinished).FirstOrDefault() == null;
                            if (bolWebSaleLog)
                            {
                                credit.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intCreditID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kredi teminatı silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kredi teminatına ait satış işlemleri var!");
                            }
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kredi teminatı bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kredi teminatı bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Credit", new { txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Credit", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string intPaymentMethodIDs = "";
                    foreach (var item in db.tblPaymentMethods.ToList())
                    {
                        if (formValues["paymentMethod-" + item.intPaymentMethodID] == "on" || formValues["paymentMethod-" + item.intPaymentMethodID] == "checked" || formValues["paymentMethod-" + item.intPaymentMethodID] == "true")
                        {
                            intPaymentMethodIDs += intPaymentMethodIDs == "" ? item.intPaymentMethodID.ToString() : "," + item.intPaymentMethodID.ToString();
                        }
                    }

                    tblCredits credit = new tblCredits();
                    credit.strName = formValues["strName"];
                    credit.strComment = formValues["strComment"];
                    credit.strNote = formValues["strNote"];
                    credit.strQuestion = formValues["strQuestion"];
                    credit.strIntegrationCode = formValues["strIntegrationCode"];
                    credit.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    credit.bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";
                    credit.intCallcenterID = Sessions.CurrentUser.intCallcenterID;
                    credit.bolIsDelete = false;
                    credit.dtRegisterDate = DateTime.Now;
                    credit.intPaymentMethodIDs = intPaymentMethodIDs;

                    db.tblCredits.Add(credit);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, credit.intCreditID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kredi teminatı eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Credit");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "", HttpPostedFileBase imagetable = null, HttpPostedFileBase image = null, HttpPostedFileBase imagethumb = null, HttpPostedFileBase video = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Credit", new { i = Request.QueryString["i"], type = Request.QueryString["type"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intCreditID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    var credit = db.tblCredits.Where(w => w.intCreditID == intCreditID).FirstOrDefault();

                    if (Request.QueryString["type"] == null)
                    {
                        string intPaymentMethodIDs = "";
                        foreach (var item in db.tblPaymentMethods.ToList())
                        {
                            if (formValues["paymentMethod-" + item.intPaymentMethodID] == "on" || formValues["paymentMethod-" + item.intPaymentMethodID] == "checked" || formValues["paymentMethod-" + item.intPaymentMethodID] == "true")
                            {
                                intPaymentMethodIDs += intPaymentMethodIDs == "" ? item.intPaymentMethodID.ToString() : "," + item.intPaymentMethodID.ToString();
                            }
                        }

                        credit.strName = formValues["strName"];
                        credit.strComment = formValues["strComment"];
                        credit.strNote = formValues["strNote"];
                        credit.strQuestion = formValues["strQuestion"];
                        credit.strIntegrationCode = formValues["strIntegrationCode"];
                        credit.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                        credit.bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";
                        credit.intPaymentMethodIDs = intPaymentMethodIDs;
                    }
                    else
                    {
                        credit.bolPublished = formValues["bolPublished"] == "on" || formValues["bolPublished"] == "checked" || formValues["bolPublished"] == "true";
                        credit.bolSlider = formValues["bolSlider"] == "on" || formValues["bolSlider"] == "checked" || formValues["bolSlider"] == "true";
                        credit.strPublishComment = formValues["strPublishComment"];
                        credit.strPublishSpot = formValues["strPublishSpot"];
                        credit.strDetailUrl = formValues["strDetailUrl"];
                        credit.strDtSub2 = formValues["strDtSub2"];

                        if (imagetable != null)
                        {
                            string strImage = SaveImage(imagetable, "CreditImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                credit.strImageTable = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                                {
                                    credit.strImageTable = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                            {
                                credit.strImageTable = "";
                            }
                        }

                        if (image != null)
                        {
                            string strImage = SaveImage(image, "CreditImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                credit.strImage = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                                {
                                    credit.strImage = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                            {
                                credit.strImage = "";
                            }
                        }

                        if (imagethumb != null)
                        {
                            string strImageThumb = SaveImage(imagethumb, "CreditImageThumb", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImageThumb))
                            {
                                credit.strImageThumb = strImageThumb;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                                {
                                    credit.strImageThumb = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                            {
                                credit.strImageThumb = "";
                            }
                        }

                        if (video != null)
                        {
                            string strVideo = SaveImage(video, "CreditVideo", ConfigurationManager.AppSettings["VideoExt"]);
                            if (!string.IsNullOrEmpty(strVideo))
                            {
                                credit.strVideo = strVideo;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                                {
                                    credit.strVideo = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                            {
                                credit.strVideo = "";
                            }
                        }
                    }

                    credit.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, credit.intCreditID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kredi teminatı güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            } 

            return RedirectToAction("List", "Credit");
        }
        #endregion

        #region SaveImage
        private string SaveImage(HttpPostedFileBase image, string strType, string strImageExtList)
        {
            bool bolEx = false;
            var ImageExtList = strImageExtList.Split(',').ToList();
            foreach (var item in ImageExtList)
            {
                if (Path.GetExtension(image.FileName).ToLower() == "." + item)
                {
                    bolEx = true;
                    break;
                }
            }

            if (bolEx)
            {
                string strImage = Utilities.RandomString(20) + Path.GetExtension(image.FileName);
                image.SaveAs(Utilities.AppPathServer + ConfigurationManager.AppSettings[strType] + strImage);

                return strImage;
            }

            return "";
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            if (Request.QueryString["type"] == null)
            {
                string strName = formValues["strName"];
                if (strName.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad giriniz!");
                    return false;
                }

                string strComment = formValues["strComment"];
                if (strComment.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                    return false;
                }

                string strNote = formValues["strNote"];
                if (strNote.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Not giriniz!");
                    return false;
                }

                string strTariffCode = formValues["strIntegrationCode"];
                if (strTariffCode.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Tarife kodu giriniz!");
                    return false;
                }
            }
            else
            { 
                string strPublishComment = formValues["strPublishComment"];
                if (strPublishComment.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
