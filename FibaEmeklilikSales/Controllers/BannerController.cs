﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;

namespace FibaEmeklilikSales.Controllers
{
    public class BannerController : Controller
    {
        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion
    }
}
