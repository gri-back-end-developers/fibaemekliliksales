﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using jCryption;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class UserController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intUserID = QueryStrings.ID;
                    if (intUserID > 0)
                    {
                        var user = db.tblUsers.Where(w => w.intUserID == intUserID).FirstOrDefault();
                        if (user != null)
                        {
                            user.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intUserID);
                        }

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı silindi.");
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kullanıcı bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "User", new { ci = Request.QueryString["ci"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        [jCryptionHandler]
        public ActionResult New(string returnUrl)
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [jCryptionHandler]
        [ValidateInput(false)]
        public ActionResult New(string txtUserName, string txtPass)
        {
            if (string.IsNullOrEmpty(txtUserName) && string.IsNullOrEmpty(txtPass))
            {
                return View();
            }

            var formValues = Request.Form;

            string strUserName = txtUserName;
            if (strUserName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kullanıcı adını giriniz!");
                return RedirectToAction("Update", "User", new { i = Request.Form["hdID"] });
            }

            string strPass = txtPass;
            if (strPass.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Şifre giriniz!");
                return RedirectToAction("New", "User", new { i = Request.Form["hdID"] });
            }

            if (!Validate())
            {
                return RedirectToAction("New", "User", new { i = Request.Form["hdID"] });
            }

            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (db.tblUsers.Count(c => !c.bolIsDelete && c.strUserName == strUserName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı kullanıcı adı sistemde mevcut!");
                        return RedirectToAction("New", "User");
                    }

                    byte intUserTypeID = !Sessions.CurrentUser.tblUserTypes.bolPowerAdmin ? (byte)Enums.UserType.Normal : Utilities.NullFixByte(formValues["intUserTypeID"]);

                    tblUsers user = new tblUsers();
                    user.intCallcenterID = !Sessions.CurrentUser.tblUserTypes.bolPowerAdmin ? Sessions.CurrentUser.intCallcenterID : Utilities.NullFixInt(cCrypto.DecryptText(formValues["intCallcenterID"]));
                    user.intUserTypeID = intUserTypeID;
                    user.strEmail = formValues["strEmail"];
                    user.strNameSurname = formValues["strNameSurname"];
                    user.intLoginCount = 0;
                    user.bolPssChange = true;
                    user.strPassword = cCrypto.EncryptDES(txtPass);
                    user.strUserName = strUserName;
                    user.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    user.bolMediator = formValues["bolMediator"] == "on" || formValues["bolMediator"] == "checked" || formValues["bolMediator"] == "true";
                    user.strMediatorID = formValues["strMediatorID"];
                    user.strMediatorName = formValues["strMediatorName"];
                    user.bolIsDelete = false;
                    user.dtRegisterDate = DateTime.Now;

                    db.tblUsers.Add(user);
                    db.SaveChanges();

                    foreach (var item in db.tblPages.ToList())
                    {
                        var auth = db.tblAuth.Where(w => w.intUserTypeID == intUserTypeID && w.intPageID == item.intPageID).FirstOrDefault();
                        if (auth != null)
                        {
                            tblUserAuth userAuth = new tblUserAuth();

                            if (auth.intPageID == 5)
                            {
                                var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == user.intCallcenterID).First();

                                userAuth.bolView = callcenter.bolCancel;
                                userAuth.bolUpdate = callcenter.bolCancel;
                            }
                            else
                            {
                                userAuth.bolUpdate = auth.bolUpdate;
                                userAuth.bolView = auth.bolView;
                            }

                            userAuth.bolDelete = auth.bolDelete;
                            userAuth.bolInsert = auth.bolInsert;

                            userAuth.dtRegisterDate = DateTime.Now;
                            userAuth.intPageID = item.intPageID;
                            userAuth.intUserID = user.intUserID;

                            db.tblUserAuth.Add(userAuth);
                        }
                    }
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, user.intUserID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "User");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Update(string returnUrl)
        {
            return View("New");
        }
        #endregion

        #region Update
        [HttpPost]
        [UserFilter]
        [jCryptionHandler]
        [ValidateInput(false)]
        public ActionResult Update(string txtUserName, string txtPass)
        {
            if (string.IsNullOrEmpty(txtUserName) && string.IsNullOrEmpty(txtPass))
            {
                return View();
            }

            db = new DBEntities();

            var formValues = Request.Form;

            string strUserName = txtUserName;
            if (strUserName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kullanıcı adını giriniz!");
                return RedirectToAction("Update", "User", new { i = Request.Form["hdID"] });
            }

            string strPass = txtPass;
            if (strPass.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Şifre giriniz!");
                return RedirectToAction("Update", "User", new { i = Request.Form["hdID"] });
            }

            if (!Validate())
            {
                return RedirectToAction("Update", "User", new { i = Request.Form["hdID"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    int intUserID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    if (db.tblUsers.Count(c => !c.bolIsDelete && c.intUserID != intUserID && c.strUserName == strUserName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı kullanıcı adı sistemde mevcut!");
                        return RedirectToAction("Update", "User", new { i = Request.Form["hdID"] });
                    }

                    var user = db.tblUsers.Where(w => w.intUserID == intUserID).FirstOrDefault();

                    byte intUserTypeID = user.intUserTypeID;
                    if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                    {
                        user.intCallcenterID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["intCallcenterID"]));
                        user.intUserTypeID = Utilities.NullFixByte(formValues["intUserTypeID"]);
                    }

                    user.strPassword = cCrypto.EncryptDES(txtPass);
                    user.strUserName = strUserName;
                    user.strEmail = formValues["strEmail"];
                    user.strNameSurname = formValues["strNameSurname"];
                    user.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    user.dtUpdateDate = DateTime.Now;
                    user.bolMediator = formValues["bolMediator"] == "on" || formValues["bolMediator"] == "checked" || formValues["bolMediator"] == "true";
                    user.strMediatorID = formValues["strMediatorID"];
                    user.strMediatorName = formValues["strMediatorName"];

                    db.SaveChanges();

                    db.Database.ExecuteSqlCommand("DELETE FROM tblUserAuth WHERE intUserID = " + user.intUserID);
                    db.SaveChanges();

                    foreach (var item in db.tblPages.ToList())
                    {
                        var auth = db.tblAuth.Where(w => w.intUserTypeID == user.intUserTypeID && w.intPageID == item.intPageID).FirstOrDefault();
                        if (auth != null)
                        {
                            tblUserAuth userAuth = new tblUserAuth();

                            if (auth.intPageID == 5 && !Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                            {
                                userAuth.bolView = user.tblCallcenters.bolCancel;
                                userAuth.bolUpdate = user.tblCallcenters.bolCancel;
                            }
                            else
                            {
                                userAuth.bolUpdate = auth.bolUpdate;
                                userAuth.bolView = auth.bolView;
                            }

                            userAuth.bolDelete = auth.bolDelete;
                            userAuth.bolInsert = auth.bolInsert;
                            userAuth.dtRegisterDate = DateTime.Now;
                            userAuth.intPageID = item.intPageID;
                            userAuth.intUserID = user.intUserID;

                            db.tblUserAuth.Add(userAuth);
                        }
                    }
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, user.intUserID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kullanıcı güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "User");
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
            {
                byte intUserTypeID = Utilities.NullFixByte(formValues["intUserTypeID"]);
                if (intUserTypeID == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kullanıcı türü seçiniz!");
                    return false;
                }
            }
            else
            { 
                int intCallcenterID = string.IsNullOrEmpty(formValues["intCallcenterID"]) || formValues["intCallcenterID"] == "0" ? 0 : Utilities.NullFixInt(cCrypto.DecryptText(formValues["intCallcenterID"]));
                if (intCallcenterID == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Satış kanalı seçiniz!");
                    return false;
                }
            }

            string strNameSurname = formValues["strNameSurname"];
            if (strNameSurname.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad soyad giriniz!");
                return false;
            }

            string strEmail = formValues["strEmail"];
            if (strEmail.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
