﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using System.Text;
using System.IO;
using System.Configuration;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class ProductController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            db = new DBEntities();
            List<tblProducts> products = new List<tblProducts>();

            if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin || Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage)
            {
                products.AddRange(db.tblProducts.Where(w => !w.bolIsDelete && w.bolSpecialSale == Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage).ToList());
            }
            else
            {
                products.AddRange(db.tblProducts.Where(w => !w.bolIsDelete && w.intCallcenterID == Sessions.CurrentUser.intCallcenterID).ToList());
            }

            string strSearch = QueryStrings.search;
            if (!string.IsNullOrEmpty(strSearch))
            {
                products = products.Where(w => w.strName.ToLower().Contains(strSearch.ToLower())).ToList();
            }

            int intPageSize = 20;
            ViewBag.intCount = (products.Count / intPageSize) + (products.Count % intPageSize > 0 ? 1 : 0);

            int intPage = QueryStrings.page;
            intPage = intPage > 0 ? intPage - 1 : intPage;

            products = products.Skip(intPage * intPageSize).Take(intPageSize).ToList();

            return View(products);
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intProductID = QueryStrings.ID;
                    if (intProductID > 0)
                    {
                        var product = db.tblProducts.Where(w => w.intProductID == intProductID).FirstOrDefault();
                        if (product != null)
                        {
                            product.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intProductID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Ürün bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Ürün bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Product", new { txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Product", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string intPaymentMethodIDs = "";
                    foreach (var item in db.tblPaymentMethods.ToList())
                    {
                        if (formValues["paymentMethod-" + item.intPaymentMethodID] == "on" || formValues["paymentMethod-" + item.intPaymentMethodID] == "checked" || formValues["paymentMethod-" + item.intPaymentMethodID] == "true")
                        {
                            intPaymentMethodIDs += intPaymentMethodIDs == "" ? item.intPaymentMethodID.ToString() : "," + item.intPaymentMethodID.ToString();
                        }
                    }

                    tblProducts product = new tblProducts
                    {
                        strName = formValues["strName"],
                        strComment = formValues["strComment"],
                        strNote = formValues["strNote"],
                        strQuestion = formValues["strQuestion"],
                        strTariffCode = formValues["strTariffCode"],
                        bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true",
                        bolIsRefresh = formValues["bolIsRefresh"] == "on" || formValues["bolIsRefresh"] == "checked" || formValues["bolIsRefresh"] == "true",
                        bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true",
                        bolScratchWin = formValues["bolScratchWin"] == "on" || formValues["bolScratchWin"] == "checked" || formValues["bolScratchWin"] == "true",
                        bolFirstBonus = formValues["bolFirstBonus"] == "on" || formValues["bolFirstBonus"] == "checked" || formValues["bolFirstBonus"] == "true",
                        bolContactForm = formValues["bolContactForm"] == "on" || formValues["bolContactForm"] == "checked" || formValues["bolContactForm"] == "true",
                        bolPersonalProduct = formValues["bolPersonalProduct"] == "on" || formValues["bolPersonalProduct"] == "checked" || formValues["bolPersonalProduct"] == "true",
                        intCallcenterID = Sessions.CurrentUser.intCallcenterID,
                        bolIsDelete = false,
                        dtRegisterDate = DateTime.Now,
                        bolSpecialSale = Sessions.CurrentUser.tblUserTypes.bolSpecialSalePage,
                        intPaymentMethodIDs = intPaymentMethodIDs
                    };

                    db.tblProducts.Add(product);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, product.intProductID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Product");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "", HttpPostedFileBase imagetable = null, HttpPostedFileBase image = null, HttpPostedFileBase imagethumb = null, HttpPostedFileBase video = null)
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Product", new { i = Request.QueryString["i"], type = Request.QueryString["type"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intProductID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    var product = db.tblProducts.Where(w => w.intProductID == intProductID).FirstOrDefault();

                    if (Request.QueryString["type"] == null)
                    {
                        string intPaymentMethodIDs = "";
                        foreach (var item in db.tblPaymentMethods.ToList())
                        {
                            if (formValues["paymentMethod-" + item.intPaymentMethodID] == "on" || formValues["paymentMethod-" + item.intPaymentMethodID] == "checked" || formValues["paymentMethod-" + item.intPaymentMethodID] == "true")
                            {
                                intPaymentMethodIDs += intPaymentMethodIDs == "" ? item.intPaymentMethodID.ToString() : "," + item.intPaymentMethodID.ToString();
                            }
                        }

                        product.strName = formValues["strName"];
                        product.strComment = formValues["strComment"];
                        product.strNote = formValues["strNote"];
                        product.strQuestion = formValues["strQuestion"];
                        product.strTariffCode = formValues["strTariffCode"];
                        product.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                        product.bolIsRefresh = formValues["bolIsRefresh"] == "on" || formValues["bolIsRefresh"] == "checked" || formValues["bolIsRefresh"] == "true";
                        product.bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";
                        product.bolScratchWin = formValues["bolScratchWin"] == "on" || formValues["bolScratchWin"] == "checked" || formValues["bolScratchWin"] == "true";
                        product.bolFirstBonus = formValues["bolFirstBonus"] == "on" || formValues["bolFirstBonus"] == "checked" || formValues["bolFirstBonus"] == "true";
                        product.bolContactForm = formValues["bolContactForm"] == "on" || formValues["bolContactForm"] == "checked" || formValues["bolContactForm"] == "true";
                        product.bolPersonalProduct = formValues["bolPersonalProduct"] == "on" || formValues["bolPersonalProduct"] == "checked" || formValues["bolPersonalProduct"] == "true";
                        product.intPaymentMethodIDs = intPaymentMethodIDs;

                        if (Sessions.ProductTable != null)
                        {
                            product.strJsonTable = JsonSerializer.Serialize<List<cProductTable>>(Sessions.ProductTable);
                        }
                        else
                        {
                            product.strJsonTable = "";
                        }
                    }
                    else
                    {
                        product.bolPublished = formValues["bolPublished"] == "on" || formValues["bolPublished"] == "checked" || formValues["bolPublished"] == "true";
                        product.bolSlider = formValues["bolSlider"] == "on" || formValues["bolSlider"] == "checked" || formValues["bolSlider"] == "true";
                        product.strPublishComment = formValues["strPublishComment"];
                        product.strPublishSpot = formValues["strPublishSpot"];
                        product.strDetailUrl = formValues["strDetailUrl"];
                        product.strDtSub2 = formValues["strDtSub2"];

                        if (imagetable != null)
                        {
                            string strImage = SaveImage(imagetable, "ProductImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                product.strImageTable = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                                {
                                    product.strImageTable = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagetable_Delete"]))
                            {
                                product.strImageTable = "";
                            }
                        }

                        if (image != null)
                        {
                            string strImage = SaveImage(image, "ProductImage", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImage))
                            {
                                product.strImage = strImage;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                                {
                                    product.strImage = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_image_Delete"]))
                            {
                                product.strImage = "";
                            }
                        }

                        if (imagethumb != null)
                        {
                            string strImageThumb = SaveImage(imagethumb, "ProductImageThumb", ConfigurationManager.AppSettings["ImageExt"]);
                            if (!string.IsNullOrEmpty(strImageThumb))
                            {
                                product.strImageThumb = strImageThumb;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                                {
                                    product.strImageThumb = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_imagethumb_Delete"]))
                            {
                                product.strImageThumb = "";
                            }
                        }

                        if (video != null)
                        {
                            string strVideo = SaveImage(video, "ProductVideo", ConfigurationManager.AppSettings["VideoExt"]);
                            if (!string.IsNullOrEmpty(strVideo))
                            {
                                product.strVideo = strVideo;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                                {
                                    product.strVideo = "";
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formValues["hd_video_Delete"]))
                            {
                                product.strVideo = "";
                            }
                        }
                    }

                    product.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, product.intProductID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Ürün güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            Sessions.ProductTable = null;
            Sessions.ProductTableDelete = false;

            return RedirectToAction("List", "Product");
        }
        #endregion

        #region SaveImage
        private string SaveImage(HttpPostedFileBase image, string strType, string strImageExtList)
        {
            bool bolEx = false;
            var ImageExtList = strImageExtList.Split(',').ToList();
            foreach (var item in ImageExtList)
            {
                if (Path.GetExtension(image.FileName).ToLower() == "." + item)
                {
                    bolEx = true;
                    break;
                }
            }

            if (bolEx)
            {
                string strImage = Utilities.RandomString(20) + Path.GetExtension(image.FileName);
                image.SaveAs(Utilities.AppPathServer + ConfigurationManager.AppSettings[strType] + strImage);

                return strImage;
            }

            return "";
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            if (Request.QueryString["type"] == null)
            {
                string strName = formValues["strName"];
                if (strName.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad giriniz!");
                    return false;
                }

                string strComment = formValues["strComment"];
                if (strComment.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                    return false;
                }

                string strNote = formValues["strNote"];
                if (strNote.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Not giriniz!");
                    return false;
                }

                string strTariffCode = formValues["strTariffCode"];
                if (strTariffCode.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Tarife kodu giriniz!");
                    return false;
                }
            }
            else
            {
                string strPublishComment = formValues["strPublishComment"];
                if (strPublishComment.Trim().Length == 0)
                {
                    Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region AgeTable
        [HttpGet]
        public ActionResult AgeTable()
        {
            return View();
        }
        #endregion

        #region AgeTable
        [HttpPost]
        public ActionResult AgeTable(string str = "")
        {
            try
            {
                List<cProductTable> productTable = new List<cProductTable>();
                if (Sessions.ProductTable != null)
                {
                    productTable = Sessions.ProductTable;
                }

                cProductTable table = new cProductTable();
                table.strColumn = Request.Form["strColumn"];

                productTable.Add(table);

                Sessions.ProductTable = productTable;
                Sessions.ProductTableDelete = true;
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = "<script>$(function() { MsgAgeTable(); }); </script>";
            }

            return View();
        }
        #endregion

        #region AgeTableRow
        [HttpPost]
        public ActionResult AgeTableRow()
        {
            try
            {
                string strType = Request.Form["hdType"];

                List<cProductTable> productTable = Sessions.ProductTable;
                foreach (var item in productTable)
                {
                    foreach (var key in Request.Form.Keys)
                    {
                        if (strType == "insert")
                        {
                            if (item.strColumn == key.ToString())
                            {
                                var values = item.Values;
                                if (values == null)
                                {
                                    values = new List<cProductTableValues>();
                                }

                                values.Add(new cProductTableValues
                                {
                                    Value = Request.Form[key.ToString()]
                                });

                                item.Values = values;
                            }
                        }
                        else
                        {
                            for (int i = 0; i < productTable.Count; i++)
                            {
                                if (item.strColumn + "_" + i == key.ToString())
                                {
                                    var values = item.Values;
                                    if (values == null)
                                    {
                                        values = new List<cProductTableValues>();
                                        values.Add(new cProductTableValues
                                        {
                                            Value = Request.Form[key.ToString()]
                                        });
                                    }
                                    values[i].Value = Request.Form[key.ToString()];

                                    item.Values = values;
                                }
                            }
                        }
                    }
                }

                Sessions.ProductTable = productTable;
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function() { MsgAgeTable(); }); </script>";
                Logs.Error(ex);
            }

            return Redirect("/Product/AgeTable?pi=" + Request.QueryString["pi"]);
        }
        #endregion

        #region AgeTableDelete
        [HttpGet]
        public ActionResult AgeTableDelete()
        {
            Sessions.ProductTable = null;
            Sessions.ProductTableDelete = true;

            return Redirect("/Product/Update?i=" + Request.QueryString["i"]);
        }
        #endregion
    }
}
