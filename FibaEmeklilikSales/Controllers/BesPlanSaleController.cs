﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using System.Text;
using System.Globalization;
using System.Net;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BesPlanSaleController : Controller
    {
        DBEntities db;

        #region New
        [HttpGet]
        [UserFilter]
        public ActionResult New()
        {
            var intLogID = Sessions.BesSaleLog;
            var intBesPlanID = QueryStrings.BesPlanID;
            if (intLogID > 0 && intBesPlanID > 0 && Request.QueryString["new"] == null)
            {
                using (db = new DBEntities())
                {
                    if (db.tblBesSaleLogs.Count(c => c.intBesPlanID == intBesPlanID && c.intLogID == intLogID) == 0)
                    {
                        Sessions.Message = "<script>$(function () { Msg(2, 'Bes ürünü için yeni bir satış başlattığınız için sonlandırdı!'); });</script>";
                        return Redirect("/main/index");
                    }
                }
            }

            var intSalePage = QueryStrings.SalePage;
            if (intSalePage == 0)
            {
                if (intLogID == 0 || Request.QueryString["new"] != null)
                {
                    Logs.BesSaleLog(true, Enums.LogType.ProductSelected, intBesPlanID);
                }
            }

            return View();
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string str = "")
        {
            db = new DBEntities();
            JavaScriptSerializer javaSer = new JavaScriptSerializer();

            bool bolEx = false;
            var intSalePage = QueryStrings.SalePage;
            if (intSalePage == 1)
            {
                Logs.BesSaleLog(false, Enums.LogType.InterMediary, QueryStrings.BesPlanID);
            }
            else if (intSalePage == 2 || intSalePage == 3)
            {
                int intLogID = Sessions.BesSaleLog;
                var saleLog = db.tblBesSaleLogs.Where(w => w.intLogID == intLogID).First();

                if (intSalePage == 2)
                {
                    var info = saleLog.tblBesSaleLogInfo.Where(w => !w.DifferentPaid).FirstOrDefault();
                    if (info != null)
                    {
                        if (info.TCKN == Request.Form["txtTCKN"])
                        {
                            Sessions.Message = "<script>$(function () { Msg(2, 'Kimlik numarası katılımcı ile aynı olmamalı!'); $('#btnMyModalClose').click(); });</script>";
                            return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = 1 });
                        }
                    }

                    Cookies.BesSaleInfo = "2";
                }
                else
                {
                    var info = saleLog.tblBesSaleLogInfo.Where(w => !w.DifferentPaid).FirstOrDefault();
                    if (info != null)
                    {
                        if (info.iletisimno == Request.Form["iletisimno"])
                        {
                            Sessions.Message = "<script>$(function () { Msg(2, 'İletişim numarası katılımcı ile aynı olmamalı!'); $('#btnMyModalClose').click(); });</script>";
                            return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = 2 });
                        }

                        if (info.email == Request.Form["email"])
                        {
                            Sessions.Message = "<script>$(function () { Msg(2, 'E-posta adresi katılımcı ile aynı olmamalı!'); $('#btnMyModalClose').click(); });</script>";
                            return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = 2 });
                        }
                    }

                    Cookies.BesSaleInfo = "2";
                }

                Logs.BesSaleLogInfo(Sessions.BesSaleLog);

                if (intSalePage == 3)
                {
                    if (saleLog.bolDifferentPaid)
                    {
                        return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = 6 });
                    }

                    Cookies.BesSaleInfo = "2";
                }
            }
            else if (intSalePage == 4 || intSalePage == 5)
            {
                Logs.BesSaleLogOrder(Sessions.BesSaleLog);
            }
            else if (intSalePage == 6)
            {
                Logs.BesSaleLog(false, Enums.LogType.InterMediary, QueryStrings.BesPlanID);
                Logs.BesSaleLogOrder(Sessions.BesSaleLog);

                bool bolDifferentPaid = Request.Form["DifferentPaid"] == "on" || Request.Form["DifferentPaid"] == "checked" || Request.Form["DifferentPaid"] == "true";

                if (bolDifferentPaid)
                {
                    return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = 1 });
                }
            }
            else if (intSalePage == 7 || intSalePage == 8)
            {
                string strMsg = "";
                int intLogID = Sessions.BesSaleLog;
                try
                {
                    FibaBES.FibaBesWebServiceClient service = new FibaBES.FibaBesWebServiceClient();

                    var saleLog = db.tblBesSaleLogs.Where(w => w.intLogID == intLogID).First();

                    var saleLogOrder = saleLog.tblBesSaleLogOrder.First();

                    if (intSalePage == 7)
                    {
                        #region genbilgi
                        var genbilgi = new FibaBES.EmkbasvurulibgeneltypUser();
                        genbilgi.odetip = "K";
                        genbilgi.iskanunivaris = "T";

                        var taksitsayisi = service.gettaksitsayisi(new FibaBES.EmkblibgataksitinputtypUser
                        {
                            dovkod = "TL",
                            isga = "T",
                            tarifeno = saleLog.tblBesPlans.strPlanCode
                        });

                        if (taksitsayisi != null && taksitsayisi.result != null && taksitsayisi.result.Count > 0)
                        {
                            genbilgi.gataksitsayisi = Utilities.NullFixDecimal(taksitsayisi.result[0].extraf1);
                        }

                        genbilgi.ilkvadetar = saleLogOrder.FirstDueDate.Replace(".", "").Replace("/", "");
                        genbilgi.tarifeno = saleLog.tblBesPlans.strPlanCode;
                        genbilgi.portfoy = saleLog.strInterMediaryCode;
                        genbilgi.kptaksitsayisi = saleLogOrder.intOrderlyContAmountPeriod;
                        genbilgi.dovkod = "TL";
                        genbilgi.katkipayi = saleLogOrder.flOrderlyContAmount;
                        genbilgi.finansaldanisman = Sessions.CurrentUser.tblCallcenters.strRegisterNo;
                        genbilgi.baslangickp = saleLogOrder.flInitilalContAmount;
                        genbilgi.basvurutar = DateTime.Now.ToString("dd-MM-yyyy").Replace("-", "");
                        genbilgi.bkodetip = "K";
                        genbilgi.ekkatkipayi = saleLogOrder.flExtraContAmount;
                        genbilgi.iskatodeyen = saleLog.bolDifferentPaid ? "F" : "T";

                        if (saleLog.tblBesPlans.intGroupID.HasValue)
                        {
                            genbilgi.grupid = saleLog.tblBesPlans.intGroupID.Value;
                        }

                        if (saleLogOrder.InitialDifferentPaid)
                            genbilgi.bkodetip = "H";
                        #endregion

                        #region perbilgi
                        var perbilgi = PerBilgi(saleLog.tblBesSaleLogInfo.Where(w => !w.DifferentPaid).First());
                        #endregion

                        #region odeyenbilgi
                        var odeyenbilgi = new FibaBES.EmkbasvurulibpersontypUser();
                        if (saleLog.bolDifferentPaid)
                            odeyenbilgi = OdeyenBilgi(saleLog.tblBesSaleLogInfo.Where(w => w.DifferentPaid).First());
                        #endregion

                        #region fnbilgi
                        var fnbilgi = new FibaBES.Emkbasvurulibfontyl();

                        List<cFonList> fonList = new List<cFonList>();
                        fonList = javaSer.Deserialize<List<cFonList>>(saleLogOrder.FonList);

                        foreach (var item in fonList)
                        {
                            fnbilgi.Add(new FibaBES.EmkbasvurulibfontypUser
                            {
                                fonkod = item.FonKod,
                                fonoran = Utilities.NullFixDecimal(item.Oran) / 100
                            });
                        }
                        #endregion

                        #region kkbilgi
                        var kkbilgi = new FibaBES.EmkbasvurulibkkarttypUser();
                        kkbilgi.cvv1 = Utilities.NullFixDecimal(Request.Form["cvv"]);
                        kkbilgi.sonkultar1 = "01" + Request.Form["kartay"] + Request.Form["kartyil"];
                        kkbilgi.kkartno1 = Request.Form["kartno"].Replace(" ", "").Trim();
                        kkbilgi.ad1 = saleLog.bolDifferentPaid ? odeyenbilgi.ad : perbilgi.ad;
                        kkbilgi.soyad1 = saleLog.bolDifferentPaid ? odeyenbilgi.soyad : perbilgi.soyad;
                        kkbilgi.kktip1 = Request.Form["karttipi"];

                        Sessions.CreditCard = new cCreditCard
                        {
                            card = Request.Form["kartno"].Replace(" ", "").Trim(),
                            month = Request.Form["kartay"],
                            year = Request.Form["kartyil"],
                            cvv = Utilities.NullFixDecimal(Request.Form["cvv"]),
                            type = Request.Form["karttipi"]
                        };
                        #endregion

                        #region hspbilgi
                        var hspbilgi = new FibaBES.EmkbasvurulibhesaptypUser();
                        if (saleLogOrder.InitialDifferentPaid)
                        {
                            hspbilgi.bankano = 103;
                            hspbilgi.hesapno = Request.Form["hesapno"];
                            hspbilgi.subeno = Request.Form["subeno"];
                            hspbilgi.hesapadi = perbilgi.ad + " " + perbilgi.soyad;
                        }
                        #endregion

                        var pUretim = new FibaBES.EmkbasvuruliburetiminputtypUser
                        {
                            perbilgi = perbilgi,
                            odeyenbilgi = odeyenbilgi,
                            genbilgi = genbilgi,
                            userbilgi = new FibaBES.EmkbasvurulibuserinputtypUser
                            {
                                password = ConfigurationManager.AppSettings["BesWsPss"],
                                kullanicikodu = Sessions.CurrentUser.tblCallcenters.strRegisterNo,
                                bankano = null,
                                username = ConfigurationManager.AppSettings["BesWsUserName"],
                                subeno = Utilities.NullFixDecimal(Sessions.CurrentUser.tblCallcenters.strBankNo)
                            },
                            kkbilgi = kkbilgi,
                            fnbilgi = fnbilgi,
                            hspbilgi = hspbilgi
                        };

                        int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Input", javaSer.Serialize(pUretim), "setbasvuru");

                        var result = service.setbasvuru(pUretim);

                        Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Output", javaSer.Serialize(result), "setbasvuru");

                        if (result.polid != null)
                        {
                            Logs.BesSaleLogPolice(false, intLogID, "setbasvuru", result.polid.Value.ToString(), result.sonuc.hataaciklama);
                        }
                        else
                        {
                            if (result.sonuc != null)
                            {
                                strMsg = result.sonuc.hataaciklama;
                                throw new Exception(result.sonuc.hataaciklama);
                            }
                            else
                            {
                                throw new Exception("Setbasvuru gönderilirken hata oluştu.");
                            }
                        }

                        if (result.polid > 0)
                        {
                            if (saleLogOrder.InitialDifferentPaid)
                            {
                                Logs.BesSaleLogOrder(intLogID);
                            }

                            Cookies.BesSaleInfo = "7";
                        }
                        else
                        {
                            return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = "6" });
                        }
                    }
                    else if (intSalePage == 8)
                    {
                        FibaBES.EmkbasvurulibuserinputtypUser pUserbilgi = new FibaBES.EmkbasvurulibuserinputtypUser
                         {
                             password = ConfigurationManager.AppSettings["BesWsPss"],
                             kullanicikodu = ConfigurationManager.AppSettings["BesWsUserCode"],
                             bankano = null,
                             username = ConfigurationManager.AppSettings["BesWsUserName"],
                             subeno = Utilities.NullFixDecimal(Sessions.CurrentUser.tblCallcenters.strBankNo)
                         };

                        int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Input", javaSer.Serialize(pUserbilgi), "tanzim");

                        var tanzim = service.settanzim(Utilities.NullFixDecimal(saleLog.tblBesSaleLogPoliceler.Last().intPolID), pUserbilgi);

                        Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Output", javaSer.Serialize(tanzim), "tanzim");

                        BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();
                        //bankaWeb.tanzimsmsgonder(tanzim.polid);

                        Logs.BesSaleLogPolice(false, intLogID, "settanzim", tanzim.polid.Value.ToString(), tanzim.sonuc.hataaciklama);

                        decimal tutar = 0;
                        if (saleLog.tblBesPlans.bolInitialCont)
                        {
                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount;
                        }
                        else if (saleLog.tblBesSaleLogOrder.First().flInitilalContAmount > 0)
                        {
                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount + saleLog.tblBesSaleLogOrder.First().flInitilalContAmount;
                        }
                        else
                        {
                            tutar = saleLog.tblBesSaleLogOrder.First().flOrderlyContAmount;
                        }

                        BankaWebV2.bankaweb_v2Client service2 = new BankaWebV2.bankaweb_v2Client();
                        BankaWebV2.BankawebV2SanalposinUser pInput = new BankaWebV2.BankawebV2SanalposinUser
                        {
                            cvv = Sessions.CreditCard.cvv,
                            expmonth = Sessions.CreditCard.month,
                            expyear = Sessions.CreditCard.year,
                            kartno = Sessions.CreditCard.card,
                            polid = tanzim.polid,
                            taksitadedi = 1,
                            tutar = tutar,
                            user = "HYSG",
                            vadelist = new BankaWebV2.Emkblibsposvadetyl
                            {
                                new BankaWebV2.EmkblibsposvadetypUser
                                {
                                    tutar = tutar,
                                    vadetar = DateTime.Now.ToString("ddMMyyyy")
                                }
                            }
                        };

                        intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Input", "", "spostaksitli");

                        var spostaksitli = service2.spostaksitli(pInput);

                        Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, saleLog.intLogID, "bes", "New", "Output", javaSer.Serialize(spostaksitli), "spostaksitli");

                        Logs.BesSaleLogPay(intLogID, spostaksitli.issuccess, spostaksitli.aciklama);

                        //Email.SendBes(saleLog, "Teklifi fibaemekliliğe gönder aracılığıyla gönderilmiştir");
                    }
                }
                catch (Exception ex)
                {
                    bolEx = true;
                    Logs.BesSaleLogEx(intLogID, Logs.Error(ex), "");

                    Sessions.Message = "<script>$(function () { Msg(2, '" + (strMsg == "" ? "Bir hata oluştu! Poliçe oluşturulamadı." : strMsg) + "'); $('#btnMyModalClose').click(); });</script>";

                    return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = (intSalePage - 1) });
                }
            }

            db.Dispose();

            if (!bolEx)
            {
                return RedirectToAction("New", "BesPlanSale", new { bp = Request.QueryString["bp"], sp = intSalePage });
            }
            else
            {
                Sessions.BesSaleLog = 0;
                Cookies.BesSaleLog = 0;

                Cookies.BesSaleInfo = "0";

                return RedirectToAction("New", "BesPlanSale");
            }
        } 

        #endregion

        #region SaveLog
        private void SaveLog(string strMethod, string strLog)
        {
            try
            {
                using (FileStream fs = new FileStream(Server.MapPath("~/Logs/" + strMethod + ".txt"), FileMode.CreateNew))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(strLog);
                    }
                }
            }
            catch { }
        }
        #endregion

        #region PerBilgi
        private FibaBES.EmkbasvurulibpersontypUser PerBilgi(tblBesSaleLogInfo saleLogInfo)
        {
            var perbilgi = new FibaBES.EmkbasvurulibpersontypUser();
            perbilgi.iskpsadres = "T";
            perbilgi.adrestip = "E";
            perbilgi.il = saleLogInfo.ilkod;
            perbilgi.tckimlikno = saleLogInfo.TCKN;
            perbilgi.ulke = saleLogInfo.ulkekod;
            perbilgi.ceptel = saleLogInfo.iletisimno;
            perbilgi.adres = saleLogInfo.adres;
            perbilgi.iskolu = "";
            perbilgi.ulke = saleLogInfo.ulkekod;
            perbilgi.evtel = saleLogInfo.iletisimno;

            var ulkeler = db.tblMapping.Where(w => w.strType == "UYR" && w.strName.Trim() == saleLogInfo.ulkekod).FirstOrDefault();
            if (ulkeler != null)
                perbilgi.dogulke = ulkeler.strCountryCode;

            perbilgi.calsekli = "3";
            perbilgi.egitimdurum = "4";
            perbilgi.uyruk = saleLogInfo.uyruk;
            perbilgi.meslek = "028";
            perbilgi.ilce = saleLogInfo.ilcekod;
            perbilgi.kimliktur = "NUF";
            perbilgi.email = saleLogInfo.email;
            perbilgi.ad = saleLogInfo.adi;

            string dogumTarihi = saleLogInfo.dogumTarihi.Replace("-", ".");
            var dogumTarihiSp = dogumTarihi.Split('.');
            dogumTarihi = "";
            for (int i = 0; i < dogumTarihiSp.Length - 1; i++)
            {
                dogumTarihi += dogumTarihiSp[i].Length == 1 ? "0" + dogumTarihiSp[i] : dogumTarihiSp[i];
            }

            dogumTarihi += dogumTarihiSp[dogumTarihiSp.Length - 1];

            perbilgi.dogtar = dogumTarihi;
            perbilgi.medenihal = saleLogInfo.medenidurum;
            perbilgi.cinsiyet = saleLogInfo.cinsiyet;
            perbilgi.babaadi = saleLogInfo.babaadi;
            perbilgi.anaadi = saleLogInfo.anneadi;
            perbilgi.soyad = saleLogInfo.soyadi;

            if (saleLogInfo.uyruk == "TR")
            {
                var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == saleLogInfo.dogumYeri.Trim()).FirstOrDefault();
                if (il != null)
                    perbilgi.dogil = il.strCode.Trim();

                var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == saleLogInfo.dogumYeri.Trim()).FirstOrDefault();
                if (ilce != null)
                    perbilgi.dogilce = ilce.strCode.Trim();

                if (perbilgi.dogil == "" && perbilgi.dogilce != "")
                    perbilgi.dogil = perbilgi.dogilce.Substring(0, 3);

                if (perbilgi.dogil == "")
                    perbilgi.dogil = "99";

                if (perbilgi.dogil != "" && perbilgi.dogilce == "")
                    perbilgi.dogilce = "99900";
            }
            else
            {
                perbilgi.dogil = "99";
                perbilgi.dogilce = "99900";
            }

            return perbilgi;
        }
        #endregion

        #region OdeyenBilgi
        private FibaBES.EmkbasvurulibpersontypUser OdeyenBilgi(tblBesSaleLogInfo saleLogInfo)
        {
            var odeyenbilgi = new FibaBES.EmkbasvurulibpersontypUser();
            odeyenbilgi.iskpsadres = "T";
            odeyenbilgi.adrestip = "E";
            odeyenbilgi.il = saleLogInfo.ilkod;
            odeyenbilgi.tckimlikno = saleLogInfo.TCKN;
            odeyenbilgi.bolgekodu = saleLogInfo.ilkod;
            odeyenbilgi.ulke = saleLogInfo.ulkekod;
            odeyenbilgi.ceptel = saleLogInfo.iletisimno;
            odeyenbilgi.adres = saleLogInfo.adres;
            odeyenbilgi.iskolu = "";
            odeyenbilgi.ulke = saleLogInfo.ulkekod;
            odeyenbilgi.evtel = saleLogInfo.iletisimno;

            var ulkeler = db.tblMapping.Where(w => w.strType == "UYR" && w.strName.Trim() == saleLogInfo.ulkekod).FirstOrDefault();
            if (ulkeler != null)
                odeyenbilgi.dogulke = ulkeler.strCountryCode;

            odeyenbilgi.calsekli = "3";
            odeyenbilgi.egitimdurum = "4";
            odeyenbilgi.uyruk = saleLogInfo.uyruk;
            odeyenbilgi.meslek = "028";
            odeyenbilgi.ilce = saleLogInfo.ilcekod;
            odeyenbilgi.kimliktur = "NUF";
            odeyenbilgi.email = saleLogInfo.email;
            odeyenbilgi.ad = saleLogInfo.adi;

            string dogumTarihi = saleLogInfo.dogumTarihi.Replace("-", ".");
            var dogumTarihiSp = dogumTarihi.Split('.');
            dogumTarihi = "";
            for (int i = 0; i < dogumTarihiSp.Length - 1; i++)
            {
                dogumTarihi += dogumTarihiSp[i].Length == 1 ? "0" + dogumTarihiSp[i] : dogumTarihiSp[i];
            }

            dogumTarihi += dogumTarihiSp[dogumTarihiSp.Length - 1];

            odeyenbilgi.dogtar = dogumTarihi;
            odeyenbilgi.medenihal = saleLogInfo.medenidurum;
            odeyenbilgi.cinsiyet = saleLogInfo.cinsiyet;
            odeyenbilgi.babaadi = saleLogInfo.babaadi;
            odeyenbilgi.anaadi = saleLogInfo.anneadi;
            odeyenbilgi.soyad = saleLogInfo.soyadi;

            if (saleLogInfo.uyruk == "TR")
            {
                var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == saleLogInfo.dogumYeri.Trim()).FirstOrDefault();
                if (il != null)
                    odeyenbilgi.dogil = il.strCode.Trim();

                var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == saleLogInfo.dogumYeri.Trim()).FirstOrDefault();
                if (ilce != null)
                    odeyenbilgi.dogilce = ilce.strCode.Trim();

                if (odeyenbilgi.dogil == "" && odeyenbilgi.dogilce != "")
                    odeyenbilgi.dogil = odeyenbilgi.dogilce.Substring(0, 3);

                if (odeyenbilgi.dogil == "")
                    odeyenbilgi.dogil = "99";

                if (odeyenbilgi.dogil != "" && odeyenbilgi.dogilce == "")
                    odeyenbilgi.dogilce = "99900";
            }
            else
            {
                odeyenbilgi.dogil = "99";
                odeyenbilgi.dogilce = "99900";
            }

            return odeyenbilgi;
        }
        #endregion

        #region PolicyView
        [HttpGet]
        [UserFilter]
        public ActionResult PolicyView()
        {
            return View();
        }
        #endregion

        #region CancelRedirect
        [HttpPost]
        public ActionResult PolicyView(string str = "")
        {
            return Redirect("/besplansale/policyview?txtSearch=" + cCrypto.EncryptText(Request.Form["txtSearch"]));
        }
        #endregion

        #region PolicyViewDetail
        [HttpGet]
        public ActionResult PolicyViewDetail()
        {
            return View();
        }
        #endregion

        #region PdfView
        [UserFilter]
        [HttpGet]
        public ActionResult PdfView()
        {
            string strPoliceNo = cCrypto.DecryptText(Request.QueryString["police"]);

            string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=sozlesme_bes_matbaa.rdf&desname=" + strPoliceNo + DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") + ".pdf&server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + strPoliceNo;

            Response.ContentEncoding = Encoding.GetEncoding("windows-1254");
            Response.Charset = "windows-1254";
            Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
            Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
            Response.ContentType = "application/pdf";
            Response.Expires = 0;
            Response.Write(UrlGet(strUrl, "GET"));

            return View();
        }

        string UrlGet(string Url, string Method)
        {
            string result = "";
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
                myRequest.Method = Method;
                myRequest.ContentType = "application/pdf";

                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                Stream encodingStream = myResponse.GetResponseStream();
                StreamReader read = new StreamReader(encodingStream, Encoding.GetEncoding("utf-8"));
                result = read.ReadToEnd();
                myResponse.Close();
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                throw ex;
            }

            return result;
        }
        #endregion
    }
}
