﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class MappingController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intID = QueryStrings.ID;
                    if (intID > 0)
                    {
                        var mapping = db.tblMapping.Where(w => w.intID == intID).FirstOrDefault();
                        if (mapping != null)
                        {
                            db.tblMapping.Remove(mapping);
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intID);
                        }

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kayıt silindi.");
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Kayıt bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Mapping", new { type = Request.QueryString["type"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            var formValues = Request.Form;

            if (!Validate())
            {
                return RedirectToAction("New", "Mapping", new { i = Request.QueryString["i"] });
            }

            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    tblMapping mapping = new tblMapping();
                    mapping.strCode = formValues["strCode"];
                    mapping.strCountryCode = formValues["strCountryCode"];
                    mapping.strName = formValues["strName"];
                    mapping.strType = formValues["strType"];
                    mapping.dtRegisterDate = DateTime.Now;

                    db.tblMapping.Add(mapping);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, mapping.intID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kayıt eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Mapping");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            var formValues = Request.Form;

            if (!Validate())
            {
                return RedirectToAction("Update", "Mapping", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    int intID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    var mapping = db.tblMapping.Where(w => w.intID == intID).FirstOrDefault();

                    mapping.strCode = formValues["strCode"];
                    mapping.strCountryCode = formValues["strCountryCode"];
                    mapping.strName = formValues["strName"];
                    mapping.strType = formValues["strType"];
                    mapping.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, mapping.intID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Kayıt güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Mapping");
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strCode = formValues["strCode"];
            if (strCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kps kodu giriniz!");
                return false;
            }

            string strName = formValues["strName"];
            if (strName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
