﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using System.Web.Script.Serialization;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class CallcenterController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intCallcenterID = QueryStrings.ID;
                    if (intCallcenterID > 0)
                    {
                        var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).FirstOrDefault();
                        if (callcenter != null)
                        {
                            if (db.tblSaleLogs.Count(w => w.tblUsers.intCallcenterID == intCallcenterID) == 0)
                            {
                                callcenter.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intCallcenterID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Satış kanalı silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Satış kanalına ait satış işlemleri var!");
                            }
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Satış kanalı bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Satış kanalı bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Callcenter", new { txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Callcenter", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strName = formValues["strName"];
                    if (db.tblCallcenters.Count(c => !c.bolIsDelete && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı isimde satış kanalı sistemde mevcut!");
                        return RedirectToAction("New", "Callcenter");
                    }


                    bool bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";
                    if (bolWebSale)
                    {
                        if (db.tblCallcenters.Count(c => c.intSaleTypeID == 1) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Farklı bir web satış kanalı sistemde mevcut!");
                            return RedirectToAction("New", "Callcenter");
                        }
                    }

                    tblCallcenters callcenter = new tblCallcenters();
                    callcenter.strName = strName;
                    callcenter.strEmail = formValues["strEmail"];
                    callcenter.strRegisterNo = formValues["strRegisterNo"];
                    callcenter.strBankNo = formValues["strBankNo"];
                    callcenter.strCallcenterCode = formValues["strCallcenterCode"];
                    callcenter.strAddress = formValues["strAddress"];
                    callcenter.strFax = formValues["strFax"];
                    callcenter.strPhone = formValues["strPhone"];
                    callcenter.strTradeName = formValues["strTradeName"];
                    callcenter.strOfferText = formValues["strOfferText"];
                    callcenter.strTaxNumber = formValues["strTaxNumber"];
                    callcenter.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    callcenter.bolInfoForm = formValues["bolInfoForm"] == "on" || formValues["bolInfoForm"] == "checked" || formValues["bolInfoForm"] == "true";
                    callcenter.bolBesSaleFinishedButton = formValues["bolBesSaleFinishedButton"] == "on" || formValues["bolBesSaleFinishedButton"] == "checked" || formValues["bolBesSaleFinishedButton"] == "true";
                    callcenter.intSaleTypeID = Utilities.NullFixByte(formValues["intSaleTypeID"]);
                    callcenter.bolIsDelete = false;
                    callcenter.dtRegisterDate = DateTime.Now;
                    callcenter.strPublishKey = Utilities.ConvertTurkish(formValues["strPublishKey"]).Replace(" ", "");
                    callcenter.bolProductIssued = formValues["bolProductIssued"] == "on" || formValues["bolProductIssued"] == "checked" || formValues["bolProductIssued"] == "true";
                    callcenter.bolBesIssued = formValues["bolBesIssued"] == "on" || formValues["bolBesIssued"] == "checked" || formValues["bolBesIssued"] == "true";
                    callcenter.bolCreditIssued = formValues["bolCreditIssued"] == "on" || formValues["bolCreditIssued"] == "checked" || formValues["bolCreditIssued"] == "true";
                    callcenter.bolValidCreditCard = formValues["bolValidCreditCard"] == "on" || formValues["bolValidCreditCard"] == "checked" || formValues["bolValidCreditCard"] == "true";
                    //callcenter.bolPolicyBesPaymentChange = formValues["bolPolicyBesPaymentChange"] == "on" || formValues["bolPolicyBesPaymentChange"] == "checked" || formValues["bolPolicyBesPaymentChange"] == "true";
                    callcenter.bolSpecialSale = formValues["bolSpecialSale"] == "on" || formValues["bolSpecialSale"] == "checked" || formValues["bolSpecialSale"] == "true";
                    callcenter.intCallcenterType = Utilities.NullFixByte(formValues["intCallcenterType"]);

                    if (callcenter.intSaleTypeID == 2)
                    {
                        callcenter.strInformationForm = formValues["strInformationForm"];
                        callcenter.strInformationFormBesPlan = formValues["strInformationFormBesPlan"];
                        callcenter.strInformationFormCredit = formValues["strInformationFormCredit"];
                        callcenter.flBSMV = Utilities.NullFixDecimal(formValues["flBSMV"]);
                        callcenter.flKKDF = Utilities.NullFixDecimal(formValues["flKKDF"]);
                    }
                    else
                    {
                        callcenter.strInformationForm = "";
                        callcenter.strInformationFormBesPlan = "";
                        callcenter.strInformationFormCredit = "";
                        callcenter.flBSMV = 0;
                        callcenter.flKKDF = 0;
                    }

                    string strVisibleInputs = "";
                    if (formValues["bolShowFather"] == "on" || formValues["bolShowFather"] == "checked" || formValues["bolShowFather"] == "true")
                    {
                        strVisibleInputs = "father";
                    }

                    if (formValues["bolShowMother"] == "on" || formValues["bolShowMother"] == "checked" || formValues["bolShowMother"] == "true")
                    {
                        strVisibleInputs = strVisibleInputs == "" ? "mother" : strVisibleInputs + ",mother";
                    }

                    if (formValues["bolShowBirtPlace"] == "on" || formValues["bolShowBirtPlace"] == "checked" || formValues["bolShowBirtPlace"] == "true")
                    {
                        strVisibleInputs = strVisibleInputs == "" ? "birthplace" : strVisibleInputs + ",birthplace";
                    }

                    callcenter.strVisibleInputs = strVisibleInputs;
                    callcenter.strReportPermisions = ReportPermisionJson(formValues);

                    db.tblCallcenters.Add(callcenter);
                    db.SaveChanges();

                    ProductAndPlanAndCredit(callcenter.intCallcenterID, callcenter.intSaleTypeID);

                    Logs.Log(Enums.LogType.Add, callcenter.intCallcenterID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Satış kanalı eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Callcenter");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Callcenter", new { i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intCallcenterID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));

                    string strName = formValues["strName"];
                    if (db.tblCallcenters.Count(c => !c.bolIsDelete && c.intCallcenterID != intCallcenterID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı isimde satış kanalı sistemde mevcut!");
                        return RedirectToAction("Update", "Callcenter", new { i = Request.QueryString["i"] });
                    }

                    bool bolWebSale = formValues["bolWebSale"] == "on" || formValues["bolWebSale"] == "checked" || formValues["bolWebSale"] == "true";

                    if (bolWebSale)
                    {
                        if (db.tblCallcenters.Count(c => c.intCallcenterID != intCallcenterID && c.intSaleTypeID == 1) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Farklı bir web satış kanalı sistemde mevcut!");
                            return RedirectToAction("Update", "Callcenter", new { i = Request.QueryString["i"] });
                        }
                    }

                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();

                    callcenter.strName = strName;
                    callcenter.strEmail = formValues["strEmail"];
                    callcenter.strRegisterNo = formValues["strRegisterNo"];
                    callcenter.strBankNo = formValues["strBankNo"];
                    callcenter.strCallcenterCode = formValues["strCallcenterCode"];
                    callcenter.strAddress = formValues["strAddress"];
                    callcenter.strFax = formValues["strFax"];
                    callcenter.strPhone = formValues["strPhone"];
                    callcenter.strTradeName = formValues["strTradeName"];
                    callcenter.strOfferText = formValues["strOfferText"];
                    callcenter.strTaxNumber = formValues["strTaxNumber"];
                    callcenter.bolCancel = formValues["bolCancel"] == "on" || formValues["bolCancel"] == "checked" || formValues["bolCancel"] == "true";
                    callcenter.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    callcenter.bolInfoForm = formValues["bolInfoForm"] == "on" || formValues["bolInfoForm"] == "checked" || formValues["bolInfoForm"] == "true";
                    callcenter.bolBesSaleFinishedButton = formValues["bolBesSaleFinishedButton"] == "on" || formValues["bolBesSaleFinishedButton"] == "checked" || formValues["bolBesSaleFinishedButton"] == "true";
                    callcenter.intSaleTypeID = Utilities.NullFixByte(formValues["intSaleTypeID"]);
                    callcenter.dtUpdateDate = DateTime.Now;
                    callcenter.strPublishKey = Utilities.ConvertTurkish(formValues["strPublishKey"]).Replace(" ", "");
                    callcenter.bolProductIssued = formValues["bolProductIssued"] == "on" || formValues["bolProductIssued"] == "checked" || formValues["bolProductIssued"] == "true";
                    callcenter.bolBesIssued = formValues["bolBesIssued"] == "on" || formValues["bolBesIssued"] == "checked" || formValues["bolBesIssued"] == "true";
                    callcenter.bolCreditIssued = formValues["bolCreditIssued"] == "on" || formValues["bolCreditIssued"] == "checked" || formValues["bolCreditIssued"] == "true";
                    callcenter.bolValidCreditCard = formValues["bolValidCreditCard"] == "on" || formValues["bolValidCreditCard"] == "checked" || formValues["bolValidCreditCard"] == "true";
                    callcenter.bolPolicyBesPaymentChange = formValues["bolPolicyBesPaymentChange"] == "on" || formValues["bolPolicyBesPaymentChange"] == "checked" || formValues["bolPolicyBesPaymentChange"] == "true";
                    callcenter.bolSpecialSale = formValues["bolSpecialSale"] == "on" || formValues["bolSpecialSale"] == "checked" || formValues["bolSpecialSale"] == "true";
                    callcenter.intCallcenterType = Utilities.NullFixByte(formValues["intCallcenterType"]);

                    if (callcenter.intSaleTypeID == 2)
                    {
                        callcenter.strInformationForm = formValues["strInformationForm"];
                        callcenter.strInformationFormBesPlan = formValues["strInformationFormBesPlan"];
                        callcenter.strInformationFormCredit = formValues["strInformationFormCredit"];
                        callcenter.flBSMV = Utilities.NullFixDecimal(formValues["flBSMV"]);
                        callcenter.flKKDF = Utilities.NullFixDecimal(formValues["flKKDF"]);
                    }
                    else
                    {
                        callcenter.strInformationForm = "";
                        callcenter.strInformationFormBesPlan = "";
                        callcenter.strInformationFormCredit = "";
                        callcenter.flBSMV = 0;
                        callcenter.flKKDF = 0;
                    }

                    string strVisibleInputs = "";
                    if (formValues["bolShowFather"] == "on" || formValues["bolShowFather"] == "checked" || formValues["bolShowFather"] == "true")
                    {
                        strVisibleInputs = "father";
                    }

                    if (formValues["bolShowMother"] == "on" || formValues["bolShowMother"] == "checked" || formValues["bolShowMother"] == "true")
                    {
                        strVisibleInputs = strVisibleInputs == "" ? "mother" : strVisibleInputs + ",mother";
                    }

                    if (formValues["bolShowBirtPlace"] == "on" || formValues["bolShowBirtPlace"] == "checked" || formValues["bolShowBirtPlace"] == "true")
                    {
                        strVisibleInputs = strVisibleInputs == "" ? "birthplace" : strVisibleInputs + ",birthplace";
                    }

                    callcenter.strVisibleInputs = strVisibleInputs;
                    callcenter.strReportPermisions = ReportPermisionJson(formValues);

                    ProductAndPlanAndCredit(intCallcenterID, callcenter.intSaleTypeID);

                    foreach (var item in db.tblUsers.Where(w => w.intCallcenterID == intCallcenterID).ToList())
                    {
                        foreach (var auth in item.tblUserAuth.Where(w => w.intUserID == item.intUserID && w.intPageID == 5).ToList())
                        {
                            auth.bolView = callcenter.bolCancel;
                            auth.bolUpdate = callcenter.bolCancel;
                        }
                    }
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, callcenter.intCallcenterID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Satış kanalı güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Callcenter");
        }
        #endregion

        #region ProductAndPlanAndCredit
        private void ProductAndPlanAndCredit(int intCallcenterID, byte intSaleTypeID)
        {
            var formValues = Request.Form;

            #region products
            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterProducts WHERE bolSpecialSale = 0 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            var products = string.IsNullOrEmpty(formValues["hdProducts"]) ? new List<string>() : formValues["hdProducts"].Split(',').ToList();
            foreach (var item in products)
            {
                tblCallcenterProducts callcenterProducts = new tblCallcenterProducts();
                callcenterProducts.intCallcenterID = intCallcenterID;
                callcenterProducts.intProductID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                callcenterProducts.bolSpecialSale = false;

                db.tblCallcenterProducts.Add(callcenterProducts);
            }
            db.SaveChanges(); 
            #endregion

            #region BesPlans
            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterBesPlans WHERE bolSpecialSale = 0 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            var besPlans = string.IsNullOrEmpty(formValues["hdBesPlans"]) ? new List<string>() : formValues["hdBesPlans"].Split(',').ToList();
            foreach (var item in besPlans)
            {
                tblCallcenterBesPlans callcenterBesPlan = new tblCallcenterBesPlans();
                callcenterBesPlan.intCallcenterID = intCallcenterID;
                callcenterBesPlan.intBesPlanID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                callcenterBesPlan.bolSpecialSale = false;

                db.tblCallcenterBesPlans.Add(callcenterBesPlan);
            }
            db.SaveChanges(); 
            #endregion

            #region CallcenterCredits
            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterCredits WHERE bolSpecialSale = 0 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            var credits = string.IsNullOrEmpty(formValues["hdCredits"]) ? new List<string>() : formValues["hdCredits"].Split(',').ToList();
            foreach (var item in credits)
            {
                tblCallcenterCredits callcenterCredit = new tblCallcenterCredits();
                callcenterCredit.intCallcenterID = intCallcenterID;
                callcenterCredit.intCreditID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                callcenterCredit.bolSpecialSale = false;

                db.tblCallcenterCredits.Add(callcenterCredit);
            }
            db.SaveChanges(); 
            #endregion

            #region FibabankProducts
            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterFibabankProducts WHERE intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            var fibabankProducts = string.IsNullOrEmpty(formValues["hdFibabankProducts"]) ? new List<string>() : formValues["hdFibabankProducts"].Split(',').ToList();
            foreach (var item in fibabankProducts)
            {
                tblCallcenterFibabankProducts callcenterFibabankProducts = new tblCallcenterFibabankProducts();
                callcenterFibabankProducts.intCallcenterID = intCallcenterID;
                callcenterFibabankProducts.intFibabankProductID = Utilities.NullFixInt(cCrypto.DecryptText(item));

                db.tblCallcenterFibabankProducts.Add(callcenterFibabankProducts);
            }
            db.SaveChanges();
            #endregion

            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterProducts WHERE bolSpecialSale = 1 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterBesPlans WHERE bolSpecialSale = 1 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            db.Database.ExecuteSqlCommand("DELETE FROM tblCallcenterCredits WHERE bolSpecialSale = 1 AND intCallcenterID = {0}", intCallcenterID);
            db.SaveChanges();

            if (intSaleTypeID == 2)
            {
                #region Products
                var specialProducts = string.IsNullOrEmpty(formValues["hdSpecialProducts"]) ? new List<string>() : formValues["hdSpecialProducts"].Split(',').ToList();
                foreach (var item in specialProducts)
                {
                    tblCallcenterProducts callcenterProducts = new tblCallcenterProducts();
                    callcenterProducts.intCallcenterID = intCallcenterID;
                    callcenterProducts.intProductID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                    callcenterProducts.bolSpecialSale = true;

                    db.tblCallcenterProducts.Add(callcenterProducts);
                }
                db.SaveChanges(); 
                #endregion

                #region BesPlans
                var specialBesPlans = string.IsNullOrEmpty(formValues["hdSpecialBesPlans"]) ? new List<string>() : formValues["hdSpecialBesPlans"].Split(',').ToList();
                foreach (var item in specialBesPlans)
                {
                    tblCallcenterBesPlans callcenterBesPlan = new tblCallcenterBesPlans();
                    callcenterBesPlan.intCallcenterID = intCallcenterID;
                    callcenterBesPlan.intBesPlanID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                    callcenterBesPlan.bolSpecialSale = true;

                    db.tblCallcenterBesPlans.Add(callcenterBesPlan);
                }
                db.SaveChanges(); 
                #endregion

                #region Credit
                var specialCredits = string.IsNullOrEmpty(formValues["hdSpecialCredit"]) ? new List<string>() : formValues["hdSpecialCredit"].Split(',').ToList();
                foreach (var item in specialCredits)
                {
                    tblCallcenterCredits callcenterCredit = new tblCallcenterCredits();
                    callcenterCredit.intCallcenterID = intCallcenterID;
                    callcenterCredit.intCreditID = Utilities.NullFixInt(cCrypto.DecryptText(item));
                    callcenterCredit.bolSpecialSale = true;

                    db.tblCallcenterCredits.Add(callcenterCredit);
                }
                db.SaveChanges(); 
                #endregion
            }
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strNameSurname = formValues["strName"];
            if (strNameSurname.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad giriniz!");
                return false;
            }

            string strEmail = formValues["strEmail"];
            if (strEmail.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta giriniz!");
                return false;
            }

            string strRegisterNo = formValues["strRegisterNo"];
            if (strRegisterNo.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Sicil numasını giriniz!");
                return false;
            }

            string strBankNo = formValues["strBankNo"];
            if (strBankNo.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Banka şube kodunu giriniz!");
                return false;
            }

            string strCallcenterCode = formValues["strCallcenterCode"];
            if (strCallcenterCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Acente no giriniz!");
                return false;
            }

            string strPublishKey = formValues["strPublishKey"];
            if (strPublishKey.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Online emeklilik satış anahtarını giriniz!");
                return false;
            }

            return true;
        }
        #endregion

        #region ReportPermisionJson
        string ReportPermisionJson(System.Collections.Specialized.NameValueCollection formValues)
        {
            List<cReportPermisions> reportPermisions = new List<cReportPermisions>();
            reportPermisions.Add(new cReportPermisions
            {
                bolExcel = formValues["bolAdminExcel"] == "on" || formValues["bolAdminExcel"] == "checked" || formValues["bolAdminExcel"] == "true",
                bolPage = formValues["bolAdminPage"] == "on" || formValues["bolAdminPage"] == "checked" || formValues["bolAdminPage"] == "true",
                UserType = Enums.UserType.Admin
            });

            reportPermisions.Add(new cReportPermisions
            {
                bolExcel = formValues["bolUserExcel"] == "on" || formValues["bolUserExcel"] == "checked" || formValues["bolUserExcel"] == "true",
                bolPage = formValues["bolUserPage"] == "on" || formValues["bolUserPage"] == "checked" || formValues["bolUserPage"] == "true",
                UserType = Enums.UserType.Normal
            });

            JavaScriptSerializer jss = new JavaScriptSerializer();

            return jss.Serialize(reportPermisions);
        } 
        #endregion
    }
}
