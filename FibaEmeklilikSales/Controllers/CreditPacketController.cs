﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class CreditPacketController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intCreditPacketID = QueryStrings.ID;
                    if (intCreditPacketID > 0)
                    {
                        var packet = db.tblCreditPackets.Where(w => w.intCreditPacketID == intCreditPacketID).FirstOrDefault();
                        if (packet != null)
                        {
                            bool bolExist = db.tblWebSaleLogs.Where(w => w.intCreditPacketID == intCreditPacketID).FirstOrDefault() != null;

                            if (!bolExist)
                            {
                                packet.bolIsDelete = true;
                                db.SaveChanges();

                                Logs.Log(Enums.LogType.Delete, intCreditPacketID);

                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket silindi.");
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Pakete ait satış işlemleri var!");
                            }
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Paket bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Paket bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditPacket", new { cr = Request.QueryString["cr"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "CreditPacket", new { cr = Request.QueryString["cr"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    int intCreditID = QueryStrings.CreditID;
                    string strName = formValues["strName"];

                    if (db.tblCreditPackets.Count(c => !c.bolIsDelete && c.intCreditID == intCreditID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi teminatına ait aynı paket başlığı sistemde mevcut!");
                        return RedirectToAction("New", "CreditPacket", new { cr = Request.QueryString["cr"] });
                    }

                    tblCreditPackets packet = new tblCreditPackets();
                    packet.strName = formValues["strName"];
                    packet.strComment = formValues["strComment"];
                    packet.intCreditID = intCreditID;
                    packet.strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
                    packet.strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
                    packet.intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
                    packet.intTime = Utilities.NullFixInt(formValues["intTime"]);
                    packet.intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
                    packet.strCrediCurrencyType = formValues["strCrediCurrencyType"];
                    packet.flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
                    packet.bolIsDelete = false;
                    packet.dtRegisterDate = DateTime.Now;

                    db.tblCreditPackets.Add(packet); 
                    db.SaveChanges();

                    foreach (var item in db.tblCreditTypes.ToList())
                    {
                        int intMaxTime = Utilities.NullFixInt(formValues["creditTypeMaxTime_" + item.intCreditTypeID]);
                        decimal intMaxPrice = Utilities.NullFixDecimal(formValues["creditTypeMaxPrice_" + item.intCreditTypeID]);

                        db.tblCreditPacketTypeValues.Add(new tblCreditPacketTypeValues
                        {
                            intCreditPacketID = packet.intCreditPacketID,
                            intCreditTypeID = item.intCreditTypeID,
                            intMaxPrice = intMaxPrice,
                            intMaxTime = intMaxTime
                        });
                    }

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, packet.intCreditPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditPacket", new { cr = Request.QueryString["cr"] });
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "CreditPacket", new { cr = Request.QueryString["cr"], i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intCreditPacketID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    int intCreditID = QueryStrings.CreditID;
                    string strName = formValues["strName"];

                    if (db.tblCreditPackets.Count(c => !c.bolIsDelete && c.intCreditID == intCreditID && c.intCreditPacketID != intCreditPacketID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi teminatına ait aynı paket adı sistemde mevcut!");
                        return RedirectToAction("Update", "CreditPacket", new { cr = Request.QueryString["cr"], i = Request.QueryString["i"] });
                    }

                    var packet = db.tblCreditPackets.Where(w => w.intCreditPacketID == intCreditPacketID).FirstOrDefault();
                    packet.strName = strName;
                    packet.strComment = formValues["strComment"];
                    packet.strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
                    packet.strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
                    packet.intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
                    packet.intTime = Utilities.NullFixInt(formValues["intTime"]);
                    packet.intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
                    packet.strCrediCurrencyType = formValues["strCrediCurrencyType"];
                    packet.flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
                    packet.dtUpdateDate = DateTime.Now;

                    foreach (var item in db.tblCreditTypes.ToList())
                    {
                        int intMaxTime = Utilities.NullFixInt(formValues["creditTypeMaxTime_" + item.intCreditTypeID]);
                        decimal intMaxPrice = Utilities.NullFixDecimal(formValues["creditTypeMaxPrice_" + item.intCreditTypeID]);

                        var creditPacketTypeValue = packet.tblCreditPacketTypeValues.Where(w => w.intCreditPacketID == intCreditPacketID && w.intCreditTypeID == item.intCreditTypeID).FirstOrDefault();

                        if (creditPacketTypeValue != null)
                        {
                            creditPacketTypeValue.intMaxPrice = intMaxPrice;
                            creditPacketTypeValue.intMaxTime = intMaxTime;
                        }
                        else
                        {
                            db.tblCreditPacketTypeValues.Add(new tblCreditPacketTypeValues
                            {
                                intCreditPacketID = intCreditPacketID,
                                intCreditTypeID = item.intCreditTypeID,
                                intMaxPrice = intMaxPrice,
                                intMaxTime = intMaxTime
                            });
                        }
                    }

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, packet.intCreditPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Paket güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "CreditPacket", new { cr = Request.QueryString["cr"] });
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strName = formValues["strName"];
            if (strName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            string strComment = formValues["strComment"];
            if (strComment.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Açıklama giriniz!");
                return false;
            }

            string strIntegrationTariffCode = formValues["strIntegrationTariffCode"];
            if (strIntegrationTariffCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Entegrasyon kodu giriniz!");
                return false;
            }

            string strPremiumPaymentPeriod = formValues["strPremiumPaymentPeriod"];
            if (strPremiumPaymentPeriod.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Prim ödeme periyodu giriniz!");
                return false;
            }

            int intPremiumPaymentTime = Utilities.NullFixInt(formValues["intPremiumPaymentTime"]);
            if (intPremiumPaymentTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Prim ödeme süresi giriniz!");
                return false;
            }

            int intTime = Utilities.NullFixInt(formValues["intTime"]);
            if (intTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Süre giriniz!");
                return false;
            }

            int intCrediTime = Utilities.NullFixInt(formValues["intCrediTime"]);
            if (intCrediTime == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi süresi giriniz!");
                return false;
            }

            string strCrediCurrencyType = formValues["strCrediCurrencyType"];
            if (strCrediCurrencyType.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi döviz tipini giriniz!");
                return false;
            }

            decimal flCrediReturnPrice = Utilities.NullFixDecimal(formValues["flCrediReturnPrice"]);
            if (flCrediReturnPrice == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kredi dönüş tutarı giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
