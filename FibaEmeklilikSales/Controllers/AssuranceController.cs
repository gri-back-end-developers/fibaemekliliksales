﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class AssuranceController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intAssuranceID = QueryStrings.ID;
                    if (intAssuranceID > 0)
                    {
                        var assurance = db.tblAssurances.Where(w => w.intAssuranceID == intAssuranceID).FirstOrDefault();
                        if (assurance != null)
                        {
                            assurance.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intAssuranceID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Teminat bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Teminat bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"], txtSearch = QueryStrings.search });
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("New", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    int intPacketID = QueryStrings.PacketID;
                    string strName = formValues["strName"];

                    if (db.tblAssurances.Count(c => !c.bolIsDelete && c.intPacketID == intPacketID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "pakete ait aynı teminat başlığı sistemde mevcut!");
                        return RedirectToAction("New", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"] });
                    }

                    tblAssurances assurances = new tblAssurances();
                    assurances.intPacketID = intPacketID;
                    assurances.strName = formValues["strName"];
                    assurances.strCode = formValues["strCode"];
                    assurances.intYear = Utilities.NullFixShort(formValues["intYear"]);
                    assurances.flPrice = Utilities.NullFixDecimal(formValues["flPrice"]);
                    assurances.bolIsDelete = false;
                    assurances.dtRegisterDate = DateTime.Now;

                    db.tblAssurances.Add(assurances);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, assurances.intAssuranceID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"] });
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"], i = Request.QueryString["i"] });
            }

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    int intAssuranceID = Utilities.NullFixInt(cCrypto.DecryptText(formValues["hdID"]));
                    int intPacketID = QueryStrings.PacketID;
                    string strName = formValues["strName"];

                    if (db.tblAssurances.Count(c => !c.bolIsDelete && c.intPacketID == intPacketID && c.intAssuranceID != intAssuranceID && c.strName == strName) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Pakete ait aynı teminat adı sistemde mevcut!");
                        return RedirectToAction("Update", "Assurance", new { pi = Request.QueryString["pi"], i = Request.QueryString["i"] });
                    }

                    var assurances = db.tblAssurances.Where(w => w.intAssuranceID == intAssuranceID).FirstOrDefault();
                    assurances.strName = strName;
                    assurances.strCode = formValues["strCode"];
                    assurances.intYear = Utilities.NullFixShort(formValues["intYear"]);
                    assurances.flPrice = Utilities.NullFixDecimal(formValues["flPrice"]);
                    assurances.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, assurances.intPacketID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Teminat güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "Assurance", new { pi = Request.QueryString["pi"], pa = Request.QueryString["pa"] });
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strName = formValues["strName"];
            if (strName.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Başlık giriniz!");
                return false;
            }

            string strCode = formValues["strCode"];
            if (strCode.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Kod giriniz!");
                return false;
            }

            decimal flPrice = Utilities.NullFixDecimal(formValues["flPrice"]);
            if (flPrice == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Tutar giriniz!");
                return false;
            }

            short intYear = Utilities.NullFixShort(formValues["intYear"]);
            if (intYear == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Yıl giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
