﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class BankBranchController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    db = new DBEntities();

                    int intBankBranchID = QueryStrings.ID;
                    if (intBankBranchID > 0)
                    {
                        var bankBranch = db.tblBankBranch.Where(w => w.intBankBranchID == intBankBranchID).FirstOrDefault();
                        if (bankBranch != null)
                        {
                            bankBranch.bolIsDelete = true;
                            db.SaveChanges();

                            Logs.Log(Enums.LogType.Delete, intBankBranchID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şube silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Şube bulunamadı!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Şube bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankBranch");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;

                    string strName = formValues["strName"];
                    short intBankID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intBankID"]));
                    short intBankCityID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intBankCityID"]));

                    if (db.tblBankBranch.Count(c => c.strName == strName && c.intBankID == intBankID && c.intBankCityID == intBankCityID && !c.bolIsDelete) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı şube adı sistemde mevcut!");
                        return RedirectToAction("New", "BankBranch");
                    }

                    tblBankBranch bankBranch = new tblBankBranch();
                    bankBranch.strName = strName;
                    bankBranch.intBankID = intBankID;
                    bankBranch.intBankCityID = intBankCityID;
                    bankBranch.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bankBranch.intWsID = Utilities.NullFixInt(formValues["intWsID"]);
                    bankBranch.dtRegisterDate = DateTime.Now;

                    db.tblBankBranch.Add(bankBranch);
                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Add, bankBranch.intBankID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şube eklendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankBranch");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            db = new DBEntities();

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    var formValues = Request.Form;
                    short intBankBranchID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["hdID"]));
                    short intBankID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intBankID"]));
                    short intBankCityID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["intBankCityID"]));
                    string strName = formValues["strName"];

                    if (db.tblBankBranch.Count(c => c.intBankBranchID != intBankBranchID && c.intBankID == intBankID && c.intBankCityID == intBankCityID && c.strName == strName && !c.bolIsDelete) > 0)
                    {
                        Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı şube adı sistemde mevcut!");
                        return RedirectToAction("Update", "BankBranch", new { i = Request.QueryString["i"] });
                    }

                    var bankBranch = db.tblBankBranch.Where(w => w.intBankBranchID == intBankBranchID).First();
                    bankBranch.strName = strName;
                    bankBranch.intBankID = intBankID;
                    bankBranch.intBankCityID = intBankCityID;
                    bankBranch.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                    bankBranch.intWsID = Utilities.NullFixInt(formValues["intWsID"]);
                    bankBranch.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    Logs.Log(Enums.LogType.Update, intBankID);

                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Şube güncellendi.");
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "BankBranch");
        }
        #endregion

        #region BankCities
        public JsonResult BankCities(string id)
        {
            db = new DBEntities();
            var intBankID = Utilities.NullFixShort(cCrypto.DecryptText(id));

            var bankCities = db.tblBankCity.Where(w => !w.bolIsDelete && w.bolIsActive && w.tblBankCities.Count(c => c.intBankID == intBankID) > 0).OrderBy(o => o.strCity).Select(s => new
            {
                s.intBankCityID,
                s.strCity
            }).ToList();

            db.Dispose();

            return Json(bankCities.Select(s => new
            {
                intBankCityID = cCrypto.EncryptText(s.intBankCityID.ToString()),
                s.strCity
            }), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
