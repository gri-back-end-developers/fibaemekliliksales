﻿using FibaEmeklilikSales.Classes; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using jCryption;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class ProfileController : Controller
    {
        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View();
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            DBEntities db = new DBEntities();

            if (!Validate())
            {
                return RedirectToAction("Update", "Profile");
            }

            try
            {
                var formValues = Request.Form;

                var user = db.tblUsers.Where(w => w.intUserID == Sessions.CurrentUser.intUserID).FirstOrDefault();
                user.strNameSurname = formValues["strNameSurname"];
                user.strEmail = formValues["strEmail"];

                db.SaveChanges();

                Logs.Log(Enums.LogType.Update, user.intUserID);

                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Profil güncellendi.");
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("Update", "Profile");
        }
        #endregion

        #region PasswordChange
        [HttpGet]
        [jCryptionHandler]
        public ActionResult PasswordChange(string returnUrl)
        {
            return View();
        }
        #endregion

        #region PasswordChange Save
        [jCryptionHandler]
        public ActionResult PasswordChange(string txtOldPass, string txtNewPass, string txtPssNewAgain)
        {
            if (string.IsNullOrEmpty(txtOldPass) && string.IsNullOrEmpty(txtNewPass) && string.IsNullOrEmpty(txtPssNewAgain))
            {
                return View();
            }

            DBEntities db = new DBEntities();

            tblUserPssChangeLogs tblUserPssChangeLog = new tblUserPssChangeLogs();
            tblUserPssChangeLog.intUserID = Sessions.CurrentUser.intUserID;
            tblUserPssChangeLog.dtRegisterDate = DateTime.Now;

            try
            {
                var tblPss = db.tblUserPssChangeLogs.Where(w => w.intUserID == Sessions.CurrentUser.intUserID).ToList();

                int intCount = tblPss.Count;
                if (intCount > 0)
                {
                    for (int i = 0; i < intCount; i++)
                    {
                        var item = tblPss[i];

                        TimeSpan time = DateTime.Now.Subtract(item.dtRegisterDate);
                        int intMinute = time.Minutes + (time.Hours * 60) + ((time.Days * 24) * 60);
                        if (intMinute > 60)
                        {
                            db.tblUserPssChangeLogs.Remove(item);
                            intCount--;

                            db.SaveChanges();
                        }
                    }
                }

                if (txtNewPass.Contains(" "))
                {
                    throw new Exception("Lütfen şifrenizde boşluk kullanmayınız!");
                }

                if (txtNewPass.ToString().Length < 8)
                {
                    throw new Exception("Şifrenizi en az 8 karakter uzunluğunda bir değer giriniz!");
                }

                string strOldPassCrypto = cCrypto.DecryptDES(Sessions.CurrentUser.strPassword);

                if (txtOldPass.ToString() != strOldPassCrypto)
                {
                    throw new Exception("Eski şifrenizi şifrenizi hatalı girdiniz!");
                }

                if (!cCrypto.PssControl(txtNewPass.ToString()))
                {
                    throw new Exception("Şifreniz en az bir sayı, küçük harf, büyük harf ve özel karakter içermelidir!");
                }

                var tblUserL2 = db.tblUserPssLogs.Where(w => w.intUserID == Sessions.CurrentUser.intUserID && w.dtLoginDate.Day == DateTime.Now.Day && w.dtLoginDate.Month == DateTime.Now.Month && w.dtLoginDate.Year == DateTime.Now.Year).ToList();

                foreach (var item in tblUserL2)
                {
                    if (cCrypto.DecryptDES(item.User_Password) == txtNewPass.ToString())
                    {
                        throw new Exception("Daha önceden kullanmadığınız bir şifre giriniz!");
                    }
                }

                var tblUserL = db.tblUserPssLogs.Where(w => w.intUserID == Sessions.CurrentUser.intUserID).OrderByDescending(o => o.dtLoginDate).Take(12).ToList();

                foreach (var item in tblUserL)
                {
                    if (cCrypto.DecryptDES(item.User_Password) == txtNewPass.ToString())
                    {
                        throw new Exception("Daha önceden kullanmadığınız bir şifre giriniz!");
                    }
                }

                string strNewPss = cCrypto.EncryptDES(txtNewPass);

                tblUserPssLogs userPssLogs = new tblUserPssLogs();
                userPssLogs.intUserID = Sessions.CurrentUser.intUserID;
                userPssLogs.dtLoginDate = DateTime.Now;
                userPssLogs.User_Password = strNewPss;

                db.tblUserPssLogs.Add(userPssLogs);

                var tblUser = db.tblUsers.Where(w => w.intUserID == Sessions.CurrentUser.intUserID).First();

                tblUser.bolPssChange = false;
                tblUser.strPassword = strNewPss;

                db.SaveChanges();

                Sessions.CurrentUser = tblUser;

                return Redirect("/Profile/PasswordChange?m=1");
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }

            return View();
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            string strEmail = formValues["strEmail"];
            if (strEmail.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "E-posta giriniz!");
                return false;
            }

            string strNameSurname = formValues["strNameSurname"];
            if (strNameSurname.Trim().Length == 0)
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Ad soyad giriniz!");
                return false;
            }

            return true;
        }
        #endregion
    }
}
