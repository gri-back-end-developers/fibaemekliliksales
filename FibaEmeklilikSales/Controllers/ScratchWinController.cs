﻿using FibaEmeklilikSales.Classes;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Controllers
{
    public class ScratchWinController : Controller
    {
        #region List
        [UserFilter]
        public ActionResult List()
        {
            List<tblScratchWin> scratchWins = new List<tblScratchWin>();

            if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
            {
                scratchWins = Caches.ScratchWins;
                if (scratchWins != null && scratchWins.Count == 0)
                {
                    DBEntities db = new DBEntities();
                    scratchWins.AddRange(db.tblScratchWin.OrderBy(o => o.bolIsUsed).ThenByDescending(t => t.dtUsedDate).ToList());

                    Caches.ScratchWins = scratchWins;
                }
            }

            string strSearch = QueryStrings.search;
            if (!string.IsNullOrEmpty(strSearch))
            {
                scratchWins = scratchWins.Where(w => w.strActivationCode.ToLower().Contains(strSearch.ToLower())
                    || w.strTariffCode.ToLower().Contains(strSearch.ToLower())
                        || w.strBarcode.ToLower().Contains(strSearch.ToLower())
                            || w.strChannel.ToLower().Contains(strSearch.ToLower())
                                || w.strPacketInfo.ToLower().Contains(strSearch.ToLower())
                                    || w.strRefNo.ToLower().Contains(strSearch.ToLower())
                                        || w.strSeller.ToLower().Contains(strSearch.ToLower())).ToList();
            }

            if (Request.QueryString["type"] == "excel")
            {
                DataTable dtExcel = new DataTable();
                dtExcel.Columns.Add("Aktivasyon_Kodu");
                dtExcel.Columns.Add("Barkod");
                dtExcel.Columns.Add("Tarife_Kod");
                dtExcel.Columns.Add("Durumu");
                dtExcel.Columns.Add("Kullanilma_Tarihi");
                dtExcel.Columns.Add("Police_No");
                dtExcel.Columns.Add("Kanal");
                dtExcel.Columns.Add("Paket_Bilgisi");
                dtExcel.Columns.Add("Referans_No");
                dtExcel.Columns.Add("Bayi");
                dtExcel.Columns.Add("Guncelleme_Tarihi");
                dtExcel.Columns.Add("Ekleme_Tarihi");

                foreach (var item in scratchWins)
                {
                    DataRow dr = dtExcel.NewRow();
                    dr[0] = item.strActivationCode;
                    dr[1] = item.strBarcode;
                    dr[2] = item.strTariffCode;
                    dr[3] = (item.bolIsActive ? "Aktif" : "Pasif");
                    dr[4] = (item.bolIsUsed ? item.dtUsedDate.ToString() : "");
                    dr[5] = (item.bolIsUsed ? item.tblScratchWinSales.Last().intPolicyID.ToString() : "");
                    dr[6] = item.strChannel;
                    dr[7] = item.strPacketInfo;
                    dr[8] = item.strRefNo;
                    dr[9] = item.strSeller;
                    dr[10] = item.dtUpdateDate.HasValue ? item.dtUpdateDate.Value.ToString("dd-MM-yyyy HH:mm") : "";
                    dr[11] = item.dtRegisterDate.ToString("dd-MM-yyyy HH:mm");

                    dtExcel.Rows.Add(dr);
                }

                ExcelExportSheet excel = new ExcelExportSheet();
                excel.ToExcel(dtExcel, "AktivasyonKodlari.xls", this.Response, "");
            }

            int intPageSize = 20;
            ViewBag.Count = (scratchWins.Count / intPageSize) + (scratchWins.Count % intPageSize > 0 ? 1 : 0);

            int intPage = QueryStrings.page;
            intPage = intPage > 0 ? intPage - 1 : intPage;

            scratchWins = scratchWins.Skip(intPage * intPageSize).Take(intPageSize).ToList();

            return View(scratchWins);
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    DBEntities db = new DBEntities();

                    int intScratchWinID = QueryStrings.ID;
                    if (intScratchWinID > 0)
                    {
                        var scratchWin = db.tblScratchWin.Where(w => w.intScratchWinID == intScratchWinID && !w.bolIsUsed).FirstOrDefault();
                        if (scratchWin != null)
                        {
                            db.tblScratchWin.Remove(scratchWin);
                            db.SaveChanges();

                            Caches.RemoveScratchWins();

                            Logs.Log(Enums.LogType.Delete, intScratchWinID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Aktivasyon kodu silindi.");
                        }
                        else
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "İşlem yapılamaz!");
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Aktivasyon kodu bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ScratchWin");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(HttpPostedFileBase file = null)
        {
            ViewBag.strActivationCode = Request.Form["strActivationCode"];
            ViewBag.strBarcode = Request.Form["strBarcode"];
            ViewBag.strChannel = Request.Form["strChannel"];
            ViewBag.strPacketInfo = Request.Form["strPacketInfo"];
            ViewBag.strRefNo = Request.Form["strRefNo"];
            ViewBag.strSeller = Request.Form["strSeller"];
            ViewBag.strTariffCode = Request.Form["strTariffCode"];
            ViewBag.bolIsActive = Request.Form["bolIsActive"];
            ViewBag.strPaycell = Request.Form["strPaycell"];

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (file != null)
                    {
                        bool bolEx = false;
                        var ExtList = ("xls,xlsx").Split(',');
                        foreach (var item in ExtList)
                        {
                            if (Path.GetExtension(file.FileName).ToLower() == "." + item)
                            {
                                bolEx = true;
                                break;
                            }
                        }

                        if (!bolEx)
                        {
                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Dosya formatını hatalı seçtiniz!");
                            return RedirectToAction("New", "ScratchWin");
                        }
                        else
                        {
                            string strName = Utilities.RandomString(3) + new Random().Next(100, 999).ToString() + Utilities.RandomString(4);
                            string strPath = Utilities.AppPathServer + ConfigurationManager.AppSettings["ScratchWinfile"];
                            string strFileName = strName + Path.GetExtension(file.FileName);

                            using (DBEntities db = new DBEntities())
                            {
                                file.SaveAs(strPath + strFileName);

                                cExcel excel = new cExcel();
                                DataTable dt = excel.Read(strPath + strFileName);
                                if (dt != null)
                                {
                                    List<tblScratchWin> scratchWins = Caches.ScratchWins;

                                    string strActivationCode = "";

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (scratchWins.Count(c => c.strActivationCode == dt.Rows[i][0].ToString()) > 0)
                                        {
                                            strActivationCode = dt.Rows[i][0].ToString();
                                            break;
                                        }
                                    }

                                    if (string.IsNullOrEmpty(strActivationCode))
                                    {
                                        tblScratchWinExcels scratchWinExcel = new tblScratchWinExcels
                                        {
                                            dtRegisterDate = DateTime.Now,
                                            intUserID = Sessions.CurrentUser.intUserID,
                                            strFile = strFileName
                                        };
                                        db.SaveChanges();

                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            db.tblScratchWin.Add(new tblScratchWin
                                            {
                                                intScratchWinExcelID = scratchWinExcel.intScratchWinExcelID,
                                                bolIsUsed = false,
                                                dtRegisterDate = DateTime.Now,
                                                intInsUserID = Sessions.CurrentUser.intUserID,
                                                strActivationCode = dt.Rows[i][0].ToString(),
                                                strBarcode = dt.Rows[i][1].ToString(),
                                                strChannel = dt.Rows[i][2].ToString(),
                                                strPacketInfo = dt.Rows[i][3].ToString(),
                                                strRefNo = dt.Rows[i][4].ToString(),
                                                strSeller = dt.Rows[i][5].ToString(),
                                                strTariffCode = dt.Rows[i][6].ToString(),
                                                bolIsActive = dt.Rows[i][7].ToString() == "1",
                                                strPaycell = dt.Rows[i][8].ToString()
                                            });
                                        }

                                        db.SaveChanges();

                                        Caches.RemoveScratchWins();

                                        Logs.Log(Enums.LogType.ExcelUpload, scratchWinExcel.intScratchWinExcelID);

                                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Aktivasyon kodu eklendi.");
                                    }
                                    else
                                    {
                                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, strActivationCode + " aktivasyon kodu sistemde mevcut! Aktivasyon kodunu excel dosyasından sildikten sonra dosyayı tekrar yüklemelisiniz.");
                                        return RedirectToAction("New", "ScratchWin");
                                    }
                                }
                                else
                                {
                                    System.IO.File.Delete(strPath + strFileName);
                                }
                            }
                        }
                    }
                    else
                    {
                        using (DBEntities db = new DBEntities())
                        {
                            string strActivationCode = Request.Form["strActivationCode"];
                            if (db.tblScratchWin.Count(c => c.strActivationCode == strActivationCode) > 0)
                            {
                                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı aktivasyon kodu sistemde mevcut!");
                                return RedirectToAction("New", "ScratchWin");
                            }

                            tblScratchWin scratchWin = new tblScratchWin
                            {
                                bolIsUsed = false,
                                dtRegisterDate = DateTime.Now,
                                intInsUserID = Sessions.CurrentUser.intUserID,
                                strActivationCode = strActivationCode,
                                strBarcode = Request.Form["strBarcode"],
                                strChannel = Request.Form["strChannel"],
                                strPacketInfo = Request.Form["strPacketInfo"],
                                strRefNo = Request.Form["strRefNo"],
                                strSeller = Request.Form["strSeller"],
                                strTariffCode = Request.Form["strTariffCode"],
                                bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true",
                                strPaycell = Request.Form["strPaycell"]
                            };

                            db.tblScratchWin.Add(scratchWin);
                            db.SaveChanges();

                            Caches.RemoveScratchWins();

                            Logs.Log(Enums.LogType.Add, scratchWin.intScratchWinID);

                            Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Aktivasyon kodu eklendi.");
                        }
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ScratchWin");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            return View("New");
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(string strUpdate = "")
        {
            ViewBag.strActivationCode = Request.Form["strActivationCode"];
            ViewBag.strBarcode = Request.Form["strBarcode"];
            ViewBag.strChannel = Request.Form["strChannel"];
            ViewBag.strPacketInfo = Request.Form["strPacketInfo"];
            ViewBag.strRefNo = Request.Form["strRefNo"];
            ViewBag.strSeller = Request.Form["strSeller"];
            ViewBag.strTariffCode = Request.Form["strTariffCode"];
            ViewBag.bolIsActive = Request.Form["bolIsActive"];
            ViewBag.strPaycell = Request.Form["strPaycell"];

            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    short intScratchWinID = Utilities.NullFixShort(cCrypto.DecryptText(Request.Form["hdID"]));
                    using (DBEntities db = new DBEntities())
                    {
                        string strActivationCode = Request.Form["strActivationCode"];
                        if (db.tblScratchWin.Count(c => c.strActivationCode == strActivationCode && c.intScratchWinID != intScratchWinID) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı aktivasyon kodu sistemde mevcut!");
                            return RedirectToAction("Update", "ScratchWin", new { i = Request.QueryString["i"] });
                        }

                        var report = db.tblScratchWin.Where(w => w.intScratchWinID == intScratchWinID).First();
                        report.dtUpdateDate = DateTime.Now;
                        report.intUpdUserID = Sessions.CurrentUser.intUserID;
                        report.strActivationCode = Request.Form["strActivationCode"];
                        report.strBarcode = Request.Form["strBarcode"];
                        report.strChannel = Request.Form["strChannel"];
                        report.strPacketInfo = Request.Form["strPacketInfo"];
                        report.strRefNo = Request.Form["strRefNo"];
                        report.strSeller = Request.Form["strSeller"];
                        report.strTariffCode = Request.Form["strTariffCode"];
                        report.bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true";
                        report.strPaycell = Request.Form["strPaycell"];

                        db.SaveChanges();

                        Caches.RemoveScratchWins();

                        Logs.Log(Enums.LogType.Update, intScratchWinID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Aktivasyon kodu güncellendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "ScratchWin");
        }
        #endregion

        #region GetDetail
        public JsonResult GetDetail(string id)
        {
            DBEntities db = new DBEntities();
            int intScratchWinID = Utilities.NullFixInt(cCrypto.DecryptText(id));

            var saleInfo = db.tblScratchWinSales.Where(w => w.tblScratchWin.bolIsUsed && w.tblScratchWin.intScratchWinID == intScratchWinID).FirstOrDefault();
            if (saleInfo != null)
            {
                var _data = new
                {
                    Date = saleInfo.dtRegisterDate.ToString(),
                    Product = saleInfo.tblPackets.tblProducts.strName + "/" + saleInfo.tblPackets.strName,
                    Status = saleInfo.bolSuccess ? "Başarılı" : "Başarısız",
                    Msg = string.IsNullOrEmpty(saleInfo.strMsg) ? "" : saleInfo.strMsg,
                    User = saleInfo.strName + " " + saleInfo.strSurname,
                    Tckn = cCrypto.DecryptText(saleInfo.strTCKN),
                    Phone = saleInfo.strPhone
                };

                return Json(_data, JsonRequestBehavior.AllowGet);
            }

            return Json("{}", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CacheClear 
        [HttpGet]
        public EmptyResult CacheClear()
        {
            Caches.RemoveScratchWins();

            return new EmptyResult();
        }
        #endregion
    }
}
