﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using FibaEmeklilikSales.Classes;
using System.Configuration;
using System.IO;
using ModelLibrary;

namespace FibaEmeklilikSales.Controllers
{
    public class NotificationGroupController : Controller
    {
        DBEntities db;

        #region List
        [UserFilter]
        public ActionResult List()
        {
            cNotificationGroup notificationGroup = new cNotificationGroup();

            using (db = new DBEntities())
            {
                List<tblNotificationGroups> notificationGroups = new List<tblNotificationGroups>();

                if (Sessions.CurrentUser.tblUserTypes.bolPowerAdmin)
                {
                    notificationGroups.AddRange(db.tblNotificationGroups.Where(w => !w.bolIsDelete).ToList());
                }

                string strSearch = QueryStrings.search;
                if (!string.IsNullOrEmpty(strSearch))
                {
                    notificationGroups = notificationGroups.Where(w => w.strGroupName.ToLower().Contains(strSearch.ToLower())).ToList();
                }

                int intPageSize = 20;
                notificationGroup.intCount = (notificationGroups.Count / intPageSize) + (notificationGroups.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                notificationGroups = notificationGroups.Skip(intPage * intPageSize).Take(intPageSize).ToList();

                notificationGroup.notificationGroups = notificationGroups;
            }

            return View(notificationGroup);
        }
        #endregion

        #region Delete
        [UserFilter]
        public ActionResult Delete()
        {
            try
            {
                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    int intNotificationGroupID = QueryStrings.ID;
                    if (intNotificationGroupID > 0)
                    {
                        using (db = new DBEntities())
                        {
                            if (db.tblNotifications.Count(c => c.intNotificationGroupID == intNotificationGroupID && !c.bolIsDelete) == 0)
                            {
                                var notificationGroup = db.tblNotificationGroups.Where(w => w.intNotificationGroupID == intNotificationGroupID).FirstOrDefault();
                                if (notificationGroup != null)
                                {
                                    notificationGroup.bolIsDelete = true;
                                    db.SaveChanges();

                                    Logs.Log(Enums.LogType.Delete, intNotificationGroupID);

                                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Duyuru listesi silindi.");
                                }
                                else
                                {
                                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Duyuru listesi bulunamadı!");
                                }
                            }
                            else
                            {
                                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Duyuru listesinin bağlı olduğu duyurular var!");
                            }
                        }
                    }
                    else
                    {
                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Duyuru listesi bulunamadı!");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır!");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return RedirectToAction("List", "NotificationGroup");
        }
        #endregion

        #region New
        [UserFilter]
        [HttpGet]
        public ActionResult New(cNotificationGroup notificationGroup = null)
        {
            if (notificationGroup.notificationGroup == null)
            {
                notificationGroup.notificationGroup = new tblNotificationGroups();

                db = new DBEntities();
                notificationGroup.callcenters = db.tblCallcenters.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strName).ToList();
            }

            return View(notificationGroup);
        }
        #endregion

        #region New
        [HttpPost]
        [UserFilter]
        [ValidateInput(false)]
        public ActionResult New(string strNew = "")
        {
            short intNotificationGroupID = 0;
            try
            {
                var formValues = Request.Form;
                ViewBag.strGroupName = formValues["strGroupName"];
                ViewBag.hdCallcenters = formValues["hdCallcenters"];
                ViewBag.bolIsActive = formValues["bolIsActive"];

                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (!Validate())
                    {
                        return New(new cNotificationGroup());
                    }

                    using (db = new DBEntities())
                    {
                        string strGroupName = formValues["strGroupName"];

                        if (db.tblNotificationGroups.Count(c => c.strGroupName == strGroupName && !c.bolIsDelete) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı liste adı sistemde mevcut!");
                            return New(new cNotificationGroup());
                        }

                        tblNotificationGroups notificationGroup = new tblNotificationGroups()
                        {
                            strGroupName = strGroupName,
                            bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true",
                            bolIsDelete = false,
                            dtRegisterDate = DateTime.Now
                        };

                        db.tblNotificationGroups.Add(notificationGroup);
                        db.SaveChanges();

                        intNotificationGroupID = notificationGroup.intNotificationGroupID;

                        SetNotificationGroupCallenters(intNotificationGroupID);

                        Logs.Log(Enums.LogType.Add, notificationGroup.intNotificationGroupID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Liste eklendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (Request.QueryString["popup"] == null)
            {
                return RedirectToAction("List", "NotificationGroup");
            }
            else
            {
                return RedirectToAction("Update", "NotificationGroup", new { i = cCrypto.EncryptText(intNotificationGroupID.ToString()), popup = "1" });
            }
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpGet]
        public ActionResult Update()
        {
            cNotificationGroup notificationGroup = new cNotificationGroup();

            db = new DBEntities();
            var _data = new tblNotificationGroups();
            int intNotificationGroupID = QueryStrings.ID;

            if ((Request.QueryString["popup"] != null || PageControl.DomainType == "update") && intNotificationGroupID > 0)
            {
                _data = db.tblNotificationGroups.Where(w => w.intNotificationGroupID == intNotificationGroupID && !w.bolIsDelete).FirstOrDefault();

                if (_data == null)
                {
                    _data = new tblNotificationGroups();
                }
            }

            notificationGroup.notificationGroup = _data;
            notificationGroup.callcenters = db.tblCallcenters.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strName).ToList();

            return View("New", notificationGroup);
        }
        #endregion

        #region Update
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(HttpPostedFileBase file = null)
        {
            short intNotificationGroupID = 0;
            try
            {
                var formValues = Request.Form;
                ViewBag.strGroupName = formValues["strGroupName"];
                ViewBag.hdCallcenters = formValues["hdCallcenters"];
                ViewBag.bolIsActive = formValues["bolIsActive"];

                bool bolResponse = PageControl.Get;
                if (bolResponse)
                {
                    if (!Validate())
                    {
                        return Update();
                    }

                    using (db = new DBEntities())
                    {
                        string strGroupName = formValues["strGroupName"];
                        intNotificationGroupID = Utilities.NullFixShort(cCrypto.DecryptText(formValues["hdID"]));

                        if (db.tblNotificationGroups.Count(c => c.intNotificationGroupID != intNotificationGroupID && c.strGroupName == strGroupName && !c.bolIsDelete) > 0)
                        {
                            Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Aynı liste adı sistemde mevcut!");
                            return Update();
                        }

                        var notificationGroup = db.tblNotificationGroups.Where(w => w.intNotificationGroupID == intNotificationGroupID).First();
                        notificationGroup.strGroupName = strGroupName;
                        notificationGroup.bolIsActive = formValues["bolIsActive"] == "on" || formValues["bolIsActive"] == "checked" || formValues["bolIsActive"] == "true";
                        notificationGroup.dtUpdateDate = DateTime.Now;

                        db.SaveChanges();

                        SetNotificationGroupCallenters(intNotificationGroupID);

                        Logs.Log(Enums.LogType.Update, intNotificationGroupID);

                        Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Success, "Liste güncellendi.");
                    }
                }
                else
                {
                    Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Warning, "Yetkiniz bulunmamaktadır.");
                }
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
                Sessions.Message = Classes.Utilities.MsgToString(Classes.Utilities.MsgType.Error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (Request.QueryString["popup"] == null)
            {
                return RedirectToAction("List", "NotificationGroup");
            }
            else
            {
                return RedirectToAction("Update", "NotificationGroup", new { i = cCrypto.EncryptText(intNotificationGroupID.ToString()), popup = "1" });
            }
        }
        #endregion

        #region Validate
        bool Validate()
        {
            var formValues = Request.Form;

            if (string.IsNullOrEmpty(formValues["strGroupName"]))
            {
                Sessions.Message = Utilities.MsgToString(Utilities.MsgType.Warning, "Liste adını giriniz!");
                return false;
            }

            return true;
        }
        #endregion

        #region SetNotificationGroupCallenters
        private void SetNotificationGroupCallenters(short intNotificationGroupID)
        {
            db.Database.ExecuteSqlCommand("DELETE FROM tblNotificationGroupCallenters WHERE intNotificationGroupID = {0}", intNotificationGroupID);
            db.SaveChanges();

            var callcenters = string.IsNullOrEmpty(Request.Form["hdCallcenters"]) ? new List<string>() : Request.Form["hdCallcenters"].Split(',').ToList();
            foreach (var item in callcenters)
            {
                tblNotificationGroupCallenters notificationGroupCallenter = new tblNotificationGroupCallenters();
                notificationGroupCallenter.intNotificationGroupID = intNotificationGroupID;
                notificationGroupCallenter.intCallcenterID = Utilities.NullFixShort(cCrypto.DecryptText(item));

                db.tblNotificationGroupCallenters.Add(notificationGroupCallenter);
            }
            db.SaveChanges();
        }
        #endregion
    }
}
