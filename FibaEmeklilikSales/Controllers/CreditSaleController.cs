﻿using FibaEmeklilikSales.Classes;
using FibaEmeklilikSales.Classes.Credits;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FibaEmeklilikSales.Controllers
{
    public class CreditSaleController : Controller
    {
        DBEntities db;

        #region New
        [HttpGet]
        [UserFilter]
        public ActionResult New()
        {
            var intLogID = Sessions.CreditSaleLog;
            var intCreditID = QueryStrings.CreditID;

            cCreditSale creditSale = new cCreditSale();

            db = new DBEntities();

            if (intLogID > 0 && intCreditID > 0 && Request.QueryString["new"] == null)
            {
                if (db.tblCreditSaleLogs.Count(c => c.intCreditID == intCreditID && c.intLogID == intLogID) == 0)
                {
                    Sessions.Message = "<script>$(function () { Msg(2, 'Kredi teminatı için yeni bir satış başlattığınız için sonlandırdı!'); });</script>";
                    return Redirect("/main/index");
                }
            }

            var intSalePage = QueryStrings.SalePage;
            if (intSalePage == 0)
            {
                if (intLogID == 0 || Request.QueryString["new"] != null)
                {
                    Cookies.CreditSaleInfo = "0";
                    intLogID = Logs.CreditSaleLog(intCreditID);

                    Logs.CreditSaleLogDetail(intLogID, Enums.LogType.ProductSelected);
                }
            }

            var creditSaleLog = db.tblCreditSaleLogs.Where(w => w.intLogID == intLogID).FirstOrDefault();
            if (creditSaleLog != null)
            {
                creditSale.saleLog = creditSaleLog;
                creditSale.creditTypes = db.tblCreditTypes.Where(w => w.intCreditTypeID == creditSaleLog.intCreditTypeID).First();

                if (intSalePage == 1 || intSalePage == 4)
                {
                    if (creditSaleLog.tblCreditSaleLogInfo.FirstOrDefault() != null)
                    {
                        creditSale.strTCKN = creditSaleLog.tblCreditSaleLogInfo.First().strTCKN;
                    }

                    if (intSalePage == 4)
                    {
                        creditSale.bolInfoForm = false;

                        var user = db.tblUsers.Where(w => w.intUserID == creditSaleLog.intUserID).FirstOrDefault();
                        if (user != null)
                        {
                            creditSale.bolInfoForm = user.tblCallcenters.bolInfoForm;
                        }
                    }
                }
                else if (intSalePage == 2)
                {
                    if (creditSaleLog.tblCreditTypes.bolBSMVKKDF)
                    {
                        var callcenter = db.tblCallcenterCredits.Where(w => w.intCreditID == intCreditID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                        if (callcenter != null)
                        {
                            creditSale.flBSMV = Utilities.Price(callcenter.tblCallcenters.flBSMV);
                            creditSale.flKKDF = Utilities.Price(callcenter.tblCallcenters.flKKDF);
                        }
                    }

                    creditSale.intMaxTime = 240;

                    int intCreditPacketID = creditSaleLog.tblCredits.tblCreditPackets.First().intCreditPacketID;
                    var creditPacketTypeValue = db.tblCreditPacketTypeValues.Where(w => w.intCreditPacketID == intCreditPacketID && w.intCreditTypeID == creditSaleLog.intCreditTypeID).FirstOrDefault();
                    if (creditPacketTypeValue != null)
                    {
                        creditSale.intMaxPrice = creditPacketTypeValue.intMaxPrice;
                        creditSale.intMaxTime = creditPacketTypeValue.intMaxTime;
                    }
                }
                else if (intSalePage == 3)
                {
                    if (QueryStrings.Type == "")
                    {
                        var isHavePacketPolicy = db.tblCreditPacketPolicy.Where(w => w.intCreditSaleLogID == intLogID && w.tblCreditPackets.intCreditID == intCreditID).FirstOrDefault();
                        if (isHavePacketPolicy == null)
                        {
                            var saleLogInfo = creditSaleLog.tblCreditSaleLogInfo.First();

                            cPaymentPlan paymentPlan = new cPaymentPlan(new PaymentPlanRequest
                            {
                                BSMV = (double)saleLogInfo.flBSMV,
                                remainingAmount = (double)saleLogInfo.flCreditAmount,
                                interest = (double)saleLogInfo.flInterest,
                                KKDF = (double)saleLogInfo.flKKDF,
                                period = saleLogInfo.intCreditMonth.Value
                            });

                            creditSale.paymentPlanList = paymentPlan.ToList();

                            db.Database.ExecuteSqlCommand("delete from tblWebSaleCreditPaymentPlans where intWebSaleLogID = {0}", intLogID);
                            db.SaveChanges();

                            for (byte i = 1; i <= saleLogInfo.intCreditMonth; i++)
                            {
                                var paymentPlanRes = creditSale.paymentPlanList.Where(w => w.installment == i).First();

                                db.tblCreditSalePaymentPlans.Add(new tblCreditSalePaymentPlans
                                {
                                    dtRegisterDate = DateTime.Now,
                                    flInstallAmount = Utilities.NullFixDecimal(paymentPlanRes.installAmount),
                                    flRemainingAmount = Utilities.NullFixDecimal(paymentPlanRes.remainingAmount),
                                    intInstallment = i,
                                    intLogID = intLogID
                                });
                            }
                            db.SaveChanges();

                            var creditPacketList = db.tblCreditPackets.Where(w => w.intCreditID == intCreditID && !w.bolIsDelete && w.tblCreditAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).ToList();
                            foreach (var item in creditPacketList)
                            {
                                var packetPolicy = db.tblCreditPacketPolicy.Where(w => w.intCreditSaleLogID == intLogID && w.intCreditPacketID == item.intCreditPacketID).FirstOrDefault();
                                if (packetPolicy != null)
                                {
                                    db.tblCreditPacketPolicy.Remove(packetPolicy);
                                }

                                if (PrimResult(intLogID, saleLogInfo, item) == null)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        creditSale.paymentPlanList = GetPaymentPlans(intLogID);
                    }

                    creditSale.packets = GetCreditPackets(intLogID, intCreditID);
                }
                else if (intSalePage == 6)
                {
                    creditSale.banks = db.tblBanks.Where(w => w.bolIsActive && !w.bolIsDelete).OrderBy(o => o.strName).ToList();
                    creditSale.paymentMethods = new List<tblPaymentMethods>();

                    if (!string.IsNullOrEmpty(creditSaleLog.tblCredits.intPaymentMethodIDs))
                    {
                        List<byte> listPaymentMethods = new List<byte>();
                        string[] strpM = creditSaleLog.tblCredits.intPaymentMethodIDs.Split(',');
                        for (int i = 0; i < strpM.Length; i++)
                        {
                            if (listPaymentMethods.Count(c => c == byte.Parse(strpM[i])) == 0)
                            {
                                listPaymentMethods.Add(byte.Parse(strpM[i]));
                            }
                        }

                        creditSale.paymentMethods = db.tblPaymentMethods.Where(w => listPaymentMethods.Count(c => c == w.intPaymentMethodID) > 0).ToList();
                    }
                }
                else if (intSalePage == 8)
                {
                    using (db = new DBEntities())
                    {
                        SendIssuedMail(creditSaleLog.tblCreditSaleLogInfo.First(), "[" + Sessions.CurrentUser.tblCallcenters.strName + "] Eacente Satış - Poliçe linki gönderilemediği için gönderilmiştir");
                    }

                    Logs.SaleLog(intLogID, false, Enums.LogType.PaySuccess, "", "", "", "", null, 0, true);

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<p>Değerli Müşterimiz,</p>");
                    sb.Append("<p>Talebiniz tarafımıza ulaşmıştır, en kısa sürede geri dönüş yapılacaktır.</p>");
                    sb.Append("<p>Teşekkür eder, mutlu ve sağlıklı günler dileriz.</p>");
                    sb.Append("<p>Fiba Emeklilik ve Hayat A.Ş.</p>");

                    Sessions.Message = "<script>$(function () { Msg(1, '" + sb.ToString() + "'); });</script>";
                    return Redirect("/main/index");
                }
            }

            return View(creditSale);
        }
        #endregion

        #region New
        [UserFilter]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult New(string str = "")
        {
            int intExceptionID = 0;
            var intSalePage = QueryStrings.SalePage;
            int intLogID = Sessions.CreditSaleLog;
            bool bolSuccessPay = false;
            bool bolPhoneCount = false;

            try
            {
                db = new DBEntities();
                var saleInfo = db.tblCreditSaleLogInfo.Where(w => w.intLogID == intLogID).FirstOrDefault();

                if (intSalePage == 1 || intSalePage == 2 || intSalePage == 3 || intSalePage == 5 || intSalePage == 6)
                {
                    Logs.CreditSaleLogInfo(intLogID);

                    if (intSalePage == 5)
                    {
                        if (Logs.PhoneLogCount(saleInfo.strTCKN, Request.Form["cepno"]) >= 3)
                        {
                            Sessions.Message = "<script>$(function () { Msg(2, 'Bir telefon numarası en fazla 3 kisiye kaydedilebilir.'); });</script>";

                            bolPhoneCount = true;
                        }
                    }
                }
                else if (intSalePage == 4)
                {
                    if (QueryStrings.Type == "offer")
                    {
                        Logs.CreditSalePaymentPlan(Sessions.CreditSaleLog);

                        var creditPacketList = db.tblCreditPackets.Where(w => w.intCreditID == saleInfo.tblCreditSaleLogs.intCreditID && !w.bolIsDelete && w.tblCreditAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).ToList();
                        foreach (var item in creditPacketList)
                        {
                            var packetPolicy = db.tblCreditPacketPolicy.Where(w => w.intCreditSaleLogID == intLogID && w.intCreditPacketID == item.intCreditPacketID).FirstOrDefault();
                            if (packetPolicy != null)
                            {
                                db.tblCreditPacketPolicy.Remove(packetPolicy);
                            }

                            if (PrimResult(intLogID, saleInfo, item) == null)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        Logs.CreditSaleLogInfo(intLogID);
                    }
                }
                else if (intSalePage == 7)
                {
                    Logs.CreditSaleLogInfo(intLogID);

                    var creditPacket = db.tblCreditPackets.Where(w => w.intCreditPacketID == saleInfo.intCreditPacketID).First();

                    bool bolUW = false;

                    var policyData = CreateCreditPolicy(intLogID, saleInfo, creditPacket, ref bolUW);
                    if (policyData != null)
                    {
                        JavaScriptSerializer java = new JavaScriptSerializer();
                        saleInfo.strPolicyData = java.Serialize(policyData);

                        db.SaveChanges();

                        if (Sessions.CurrentUser.tblCallcenters.bolCreditIssued)
                        {
                            Logs.CreditSaleMessageSend(intLogID, false);
                        }
                        else
                        {
                            SendIssuedMail(saleInfo, "Teklifi fibaemekliliğe gönder aracılığıyla gönderilmiştir");

                            if (ConfigurationManager.AppSettings["CreditSaleCardSuccess"] == "0")
                            {
                                bolSuccessPay = true;
                            }
                        }
                    }

                    if (bolUW && Request.Form["InfoFormSendType"] != null)
                    {
                        saleInfo.InfoFormSendType = Request.Form["InfoFormSendType"];

                        saleInfo.tblCreditSaleLogs.strCreditCardInfo = cCrypto.EncryptDES(Newtonsoft.Json.JsonConvert.SerializeObject(new cCreditCard
                        {
                            month = Request.Form["kartay"],
                            year = Request.Form["kartyil"],
                            card = Request.Form["kartno"].Replace(" ", "").Trim(),
                            type = Request.Form["paymentMethods"]
                        }));

                        db.SaveChanges();
                    }
                }

                db.Dispose();
            }
            catch (Exception ex)
            {
                intExceptionID = Logs.Error(ex);

                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Sessions.Message = "<script>$(function () { var exMsg = '" + intLine + " - " + ex.ToString() + "'; });</script>";
            }

            Enums.LogType logType = Enums.LogType.Update;
            switch (intSalePage)
            {
                case 1:
                    logType = Enums.LogType.TCKNInput;
                    break;
                case 2:
                    logType = Enums.LogType.CustomerInfo;
                    break;
                case 3:
                    logType = Enums.LogType.CreditInfo;
                    break;
                case 4:
                    logType = Enums.LogType.PremiumInfo;
                    break;
                case 5:
                    logType = Enums.LogType.ContactInfo;
                    break;
                case 6:
                    logType = Enums.LogType.HealtCheck;
                    break;
                case 7:
                    logType = Enums.LogType.Sales;
                    break;
            }

            Logs.CreditSaleLogDetail(intLogID, logType, intExceptionID);

            object objQuery = null;
            if (intExceptionID == 0 && !bolPhoneCount)
            {
                objQuery = new
                {
                    cr = Request.QueryString["cr"],
                    sp = intSalePage
                };

                if (intSalePage == 7 && !bolSuccessPay)
                {
                    intSalePage = 6;
                    Cookies.CreditSaleInfo = "6";
                }
                else if (intSalePage == 4 && QueryStrings.Type == "offer")
                {
                    intSalePage = 3;
                    Cookies.CreditSaleInfo = "3";
                    objQuery = new
                    {
                        cr = Request.QueryString["cr"],
                        sp = intSalePage,
                        type = "offer"
                    };
                }
                else
                {
                    Cookies.CreditSaleInfo = intSalePage.ToString();
                }
            }
            else
            {
                intSalePage--;
                Cookies.CreditSaleInfo = intSalePage.ToString();
                objQuery = new
                {
                    cr = Request.QueryString["cr"],
                    sp = intSalePage
                };
            }

            return RedirectToAction("New", "CreditSale", objQuery);
        }
        #endregion

        #region PrimResult
        cPolicyData PrimResult(int intLogID, tblCreditSaleLogInfo saleInfo, tblCreditPackets item)
        {
            cPolicyData policyData = new cPolicyData();
            BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();

            try
            {
                BankaWebV2.BankawebV2PrimsiminUser pInput = new BankaWebV2.BankawebV2PrimsiminUser();

                pInput.aylikazal = "T";
                pInput.odeperiyod = item.strPremiumPaymentPeriod;
                pInput.sure = saleInfo.intCreditMonth;
                pInput.doviz = "TL";
                pInput.kreditip = item.strIntegrationTariffCode;
                pInput.ad = saleInfo.strName;
                pInput.soyad = saleInfo.strSurname;
                pInput.dogtar = Utilities.NullFixDate(saleInfo.strBirthDate).ToString("ddMMyyyy");
                pInput.cins = saleInfo.strGender;
                pInput.tmnttutar = saleInfo.flCreditAmount;

                #region kredi
                BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                foreach (var paymentItem in saleInfo.tblCreditSaleLogs.tblCreditSalePaymentPlans.ToList())
                {
                    foreach (var assurance in item.tblCreditAssurances.ToList())
                    {
                        if (assurance.strCoefficientType == "1")
                        {
                            if (paymentItem.intInstallment == 1)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = 1,
                                    tmntkod = assurance.strCode,
                                    yil = 1,
                                    bedel = saleInfo.flCreditAmount * assurance.flCoefficient
                                });
                            }

                            int intInstallment = paymentItem.intInstallment + 1;
                            int intInstallmentMax = saleInfo.tblCreditSaleLogs.tblCreditSalePaymentPlans.OrderByDescending(o => o.intInstallment).First().intInstallment;

                            if (intInstallment <= intInstallmentMax)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = intInstallment,
                                    tmntkod = assurance.strCode,
                                    yil = intInstallment > 12 ? (intInstallment / 12) + (intInstallment % 12 > 0 ? 1 : 0) : 1,
                                    bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                                });
                            }
                        }
                        else
                        {
                            yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                            {
                                ay = paymentItem.intInstallment,
                                tmntkod = assurance.strCode,
                                yil = paymentItem.intInstallment > 12 ? (paymentItem.intInstallment / 12) + (paymentItem.intInstallment % 12 > 0 ? 1 : 0) : 1,
                                bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                            });
                        }
                    }
                }

                pInput.yillikkreditaksit = yillikkreditaksit;
                #endregion

                JavaScriptSerializer jss = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, intLogID, "krediteminatı", "PrimResult", "Input", jss.Serialize(pInput), "getprimsimresult");

                var primsimresult = bankaWeb.getprimsimresult(pInput);

                Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, intLogID, "krediteminatı", "PrimResult", "Output", jss.Serialize(primsimresult), "getprimsimresult");

                if (primsimresult != null && primsimresult.cevap.Contains("OK"))
                {
                    db.tblCreditPacketPolicy.Add(new tblCreditPacketPolicy
                    {
                        dtRegisterDate = DateTime.Now,
                        flFirstPremium = primsimresult.prim.Value,
                        flTotalPremium = primsimresult.prim.Value,
                        intCreditPacketID = item.intCreditPacketID,
                        intCreditSaleLogID = intLogID
                    });
                    db.SaveChanges();
                }
                else if (primsimresult != null && primsimresult.cevap != null)
                {
                    policyData = null;

                    string strMsg = "";
                    if (primsimresult.cevap != null)
                    {
                        strMsg = MessageClear(primsimresult.cevap);
                    }

                    if (strMsg == "")
                    {
                        int intExceptionID = Logs.Error(new Exception("primsimresult is null --> cevap : " + primsimresult.cevap));
                        Logs.CreditSaleLogDetail(intLogID, Enums.LogType.PremiumInfo, intExceptionID);

                        Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorMessage('" + strMsg + "'); });</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                policyData = null;
                Logs.CreditSaleLogDetail(intLogID, Enums.LogType.PremiumInfo, Logs.Error(ex));
            }

            return policyData;
        }
        #endregion

        #region CreateCreditPolicy
        cPolicyData CreateCreditPolicy(int intLogID, tblCreditSaleLogInfo saleInfo, tblCreditPackets item, ref bool bolUW)
        {
            cPolicyData policyData = new cPolicyData();
            BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();

            try
            {
                BankaWebV2.BankawebV2PolreqUser polreqUser = new BankaWebV2.BankawebV2PolreqUser();

                polreqUser.yenileme = "F";
                polreqUser.isaylikazalan = "T";

                short intBankBranchID = Utilities.NullFixShort(Request.Form["intBankBranchID"]);
                var bankBranch = db.tblBankBranch.Where(w => w.intBankBranchID == intBankBranchID).FirstOrDefault();
                if (bankBranch != null)
                {
                    polreqUser.sigetid = bankBranch.intWsID;
                }

                #region odeme
                int intPaymentMethodID = Utilities.NullFixInt(Request.Form["paymentMethods"]);

                tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                polreqUser.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                {
                    primodemeperiyod = paymentMethod.strPeriod, // ilk prim T
                    primodemesure = paymentMethod.intInstallment, //ilk primde 1 yolalnacak
                    odemetip = "KRD",
                    dovizkod = "TL",
                    kartno = Request.Form["kartno"].Replace(" ", "").Trim(),
                    sonkullanmatarih = Request.Form["kartay"] + "20" + Request.Form["kartyil"]
                };
                #endregion

                #region ozluk
                polreqUser.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                {
                    ad = saleInfo.strName,
                    anneadi = saleInfo.strMotherName,
                    babaad = saleInfo.strFatherName,
                    cinsiyet = saleInfo.strGender,
                    dogumil = string.IsNullOrEmpty(saleInfo.strBirthRegionCode) ? "" : saleInfo.strBirthRegionCode.Trim(),
                    dogumilce = string.IsNullOrEmpty(saleInfo.strBirthDistrictCode) ? "" : saleInfo.strBirthDistrictCode.Trim(),
                    dogumtarih = Utilities.NullFixDate(saleInfo.strBirthDate),
                    medenidurum = saleInfo.strMaritalStatus,
                    soyad = saleInfo.strSurname,
                    tckimlikno = saleInfo.strTCKN,
                    musteritip = "G",
                    meslekkod = "003",
                    uyruk = saleInfo.strCountryCode,
                    sirkettip = "A"
                };
                #endregion

                #region adres
                polreqUser.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                {
                    adres = saleInfo.strAddress,
                    adrestip = "E",
                    ilcekod = string.IsNullOrEmpty(saleInfo.strDistrictCode) ? "" : saleInfo.strDistrictCode.Trim(),
                    iletisimtip = "CEP",
                    ilkod = string.IsNullOrEmpty(saleInfo.strRegionCode) ? "" : saleInfo.strRegionCode.Trim(),
                    ulkekod = saleInfo.strCountryCode.Trim(),
                    iletisimno = saleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", ""),
                };
                #endregion

                #region araci
                polreqUser.araci = new BankaWebV2.Bankawebv2polreqAracireqUser
                {
                    bankasube = Utilities.NullFixDecimal(Sessions.CurrentUser.tblCallcenters.strBankNo),
                    sicilno = Sessions.CurrentUser.tblCallcenters.strRegisterNo
                };
                #endregion

                #region kredi
                BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                foreach (var paymentItem in saleInfo.tblCreditSaleLogs.tblCreditSalePaymentPlans.ToList())
                {
                    foreach (var assurance in item.tblCreditAssurances.ToList())
                    {
                        if (assurance.strCoefficientType == "1")
                        {
                            if (paymentItem.intInstallment == 1)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = 1,
                                    tmntkod = assurance.strCode,
                                    yil = 1,
                                    bedel = saleInfo.flCreditAmount * assurance.flCoefficient
                                });
                            }

                            int intInstallment = paymentItem.intInstallment + 1;
                            int intInstallmentMax = saleInfo.tblCreditSaleLogs.tblCreditSalePaymentPlans.OrderByDescending(o => o.intInstallment).First().intInstallment;

                            if (intInstallment <= intInstallmentMax)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = intInstallment,
                                    tmntkod = assurance.strCode,
                                    yil = intInstallment > 12 ? (intInstallment / 12) + (intInstallment % 12 > 0 ? 1 : 0) : 1,
                                    bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                                });
                            }
                        }
                        else
                        {
                            yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                            {
                                ay = paymentItem.intInstallment,
                                tmntkod = assurance.strCode,
                                yil = paymentItem.intInstallment > 12 ? (paymentItem.intInstallment / 12) + (paymentItem.intInstallment % 12 > 0 ? 1 : 0) : 1,
                                bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                            });
                        }
                    }
                }

                string strKrediNo = "x" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();

                Sessions.CreditSessionKey = strKrediNo;

                polreqUser.kredi = new BankaWebV2.Bankawebv2polreqKredireqUser
                {
                    kredisure = saleInfo.intCreditMonth,
                    kredidoviztip = "TL",
                    kreditaksittip = "S",
                    sure = saleInfo.intCreditMonth,
                    kredino = strKrediNo,
                    kredino2 = strKrediNo,
                    yillikkreditaksit = yillikkreditaksit,
                    kreditutar = saleInfo.flCreditAmount,
                    kreditip = item.strIntegrationTariffCode,
                    kredidonustutar = saleInfo.flCreditAmount
                };
                #endregion

                JavaScriptSerializer jss = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intUserID, intLogID, "krediteminatı", "CreateCreditPolicy", "Input", jss.Serialize(polreqUser), "policelesme");

                var policy = bankaWeb.policelesme(polreqUser, "T");

                Logs.WSLog(intWSLogID, Sessions.CurrentUser.intUserID, intLogID, "krediteminatı", "CreateCreditPolicy", "Output", jss.Serialize(policy), "policelesme");

                if (policy.policeno != null)
                {
                    policyData.IlkPrim = policy.ilkprim.Value;
                    policyData.ToplamPirim = policy.toplamprim.Value;
                    policyData.PoliceNo = policy.policeno.Value;
                    policyData.IlkPrimKomisyon = policy.ilkprimkomisyon.Value;
                    policyData.KrediNo = policy.kredino;
                    policyData.TeklifDurum = policy.teklifdurum;
                    policyData.ToplamKomisyon = policy.toplamkomisyon.Value;

                    if (policy.teklifdurum == "UW")
                    {
                        Sessions.Message = "<script>$(function () { ErrorPolicy2(); });</script> ";
                        policyData = null;
                        bolUW = true;
                    }

                    db.tblCreditPacketPolicy.Add(new tblCreditPacketPolicy
                    {
                        dtRegisterDate = DateTime.Now,
                        flFirstPremium = policy.ilkprim.Value,
                        flTotalPremium = policy.toplamprim.Value,
                        intCreditPacketID = item.intCreditPacketID,
                        intCreditSaleLogID = intLogID,
                        strPolicyID = policy.policeno.Value.ToString()
                    });
                    db.SaveChanges();
                }
                else
                {
                    if (policy != null)
                    {
                        if (policy.teklifdurum == "UW")
                        {
                            Sessions.Message = "<script>$(function () { ErrorPolicy2();});</script>";
                            bolUW = true;
                        }
                    }

                    policyData = null;

                    if (!bolUW)
                    {
                        string strMsg = MessageClear(policy.cevap);
                        if (strMsg == "")
                        {
                            if (policy != null)
                            {
                                int intExceptionID = Logs.Error(new Exception("policy.policeno is null --> cevap : " + policy.cevap));
                                Logs.CreditSaleLogDetail(intLogID, Enums.LogType.Sales, intExceptionID);
                            }

                            Sessions.Message = "<script>$(function () { ErrorPolicy();});</script>";
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { ErrorMessage('" + strMsg + "');});</script> ";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                policyData = null;
                Logs.CreditSaleLogDetail(intLogID, Enums.LogType.Sales, Logs.Error(ex));
            }

            return policyData;
        }
        #endregion

        #region MessageClear
        private string MessageClear(string strText)
        {
            string strMsg = "";
            if (strText.Contains("Err|"))
            {
                strMsg = strText.Split('|')[1].Trim();
                strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                if (strMsg.Split('>').Length > 0)
                {
                    strMsg = strMsg.Split('>')[strMsg.Split('>').Length - 1].Trim();
                    if (strMsg.Split(':').Length > 0)
                    {
                        if (strMsg.Split('-').Length > 0)
                        {
                            strMsg = strMsg.Split('-')[0].Trim();
                        }
                        else
                        {
                            strMsg = strMsg.Split(':')[strMsg.Split(':').Length - 1].Trim();
                        }
                    }
                }
            }

            return strMsg;
        }

        private string MessageClear2(string strText)
        {
            strText = strText.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

            if (strText.Split('>').Length > 0)
            {
                strText = strText.Split('>')[strText.Split('>').Length - 1].Trim();
                if (strText.Split('.').Length > 0)
                {
                    strText = strText.Split('.')[0].Trim() + ".";
                }
            }

            strText = strText.Replace("'", "");

            return strText;
        }
        #endregion

        #region SendIssuedMail
        private void SendIssuedMail(tblCreditSaleLogInfo saleInfo, string strTitle)
        {
            JavaScriptSerializer java = new JavaScriptSerializer();

            //string strTemplate = Utilities.GetFileText(Server.MapPath("~/htmls/MailIssued.html"));
            //strTemplate = strTemplate.Replace("#adsoyad#", saleInfo.strName + " " +saleInfo.strSurname);
            //strTemplate = strTemplate.Replace("#tel#", saleInfo.strPhone);
            //strTemplate = strTemplate.Replace("#ceptel#", saleInfo.strMobile);

            string strPoliceNo = "";
            string strDetail = "";
            if (!string.IsNullOrEmpty(saleInfo.strPolicyData))
            {
                var policy = java.Deserialize<cPolicyData>(saleInfo.strPolicyData);
                strDetail += "<p><b>Poliçe No :</b>" + policy.PoliceNo + "</p>";
                strDetail += "<p><b>Prim :</b>" + policy.ToplamPirim + " TL</p>";

                strPoliceNo = policy.PoliceNo.HasValue ? policy.PoliceNo.Value.ToString() : "";
            }

            strDetail += "<p><b>Ürün :</b>" + saleInfo.tblCreditSaleLogs.tblCredits.strName + "</p>";

            var intCreditPacketID = saleInfo.tblCreditSaleLogs.tblCreditSaleLogInfo.First().intCreditPacketID;
            var packet = db.tblCreditPackets.Where(w => w.intCreditPacketID == intCreditPacketID).First();
            strDetail += "<p><b>Paket :</b>" + packet.strName + "</p>";

            //strTemplate = strTemplate.Replace("#detay#", strDetail);

            //List<string> emails = ConfigurationManager.AppSettings["InsuedMail"].Split(';').ToList();
            //foreach (var item in emails)
            //{
            //Email.SendEmail("Teklifi fibaemekliliğe gönder aracılığıyla gönderilmiştir", strTemplate, item);
            Email.SendCRM(ConfigurationManager.AppSettings["crmTeklifGonderOwnerId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderQueueId"].ToString(), ConfigurationManager.AppSettings["crmTeklifGonderPhoneCallType"].ToString(), saleInfo.strName, saleInfo.strSurname, strPoliceNo, saleInfo.strMobile, strDetail.Replace("<p><b>", Environment.NewLine).Replace("</b>", "").Replace("</p>", ""), strTitle);
            //}
        }
        #endregion

        #region OfferView
        [UserFilter]
        [HttpGet]
        public ActionResult OfferView()
        {
            return View();
        }

        [UserFilter]
        [HttpGet]
        public ActionResult OfferViewFrame()
        {
            cCreditSale creditSale = new cCreditSale();

            db = new DBEntities();

            var intLogID = Sessions.CreditSaleLog;
            //var intLogID = 1216;

            var creditSaleLog = db.tblCreditSaleLogs.Where(w => w.intLogID == intLogID).FirstOrDefault();
            if (creditSaleLog != null)
            {
                creditSale.saleLog = creditSaleLog;
                creditSale.paymentPlanList = GetPaymentPlans(intLogID);
                creditSale.packets = GetCreditPackets(intLogID, creditSaleLog.intCreditID);
            }

            return View(creditSale);
        }
        #endregion

        #region GetPaymentPlans
        private List<PaymentPlanResponse> GetPaymentPlans(int intLogID)
        {
            List<PaymentPlanResponse> paymentPlans = new List<PaymentPlanResponse>();

            var creditSalePaymentPlans = db.tblCreditSalePaymentPlans.Where(w => w.intLogID == intLogID).ToList();
            foreach (var item in creditSalePaymentPlans)
            {
                paymentPlans.Add(new PaymentPlanResponse
                {
                    installAmount = item.flInstallAmount.ToString("0.00"),
                    installment = item.intInstallment,
                    remainingAmount = item.flRemainingAmount.ToString("0.00")
                });
            }

            return paymentPlans;
        }
        #endregion

        #region GetCreditPackets
        private List<cCreditPackets> GetCreditPackets(int intLogID, int intCreditID)
        {
            List<cCreditPackets> packets = new List<cCreditPackets>();

            var creditPackets = db.tblCreditPackets.Where(w => w.intCreditID == intCreditID && !w.bolIsDelete && w.tblCreditAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).Select(s => new
            {
                id = s.intCreditPacketID,
                name = s.strName,
                comment = s.strComment,
                assurances = s.tblCreditAssurances.Where(w => !w.bolIsDelete),
                policy = s.tblCreditPacketPolicy.Where(w => w.intCreditSaleLogID == intLogID).FirstOrDefault()
            }).ToList();

            if (creditPackets != null)
            {
                foreach (var item in creditPackets)
                {
                    packets.Add(new cCreditPackets
                    {
                        assurances = item.assurances.Select(s => new cCreditAssurances
                        {
                            name = s.strName
                        }).ToList(),
                        id = item.id,
                        name = item.name,
                        strComment = item.comment,
                        policy = item.policy != null ? new cCreditPolicy
                        {
                            flFirstPremium = item.policy.flFirstPremium,
                            flTotalPremium = item.policy.flTotalPremium,
                            strPolicyID = item.policy.strPolicyID
                        } : null
                    });
                }
            }

            return packets;
        }
        #endregion

        #region BankCities
        public JsonResult BankCities(string bankid)
        {
            db = new DBEntities();
            var intBankID = Utilities.NullFixShort(bankid);

            var bankCities = db.tblBankCity.Where(w => !w.bolIsDelete && w.bolIsActive && w.tblBankCities.Count(c => c.intBankID == intBankID) > 0).OrderBy(o => o.strCity).Select(s => new
            {
                s.intBankCityID,
                s.strCity
            }).ToList();

            db.Dispose();

            return Json(bankCities, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BankBranches
        public JsonResult BankBranches(string cityid, string bankid)
        {
            db = new DBEntities();
            var intBankCityID = Utilities.NullFixShort(cityid);
            var intBankID = Utilities.NullFixShort(bankid);

            var banks = db.tblBankBranch.Where(w => !w.bolIsDelete && w.bolIsActive && w.intBankCityID == intBankCityID && w.tblBanks.tblBankCities.Count(c => c.intBankID == intBankID) > 0).OrderBy(o => o.strName).Select(s => new
            {
                s.intBankBranchID,
                s.strName
            }).ToList();

            db.Dispose();

            return Json(banks, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}