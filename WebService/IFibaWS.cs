﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text; 
using WebService.BankaWebService;
using WebService.Classes;
using WebService.FibawsService;

namespace WebService
{
    [ServiceContract]
    public interface IFibaWS
    {
        [OperationContract]
        FibawsHasarlistrecUser FibawsGethasarlistrec(decimal? pPolid);

        [OperationContract]
        FibawsPolistirarecUser FibawsGetistiradeger(decimal? pPolid);

        [OperationContract]
        FibawsKisibilgirecUser FibawsGetkisibilgirec(decimal? pPid);

        [OperationContract]
        decimal? FibawsGetkisipidbilgi(string pTckno, string pEmail, string pTelno);

        [OperationContract]
        FibawsOdemebilgirecUser FibawsGetodemebilgirec(decimal? pPolid);

        [OperationContract]
        FibawsPolaytmntlistrecUser FibawsGetpolaytmntrec(decimal? pPolid);

        [OperationContract]
        FibawsPolicebilgirecUser FibawsGetpolicebilgirec(string pPoliceno);

        [OperationContract]
        FibawsPolicelistrecUser FibawsGetpolicelist(decimal? pPid);

        [OperationContract]
        FibawsPolicesorguoutrecUser FibawsGetpolicesorgu(FibawsPolicesorguinrecUser pInput);

        [OperationContract]
        FibawsTahsilatbilgilistrecUser FibawsGettahsilatbilgirec(decimal? pPolid);

        [OperationContract]
        FibawsSanalposout3dUser FibawsSpostaksitli3d(FibawsSanalposin3dUser pInput);

        [OperationContract]
        FibawsPolicesorguoutrecUser FibawsGetPoliceSorguilksan(FibawsPolicesorguinrecUser pInput);
        
        [OperationContract]
        BankawebV2HspotooutUser Bankawebv2Hspotomatik(BankawebV2HspotoinUser pInput);

        [OperationContract]
        BankawebV2PolresUser Bankawebv2Policelesme(BankawebV2PolreqUser polreq, string pUwonay);

        [OperationContract]
        BankawebV2PolresUser Bankawebv2PolicelesmeKPS(BankawebV2PolreqUser polreq, string pUwonay, bool bolKPS, bool bolAddress, bool bolVKN, UserInfo userInfo);

        [OperationContract]
        BankawebV2PoliptrecUser Bankawebv2Poliptal(string pUser, BankawebV2PoliptinUser pIpt);

        [OperationContract]
        BankawebV2SanalposoutUser Bankawebv2Spostaksitli(BankawebV2SanalposinUser pInput);

        [OperationContract]
        BankawebV2PolresUser Bankawebv2Teklifpolicelesme(decimal? tkl, decimal? insCan, string pKredino2, string krd);

        [OperationContract]
        BankawebV2StatureqUser Bankawebv2TeklifStatu(decimal? pol, string krd);

        [OperationContract]
        byte[] PdfCreate(PdfData pdfData);

        [OperationContract]
        string PdfCreateURL(PdfData pdfData);

        [OperationContract]
        IdentityInfo KimlikBilgileri(string strTCKN);

        [OperationContract]
        AddressInfo AdresSorgula(string strTCKN);

        [OperationContract]
        VknInfo VKNSorgula(string strVKN);

        [OperationContract]
        decimal? BesGetkisipidbilgi(string pTckno, string pEmail, string pTelno);
    }
}
