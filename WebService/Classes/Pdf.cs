﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using WebService.FibawsService;
using WebService.IntBesService;

namespace WebService.Classes
{
    public class Pdf
    {
        //sUrl = linkFile + "/reports/rwservlet?report=hytpoliceweb&p_polid=" + polid.ToString() + "&cmdkey=fibahayat&t=" + DateTime.Now.Ticks + "&destype=cache&desformat=pdf";
        //sUrl = linkFile + "/reports/rwservlet?report=makbuzbas_hyt&p_makbuzno=" + makbuzno.ToString() + "&cmdkey=fibahayat&t=" + DateTime.Now.Ticks + "&destype=cache&desformat=pdf";
        //sUrl = linkFile + "/reports/rwservlet?desname=" + desName + "_" + policeno + "_w" + DateTime.Now.Ticks + ".pdf&report=" + report + "&p_polid=" + policeno + "&cmdkey=" + cmdkey + "&destype=cache&desformat=pdf";
        //sUrl = linkFile + "/reports/rwservlet?desname=MakbuzBas_" + PolNo + "_w" + DateTime.Now.Ticks + ".pdf&report=makbuzbas_bes&p_makbuzno=" + odemeMakbuzNo + "&cmdkey=fibauser&destype=cache&desformat=pdf";
        //sUrl = linkFile + "/reports/rwservlet?desname=" + desName + "_" + PolNo + "_w" + DateTime.Now.Ticks + ".pdf&report=" + report + "&p_polid=" + PolNo + "&p_zeylno=" + ZeylNo + "&cmdkey=fibauser&destype=cache&desformat=pdf";
        //sUrl = linkFile + "/reports/rwservlet?desname=" + desName + "_" + policeno + "_w" + DateTime.Now.Ticks + ".pdf&report=" + report + "&p_polid=" + ZeylNo + "&cmdkey=" + cmdkey + "&destype=cache&desformat=pdf&p_hspozettalepndn=AKTARIM&p_zeylno=" + Request.QueryString["p_zeylno"];
        //sUrl = linkFile + "/reports/rwservlet?report=hesapekstre_ferdi_aktarim.rdf&desname=31050909/12/2014.pdf&server=" + serverParameter + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + polIds + "";
        //sUrl = linkFile + "/reports/rwservlet?report=sozlesme_grup_matbaa.rdf&desname=31050909/12/2014.pdf&server=" + serverParameter + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + polIds + "";

        public object Create(bool bolGetUrl, PdfData pdfData, string strAgencyID)
        {
            try
            {
                //bool isHavePolicy = IsHavePolicy(strAgencyID, pdfData.p_polid);

                bool isHavePolicy = true;

                if (isHavePolicy)
                {
                    string strUrl = "";

                    if (pdfData.getUrl_Service)
                    {
                        strUrl = getRaporName(pdfData.p_polid, pdfData.reporttype);
                    }
                    else
                    {
                        strUrl = string.Format("{0}/reports/rwservlet{1}{2}{3}{4}{5}{6}{7}{8}{9}&destype=cache&desformat=pdf",
                            ConfigurationManager.AppSettings["ServiceAddress"].ToString(),
                                "?report=" + pdfData.report,
                                    (pdfData.p_polid > 0 ? "&p_polid=" + pdfData.p_polid.ToString() : ""),
                                        "&cmdkey=" + pdfData.cmdkey,
                                            (pdfData.t > 0 ? "&t=" + pdfData.t.ToString() : ""),
                                                (pdfData.p_makbuzno > 0 ? "&p_makbuzno=" + pdfData.p_makbuzno.ToString() : ""),
                                                    "&desname=" + pdfData.desname,
                                                        (pdfData.p_zeylno > 0 ? "&p_zeylno=" + pdfData.p_zeylno.ToString() : ""),
                                                        (string.IsNullOrEmpty(pdfData.p_hspozettalepndn) ? "" : "&p_hspozettalepndn=" + pdfData.p_hspozettalepndn),
                                                                "&server=" + ConfigurationManager.AppSettings["SalePdfserver"]
                        );
                    }

                    if (!string.IsNullOrEmpty(strUrl))
                    {
                        if (bolGetUrl)
                        {
                            return strUrl;
                        }
                        else
                        {
                            using (WebClient client = new WebClient())
                            {
                                return client.DownloadData(strUrl);
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Kullanılan poliçe acenteye ait değil!");
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception("Hatalı Kullanım! (" + ex.ToString() + ")");
            }
        } 

        #region IsHavePolicy
        private bool IsHavePolicy(string strAgencyID, int polid)
        {
            try
            {
                fibawsClient clt = new fibawsClient();
                var policeList = clt.getpolicesorgu(new FibawsPolicesorguinrecUser
                {
                    acenteno = strAgencyID,
                    polid = polid
                });

                if (policeList != null && policeList.pollist != null && policeList.pollist.Count > 0)
                {
                    return policeList.pollist.Where(w => w.polid == polid).ToList().Count > 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }
        #endregion

        #region getRaporName
        private string getRaporName(decimal pPolid, string reporttype)
        {
            try
            {
                if (reporttype == "E")
                {
                    NTBESClient bes = new NTBESClient();
                    return bes.getraporname(pPolid);
                }
                else if (reporttype == "H")
                {
                    fibawsClient fibaws = new fibawsClient();
                    return fibaws.getraporname(pPolid);
                }
            }
            catch { }

            return "";
        }
        #endregion
    }
}