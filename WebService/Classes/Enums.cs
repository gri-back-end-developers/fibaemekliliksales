﻿
namespace WebService.Classes
{
    public class Enums
    {
        public enum ServiceMethods : byte
        {
            Fibaws2Hspotomatik = 1,
            Fibaws2Policelesme = 2,
            Fibaws2Poliptal = 3,
            Fibaws2Spostaksitli = 4,
            Fibaws2Teklifpolicelesme = 5,
            Fibaws2TeklifStatu = 6,
            Bankawebv2Hspotomatik = 7,
            Bankawebv2Policelesme = 8,
            Bankawebv2Poliptal = 9,
            Bankawebv2Spostaksitli = 10,
            Bankawebv2Teklifpolicelesme = 11,
            Bankawebv2TeklifStatu = 12,
            PdfCreate = 13,
            FibawsGethasarlistrec = 14,
            FibawsGetistiradeger = 15,
            FibawsGetkisibilgirec = 16,
            FibawsGetkisipidbilgi = 17,
            FibawsGetodemebilgirec = 18,
            FibawsGetpolaytmntrec = 19,
            FibawsGetpolicebilgirec = 20,
            FibawsGetpolicelist = 21,
            FibawsGetpolicesorgu = 22,
            FibawsGettahsilatbilgirec = 23,
            FibawsSpostaksitli3d = 24,
            PdfCreateURL = 25,
            KimlikBilgileri = 26,
            AdresSorgula = 27,
            VKNSorgula = 28,
            FibawsGetPoliceSorguilksan = 29,
            Bankawebv2PolicelesmeKPS = 30,
            BesGetkisipidbilgi = 31
        } 
    }
}