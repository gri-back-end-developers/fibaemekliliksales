﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 

namespace WebService.Classes
{
    public class Logs
    {
        #region Write
        public static int Write(Enums.ServiceMethods serviceMethod, int intServiceUserID, string strException)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblServiceLogs serviceLog = new tblServiceLogs
                     {
                         dtRegisterDate = DateTime.Now,
                         intServiceMethodID = (byte)serviceMethod,
                         intServiceUserID = intServiceUserID,
                         strException = strException
                     };

                    db.tblServiceLogs.Add(serviceLog);
                    db.SaveChanges();

                    return serviceLog.intServiceLogID;
                }
            }
            catch { }

            return 0;
        } 
        #endregion

        #region Error
        public static int Error(Exception ex)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblExceptions exceptions = new tblExceptions()
                    { 
                        intPageID = 0,
                        strException = ex.ToString(),
                        dtRegisterDate = DateTime.Now
                    };

                    db.tblExceptions.Add(exceptions);
                    db.SaveChanges();

                }
            }
            catch { } 

            return 0;
        }
        #endregion
    }
}