﻿using System;
using WebService.aileBirey;

namespace WebService.Classes
{
    public class AddressInfo
    {
        public string acikAdres { get; set; }
        public long adresNo { get; set; }
        public bool adresNoSpecified { get; set; }
        public adresType adresTipi { get; set; }
        public bool adresTipiSpecified { get; set; }
        public DateTime beyanTarihi { get; set; }
        public bool beyanTarihiSpecified { get; set; }
        public long beyandaBulunanKimlikNo { get; set; }
        public bool beyandaBulunanKimlikNoSpecified { get; set; }
        public string binaAda { get; set; }
        public string binaBlokAdi { get; set; }
        public long binaKodu { get; set; }
        public bool binaKoduSpecified { get; set; }
        public string binaPafta { get; set; }
        public string binaParsel { get; set; }
        public string binaSiteAdi { get; set; }
        public string bucak { get; set; }
        public int bucakKodu { get; set; }
        public bool bucakKoduSpecified { get; set; }
        public string csbm { get; set; }
        public int csbmKodu { get; set; }
        public bool csbmKoduSpecified { get; set; }
        public string disKapiNo { get; set; }
        public string icKapiNo { get; set; }
        public string ilAd { get; set; }
        public string ilKodu { get; set; }
        public string ilceAd { get; set; }
        public string ilceKodu { get; set; }
        public long kimlikNo { get; set; }
        public bool kimlikNoSpecified { get; set; }
        public string koy { get; set; }
        public long koyKayitNo { get; set; }
        public bool koyKayitNoSpecified { get; set; }
        public long koyKodu { get; set; }
        public bool koyKoduSpecified { get; set; }
        public DateTime kpsdenSorgulanmaTarihi { get; set; }
        public bool kpsdenSorgulanmaTarihiSpecified { get; set; }
        public string mahalle { get; set; }
        public int mahalleKodu { get; set; }
        public bool mahalleKoduSpecified { get; set; }
        public int mernisIlKodu { get; set; }
        public bool mernisIlKoduSpecified { get; set; }
        public int mernisIlceKodu { get; set; }
        public bool mernisIlceKoduSpecified { get; set; }
        public DateTime tasinmaTarihi { get; set; }
        public bool tasinmaTarihiSpecified { get; set; }
        public DateTime tescilTarihi { get; set; }
        public bool tescilTarihiSpecified { get; set; }
        public string veriKonum { get; set; }
        public string yabanciAdres { get; set; }
        public string yabanciSehir { get; set; }
        public string yabanciUlke { get; set; }
        public int yabanciUlkeKodu { get; set; }
        public bool yabanciUlkeKoduSpecified { get; set; }
    }
}