﻿using System; 

namespace WebService.Classes
{
    public class UserInfo
    {
        public string tckn { get; set; }
        public string vkn { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public DateTime dogumtarihi { get; set; }
        public string email { get; set; }
        public string ceptelefon { get; set; }
    }
}