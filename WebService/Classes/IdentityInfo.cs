﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService.Classes
{
    public class IdentityInfo
    {
        public string adi { get; set; }
        public long aileSiraNo { get; set; }
        public bool aileSiraNoSpecified { get; set; }
        public string anneAdi { get; set; }
        public string babaAdi { get; set; }
        public long bireySiraNo { get; set; }
        public bool bireySiraNoSpecified { get; set; }
        public string ciltAdi { get; set; }
        public long ciltKodu { get; set; }
        public bool ciltKoduSpecified { get; set; }
        public string cinsiyeti { get; set; }
        public DateTime dogumTarihi { get; set; }
        public bool dogumTarihiSpecified { get; set; }
        public string dogumYeri { get; set; }
        public string durumu { get; set; }
        public string ilAdi { get; set; }
        public long ilTrafikKodu { get; set; }
        public bool ilTrafikKoduSpecified { get; set; }
        public string ilceAdi { get; set; }
        public long ilceKodu { get; set; }
        public bool ilceKoduSpecified { get; set; }
        public string medeniHali { get; set; }
        public DateTime olumTarihi { get; set; }
        public bool olumTarihiSpecified { get; set; }
        public string soyadi { get; set; }
        public long tckimlikNo { get; set; }
        public bool tckimlikNoSpecified { get; set; }
        public string veriKonum { get; set; }
        public string yakinlik { get; set; }
        public string uyrukAd { get; set; } 
        public int uyrukKod { get; set; } 
        public bool uyrukKodSpecified { get; set; }
    }
}