﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;

namespace WebService.Classes
{
    public class cCrypto
    {
        private static string strKey = ConfigurationSettings.AppSettings["cryptoKey"].ToString();
        private static byte[] _keyByte = { };
        private static byte[] _ivByte = { 0x01, 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78 };

        public static string EncryptDES(string value)
        {
            string strEncryptValue = string.Empty;

            MemoryStream ms = null;
            CryptoStream cs = null;

            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    _keyByte = Encoding.UTF8.GetBytes(strKey);

                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        byte[] inputByteArray = Encoding.UTF8.GetBytes(value);

                        ms = new MemoryStream();
                        cs = new CryptoStream(ms, des.CreateEncryptor(_keyByte, _ivByte), CryptoStreamMode.Write);

                        cs.Write(inputByteArray, 0, inputByteArray.Length);
                        cs.FlushFinalBlock();

                        strEncryptValue = Convert.ToBase64String(ms.ToArray());
                    }
                }
                catch { }
                finally
                {
                    cs.Dispose();
                    ms.Dispose();
                }
            }

            return strEncryptValue;
        }

        public static string DecryptDES(string strValue)
        {
            string strDecrptValue = string.Empty;

            if (!string.IsNullOrEmpty(strValue))
            {
                MemoryStream ms = null;
                CryptoStream cs = null;

                strValue = strValue.Replace(" ", "+");

                byte[] inputByteArray = new byte[strValue.Length];

                try
                {
                    _keyByte = Encoding.UTF8.GetBytes(strKey);

                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        inputByteArray = Convert.FromBase64String(strValue);

                        ms = new MemoryStream();
                        cs = new CryptoStream(ms, des.CreateDecryptor(_keyByte, _ivByte), CryptoStreamMode.Write);

                        cs.Write(inputByteArray, 0, inputByteArray.Length);
                        cs.FlushFinalBlock();

                        Encoding encoding = Encoding.UTF8;

                        strDecrptValue = encoding.GetString(ms.ToArray());
                    }
                }
                catch { }
                finally
                {
                    cs.Dispose();
                    ms.Dispose();
                }
            }

            return strDecrptValue;
        }

        public static bool PssControl(string strValue)
        {
            Match letterCase = Regex.Match(strValue, "(?=.*[a-z])");
            Match upperCase = Regex.Match(strValue, "(?=.*[A-Z])");
            Match symbolCase = Regex.Match(strValue, @"(?=.*\W)");
            Match numberCase = Regex.Match(strValue, "(?=.*[0-9])");

            int intCount = 0;
            if (letterCase.Success)
                intCount++;

            if (upperCase.Success)
                intCount++;

            if (symbolCase.Success)
                intCount++;

            if (numberCase.Success)
                intCount++;

            return intCount >= 3;
        }

        public static string EncryptText(string strValue)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(strValue);
            return Convert.ToBase64String(bytes);
        }

        public static string DecryptText(string strValue)
        {
            byte[] bytes = Convert.FromBase64String(strValue);
            return System.Text.Encoding.Unicode.GetString(bytes);
        }

        static string ByteToString(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        public static string Create(string message, string secret)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);

            HMACMD5 hmacmd5 = new HMACMD5(keyByte);
            HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);

            byte[] messageBytes = encoding.GetBytes(message);
            byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
            return ByteToString(hashmessage);
        }
    }
}