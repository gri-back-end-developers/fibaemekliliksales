﻿namespace WebService.Classes
{
    public class PdfData
    { 
        public string report { get; set; }
        public string server { get; set; }
        public int p_polid { get; set; } 
        public int p_makbuzno { get; set; }
        public string desname { get; set; }
        public string cmdkey { get; set; }
        public int p_zeylno { get; set; }
        public long t { get; set; }
        public string p_hspozettalepndn { get; set; }
        public bool getUrl_Service { get; set; }
        public string reporttype { get; set; }
    }
}