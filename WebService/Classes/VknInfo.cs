﻿using System;

namespace WebService.Classes
{
    public class VknInfo
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string BabaAdi { get; set; }
        public DateTime DogumTarihi { get; set; }
        public string DogumYeri { get; set; }
        public string Durum { get; set; }
        public string SirketTuru { get; set; }
        public string VergiNo { get; set; }
        public string VeriKonum { get; set; }
    }
}