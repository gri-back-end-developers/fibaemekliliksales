﻿#region Namespaces
using ModelLibrary;
using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using WebService.BankaWebService;
using WebService.Classes; 
using WebService.FibawsService;
#endregion

namespace WebService
{
    public class FibaWS : IFibaWS
    {
        #region Member Variables
        fibawsClient fibawsService; 
        bankaweb_v2Client bankawebService;
        #endregion

        #region ValidService
        void ValidService(Enums.ServiceMethods serviceMethods, ref ServiceUser serviceUser)
        {
            var ctx = WebOperationContext.Current;

            string strKey = ctx.IncomingRequest.Headers["key"];
            if (string.IsNullOrEmpty(strKey))
            {
                throw new Exception("Erişim yetkiniz bulunmamaktadır!")
                {
                    Source = "valid"
                };
            }
            else
            {
                using (DBEntities db = new DBEntities())
                {
                    bool bolValid = false;

                    var ServiceHashKey = db.tblServiceKey.FirstOrDefault();
                    if (ServiceHashKey != null)
                    {
                        if (db.tblServiceMethods.Count(c => c.intServiceMethodID == (byte)serviceMethods && c.bolIsActive) > 0)
                        {
                            var users = db.tblServiceUsers.Where(w => w.bolIsActive && !w.bolIsDelete).ToList();
                            foreach (var item in users)
                            {
                                string strUserKey = cCrypto.Create(item.strPassword, ServiceHashKey.strValue);
                                if (strKey == strUserKey)
                                {
                                    bolValid = item.tblServiceUserPermisions.Count(c => c.intServiceMethodID == (byte)serviceMethods) > 0;

                                    serviceUser = new ServiceUser
                                    {
                                        intServiceUserID = item.intServiceUserID,
                                        strAgencyID = item.strAgencyID
                                    };

                                    break;
                                }
                            }
                        }
                    }

                    if (!bolValid)
                    {
                        throw new Exception("Erişim yetkiniz bulunmamaktadır!")
                        {
                            Source = "valid"
                        };
                    }
                }
            }
        }
        #endregion

        #region FIBAWS

        #region FibawsGethasarlistrec
        public FibawsHasarlistrecUser FibawsGethasarlistrec(decimal? pPolid)
        {
            FibawsHasarlistrecUser hasarlistrecUser = new FibawsHasarlistrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGethasarlistrec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    hasarlistrecUser = fibawsService.gethasarlistrec(pPolid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGethasarlistrec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGethasarlistrec, serviceUser.intServiceUserID, "");

            return hasarlistrecUser;
        }
        #endregion

        #region FibawsGetistiradeger
        public FibawsPolistirarecUser FibawsGetistiradeger(decimal? pPolid)
        {
            FibawsPolistirarecUser polistirarecUser = new FibawsPolistirarecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetistiradeger, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    polistirarecUser = fibawsService.getistiradeger(pPolid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetistiradeger, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetistiradeger, serviceUser.intServiceUserID, "");

            return polistirarecUser;
        }
        #endregion

        #region FibawsGetkisibilgirec
        public FibawsKisibilgirecUser FibawsGetkisibilgirec(decimal? pPid)
        {
            FibawsKisibilgirecUser kisibilgirecUser = new FibawsKisibilgirecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetkisibilgirec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    kisibilgirecUser = fibawsService.getkisibilgirec(pPid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetkisibilgirec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetkisibilgirec, serviceUser.intServiceUserID, "");

            return kisibilgirecUser;
        }
        #endregion

        #region FibawsGetkisipidbilgi
        public decimal? FibawsGetkisipidbilgi(string pTckno, string pEmail, string pTelno)
        {
            decimal? kisipidbilgi = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetkisipidbilgi, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    kisipidbilgi = fibawsService.getkisipidbilgi(pTckno, pEmail, pTelno);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetkisipidbilgi, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetkisipidbilgi, serviceUser.intServiceUserID, "");

            return kisipidbilgi;
        }
        #endregion

        #region FibawsGetodemebilgirec
        public FibawsOdemebilgirecUser FibawsGetodemebilgirec(decimal? pPolid)
        {
            FibawsOdemebilgirecUser odemebilgirecUser = new FibawsOdemebilgirecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetodemebilgirec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    odemebilgirecUser = fibawsService.getodemebilgirec(pPolid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetodemebilgirec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetodemebilgirec, serviceUser.intServiceUserID, "");

            return odemebilgirecUser;
        }
        #endregion

        #region FibawsGetpolaytmntrec
        public FibawsPolaytmntlistrecUser FibawsGetpolaytmntrec(decimal? pPolid)
        {
            FibawsPolaytmntlistrecUser polaytmntlistrecUser = new FibawsPolaytmntlistrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetpolaytmntrec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    polaytmntlistrecUser = fibawsService.getpolaytmntrec(pPolid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetpolaytmntrec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetpolaytmntrec, serviceUser.intServiceUserID, "");

            return polaytmntlistrecUser;
        }
        #endregion

        #region FibawsGetpolicebilgirec
        public FibawsPolicebilgirecUser FibawsGetpolicebilgirec(string pPoliceno)
        {
            FibawsPolicebilgirecUser policebilgirecUser = new FibawsPolicebilgirecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetpolicebilgirec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    policebilgirecUser = fibawsService.getpolicebilgirec(pPoliceno);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetpolicebilgirec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetpolicebilgirec, serviceUser.intServiceUserID, "");

            return policebilgirecUser;
        }
        #endregion

        #region FibawsGetpolicelist
        public FibawsPolicelistrecUser FibawsGetpolicelist(decimal? pPid)
        {
            FibawsPolicelistrecUser policelistrecUser = new FibawsPolicelistrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetpolicelist, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    policelistrecUser = fibawsService.getpolicelist(pPid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetpolicelist, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetpolicelist, serviceUser.intServiceUserID, "");

            return policelistrecUser;
        }
        #endregion

        #region FibawsGetpolicesorgu
        public FibawsPolicesorguoutrecUser FibawsGetpolicesorgu(FibawsPolicesorguinrecUser pInput)
        {
            FibawsPolicesorguoutrecUser policesorguoutrecUser = new FibawsPolicesorguoutrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetpolicesorgu, ref serviceUser);

                if (string.IsNullOrEmpty(pInput.acenteno))
                {
                    throw new FaultException("Acente no alanını boş bıraktınız!");
                }

                using (fibawsService = new fibawsClient())
                {
                    policesorguoutrecUser = fibawsService.getpolicesorgu(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetpolicesorgu, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetpolicesorgu, serviceUser.intServiceUserID, "");

            return policesorguoutrecUser;
        }
        #endregion

        #region FibawsGettahsilatbilgirec
        public FibawsTahsilatbilgilistrecUser FibawsGettahsilatbilgirec(decimal? pPolid)
        {
            FibawsTahsilatbilgilistrecUser tahsilatbilgilistrecUser = new FibawsTahsilatbilgilistrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGettahsilatbilgirec, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    tahsilatbilgilistrecUser = fibawsService.gettahsilatbilgirec(pPolid);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGettahsilatbilgirec, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGettahsilatbilgirec, serviceUser.intServiceUserID, "");

            return tahsilatbilgilistrecUser;
        }
        #endregion

        #region FibawsSpostaksitli3d
        public FibawsSanalposout3dUser FibawsSpostaksitli3d(FibawsSanalposin3dUser pInput)
        {
            FibawsSanalposout3dUser sanalposout3dUser = new FibawsSanalposout3dUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsSpostaksitli3d, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    sanalposout3dUser = fibawsService.spostaksitli3d(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsSpostaksitli3d, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsSpostaksitli3d, serviceUser.intServiceUserID, "");

            return sanalposout3dUser;
        }
        #endregion

        #region FibawsGetPoliceSorguilksan
        public FibawsPolicesorguoutrecUser FibawsGetPoliceSorguilksan(FibawsPolicesorguinrecUser pInput)
        {
            FibawsPolicesorguoutrecUser policesorguoutrecUser = new FibawsPolicesorguoutrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.FibawsGetPoliceSorguilksan, ref serviceUser);

                using (fibawsService = new fibawsClient())
                {
                    policesorguoutrecUser = fibawsService.getpolicesorguilksan(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.FibawsGetPoliceSorguilksan, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.FibawsGetPoliceSorguilksan, serviceUser.intServiceUserID, "");

            return policesorguoutrecUser;
        }
        #endregion

        #endregion
        
        #region Bankawebv2

        #region Bankawebv2Hspotomatik
        public BankawebV2HspotooutUser Bankawebv2Hspotomatik(BankawebV2HspotoinUser pInput)
        {
            BankawebV2HspotooutUser hspotomatik = new BankawebV2HspotooutUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2Hspotomatik, ref serviceUser);

                using (bankawebService = new bankaweb_v2Client())
                {
                    hspotomatik = bankawebService.hspotomatik(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2Hspotomatik, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2Hspotomatik, serviceUser.intServiceUserID, "");

            return hspotomatik;
        }
        #endregion

        #region Bankawebv2Policelesme
        public BankawebV2PolresUser Bankawebv2Policelesme(BankawebV2PolreqUser polreq, string pUwonay)
        {
            BankawebV2PolresUser policelesme = new BankawebV2PolresUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2Policelesme, ref serviceUser);

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogPath"].ToString()))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                    Utilities.TxtWrite(ConfigurationManager.AppSettings["LogPath"].ToString() + "Logs.txt", jss.Serialize(polreq));
                }

                using (bankawebService = new bankaweb_v2Client())
                {
                    policelesme = bankawebService.policelesme(polreq, pUwonay);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2Policelesme, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2Policelesme, serviceUser.intServiceUserID, "");

            return policelesme;
        }
        #endregion

        #region Bankawebv2PolicelesmeKPS
        public BankawebV2PolresUser Bankawebv2PolicelesmeKPS(BankawebV2PolreqUser polreq, string pUwonay, bool bolKPS, bool bolAddress, bool bolVKN, UserInfo userInfo)
        {
            BankawebV2PolresUser policelesme = new BankawebV2PolresUser();
            ServiceUser serviceUser = new ServiceUser();

            DBEntities db = new DBEntities();
            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2PolicelesmeKPS, ref serviceUser);

                if ((bolKPS || bolVKN || bolAddress) && userInfo == null)
                {
                    throw new Exception("Kullanıcı bilgisini boş gönderdiniz!")
                    {
                        Source = "valid"
                    };
                }

                #region kps
                if (bolKPS)
                {
                    if (string.IsNullOrEmpty(userInfo.tckn))
                    {
                        throw new FaultException("Tckn giriniz!")
                        {
                            Source = "valid"
                        };
                    }

                    IdentityInfo identityInfo = null;

                    KPS(userInfo.tckn, ref identityInfo);

                    if (identityInfo == null)
                    {
                        throw new FaultException("Kimlik numarasına ait kayıt bulunamadı!")
                        {
                            Source = "valid"
                        };
                    }

                    bool bolNameSurnameValid = identityInfo.adi.ToLower().Trim() + " " + identityInfo.soyadi.ToLower().Trim() == userInfo.ad.ToLower().Trim() + " " + userInfo.soyad.ToLower().Trim();
                    bool bolBirthDateValid = identityInfo.dogumTarihi.ToString("dd-MM-yyyy") == userInfo.dogumtarihi.ToString("dd-MM-yyyy");

                    if (!(bolNameSurnameValid && bolBirthDateValid))
                    {
                        throw new FaultException("Kimlik bilgileri doğrulanmadı!")
                        {
                            Source = "valid"
                        };
                    }

                    string dogumil = "";
                    var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == identityInfo.dogumYeri.Trim()).FirstOrDefault();
                    if (il != null)
                        dogumil = il.strCode.Trim();

                    string dogumilce = "";
                    var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == identityInfo.dogumYeri.Trim()).FirstOrDefault();
                    if (ilce != null)
                        dogumilce = ilce.strCode.Trim();

                    if (dogumil == "" && dogumilce != "")
                    {
                        dogumil = dogumilce.Substring(0, 3);
                    }

                    if (dogumil == "")
                    {
                        dogumil = "99";
                    }

                    if (dogumil != "" && dogumilce == "")
                    {
                        dogumilce = "99900";
                    }

                    polreq.ozluk = new Bankawebv2polreqOzlukreqUser
                    {
                        ad = identityInfo.adi,
                        anneadi = identityInfo.anneAdi,
                        babaad = identityInfo.babaAdi,
                        cinsiyet = identityInfo.cinsiyeti,
                        dogumil = dogumil.Trim(),
                        dogumilce = dogumilce.Trim(),
                        dogumtarih = identityInfo.dogumTarihi,
                        medenidurum = identityInfo.medeniHali == "EVLİ" ? "E" : "B",
                        soyad = identityInfo.soyadi,
                        tckimlikno = identityInfo.tckimlikNo.ToString(),
                        musteritip = "G",
                        ilgilikisitel = userInfo.ceptelefon
                    };
                }
                #endregion

                #region adres
                if (bolAddress)
                {
                    if (string.IsNullOrEmpty(userInfo.tckn))
                    {
                        throw new FaultException("Tckn giriniz!")
                        {
                            Source = "valid"
                        };
                    }

                    AddressInfo addressInfo = null;

                    Address(userInfo.tckn, ref addressInfo);

                    if (addressInfo == null)
                    {
                        throw new FaultException("Kimlik numarasına ait adres bilgisi bulunamadı!")
                        {
                            Source = "valid"
                        };
                    }

                    polreq.adres = new Bankawebv2polreqOzlukadresreUser
                    {
                        adres = addressInfo.acikAdres,
                        adrestip = "E",
                        email = userInfo.email,
                        ilcekod = addressInfo.ilceKodu,
                        iletisimno = userInfo.ceptelefon.Replace("(", "").Replace(")", "").Replace(" ", ""),
                        iletisimtip = "CEP",
                        ilkod = addressInfo.ilKodu
                    };
                }
                #endregion

                #region vkn
                if (bolVKN)
                {
                    if (string.IsNullOrEmpty(userInfo.vkn))
                    {
                        throw new FaultException("Vergi kimlik numarası giriniz!")
                        {
                            Source = "valid"
                        };
                    }

                    VknService.vknBilgisiType vknData = VKN(userInfo.vkn);
                    if (vknData == null)
                    {
                        throw new FaultException("Vergi kimlik numarasına ait kayıt bulunamadı!")
                        {
                            Source = "valid"
                        };
                    }

                    polreq.odeyen = new Bankawebv2polreqOzlukreqUser
                    {
                        ad = vknData.ad,
                        soyad = vknData.soyad,
                        ilgilikisitel = userInfo.ceptelefon,
                        vergino = vknData.vergiNo
                    };
                }
                #endregion

                using (bankawebService = new bankaweb_v2Client())
                {
                    policelesme = bankawebService.policelesme(polreq, pUwonay);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2PolicelesmeKPS, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }
            finally
            {
                db.Dispose();
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2PolicelesmeKPS, serviceUser.intServiceUserID, "");

            return policelesme;
        }
        #endregion

        #region Bankawebv2Poliptal
        public BankawebV2PoliptrecUser Bankawebv2Poliptal(string pUser, BankawebV2PoliptinUser pIpt)
        {
            BankawebV2PoliptrecUser poliptal = new BankawebV2PoliptrecUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2Poliptal, ref serviceUser);

                using (bankawebService = new bankaweb_v2Client())
                {
                    poliptal = bankawebService.poliptal(pUser, pIpt);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2Poliptal, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2Poliptal, serviceUser.intServiceUserID, "");

            return poliptal;
        }
        #endregion

        #region Bankawebv2Spostaksitli
        public BankawebV2SanalposoutUser Bankawebv2Spostaksitli(BankawebV2SanalposinUser pInput)
        {
            BankawebV2SanalposoutUser spostaksitli = new BankawebV2SanalposoutUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2Spostaksitli, ref serviceUser);

                using (bankawebService = new bankaweb_v2Client())
                {
                    spostaksitli = bankawebService.spostaksitli(pInput);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2Spostaksitli, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2Spostaksitli, serviceUser.intServiceUserID, "");

            return spostaksitli;
        }
        #endregion

        #region Bankawebv2Teklifpolicelesme
        public BankawebV2PolresUser Bankawebv2Teklifpolicelesme(decimal? tkl, decimal? insCan, string pKredino2, string krd)
        {
            BankawebV2PolresUser teklifpolicelesme = new BankawebV2PolresUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2Teklifpolicelesme, ref serviceUser);

                using (bankawebService = new bankaweb_v2Client())
                {
                    teklifpolicelesme = bankawebService.teklifpolicelesme(tkl, insCan, pKredino2, krd);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2Teklifpolicelesme, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2Teklifpolicelesme, serviceUser.intServiceUserID, "");

            return teklifpolicelesme;
        }
        #endregion

        #region Bankawebv2TeklifStatu
        public BankawebV2StatureqUser Bankawebv2TeklifStatu(decimal? pol, string krd)
        {
            BankawebV2StatureqUser teklifStatu = new BankawebV2StatureqUser();
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.Bankawebv2TeklifStatu, ref serviceUser);

                using (bankawebService = new bankaweb_v2Client())
                {
                    teklifStatu = bankawebService.teklifStatu(pol, krd);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.Bankawebv2TeklifStatu, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.Bankawebv2TeklifStatu, serviceUser.intServiceUserID, "");

            return teklifStatu;
        }
        #endregion

        #endregion

        #region INTBES

        #region BesGetkisipidbilgi
        public decimal? BesGetkisipidbilgi(string pTckno, string pEmail, string pTelno)
        {
            decimal? kisipidbilgi = null;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.BesGetkisipidbilgi, ref serviceUser);

                using (IntBesService.NTBESClient intBesService = new IntBesService.NTBESClient())
                {
                    kisipidbilgi = intBesService.getkisipidbilgi(pTckno, pEmail, pTelno);
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.BesGetkisipidbilgi, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.BesGetkisipidbilgi, serviceUser.intServiceUserID, "");

            return kisipidbilgi;
        }
        #endregion

        #endregion

        #region PdfCreate
        public byte[] PdfCreate(PdfData pdfData)
        {
            byte[] createData;
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.PdfCreate, ref serviceUser);

                Pdf pdf = new Pdf();
                createData = (byte[])pdf.Create(false, pdfData, serviceUser.strAgencyID);
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.PdfCreate, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            Logs.Write(Enums.ServiceMethods.PdfCreate, serviceUser.intServiceUserID, "");

            return createData;
        }
        #endregion

        #region PdfCreateURL
        public string PdfCreateURL(PdfData pdfData)
        {
            ServiceUser serviceUser = new ServiceUser();

            try
            {
                ValidService(Enums.ServiceMethods.PdfCreateURL, ref serviceUser);

                Pdf pdf = new Pdf();
                string strUrl = pdf.Create(true, pdfData, serviceUser.strAgencyID).ToString();

                int intServiceLogID = Logs.Write(Enums.ServiceMethods.PdfCreateURL, serviceUser.intServiceUserID, "");

                if (!string.IsNullOrEmpty(strUrl))
                {
                    string strKey = Utilities.RandomString(2) + intServiceLogID + Utilities.RandomString(4);

                    using (DBEntities db = new DBEntities())
                    {
                        db.tblServicePdf.Add(new tblServicePdf
                        {
                            intServiceLogID = intServiceLogID,
                            strKey = strKey,
                            strURL = strUrl.Replace("\"", "")
                        });
                        db.SaveChanges();
                    }

                    return ConfigurationManager.AppSettings["PdfPage"] + "?k=" + strKey;
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.PdfCreateURL, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            return "";
        }
        #endregion

        #region KimlikBilgileri
        public IdentityInfo KimlikBilgileri(string strTCKN)
        {
            ServiceUser serviceUser = new ServiceUser();
            IdentityInfo identityInfo = null;

            try
            {
                ValidService(Enums.ServiceMethods.KimlikBilgileri, ref serviceUser);

                int intServiceLogID = Logs.Write(Enums.ServiceMethods.KimlikBilgileri, serviceUser.intServiceUserID, "TCKN: " + cCrypto.EncryptDES(strTCKN));

                KPS(strTCKN, ref identityInfo);
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.KimlikBilgileri, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            return identityInfo;
        }
        #endregion

        #region AdresSorgula
        public AddressInfo AdresSorgula(string strTCKN)
        {
            ServiceUser serviceUser = new ServiceUser();
            AddressInfo addressInfo = null;

            try
            {
                ValidService(Enums.ServiceMethods.KimlikBilgileri, ref serviceUser);

                int intServiceLogID = Logs.Write(Enums.ServiceMethods.AdresSorgula, serviceUser.intServiceUserID, "TCKN: " + cCrypto.EncryptDES(strTCKN));

                Address(strTCKN, ref addressInfo);
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.AdresSorgula, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            return addressInfo;
        }
        #endregion

        #region VKNSorgula
        public VknInfo VKNSorgula(string strVKN)
        {
            ServiceUser serviceUser = new ServiceUser();
            VknInfo vnkInfo = null;

            try
            {
                ValidService(Enums.ServiceMethods.VKNSorgula, ref serviceUser);

                int intServiceLogID = Logs.Write(Enums.ServiceMethods.VKNSorgula, serviceUser.intServiceUserID, "VKN: " + cCrypto.EncryptDES(strVKN));

                if (!string.IsNullOrEmpty(strVKN))
                {
                    VknService.vknBilgisiType vknData = VKN(strVKN);
                    if (vknData != null)
                    {
                        vnkInfo = new VknInfo
                        {
                            Ad = vknData.ad,
                            BabaAdi = vknData.babaAdi,
                            DogumTarihi = vknData.dogumTarihi,
                            DogumYeri = vknData.dogumYeri,
                            Durum = vknData.durum,
                            SirketTuru = vknData.sirketTuru,
                            Soyad = vknData.soyad,
                            VergiNo = vknData.vergiNo,
                            VeriKonum = vknData.veriKonum
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Write(Enums.ServiceMethods.VKNSorgula, serviceUser.intServiceUserID, ex.ToString());

                throw new FaultException(ex.Message);
            }

            return vnkInfo;
        }
        #endregion

        #region KPS
        private void KPS(string strTCKN, ref IdentityInfo identityInfo)
        {
            if (!string.IsNullOrEmpty(strTCKN))
            {
                using (DBEntities db = new DBEntities())
                {
                    aileBirey.KpsBireySorguServiceClient aileService = new aileBirey.KpsBireySorguServiceClient();
                    try
                    {
                        var kpsBireySorgu = aileService.kpsBireySorgu(new aileBirey.bireySorguInputType
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });

                        identityInfo = new IdentityInfo
                        {
                            adi = kpsBireySorgu.adi,
                            aileSiraNo = kpsBireySorgu.aileSiraNo,
                            aileSiraNoSpecified = kpsBireySorgu.aileSiraNoSpecified,
                            anneAdi = kpsBireySorgu.anneAdi,
                            babaAdi = kpsBireySorgu.babaAdi,
                            bireySiraNo = kpsBireySorgu.bireySiraNo,
                            bireySiraNoSpecified = kpsBireySorgu.bireySiraNoSpecified,
                            ciltAdi = kpsBireySorgu.ciltAdi,
                            ciltKodu = kpsBireySorgu.ciltKodu,
                            ciltKoduSpecified = kpsBireySorgu.ciltKoduSpecified,
                            cinsiyeti = kpsBireySorgu.cinsiyeti,
                            dogumTarihi = kpsBireySorgu.dogumTarihi,
                            dogumTarihiSpecified = kpsBireySorgu.dogumTarihiSpecified,
                            dogumYeri = kpsBireySorgu.dogumYeri,
                            durumu = kpsBireySorgu.durumu,
                            ilAdi = kpsBireySorgu.ilAdi,
                            ilceAdi = kpsBireySorgu.ilceAdi,
                            ilceKodu = kpsBireySorgu.ilceKodu,
                            ilceKoduSpecified = kpsBireySorgu.ilceKoduSpecified,
                            ilTrafikKodu = kpsBireySorgu.ilTrafikKodu,
                            ilTrafikKoduSpecified = kpsBireySorgu.ilTrafikKoduSpecified,
                            medeniHali = kpsBireySorgu.medeniHali,
                            olumTarihi = kpsBireySorgu.olumTarihi,
                            olumTarihiSpecified = kpsBireySorgu.olumTarihiSpecified,
                            soyadi = kpsBireySorgu.soyadi,
                            tckimlikNo = kpsBireySorgu.tckimlikNo,
                            tckimlikNoSpecified = kpsBireySorgu.tckimlikNoSpecified,
                            veriKonum = kpsBireySorgu.veriKonum,
                            yakinlik = kpsBireySorgu.yakinlik
                        };
                    }
                    catch
                    {
                        try
                        {
                            var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new aileBirey.bireySorguInputType
                            {
                                cacheStyle = 1,
                                maxAge = 180,
                                kimlikNo = Utilities.NullFixLong(strTCKN),
                                kimlikNoSpecified = true
                            });

                            identityInfo = new IdentityInfo
                            {
                                adi = kpsYabanciSorgu.adi,
                                babaAdi = kpsYabanciSorgu.babaAdi,
                                cinsiyeti = kpsYabanciSorgu.cinsiyeti,
                                dogumTarihi = kpsYabanciSorgu.dogumTarihi,
                                dogumTarihiSpecified = kpsYabanciSorgu.dogumTarihiSpecified,
                                durumu = kpsYabanciSorgu.durumu,
                                soyadi = kpsYabanciSorgu.soyadi,
                                tckimlikNo = kpsYabanciSorgu.tckimlikNo,
                                tckimlikNoSpecified = kpsYabanciSorgu.tckimlikNoSpecified,
                                uyrukAd = kpsYabanciSorgu.uyrukAd,
                                uyrukKod = kpsYabanciSorgu.uyrukKod,
                                uyrukKodSpecified = kpsYabanciSorgu.uyrukKodSpecified,
                                veriKonum = kpsYabanciSorgu.veriKonum
                            };
                        }
                        catch
                        {
                            throw new FaultException("Kimlik bilgisi bulunamadı!");
                        }
                    }
                }
            }
        }
        #endregion

        #region VKN
        private VknService.vknBilgisiType VKN(string strVKN)
        {
            using (VknService.VknSorguServiceClient vknService = new VknService.VknSorguServiceClient())
            {
                var vknData = vknService.vknSorgu(new VknService.vergiSorguInputType
                {
                    acenteKod = "",
                    vergiNo = strVKN,
                    cacheStyle = 1
                });

                return vknData;
            }
        }
        #endregion

        #region Address
        private void Address(string strTCKN, ref AddressInfo addressInfo)
        {
            if (!string.IsNullOrEmpty(strTCKN))
            {
                aileBirey.KpsBireySorguServiceClient aileService = new aileBirey.KpsBireySorguServiceClient();
                aileBirey.kisiAcikAdresBilgisiType acikAdresBilgisi = new aileBirey.kisiAcikAdresBilgisiType();

                try
                {
                    acikAdresBilgisi = aileService.kpsTCKisiAcikAdresSorgu(new aileBirey.kisiAdresSorguInputType
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTCKN),
                        kimlikNoSpecified = true
                    });
                }
                catch
                {
                    try
                    {
                        acikAdresBilgisi = aileService.kpsYabanciKisiAcikAdresSorgu(new aileBirey.kisiAdresSorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(strTCKN),
                            kimlikNoSpecified = true
                        });
                    }
                    catch
                    {
                        throw new FaultException("Adres bilgisi bulunamadı!");
                    }
                }

                if (acikAdresBilgisi.kimlikNo > 0)
                {
                    addressInfo = new AddressInfo
                    {
                        acikAdres = acikAdresBilgisi.acikAdres,
                        adresNo = acikAdresBilgisi.adresNo,
                        adresNoSpecified = acikAdresBilgisi.adresNoSpecified,
                        adresTipi = acikAdresBilgisi.adresTipi,
                        adresTipiSpecified = acikAdresBilgisi.adresTipiSpecified,
                        beyandaBulunanKimlikNo = acikAdresBilgisi.beyandaBulunanKimlikNo,
                        beyandaBulunanKimlikNoSpecified = acikAdresBilgisi.beyandaBulunanKimlikNoSpecified,
                        beyanTarihi = acikAdresBilgisi.beyanTarihi,
                        beyanTarihiSpecified = acikAdresBilgisi.beyanTarihiSpecified,
                        binaAda = acikAdresBilgisi.binaAda,
                        binaBlokAdi = acikAdresBilgisi.binaBlokAdi,
                        binaKodu = acikAdresBilgisi.binaKodu,
                        binaKoduSpecified = acikAdresBilgisi.binaKoduSpecified,
                        binaPafta = acikAdresBilgisi.binaPafta,
                        binaParsel = acikAdresBilgisi.binaParsel,
                        binaSiteAdi = acikAdresBilgisi.binaSiteAdi,
                        bucak = acikAdresBilgisi.bucak,
                        bucakKodu = acikAdresBilgisi.bucakKodu,
                        bucakKoduSpecified = acikAdresBilgisi.bucakKoduSpecified,
                        csbm = acikAdresBilgisi.csbm,
                        csbmKodu = acikAdresBilgisi.csbmKodu,
                        csbmKoduSpecified = acikAdresBilgisi.csbmKoduSpecified,
                        disKapiNo = acikAdresBilgisi.disKapiNo,
                        icKapiNo = acikAdresBilgisi.icKapiNo,
                        ilAd = acikAdresBilgisi.ilAd,
                        ilceAd = acikAdresBilgisi.ilceAd,
                        ilceKodu = acikAdresBilgisi.ilceKodu,
                        ilKodu = acikAdresBilgisi.ilKodu,
                        kimlikNo = acikAdresBilgisi.kimlikNo,
                        kimlikNoSpecified = acikAdresBilgisi.kimlikNoSpecified,
                        koy = acikAdresBilgisi.koy,
                        koyKayitNo = acikAdresBilgisi.koyKayitNo,
                        koyKayitNoSpecified = acikAdresBilgisi.koyKayitNoSpecified,
                        koyKodu = acikAdresBilgisi.koyKodu,
                        koyKoduSpecified = acikAdresBilgisi.koyKoduSpecified,
                        kpsdenSorgulanmaTarihi = acikAdresBilgisi.kpsdenSorgulanmaTarihi,
                        kpsdenSorgulanmaTarihiSpecified = acikAdresBilgisi.kpsdenSorgulanmaTarihiSpecified,
                        mahalle = acikAdresBilgisi.mahalle,
                        mahalleKodu = acikAdresBilgisi.mahalleKodu,
                        mahalleKoduSpecified = acikAdresBilgisi.mahalleKoduSpecified,
                        mernisIlceKodu = acikAdresBilgisi.mernisIlceKodu,
                        mernisIlceKoduSpecified = acikAdresBilgisi.mernisIlceKoduSpecified,
                        mernisIlKodu = acikAdresBilgisi.mernisIlKodu,
                        mernisIlKoduSpecified = acikAdresBilgisi.mernisIlKoduSpecified,
                        tasinmaTarihi = acikAdresBilgisi.tasinmaTarihi,
                        tasinmaTarihiSpecified = acikAdresBilgisi.tasinmaTarihiSpecified,
                        tescilTarihi = acikAdresBilgisi.tescilTarihi,
                        tescilTarihiSpecified = acikAdresBilgisi.tescilTarihiSpecified,
                        veriKonum = acikAdresBilgisi.veriKonum,
                        yabanciAdres = acikAdresBilgisi.yabanciAdres,
                        yabanciSehir = acikAdresBilgisi.yabanciSehir,
                        yabanciUlke = acikAdresBilgisi.yabanciUlke,
                        yabanciUlkeKodu = acikAdresBilgisi.yabanciUlkeKodu,
                        yabanciUlkeKoduSpecified = acikAdresBilgisi.yabanciUlkeKoduSpecified
                    };
                }
            }
        }
        #endregion
    }
}
