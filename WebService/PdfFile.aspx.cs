﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebService.Classes;

namespace WebService
{
    public partial class PdfFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strKey = Request.QueryString["k"];
                if (!string.IsNullOrEmpty(strKey))
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var servicePdf = db.tblServicePdf.Where(w => w.strKey == strKey).FirstOrDefault();
                        if (servicePdf != null)
                        {
                            UrlGet(servicePdf.strURL.Replace("\"", ""));
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                Logs.Error(ex);
            }
        }

        #region UrlGet
        void UrlGet(string Url)
        {
            HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
            myRequest.Method = "GET";
            myRequest.ContentType = "application/pdf";

            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            Stream encodingStream = myResponse.GetResponseStream();
            StreamReader read = new StreamReader(encodingStream);
            string result = read.ReadToEnd();
            myResponse.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.Write(result); 
        }
        #endregion
    }
}