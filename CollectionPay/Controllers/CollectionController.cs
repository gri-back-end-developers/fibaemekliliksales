﻿using CollectionPay.Classes;
using CollectionPay.Classes.Data;
using jCryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CollectionPay.Controllers
{
    public class CollectionController : Controller
    { 
        #region Get
        [HttpGet]
        [jCryptionHandler]
        [UserFilter]
        public ActionResult Pay()
        {
            List<CollectionInfo> collectionInfo = new List<CollectionInfo>();

            if (Request.QueryString["getPublicKey"] == null)
            {
                Services services = new Services();
                //Sessions.CurrentUser = new UserInfo
                //{
                //    intCollectionUserID = 24,
                //    strEmail = "",
                //    strTCKN = "20744542086",
                //    strIP = Utilities.ClientIP,
                //    intPolID = 0,
                //    strToken = "",
                //    intCallcenterID = 9,
                //    intPidID = 1596231
                //};

                List<decimal> policy = services.GetPoliceBilgiRec(Sessions.CurrentUser.intPidID);

                if (policy != null && policy.Count > 0)
                {
                    if (!string.IsNullOrEmpty(QueryStrings.t))
                    {
                        if (policy.Count(c => c == Sessions.CurrentUser.intPolID) > 0)
                        {
                            collectionInfo = services.CollectionList(Sessions.CurrentUser.intPolID);
                        } 
                    }
                    else
                    {
                        for (int i = 0; i < policy.Count; i++)
                        {
                            collectionInfo.AddRange(services.CollectionList(policy[i]));
                        }
                    }

                    Sessions.CollectionInfo = collectionInfo;
                }

                if (collectionInfo == null || collectionInfo.Count == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Info, "Ödenmemiş tahsilatınız yoktur.");
                }
            }

            return View(collectionInfo);
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        [UserFilter]
        public ActionResult Pay(string collection, string name, string card, string month, string year, string cvv, string payupdate)
        {
            if (string.IsNullOrEmpty(collection) && string.IsNullOrEmpty(name) && string.IsNullOrEmpty(card))
            {
                return View();
            }

            #region Validate
            if (collection.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Tahsilat seçiniz!");
                return View();
            }

            if (name.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Adınızı ve soyadınızı giriniz!");
                return View();
            }

            if (card.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kart numarasını giriniz!");
                return View();
            }

            if (month.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Ay seçiniz!");
                return View();
            }

            if (year.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Yıl giriniz!");
                return View();
            }

            if (cvv.Trim().Length == 0)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "CCV giriniz!");
                return View();
            } 
            #endregion

            try
            {
                List<CollectionInfo> collectionInfos = Sessions.CollectionInfo;

                if (collectionInfos != null)
                {
                    CollectionInfo collectionInfo = collectionInfos.Where(w => w.strID == collection).FirstOrDefault();
                    if (collectionInfo != null)
                    {
                        if (collectionInfo.intPolID > 0)
                        {
                            PaymentStart paymentStart = new PaymentStart();

                            string[] nameSurname = name.Split(' ');
                            Classes.Data.Pay.Card cardInfo = new Classes.Data.Pay.Card
                            {
                                cardno = card.Replace("-", "").Replace(" ", "").Trim(),
                                cvv2 = cvv,
                                month = month,
                                name = nameSurname.Length == 1 ? nameSurname[0] : name.Replace(nameSurname[nameSurname.Length - 1], "").TrimEnd(),
                                surname = nameSurname.Length > 1 ? nameSurname[nameSurname.Length -1] : "",
                                paymentmethod = "0",
                                year = year.Substring(2, 2)
                            };

                            bool bolPayUpdate = (payupdate == "on" || payupdate == "checked" || payupdate == "true");

                            CollectionPay.Classes.Data.Pay.PaymentInfo paymentInfo = paymentStart.Start(cardInfo, collectionInfo.intPolID, collectionInfo.flPrice, collectionInfo.strVadeTar, collection, bolPayUpdate);

                            if (paymentInfo != null)
                            {
                                Sessions.CardInfo = cardInfo;

                                return View("Start", paymentInfo);
                            }
                        }
                        else
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Poliçe bulunamadı!");
                            return Pay();
                        }
                    }
                }

                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Collection Pay Post");
            }

            return Pay();
        }
        #endregion

        #region Start
        [UserFilter]
        public ActionResult Start(CollectionPay.Classes.Data.Pay.PaymentInfo payment)
        {
            return View(payment);
        }
        #endregion

        #region Finish
        public ActionResult Finish()
        {
            PaymentStart paymentStart = new PaymentStart();
            ViewBag.Message = paymentStart.Finish();

            return View();
        }
        #endregion
    }
}
