﻿using CollectionPay.Classes;
using CollectionPay.Classes.Data;
using jCryption;
using ModelLibrary;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace CollectionPay.Controllers
{
    public class DefaultController : Controller
    {
        #region PolID
        private CollectionSendInfo CollectionInfo(string strTCKN)
        {
            try
            {
                string strToken = RouteData.Values["token"].ToString();

                if (strToken != "")
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var sendLog = db.tblCollectionSendLogs.Where(w => w.bolIsSend && w.strToken == strToken).FirstOrDefault();
                        if (sendLog != null)
                        {
                            if (!string.IsNullOrEmpty(strTCKN))
                            {
                                if (sendLog.strTCKN == strTCKN)
                                {
                                    return new CollectionSendInfo { intPolID = sendLog.intPolID, intCallcenterID = sendLog.intCallcenterID.Value };
                                }
                                else
                                {
                                    return new CollectionSendInfo { intPolID = 0, intCallcenterID = sendLog.intCallcenterID.Value };
                                }
                            }
                            else
                            {
                                return new CollectionSendInfo { intPolID = sendLog.intPolID, intCallcenterID = sendLog.intCallcenterID.Value };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Default PolID");
            }

            return null;
        }
        #endregion

        #region Login

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Login()
        {
            if (Request.QueryString["getPublicKey"] == null)
            {
                try
                {
                    if (Sessions.Message != null)
                    {
                        ViewBag.Message = Sessions.Message;
                        Sessions.Message = null;
                    }

                    Session.Clear();
                    Session.RemoveAll();
                    Session.Abandon();

                    SessionIDManager manager = new SessionIDManager();
                    string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                    bool redirected = false;
                    bool IsAdded = false;
                    manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                    if (RouteData.Values["token"] == null)
                    {

                    }
                    else
                    {
                        CollectionSendInfo collectionInfo = CollectionInfo("");
                        if (collectionInfo == null || collectionInfo.intPolID == 0)
                        {
                            return Redirect("/error/permision");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "Default Login Get");
                }
            }

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Login(String tckn, String password)
        {
            if (string.IsNullOrEmpty(tckn) && string.IsNullOrEmpty(password))
            {
                return View();
            }

            try
            {
                if (tckn.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "T.C. kimlik numaranızı giriniz!");
                    return View();
                }

                if (password.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifrenizi giriniz!");
                    return View();
                }

                string strTCKN = cCrypto.EncryptDES(tckn.Trim()); 
                string strPassword = cCrypto.EncryptDES(password.Trim());
                    
                using (DBEntities db = new DBEntities())
                {
                    if (RouteData.Values["token"] != null)
                    {
                        CollectionSendInfo collectionInfo = CollectionInfo(strTCKN);
                        string strToken = RouteData.Values["token"].ToString();

                        if (collectionInfo == null || collectionInfo.intPolID == 0)
                        {
                            if (strToken == "")
                            {
                                return Redirect("/error/permision");
                            }
                            else
                            {
                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı T.C. kimlik numarası girdiniz!");
                                return View();
                            }
                        }

                        var user = db.tblCollectionUsers.Where(w => w.bolAcente && w.strTCKN == strTCKN && w.strPss == strPassword && w.strToken == strToken).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.bolIsActive)
                            {
                                Services services = new Services();
                                decimal pid = services.GetKisiPidbilgi(cCrypto.DecryptDES(strTCKN), user.strEmail, user.strPhone);
                                if (pid > 0)
                                {
                                    Logs.UserLog(user.intCollectionUserID, "login", "Giriş yapıldı");

                                    Sessions.CurrentUser = new UserInfo
                                    {
                                        intCollectionUserID = user.intCollectionUserID,
                                        strEmail = user.strEmail,
                                        strTCKN = tckn,
                                        strIP = Utilities.ClientIP,
                                        intPolID = collectionInfo.intPolID,
                                        strToken = strToken,
                                        intCallcenterID = collectionInfo.intCallcenterID,
                                        intPidID = pid
                                    };

                                    return Redirect("/collection/pay?t=" + strToken);
                                }
                                else
                                {
                                    Logs.UserLog(user.intCollectionUserID, "login", "Kullanıcı bulunamadı!");

                                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kullanıcı bulunamadı!");
                                }
                            }
                            else
                            {
                                Logs.UserLog(user.intCollectionUserID, "login", "Kullanıcı aktif değil");

                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kullanıcı aktif değil! Şifremi unuttum bölümünden aktif edebilirsiniz.");
                            }
                        }
                        else
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                        }
                    }
                    else
                    {
                        var user = db.tblCollectionUsers.Where(w => !w.bolAcente && w.strTCKN == strTCKN && w.strPss == strPassword).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.bolIsActive)
                            {
                                Services services = new Services();
                                decimal pid = services.GetKisiPidbilgi(cCrypto.DecryptDES(strTCKN), user.strEmail, user.strPhone);

                                if (pid > 0)
                                {
                                    Logs.UserLog(user.intCollectionUserID, "login", "Giriş yapıldı");

                                    Sessions.CurrentUser = new UserInfo
                                    {
                                        intCollectionUserID = user.intCollectionUserID,
                                        strEmail = user.strEmail,
                                        strTCKN = tckn,
                                        strIP = Utilities.ClientIP,
                                        intPidID = pid,
                                        intCallcenterID = Utilities.NullFixInt(System.Configuration.ConfigurationManager.AppSettings["CallcenterID"].ToString())
                                    };

                                    return Redirect("/collection/pay");
                                }
                                else
                                {
                                    Logs.UserLog(user.intCollectionUserID, "login", "Kullanıcı bulunamadı!");

                                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kullanıcı bulunamadı!");
                                }
                            }
                            else
                            {
                                Logs.UserLog(user.intCollectionUserID, "login", "Kullanıcı aktif değil");

                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kullanıcı aktif değil! Şifremi unuttum bölümünden aktif edebilirsiniz.");
                            }
                        }
                        else
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");
                Logs.Exception(ex, "Default Login Post");
            }

            return View();
        }
        #endregion

        #endregion
    }
}
