﻿using CollectionPay.Classes;
using CollectionPay.Classes.Data;
using jCryption;
using ModelLibrary;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CollectionPay.Controllers
{
    public class UserController : Controller
    {
        #region Forgot

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Forgot()
        {
            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Forgot(String sendtype)
        {
            if (string.IsNullOrEmpty(sendtype))
            {
                return View();
            }

            try
            {
                string strEmail = Request.Form["email"].Trim();
                string strPhone = Request.Form["phone"].Trim();

                using (DBEntities db = new DBEntities())
                {
                    tblCollectionUsers user = null;
                    if (sendtype == "email")
                    {
                        if (string.IsNullOrEmpty(strEmail))
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta adresinizi giriniz!");
                            return View();
                        }

                        user = db.tblCollectionUsers.Where(w => w.strEmail == strEmail).FirstOrDefault();
                    }
                    else if (sendtype == "sms")
                    {
                        if (string.IsNullOrEmpty(strPhone))
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Telefon numaranızı giriniz!");
                            return View();
                        }

                        user = db.tblCollectionUsers.Where(w => w.strPhone == strPhone).FirstOrDefault();
                    }

                    if (user != null)
                    {
                        string strToken = Utilities.RandomString(10);

                        if (sendtype == "email")
                        {
                            if (Email.SendEmail(ConfigurationManager.AppSettings["ForgotTitle"], "Merhaba, <p><a href=\"" + ConfigurationManager.AppSettings["SitePath"] + "/user/change/" + QueryStrings.t + "/" + strToken + "\" target=\"_blank\">Yeni şifre oluşturmak için tıklayınız</a></p>", strEmail))
                            {
                                Logs.UserLog(user.intCollectionUserID, "forgot", "Şifremi unuttum mail gönderildi", strToken);

                                Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifre oluşturma linki e-posta adresinize gönderildi.");

                                return Redirect("/" + QueryStrings.t);
                            }
                            else
                            {
                                Logs.UserLog(user.intCollectionUserID, "forgot", "E-posta adresi hatalı", strEmail);

                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta gönderilemedi! E-posta adresinizi kontrol ediniz!");
                            }
                        }
                        else if (sendtype == "sms")
                        {
                            TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                            {
                                ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                                Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                                PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                                Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                                UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                            });

                            string smsSend = sms.Send("Yeni şifre oluşturmak için " + ConfigurationManager.AppSettings["SitePath"] + "/user/change/" + QueryStrings.t + "/" + strToken + " linkine tıklayınız.", strPhone);

                            if (smsSend.Contains("ID"))
                            {
                                Logs.UserLog(user.intCollectionUserID, "forgot", "Şifremi unuttum sms gönderildi", strToken);

                                Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifre oluşturma linki telefon numaranıza gönderildi.");

                                return Redirect("/" + QueryStrings.t);
                            }
                            else
                            {
                                Logs.UserLog(user.intCollectionUserID, "forgot", "Telefon numarası hatalı", strPhone);

                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Sms gönderilemedi! Telefon numaranızı kontrol ediniz!");
                            }
                        }
                    }
                    else
                    {
                        if (sendtype == "email")
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kayıtlı e-posta adresi bulunamadı!");
                        }
                        else if (sendtype == "sms")
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kayıtlı telefon numarası bulunamadı!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");

                Logs.Exception(ex, "User Forgot Post");
            }

            return View();
        }
        #endregion

        #endregion

        #region Change

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Change()
        {
            try
            {
                if (Request.QueryString["getPublicKey"] == null)
                {
                    string strT = RouteData.Values["t"].ToString();
                    string strGetToken = RouteData.Values["token"].ToString();
                    string strToken = string.IsNullOrEmpty(strGetToken) ? Sessions.Token : strGetToken;

                    if (strToken == "")
                    {
                        Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                        return Redirect("/" + strT);
                    }
                    else if (!string.IsNullOrEmpty(strGetToken))
                    {
                        using (DBEntities db = new DBEntities())
                        {
                            var userLog = db.tblCollectionUserLogs.Where(w => w.strToken == strGetToken).FirstOrDefault();
                            if (userLog != null)
                            {
                                Sessions.Token = strGetToken;

                                userLog.strToken = "";
                                db.SaveChanges();
                            }
                            else
                            {
                                Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                                return Redirect("/" + strT);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "User Change Get");
            }

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Change(String tckn, String password, String passwordagain)
        {
            if (string.IsNullOrEmpty(tckn) && string.IsNullOrEmpty(password) && string.IsNullOrEmpty(passwordagain))
            {
                return View();
            }

            try
            {
                #region Validate
                if (tckn.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "T.C. kimlik numaranızı giriniz!");
                    return View();
                }

                if (password.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifrenizi giriniz!");
                    return View();
                }

                if (passwordagain.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifre tekrarını giriniz!");
                    return View();
                }

                if (password.Trim() != passwordagain.Trim())
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifreniz doğrulanmadı!");
                    return View();
                }

                if (password.Trim().Length < 6)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifreniz en az 6 karakterden oluşmalıdır!");
                    return View();
                }
                #endregion

                string strT = RouteData.Values["t"].ToString();

                if (!string.IsNullOrEmpty(Sessions.Token))
                {
                    using (DBEntities db = new DBEntities())
                    {
                        string strTCKN = cCrypto.EncryptDES(tckn.Trim());

                        var user = db.tblCollectionUsers.Where(w => w.strTCKN == strTCKN && w.strToken == strT).FirstOrDefault();
                        if (user != null)
                        {
                            user.bolIsActive = true;
                            user.strPss = cCrypto.EncryptDES(password.Trim());

                            db.SaveChanges();

                            Logs.UserLog(user.intCollectionUserID, "change", "Şifre oluşturuldu");

                            Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifreniz onaylandı. Giriş yapabilirsiniz.");
                            return Redirect("/" + strT);
                        }
                        else
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Kayıtlı T.C. kimlik numarası bulunamadı! ");
                        }
                    }
                }
                else
                {
                    Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                    return Redirect("/" + strT);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");

                Logs.Exception(ex, "User Change Post");
            }

            return View();
        }
        #endregion

        #endregion

        #region SignUp

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult SignUp()
        {
            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult SignUp(String tckn, String sendtype)
        {
            if (string.IsNullOrEmpty(tckn))
            {
                return View();
            }

            ViewBag.tckn = tckn;
            ViewBag.email = Request.Form["email"];
            ViewBag.phone = Request.Form["phone"];
            ViewBag.sendtype = sendtype;

            try
            {
                string strT = QueryStrings.t;
                string strEmail = Request.Form["email"].Trim();
                string strPhone = Request.Form["phone"].Trim();

                if (tckn.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "T.C. kimlik numaranızı giriniz!");
                    return View();
                }

                //if (!string.IsNullOrEmpty(strT))
                //{
                //    if (string.IsNullOrEmpty(strEmail))
                //    {
                //        ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta adresinizi giriniz!");
                //        return View();
                //    }

                //    if (string.IsNullOrEmpty(strPhone))
                //    {
                //        ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Telefon numaranızı giriniz!");
                //        return View();
                //    }
                //}
                //else
                //{
                    if (sendtype == "sms")
                    {
                        if (string.IsNullOrEmpty(strPhone))
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Telefon numaranızı giriniz!");
                            return View();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(strEmail))
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta adresinizi giriniz!");
                            return View();
                        }
                    }
                //}

                using (DBEntities db = new DBEntities())
                {
                    string strTc = cCrypto.EncryptDES(tckn);

                    if (!string.IsNullOrEmpty(strT))
                    {
                        var user = db.tblCollectionUsers.Where(w => w.bolAcente && w.strTCKN == strTc && w.strToken == strT).FirstOrDefault();
                        if (user != null)
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir üyeliğiniz mevcut! Şifrenizi hatırlamıyorsanız şifremi unuttum bölümünden alabilirsiniz.");
                        }
                        else
                        {
                            var sendLog = db.tblCollectionSendLogs.Where(w => w.strTCKN == strTc && w.strToken == strT).FirstOrDefault();
                            if (sendLog == null)
                            {
                                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı TC kimlik numarası girdiniz!'");
                            }
                            else
                            {
                                string strToken = Utilities.RandomString(10);

                                if (sendtype == "email")
                                {
                                    if (Email.SendEmail(ConfigurationManager.AppSettings["SignUpTitle"], "Merhaba, <p><a href=\"" + ConfigurationManager.AppSettings["SitePath"] + "/user/new/" + strT + "/" + strToken + "\" target=\"_blank\">Şifre oluşturmak için tıklayınız</a></p>", strEmail))
                                    {
                                        SignUpData signUpData = new SignUpData
                                        {
                                            Email = strEmail,
                                            Phone = strPhone,
                                            TCKN = cCrypto.EncryptDES(tckn)
                                        };

                                        Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "Şifre al mail gönderildi", strToken, Utilities.JsonSerialize(signUpData));

                                        Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifre oluşturma linki E-posta adresinize gönderildi.");

                                        return Redirect("/" + strT);
                                    }
                                    else
                                    {
                                        Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "E-posta adresi hatalı", strEmail);

                                        ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta gönderilemedi! E-posta adresinizi kontrol ediniz!");
                                    }
                                }
                                else if (sendtype == "sms")
                                {
                                    Services service = new Services();
                                    decimal pidID = service.GetKisiPidbilgi(tckn, "", strPhone);

                                    var kisiBilgi = service.GetKisiPidbilgi(pidID);

                                    TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                                    {
                                        ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                                        Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                                        PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                                        Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                                        UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                                    });

                                    string smsSend = sms.Send("Sayın " + (kisiBilgi != null ? kisiBilgi.kimlikad + " " + kisiBilgi.kimliksoyad : "") + ", Tahsilat sayfası girişinde kullanılmak üzere şifre oluşturmak için " + ConfigurationManager.AppSettings["SitePath"] + "/user/new/" + strT + "/" + strToken + " tıklayınız.", strPhone);

                                    if (smsSend.Contains("ID"))
                                    {
                                        SignUpData signUpData = new SignUpData
                                        {
                                            Email = strEmail,
                                            Phone = strPhone,
                                            TCKN = cCrypto.EncryptDES(tckn)
                                        };

                                        Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "Şifre al sms gönderildi", strToken, Utilities.JsonSerialize(signUpData));

                                        Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifre oluşturma linki telefon numaranıza gönderildi.");

                                        return Redirect("/" + strT);
                                    }
                                    else
                                    {
                                        Logs.UserLog(user != null ? user.intCollectionUserID : 0, "forgot", "Telefon numarası hatalı", strPhone);

                                        ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Sms gönderilemedi! Telefon numaranızı kontrol ediniz!");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Services services = new Services();
                        decimal pid = services.GetKisiPidbilgi(tckn, strEmail, strPhone);

                        if (pid == 0)
                        {
                            ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bilgileriniz doğrulanamadı!'");
                        }
                        else
                        {
                            var user = db.tblCollectionUsers.Where(w => !w.bolAcente && w.strTCKN == strTc).FirstOrDefault();

                            string strPss = new Random().Next(100000, 999999).ToString();

                            var userPid = services.GetKisiPidbilgi(pid);

                            if (sendtype == "email")
                            {
                                if (Email.SendEmail(ConfigurationManager.AppSettings["SignUpTitle"], "Sayın " + (userPid != null ? userPid.kimlikad + " " + userPid.kimliksoyad : "") + ", <p>Fiba Emeklilik Tahsilat Ekranı Şifreniz : " + strPss + "</p><p>İyi günler dileriz.</p>", strEmail))
                                {
                                    SignUpData signUpData = new SignUpData
                                    {
                                        Email = strEmail,
                                        Phone = strPhone,
                                        TCKN = cCrypto.EncryptDES(tckn)
                                    };

                                    Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "Şifre al mail gönderildi", "", Utilities.JsonSerialize(signUpData));

                                    Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifreniz E-posta adresinize gönderildi.");

                                    UserInsertUpdate(db, user, tckn, strEmail, strPhone, strPss);

                                    return Redirect("/");
                                }
                                else
                                {
                                    Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "E-posta adresi hatalı", strEmail);

                                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "E-posta gönderilemedi! E-posta adresinizi kontrol ediniz!");
                                }
                            }
                            else if (sendtype == "sms")
                            {
                                TuratelSms.Service sms = new TuratelSms.Service(new TuratelSms.Data
                                {
                                    ChannelCode = ConfigurationManager.AppSettings["smsChannelCode"].ToString(),
                                    Originator = ConfigurationManager.AppSettings["smsOriginator"].ToString(),
                                    PassWord = ConfigurationManager.AppSettings["smsPassword"].ToString(),
                                    Path = ConfigurationManager.AppSettings["TuratelSMSService"].ToString(),
                                    UserName = ConfigurationManager.AppSettings["smsUserName"].ToString()
                                });

                                string smsSend = sms.Send("Sayın " + (userPid != null ? userPid.kimlikad + " " + userPid.kimliksoyad : "") + ", Fiba Emeklilik Tahsilat Sayfası Şifreniz : " + strPss + " İyi günler dileriz.", strPhone);

                                if (smsSend.Contains("ID"))
                                {
                                    SignUpData signUpData = new SignUpData
                                    {
                                        Email = strEmail,
                                        Phone = strPhone,
                                        TCKN = cCrypto.EncryptDES(tckn)
                                    };

                                    Logs.UserLog(user != null ? user.intCollectionUserID : 0, "signup", "Şifre al sms gönderildi", "", Utilities.JsonSerialize(signUpData));

                                    Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Şifreniz telefon numaranıza gönderildi.");

                                    UserInsertUpdate(db, user, tckn, strEmail, strPhone, strPss);

                                    return Redirect("/");
                                }
                                else
                                {
                                    Logs.UserLog(user != null ? user.intCollectionUserID : 0, "forgot", "Telefon numarası hatalı", strPhone);

                                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Sms gönderilemedi! Telefon numaranızı kontrol ediniz!");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");

                Logs.Exception(ex, "User SignUp Post");
            }

            return View();
        }

        private void UserInsertUpdate(DBEntities db, tblCollectionUsers user, string strTc, string strEmail, string strPhone, string strPss)
        {
            if (user == null)
            {
                user = new tblCollectionUsers
                {
                    bolAcente = false,
                    bolIsActive = true,
                    dtRegisterDate = DateTime.Now,
                    strEmail = strEmail,
                    strPhone = strPhone,
                    strPss = cCrypto.EncryptDES(strPss),
                    strTCKN = cCrypto.EncryptDES(strTc)
                };
                db.tblCollectionUsers.Add(user);
            }
            else
            {
                user.strPss = cCrypto.EncryptDES(strPss);
                user.strPhone = strPhone;
                user.strEmail = strEmail;
                user.dtRegisterDate = DateTime.Now;
            }
            db.SaveChanges();
        }
        #endregion

        #endregion

        #region New

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult New()
        {
            try
            {
                if (Request.QueryString["getPublicKey"] == null)
                {
                    string strT = RouteData.Values["t"].ToString();
                    string strGetToken = RouteData.Values["token"].ToString();
                    string strToken = string.IsNullOrEmpty(strGetToken) ? Sessions.Token : strGetToken;

                    if (string.IsNullOrEmpty(strToken))
                    {
                        Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                        return Redirect("/" + strT);
                    }
                    else if (!string.IsNullOrEmpty(strGetToken))
                    {
                        using (DBEntities db = new DBEntities())
                        {
                            var userLog = db.tblCollectionUserLogs.Where(w => w.strToken == strGetToken).FirstOrDefault();
                            if (userLog != null)
                            {
                                userLog.strToken = "";
                                db.SaveChanges();

                                Sessions.CollectionUserLogID = userLog.intCollectionUserLogID;
                            }
                            else
                            {
                                Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                                return Redirect("/" + strT);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "User New Get");
                Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
            }

            return View();
        }
        #endregion

        #region Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult New(String password, String passwordagain)
        {
            if (string.IsNullOrEmpty(password) && string.IsNullOrEmpty(passwordagain))
            {
                return View();
            }

            try
            {
                #region Validate
                if (password.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifrenizi giriniz!");
                    return View();
                }

                if (passwordagain.Trim().Length == 0)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifre tekrarını giriniz!");
                    return View();
                }

                if (password.Trim() != passwordagain.Trim())
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifreniz doğrulanmadı!");
                    return View();
                }

                if (password.Trim().Length < 6)
                {
                    ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Şifreniz en az 6 karakterden oluşmalıdır!");
                    return View();
                }
                #endregion

                string strT = RouteData.Values["t"].ToString();
                string strToken = RouteData.Values["token"].ToString();

                if (Sessions.CollectionUserLogID > 0)
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var userLog = db.tblCollectionUserLogs.Where(w => w.intCollectionUserLogID == Sessions.CollectionUserLogID).First();

                        SignUpData signUpData = Utilities.JsonDeserialize<SignUpData>(userLog.strData);

                        var user = db.tblCollectionUsers.Where(w => w.strTCKN == signUpData.TCKN && w.strToken == strToken).FirstOrDefault();
                        if (user == null)
                        {
                            user = new tblCollectionUsers
                            {
                                bolIsActive = true,
                                dtRegisterDate = DateTime.Now,
                                strEmail = signUpData.Email,
                                strPss = cCrypto.EncryptDES(password),
                                strTCKN = signUpData.TCKN,
                                strToken = strT,
                                strPhone = signUpData.Phone,
                                bolAcente = true
                            };

                            db.tblCollectionUsers.Add(user);
                            db.SaveChanges();

                            Logs.UserLog(user.intCollectionUserID, "new", "Şifre alındı");

                            Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Success, "Üyeliğiniz oluşturuldu. Giriş yapabilirsiniz.");
                        }
                        else
                        {
                            Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir üyeliğiniz mevcut! Şifrenizi hatırlamıyorsanız şifremi unuttum bölümünden alabilirsiniz.");
                        }

                        return Redirect("/" + strT);
                    }
                }
                else
                {
                    Sessions.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Hatalı giriş yaptınız!");
                    return Redirect("/" + strT);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = Utilities.MsgAlert(Utilities.MsgType.Warning, "Bir hata oluştu! Tekrar deneyiniz.");

                Logs.Exception(ex, "User New Post");
            }

            return View();
        }
        #endregion

        #endregion

        #region UserControl
        public ActionResult UserControl()
        {
            if (Sessions.CurrentUser == null)
                return Json("", JsonRequestBehavior.AllowGet);

            return Json("1", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BrCntrl
        public string BrCntrl()
        {
            Sessions.CurrentUser = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            return "";
        }
        #endregion
    }
}
