﻿using ModelLibrary;
using System;

namespace CollectionPay.Classes
{
    public class Logs
    {
        #region UserLog
        public static void UserLog(int intCollectionUserID, string strType, string strMsg, string strToken = "", string strData = "")
        {
            using (DBEntities db = new DBEntities())
            {
                db.tblCollectionUserLogs.Add(new tblCollectionUserLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intCollectionUserID = intCollectionUserID,
                    strIP = Utilities.ClientIP,
                    strMsg = strMsg,
                    strType = strType,
                    strData = strData,
                    strToken = strToken
                });
                db.SaveChanges();
            }
        }
        #endregion

        #region Exception
        public static void Exception(Exception ex, string strPage)
        {
            using (DBEntities db = new DBEntities())
            {
                try
                {
                    db.tblCollectionExceptions.Add(new tblCollectionExceptions
                    {
                        dtRegisterDate = DateTime.Now,
                        strException = ex.ToString(),
                        strPage = strPage,
                        intCollectionUserID = Sessions.CurrentUser != null ? Sessions.CurrentUser.intCollectionUserID : 0
                    });
                    db.SaveChanges();
                }
                catch (Exception ex2)
                {
                    db.tblExceptions.Add(new tblExceptions
                    {
                        dtRegisterDate = DateTime.Now,
                        strException = ex2.ToString(),
                        intPageID = 0
                    });
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region PayLog
        public static void PayLog(int intPolID, decimal flPrice, string strExpiryDate, string strSecure3dHash, string strToken, string strOrderID, bool bolPayUpdate)
        {
            using (DBEntities db = new DBEntities())
            {
                db.tblCollectionPayLogs.Add(new tblCollectionPayLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intCollectionUserID = Sessions.CurrentUser.intCollectionUserID,
                    flPrice = flPrice,
                    intPolID = intPolID,
                    strSecure3dHash = strSecure3dHash,
                    bolApproved = false,
                    strTransID = "",
                    strToken = strToken,
                    strOrderID = strOrderID,
                    strExpiryDate = strExpiryDate,
                    bolPayUpdate = bolPayUpdate
                });
                db.SaveChanges();
            }
        }
        #endregion

        #region WSLog
        public static int WSLog(int intWSLogID, int intUserID, int intSaleLogID, string strSaleType, string strClassName, string strMethodType, string strJson, string strMethod)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblWSLogs wslog = new tblWSLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intReleationWSLogID = intWSLogID,
                    intSaleLogID = intSaleLogID,
                    intStep = 0,
                    intUserID = intUserID,
                    strClassName = strClassName,
                    strJson = strJson,
                    strMethod = strMethod,
                    strMethodType = strMethodType,
                    strProject = "tahsilat",
                    strSaleType = strSaleType
                };

                db.tblWSLogs.Add(wslog);
                db.SaveChanges();

                intWSLogID = wslog.intWSLogID;
            }
            catch { }

            db.Dispose();

            return intWSLogID;
        }
        #endregion
    }
}