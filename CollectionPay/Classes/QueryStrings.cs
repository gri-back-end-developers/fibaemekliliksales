using System.Web;

namespace CollectionPay.Classes
{
    public class QueryStrings
    {
        #region token
        public static string token
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["token"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region t
        public static string t
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["t"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion
    }
}