﻿#region Directives
using System;
using System.IO;
using System.Text;
using System.Web;
#endregion

namespace CollectionPay.Classes
{
    public class Utilities
    {
        #region AppPath
        private static string _strAppPath = "";
        public static string AppPath
        {
            get
            {
                _strAppPath = System.Configuration.ConfigurationManager.AppSettings["siteUrl"].ToString();
                return _strAppPath;
            }
        }
        #endregion

        #region AppPathServer
        private static string _strAppPathServer = "";
        public static string AppPathServer
        {
            get
            {
                _strAppPathServer = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath);
                return _strAppPathServer;
            }
        }
        #endregion

        #region NullFixLong
        public static long NullFixLong(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return long.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixInt
        public static int NullFixInt(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return int.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixByte
        public static byte NullFixByte(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return byte.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixShort
        public static short NullFixShort(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return short.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixDate
        public static DateTime NullFixDate(string Value)
        {
            if (Value == "" || Value == null)
            {
                return DateTime.Now;
            }
            try
            {
                return DateTime.Parse(Value);
            }
            catch
            {
                return DateTime.Now;
            }
        }
        #endregion

        #region NullFixString
        public static string NullFixString(object Value)
        {
            if (Value == null)
            {
                Value = "";
            }
            try
            {
                return Value.ToString();
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region NullFixDouble
        public static double NullFixDouble(string Value)
        {
            if (Value == "" || Value == null)
            {
                Value = "0";
            }
            try
            {
                return double.Parse(Value);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region NullFixBool
        public static bool NullFixBool(string Value)
        {
            if (Value == null || Value == "0")
            {
                return false;
            }
            else if (Value == "1")
            {
                return true;
            }
            try
            {
                return bool.Parse(Value);
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region NullFixFloat
        public static float NullFixFloat(string strValue)
        {
            try
            {
                return float.Parse(strValue.ToString());
            }

            catch
            {
                return 0.0F;
            }
        }
        #endregion

        #region NullFixDecimal
        public static decimal NullFixDecimal(string strValue)
        {
            try
            {
                return decimal.Parse(strValue.ToString());
            }

            catch
            {
                return 0;
            }
        }
        #endregion

        #region MonthName
        public static string MonthName(int intMonth)
        {
            switch (intMonth)
            {
                case 1:
                    return "Ocak";
                case 2:
                    return "Şubat";
                case 3:
                    return "Mart";
                case 4:
                    return "Nisan";
                case 5:
                    return "Mayıs";
                case 6:
                    return "Haziran";
                case 7:
                    return "Temmuz";
                case 8:
                    return "Ağustos";
                case 9:
                    return "Eylül";
                case 10:
                    return "Ekim";
                case 11:
                    return "Kasım";
                case 12:
                    return "Aralık";
            }
            return intMonth.ToString();
        }
        #endregion

        #region AsciiControl
        public static bool AsciiControl(string strValue)
        {
            foreach (char charCode in strValue)
            {
                if ((int)charCode > 127)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region PageName
        public static string PageName
        {
            get
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(HttpContext.Current.Request.Url.AbsolutePath);
                return fileInfo.Name;
            }
        }
        #endregion

        #region Url
        public static string Url
        {
            get
            {
                return HttpContext.Current.Request.Url.AbsolutePath;
            }
        }
        #endregion

        #region Encrypt&Decrypt
        public string Encrypt(string strValue)
        {
            byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(strValue);
            return System.Convert.ToBase64String(byteData);
        }

        public string Decrypt(string strValue)
        {
            byte[] byteData = System.Convert.FromBase64String(strValue);
            return System.Text.ASCIIEncoding.UTF8.GetString(byteData);
        }

        //public string Encrypt(string strValue)
        //{
        //    byte[] keyArray;
        //    byte[] toEncryptArray = System.Text.ASCIIEncoding.UTF8.GetBytes(strValue);

        //    string key = "furkan";
        //    bool useHashing = true;
        //    if (useHashing)
        //    {
        //        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
        //        keyArray = hashmd5.ComputeHash(System.Text.ASCIIEncoding.UTF8.GetBytes(key));

        //        hashmd5.Clear();
        //    }
        //    else
        //    {
        //        keyArray = UTF8Encoding.ASCII.GetBytes(key);
        //    }

        //    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //    tdes.Key = keyArray;
        //    tdes.Mode = CipherMode.ECB;
        //    tdes.Padding = PaddingMode.PKCS7;

        //    ICryptoTransform cTransform = tdes.CreateEncryptor();
        //    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
        //    tdes.Clear();

        //    return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        //}

        //public string Decrypt(string strValue)
        //{
        //    byte[] keyArray;
        //    byte[] toEncryptArray = Convert.FromBase64String(strValue);

        //    string key = "furkan";
        //    bool useHashing = true;
        //    if (useHashing)
        //    {
        //        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
        //        keyArray = hashmd5.ComputeHash(System.Text.ASCIIEncoding.UTF8.GetBytes(key));

        //        hashmd5.Clear();
        //    }
        //    else
        //    {
        //        keyArray = UTF8Encoding.UTF8.GetBytes(key);
        //    }

        //    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //    tdes.Key = keyArray;
        //    tdes.Mode = CipherMode.ECB;
        //    tdes.Padding = PaddingMode.PKCS7;

        //    ICryptoTransform cTransform = tdes.CreateDecryptor();
        //    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
        //    tdes.Clear();

        //    return System.Text.ASCIIEncoding.UTF8.GetString(resultArray);
        //}
        #endregion

        #region ConvertTurkish
        public static string ConvertTurkish(string input)
        {
            input = input.Replace("ç", "c");
            input = input.Replace("Ç", "C");
            input = input.Replace("İ", "I");
            input = input.Replace("ı", "i");
            input = input.Replace("Ö", "O");
            input = input.Replace("ö", "o");
            input = input.Replace("Ü", "U");
            input = input.Replace("ü", "u");
            input = input.Replace("Ğ", "G");
            input = input.Replace("ğ", "g");
            input = input.Replace("Ş", "S");
            input = input.Replace("ş", "s");

            return input;
        }
        #endregion

        #region IsEmail
        public static bool IsEmail(string strEmail)
        {
            string MatchEmailPattern =
                @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            if (strEmail != null)
            {
                return System.Text.RegularExpressions.Regex.IsMatch(strEmail, MatchEmailPattern);
            }
            return false;
        }
        #endregion

        #region XMLConverter
        public static string XMLConverter(string strVariant)
        {
            strVariant = strVariant.Replace("&", "&amp;");
            strVariant = strVariant.Replace("\"", "&quot;");
            strVariant = strVariant.Replace("'", "&apos;");
            strVariant = strVariant.Replace("<", "&lt;");
            strVariant = strVariant.Replace(">", "&gt;");

            return strVariant;
        }
        #endregion

        #region Msg
        public static HtmlString MsgAlert(MsgType msgType, string strMsg, bool bolFormModal = true)
        {
            return new HtmlString(MsgString(msgType, strMsg, bolFormModal) + "<script type=\"text/javascript\">$(function () { OpenMessage('#popupMsg'); });</script>");
        }

        public static HtmlString MsgHtml(MsgType msgType, string strMsg, bool bolFormModal = true)
        {
            return MsgString(msgType, strMsg, bolFormModal);
        }

        static HtmlString MsgString(MsgType msgType, string strMsg, bool bolFormModal)
        {
            string strHtml = "<div id=\"popupMsg\" class=\"perfect-form " + (bolFormModal ? "perfect-form-modal" : "") +"\">";
            strHtml += "<a href=\"javascript:void(0);\" class=\"modal-closer\" style=\"position:absolute; right:10px; top:10px; text-decoration:none; font-size:16px;\">X</a>";
            strHtml += "<div id=\"modal-page\">";
            strHtml += "<img src=\"/img/{0}\" alt=\"{1}\">";
            strHtml += "<p>" + strMsg + "</p></div></div>";

            switch (msgType)
            {
                case MsgType.Success:
                case MsgType.Info:
                    strHtml = string.Format(strHtml, "basarili.png", "Başarılı");
                    break;
                case MsgType.Warning:
                case MsgType.Error:
                    strHtml = string.Format(strHtml, "hata.png", "Başarısız");
                    break;
            }

            return new HtmlString(strHtml);
        }

        public enum MsgType
        {
            Success = 1,
            Info = 2,
            Warning = 3,
            Error = 4
        }
        #endregion

        #region RandomString
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
        #endregion

        #region GetFileText
        public static string GetFileText(string strFile)
        {
            string strText = "";
            using (StreamReader stDr = new StreamReader(strFile))
            {
                strText = stDr.ReadToEnd();
            }

            return strText;
        }
        #endregion

        #region Price
        public static string Price(decimal flPrice)
        {
            string strHacim = flPrice.ToString("N");
            return strHacim.Split(',')[0].Replace(",", ".") + "," + strHacim.Split(',')[1];
        }
        #endregion

        #region ClientIP
        public static string ClientIP
        {
            get
            {
                string strIP = "";
                try
                {
                    strIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Trim();
                }
                catch { }

                if (strIP == "")
                {
                    strIP = HttpContext.Current.Request.UserHostAddress;
                }

                return strIP;
            }
        }
        #endregion

        #region Percentage
        public static decimal Percentage(decimal intPerNumber, int intTotalNumber)
        {
            try
            {
                if (intTotalNumber != 0)
                {
                    return (decimal)(intPerNumber * 100) / (intTotalNumber);
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region Percentage
        public static string DataParse(string strDate)
        {
            try
            {
                return strDate.Substring(0, 2) + "." + strDate.Substring(2, 2) + "." + strDate.Substring(4, 4);
            }
            catch
            {
                return strDate;
            }
        }
        #endregion

        #region Friendly
        public static string Friendly(string srtName)
        {
            srtName = (srtName ?? "").ToLower();
            srtName = srtName.Replace("ı", "i");
            srtName = srtName.Replace("ü", "u");
            srtName = srtName.Replace("ş", "s");
            srtName = srtName.Replace("ö", "o");
            srtName = srtName.Replace("ç", "c");
            srtName = srtName.Replace("ğ", "g");

            srtName = System.Text.RegularExpressions.Regex.Replace(srtName, @"\&+", "and");
            srtName = srtName.Replace("'", "");

            srtName = System.Text.RegularExpressions.Regex.Replace(srtName, @"[^a-z0-9]", "-");
            srtName = System.Text.RegularExpressions.Regex.Replace(srtName, @"-+", "-");
            srtName = srtName.Trim('-');

            return srtName;
        }
        #endregion

        #region IsHaveQuery
        public static bool IsHaveQuery(string strUrl)
        {
            var isHaveQuery = HttpUtility.ParseQueryString(strUrl);
            for (int i = 0; i < isHaveQuery.Count; i++)
            {
                if (isHaveQuery.Keys[i] != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region SearchClear
        public static string SearchClear(string strName)
        {
            strName = System.Text.RegularExpressions.Regex.Replace(strName, @"<[^>]*>", String.Empty);
            strName = HttpUtility.HtmlDecode(strName);
            strName = (strName ?? "").ToLower();
            strName = strName.Replace("ı", "i");
            strName = strName.Replace("ü", "u");
            strName = strName.Replace("ş", "s");
            strName = strName.Replace("ö", "o");
            strName = strName.Replace("ç", "c");
            strName = strName.Replace("ğ", "g");
            strName = strName.Replace("à", "a");
            strName = strName.Replace("á", "a");
            strName = strName.Replace("â", "a");
            strName = strName.Replace("ã", "a");
            strName = strName.Replace("ä", "a");
            strName = strName.Replace("å", "a");
            strName = strName.Replace("é", "e");
            strName = strName.Replace("è", "e");
            strName = strName.Replace("ê", "e");
            strName = strName.Replace("ë", "e");
            strName = strName.Replace("ì", "i");
            strName = strName.Replace("í", "i");
            strName = strName.Replace("î", "i");
            strName = strName.Replace("ï", "i");
            strName = strName.Replace("ð", "o");
            strName = strName.Replace("ñ", "n");
            strName = strName.Replace("ò", "o");
            strName = strName.Replace("ó", "o");
            strName = strName.Replace("ô", "o");
            strName = strName.Replace("õ", "o");
            strName = strName.Replace("ö", "o");
            strName = strName.Replace("ù", "u");
            strName = strName.Replace("ú", "u");
            strName = strName.Replace("û", "u");
            strName = strName.Replace("ý", "y");
            strName = strName.Replace("ÿ", "y");
            strName = strName.Replace("š", "s");

            strName = System.Text.RegularExpressions.Regex.Replace(strName, @"[^a-z0-9]", " ");

            do
            {
                strName = strName.Replace(" ", "");
            } while (strName.Contains(" "));

            strName = strName.Trim();

            return strName;
        }
        #endregion

        #region JsonSerialize
        public static string JsonSerialize(object objData)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            return jss.Serialize(objData);
        }
        #endregion

        #region JsonDeserialize
        public static T JsonDeserialize<T>(string strData)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            return jss.Deserialize<T>(strData);
        }
        #endregion
    }
}