﻿using CollectionPay.Classes.Data;
using CollectionPay.Classes.Data.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CollectionPay.Classes
{
    public class Services
    {
        #region GetKisiPidbilgi
        public decimal GetKisiPidbilgi(string strTCKN, string strEmail, string strPhone)
        {
            try
            {
                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, 0, 0, "", "Services", "Input", java.Serialize(new { strTCKN = strTCKN, strEmail = strEmail, strPhone  = strPhone }), "GetKisiPidbilgi");

                IntBesWS.NTBESClient ntBes = new IntBesWS.NTBESClient();
                decimal? kisi = ntBes.getkisipidbilgi(strTCKN, strEmail, strPhone);

                Logs.WSLog(intWSLogID, 0, 0, "", "Services", "Output", kisi.HasValue ? kisi.Value.ToString() : "", "GetKisiPidbilgi");

                if (kisi.HasValue)
                {
                    return kisi.Value;
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "IntBesWS.GetKisiPidbilgi");
            }

            return 0;
        }
        #endregion

        #region CollectionList
        public List<CollectionInfo> CollectionList(decimal polID)
        {
            List<CollectionInfo> collectionInfo = new List<CollectionInfo>();
            try
            {
                using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Input", polID.ToString(), "CollectionList");
                    
                    var tahsilatbilgirec = fibaws.gettahsilatbilgirec(polID);

                    Logs.WSLog(intWSLogID, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Output", java.Serialize(tahsilatbilgirec), "CollectionList");

                    if (tahsilatbilgirec != null)
                    {
                        if (tahsilatbilgirec.tahsilatlist != null && tahsilatbilgirec.tahsilatlist.Count > 0)
                        {
                            string strGruptip = "";

                            IntBesWS.NTBESClient ntBes = new IntBesWS.NTBESClient();
                            IntBesWS.IntbesSozbilgilisttyperecUser bilgi = ntBes.getpolicebilgirec(Sessions.CurrentUser.intPidID);
                            if (bilgi != null && bilgi.ASoz != null && bilgi.ASoz.Count > 0)
                            {
                                var asoz = bilgi.ASoz.Where(w => w.polid == polID).FirstOrDefault();
                                if (asoz != null)
                                {
                                    if (asoz.sozlesmedetay != null && asoz.sozlesmedetay.Count > 0)
                                    {
                                        strGruptip = asoz.sozlesmedetay[0].gruptip;
                                    }
                                }
                            }

                            foreach (var item in tahsilatbilgirec.tahsilatlist)
                            {
                                DateTime vadeTar = Convert.ToDateTime(item.vadetar);
                                if (vadeTar <= Convert.ToDateTime(DateTime.Now.ToShortDateString()) && item.tahs == 0 && item.vadetutar > 0)
                                {
                                    bool bolAdd = true;
                                    if (strGruptip == "GBB")
                                    {
                                        bolAdd = !(vadeTar.Year == DateTime.Now.Year && vadeTar.Month == DateTime.Now.Month);
                                    }

                                    if (bolAdd)
                                    {
                                        string strCollection = (item.polid.HasValue ? item.polid.Value.ToString() : "0") + " - " + item.vadetar + " - " + (item.vadetutar.HasValue ? item.vadetutar.Value.ToString() + " TL" : "");

                                        collectionInfo.Add(new CollectionInfo
                                        {
                                            intPolID = item.polid.HasValue ? (int)item.polid.Value : 0,
                                            strID = (strCollection + (item.vadetutar.HasValue ? item.vadetutar.Value.ToString() : "")).Replace(" ", "").Replace("-", "").Replace(",", "").Replace(".", "").Replace("/", ""),
                                            strCollection = strCollection,
                                            strPrice = Utilities.Price(item.vadetutar.HasValue ? item.vadetutar.Value : 0),
                                            strVadeTar = vadeTar.ToString("ddMMyyyy"),
                                            flPrice = item.vadetutar.HasValue ? item.vadetutar.Value : 0
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "CollectionList");
            }

            return collectionInfo;
        }
        #endregion

        #region GetPoliceBilgiRec
        public List<decimal> GetPoliceBilgiRec(decimal pid)
        {
            List<decimal> policy = new List<decimal>();
            try
            {
                if (pid > 0)
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Input", pid.ToString(), "GetPoliceBilgiRec");

                    IntBesWS.NTBESClient ntBes = new IntBesWS.NTBESClient();
                    IntBesWS.IntbesSozbilgilisttyperecUser bilgi = ntBes.getpolicebilgirec(pid);

                    Logs.WSLog(intWSLogID, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Output", java.Serialize(bilgi), "GetPoliceBilgiRec");

                    if (bilgi != null && bilgi.ASoz != null && bilgi.ASoz.Count > 0)
                    {
                        for (int i = 0; i < bilgi.ASoz.Count; i++)
                        {
                            var sozlesmeDetay = bilgi.ASoz[i].sozlesmedetay;
                            if (sozlesmeDetay != null && sozlesmeDetay.Count > 0)
                            {
                                if (sozlesmeDetay[0].gruptip != "İGES" && !sozlesmeDetay[0].uzunad.Contains("OTOMATİK KATILIM PLANI"))
                                {
                                    policy.Add(Utilities.NullFixDecimal(bilgi.ASoz[i].policeno));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "GetKisiPidbilgi");
            }

            return policy;
        }
        #endregion

        #region GetKisiPidbilgi
        public FIBAWS.FibawsKisibilgirecUser GetKisiPidbilgi(decimal intPid)
        {
            try
            {
                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser != null ? Sessions.CurrentUser.intCollectionUserID: 0, 0, "", "Services", "Input", intPid.ToString(), "GetKisiPidbilgi");
                
                FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient();

                FIBAWS.FibawsKisibilgirecUser data = fibaws.getkisibilgirec(intPid);

                Logs.WSLog(intWSLogID, Sessions.CurrentUser != null ? Sessions.CurrentUser.intCollectionUserID : 0, 0, "", "Services", "Output", java.Serialize(data), "GetKisiPidbilgi");

                return data;
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "FIBAWS.GetKisiPidbilgi");
            }

            return null;
        }
        #endregion

        #region GetOdemeAracDeg
        public string GetOdemeAracDeg(decimal polID, Card cardInfo)
        { 
            try
            {
                IntBesWS.IntbesPerkartrecUser intbesPerkartrecUser = new IntBesWS.IntbesPerkartrecUser
                {
                    adi = cardInfo.name,
                    cvv = Utilities.NullFixDecimal(cardInfo.cvv2),
                    kartno = cardInfo.cardno,
                    sonkultar = cardInfo.month + cardInfo.year,
                    soyadi = cardInfo.surname
                };

                JavaScriptSerializer java = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Input", polID.ToString(), "GetOdemeAracDeg");

                IntBesWS.NTBESClient ntBes = new IntBesWS.NTBESClient();

                string strData = ntBes.getodemearacdeg(polID, "K", intbesPerkartrecUser, null);
                
                Logs.WSLog(intWSLogID, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Output", strData, "GetOdemeAracDeg");
                
                return strData;
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "GetOdemeAracDeg");
            }

            return "hata";
        }
        #endregion

        #region SposTaksitli3d
        public SposTaksitli SposTaksitli3d(FibaBes.EmkblibspostaksitliinputtypUser pInput)
        {
            try
            { 
                using (FibaBes.FibaBesWebServiceClient fibws = new FibaBes.FibaBesWebServiceClient())
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    int intWSLogID = Logs.WSLog(0, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Input", pInput.polid.ToString(), "SposTaksitli3d");

                    FibaBes.EmkblibspostaksitlioutputtypUser spos = fibws.spostaksitli3d(pInput);

                    Logs.WSLog(intWSLogID, Sessions.CurrentUser.intCollectionUserID, 0, "", "Services", "Output", java.Serialize(spos), "SposTaksitli3d");

                    return new SposTaksitli { aciklama = spos.aciklama, issuccess = spos.issuccess };
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SposTaksitli3d");
            }

            return new SposTaksitli { aciklama = "hata" };
        }
        #endregion
    }
}