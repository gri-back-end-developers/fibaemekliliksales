#region Directives
using System;
using System.Net;
using System.Net.Mail;
#endregion

namespace CollectionPay.Classes
{
    public class Email
    {
        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString());
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());

                server.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody, string strTo)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), strTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());
                server.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}