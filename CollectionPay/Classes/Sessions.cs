﻿using CollectionPay.Classes.Data;
using CollectionPay.Classes.Data.Pay;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;

namespace CollectionPay.Classes
{
    public class Sessions
    {
        #region CurrentUser
        public static UserInfo CurrentUser
        {
            get
            {
                try
                {
                    var user = (UserInfo)HttpContext.Current.Session["UsrCrnt"];

                    return user != null ? user : null;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["UsrCrnt"] = value;
            }
        }
        #endregion

        #region Message
        public static HtmlString Message
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session["Message"] != null ? (HtmlString)HttpContext.Current.Session["Message"] : null;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion

        #region Token
        public static string Token
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session["Token"] != null ? HttpContext.Current.Session["Token"].ToString() : "";
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session["Token"] = value;
            }
        }
        #endregion

        #region CollectionUserLogID
        public static int CollectionUserLogID
        {
            get
            {
                try
                {
                    return HttpContext.Current.Session["CollectionUserLogID"] != null ? Utilities.NullFixInt(HttpContext.Current.Session["CollectionUserLogID"].ToString()) : 0;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                HttpContext.Current.Session["CollectionUserLogID"] = value;
            }
        }
        #endregion

        #region CardInfo
        public static Card CardInfo
        {
            get
            {
                if (HttpContext.Current.Session["CardInfo"] == null)
                {
                    return null;
                }
                else
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    return java.Deserialize<Card>(cCrypto.DecryptDES(HttpContext.Current.Session["CardInfo"].ToString()));
                }
            }
            set
            {
                JavaScriptSerializer java = new JavaScriptSerializer();
                HttpContext.Current.Session["CardInfo"] = cCrypto.EncryptDES(java.Serialize(value));
            }
        }
        #endregion

        #region CollectionInfo
        public static List<CollectionInfo> CollectionInfo
        {
            get
            {
                try
                {
                    var collectionInfo = (List<CollectionInfo>)HttpContext.Current.Session["CollectionInfo"];

                    return collectionInfo != null ? collectionInfo : null;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["CollectionInfo"] = value;
            }
        }
        #endregion
    }
}