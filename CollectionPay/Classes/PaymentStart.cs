﻿using CollectionPay.Classes.Data;
using CollectionPay.Classes.Data.Pay;
using ModelLibrary;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CollectionPay.Classes
{
    public class PaymentStart
    {
        #region Start
        public PaymentInfo Start(Card card, int intPolID, decimal price, string strExpiryDate, string productid, bool bolPayUpdate)
        {
            PaymentInfo paymentInfo = new PaymentInfo();
            try
            {
                Garanti garanti = new Garanti();
                garanti.mode = ConfigurationManager.AppSettings["GarantiMode"];
                garanti.apiversion = "v0.01";
                garanti.terminalprovuserid = ConfigurationManager.AppSettings["GarantiTerminalProvUserId"];
                garanti.txntype = "sales";
                garanti.txnamount = (price * 100).ToString().Replace(".", ",").Split(',')[0];
                garanti.txncurrencycode = "949";
                garanti.txninstallmentcount = card.paymentmethod == "0" ? "" : card.paymentmethod;
                garanti.orderid = (DateTime.Now.Day >= 10 ? DateTime.Now.Day.ToString() : "0" + DateTime.Now.Day) + (DateTime.Now.Month >= 10 ? DateTime.Now.Month.ToString() : "0" + DateTime.Now.Month) + DateTime.Now.Year.ToString() + DateTime.Now.Millisecond.ToString() + "_FIBA_" + intPolID + "_" + new Random().Next(10000000, 99999999).ToString();
                garanti.customeripaddress = HttpContext.Current.Request.Url.ToString().Contains("localhost:") ? "85.105.198.251" : Utilities.ClientIP;
                garanti.customeremailaddress = Sessions.CurrentUser.strEmail;
                garanti.orderitemprice1 = price.ToString().Replace(",", ".");
                garanti.orderitemproductid1 = productid;
                garanti.terminalid = ConfigurationManager.AppSettings["GarantiTerminalId"];
                garanti.terminaluserid = Sessions.CurrentUser.intCollectionUserID.ToString();
                garanti.orderitemdescription1 = "Tahsilat Ödeme";

                string _strTerminalID = "0" + garanti.terminalid;
                garanti.terminalmerchantid = ConfigurationManager.AppSettings["GarantiMerchantId"];

                string strStoreKey = ConfigurationManager.AppSettings["GarantiStoreKey"]; //3D Secure şifresi
                string strProvisionPassword = ConfigurationManager.AppSettings["GarantiProvisionPassword"]; //TerminalProvUserID şifresi

                string strToken = Utilities.RandomString(20);
                garanti.successurl = ConfigurationManager.AppSettings["SitePath"] + "/collection/finish?token=" + strToken + (!string.IsNullOrEmpty(QueryStrings.t) ? "&t=" + QueryStrings.t : "");
                garanti.errorurl = ConfigurationManager.AppSettings["SitePath"] + "/collection/finish?token=" + strToken + (!string.IsNullOrEmpty(QueryStrings.t) ? "&t=" + QueryStrings.t : "");

                string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                garanti.secure3dhash = GetSHA1(garanti.terminalid + garanti.orderid + garanti.txnamount + garanti.successurl + garanti.errorurl + garanti.txntype + garanti.txninstallmentcount + strStoreKey + SecurityData).ToUpper();

                paymentInfo.card = card;
                paymentInfo.garanti = garanti;
                paymentInfo.PostPath = ConfigurationManager.AppSettings["GarantiPostPath"];

                Logs.PayLog(intPolID, price, strExpiryDate, garanti.secure3dhash, strToken, garanti.orderid, bolPayUpdate);
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Payment Start Class");
                paymentInfo = null;
            }

            return paymentInfo;
        }
        #endregion

        #region Finish
        public HtmlString Finish()
        {
            string strMDStatusText = "";

            using (DBEntities db = new DBEntities())
            {
                try
                {
                    var payLog = db.tblCollectionPayLogs.Where(w => w.strToken == QueryStrings.token).FirstOrDefault();
                    if (payLog != null)
                    {
                        bool bolApproved = false;

                        if (payLog.strOrderID == HttpContext.Current.Request.Form.Get("oid") && payLog.strSecure3dHash == HttpContext.Current.Request.Form.Get("secure3dhash"))
                        {
                            string strMode = HttpContext.Current.Request.Form.Get("mode");
                            string strApiVersion = HttpContext.Current.Request.Form.Get("apiversion");
                            string strTerminalProvUserID = HttpContext.Current.Request.Form.Get("terminalprovuserid");
                            string strTerminalUserID = HttpContext.Current.Request.Form.Get("terminaluserid");
                            string strTerminalMerchantID = HttpContext.Current.Request.Form.Get("terminalmerchantid");
                            string strType = HttpContext.Current.Request.Form.Get("txntype");
                            string strAmount = HttpContext.Current.Request.Form.Get("txnamount");
                            string strCurrencyCode = HttpContext.Current.Request.Form.Get("txncurrencycode");
                            string strInstallmentCount = HttpContext.Current.Request.Form.Get("txninstallmentcount");
                            string strOrID = HttpContext.Current.Request.Form.Get("orderid");
                            string strSuccessURL = HttpContext.Current.Request.Form.Get("successurl");
                            string strErrorURL = HttpContext.Current.Request.Form.Get("errorurl");
                            string strcustomeremailaddress = HttpContext.Current.Request.Form.Get("customeremailaddress");
                            string strCustomeripaddress = HttpContext.Current.Request.Form.Get("customeripaddress");
                            string strOrderAddressCity1 = HttpContext.Current.Request.Form.Get("orderaddresscity1");
                            string strOrderAddressCountry1 = HttpContext.Current.Request.Form.Get("orderaddresscountry1");
                            string strOrderAddressDistrict1 = HttpContext.Current.Request.Form.Get("orderaddressdistrict1");
                            string strOrderAddressLastname1 = HttpContext.Current.Request.Form.Get("orderaddresslastname1");
                            string strOrderAddressName1 = HttpContext.Current.Request.Form.Get("orderaddressname1");
                            string strOrderAddressPhonenumber1 = HttpContext.Current.Request.Form.Get("orderaddressphonenumber1");
                            string strOrderAddressText1 = HttpContext.Current.Request.Form.Get("orderaddresstext1");
                            string strTerminalID = HttpContext.Current.Request.Form.Get("clientid");
                            string _strTerminalID = "0" + strTerminalID;
                            string strStoreKey = ConfigurationManager.AppSettings["GarantiStoreKey"];
                            string strProvisionPassword = ConfigurationManager.AppSettings["GarantiProvisionPassword"];
                            string strCardholderPresentCode = "13";
                            string strMotoInd = "N";
                            string strNumber = "";
                            string strExpireDate = "";
                            string strCVV2 = "";
                            string strAuthenticationCode = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Form.Get("cavv"));
                            string strSecurityLevel = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Form.Get("eci"));
                            string strTxnID = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Form.Get("xid"));
                            string strMD = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Form.Get("md"));
                            string strMDStatus = HttpContext.Current.Request.Form.Get("mdstatus");
                            strMDStatusText = HttpContext.Current.Request.Form.Get("mderrormessage");
                            string strHostAddress = ConfigurationManager.AppSettings["GarantiHostAdress"];
                            string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                            string ValidateHashData = GetSHA1(strTerminalID + strOrID + strAmount + strSuccessURL + strErrorURL + strType + strInstallmentCount + strStoreKey + SecurityData).ToUpper();
                            string strHashData = HttpContext.Current.Request.Form.Get("secure3dhash");

                            string HashData = GetSHA1(strOrID + strTerminalID + strAmount + SecurityData).ToUpper();
                            string responseFromServer = "";

                            if (strHashData == ValidateHashData)
                            {
                                if (strMDStatus == "1" | strMDStatus == "2" | strMDStatus == "3" | strMDStatus == "4")
                                {
                                    string strTaxNumber = "";
                                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == Sessions.CurrentUser.intCallcenterID).FirstOrDefault();
                                    if (callcenter != null)
                                    {
                                        strTaxNumber = callcenter.strTaxNumber;
                                    }

                                    string strXML = null;
                                    strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                        + "<GVPSRequest>"
                                            + "<Mode>" + strMode + "</Mode>"
                                            + "<Version>" + strApiVersion + "</Version>"
                                            + "<ChannelCode></ChannelCode>"
                                            + "<Terminal>"
                                                + "<ProvUserID>" + strTerminalProvUserID + "</ProvUserID>"
                                                + "<HashData>" + HashData + "</HashData>"
                                                + "<UserID>" + strTerminalUserID + "</UserID>"
                                                + "<ID>" + strTerminalID + "</ID>"
                                                + "<MerchantID>" + strTerminalMerchantID + "</MerchantID>"
                                            + "</Terminal>"
                                            + "<Customer>"
                                                + "<IPAddress>" + strCustomeripaddress + "</IPAddress>"
                                                + "<EmailAddress>" + strcustomeremailaddress + "</EmailAddress>"
                                            + "</Customer>"
                                            + "<Card>"
                                                + "<Number>" + strNumber + "</Number>"
                                                + "<ExpireDate>" + strExpireDate + "</ExpireDate>"
                                                + "<CVV2>" + strCVV2 + "</CVV2>"
                                            + "</Card>"
                                            + "<Order>"
                                                + "<OrderID>" + strOrID + "</OrderID>"
                                                + "<GroupID></GroupID>"
                                            + "</Order>"
                                            + "<Transaction>"
                                                + "<Type>" + strType + "</Type>"
                                                + "<InstallmentCnt>" + strInstallmentCount + "</InstallmentCnt>"
                                                + "<Amount>" + strAmount + "</Amount>"
                                                + "<CurrencyCode>" + strCurrencyCode + "</CurrencyCode>"
                                                + "<CardholderPresentCode>" + strCardholderPresentCode + "</CardholderPresentCode>"
                                                + "<MotoInd>" + strMotoInd + "</MotoInd>"
                                                + "<Secure3D>"
                                                    + "<AuthenticationCode>" + strAuthenticationCode + "</AuthenticationCode>"
                                                    + "<SecurityLevel>" + strSecurityLevel + "</SecurityLevel>"
                                                    + "<TxnID>" + strTxnID + "</TxnID>"
                                                    + "<Md>" + strMD + "</Md>"
                                                + "</Secure3D>"
                                             + "<Verification>"
                                                + "<Identity>" + strTaxNumber + "</Identity>"
                                             + "</Verification>"
                                            + "</Transaction>"
                                        + "</GVPSRequest>";

                                    try
                                    {
                                        string data = "data=" + strXML;

                                        WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                                        _WebRequest.Method = "POST";

                                        byte[] byteArray = Encoding.UTF8.GetBytes(data);
                                        _WebRequest.ContentType = "application/x-www-form-urlencoded";
                                        _WebRequest.ContentLength = byteArray.Length;

                                        Stream dataStream = _WebRequest.GetRequestStream();
                                        dataStream.Write(byteArray, 0, byteArray.Length);
                                        dataStream.Close();

                                        WebResponse _WebResponse = _WebRequest.GetResponse();
                                        dataStream = _WebResponse.GetResponseStream();

                                        string strStatusMessage = ((HttpWebResponse)_WebResponse).StatusDescription;

                                        StreamReader reader = new StreamReader(dataStream);
                                        responseFromServer = reader.ReadToEnd();

                                        if (responseFromServer.Contains("<ReasonCode>00</ReasonCode>"))
                                        {
                                            bolApproved = true;
                                        }
                                        else
                                        {
                                            bolApproved = false;
                                            payLog.strErrMsg = "İşlem Başarısız";

                                            if (string.IsNullOrEmpty(strMDStatusText))
                                            {
                                                strMDStatusText = payLog.strErrMsg;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        bolApproved = false;
                                        payLog.strErrMsg = ex.Message;
                                    }
                                }
                                else
                                {
                                    bolApproved = false;
                                    payLog.strErrMsg = "İşlem başarısız!";

                                    if (string.IsNullOrEmpty(strMDStatusText))
                                    {
                                        strMDStatusText = payLog.strErrMsg;
                                    }
                                }
                            }
                            else
                            {
                                bolApproved = false;
                                payLog.strErrMsg = "Güvenlik Uyarısı. Sayısal Imza Geçerli Degil";

                                if (string.IsNullOrEmpty(strMDStatusText))
                                {
                                    strMDStatusText = payLog.strErrMsg;
                                }
                            }

                            payLog.bolApproved = bolApproved;

                            if (bolApproved)
                            {
                                Card cardInfo = Sessions.CardInfo;

                                Services service = new Services();

                                if (payLog.bolPayUpdate)
                                {
                                    payLog.strPayUpdateMsg = service.GetOdemeAracDeg(payLog.intPolID, cardInfo);
                                }

                                FibaBes.Emkblibsposvadetyl vadelist = new FibaBes.Emkblibsposvadetyl();
                                vadelist.Add(new FibaBes.EmkblibsposvadetypUser
                                {
                                    tutar = payLog.flPrice,
                                    vadetar = payLog.strExpiryDate
                                });

                                SposTaksitli spos = service.SposTaksitli3d(new FibaBes.EmkblibspostaksitliinputtypUser
                                {
                                    authcode = payLog.strAuthCode,
                                    cvv = Utilities.NullFixDecimal(cardInfo.cvv2),
                                    expmonth = cardInfo.month,
                                    expyear = cardInfo.year,
                                    groupid = null,
                                    hostrefnum = payLog.strHostrefnum,
                                    kartno = cardInfo.cardno,
                                    orderid = payLog.strOrderID,
                                    polid = payLog.intPolID,
                                    taksitadedi = Utilities.NullFixDecimal(HttpContext.Current.Request.Form.Get("txninstallmentcount")),
                                    transid = payLog.strTransID,
                                    tutar = payLog.flPrice,
                                    tuser = "HYSG",
                                    vadelist = vadelist,
                                    borctip = "P"
                                });

                                payLog.strSposTaksitliMsg = spos.aciklama;

                                db.SaveChanges();

                                if (spos.issuccess == "T")
                                {
                                    return Utilities.MsgHtml(Utilities.MsgType.Info, "Tahsilatınız alındı.<br>Fibaemeklilik ile bir ömür dileriz.", false);
                                }
                                else
                                {
                                    return Utilities.MsgHtml(Utilities.MsgType.Warning, "Ödemeniz gerçekleşti fakat sistemde bir hata oluştu!", false);
                                }
                            }
                            else
                            {
                                payLog.strErrMsg = "errmsg : " + HttpContext.Current.Request.Form.Get("errmsg") + " ---- mderrormessage : " + HttpContext.Current.Request.Form.Get("mderrormessage");
                            }
                        }
                        else
                        {
                            payLog.strErrMsg = "not equal= errmsg : " + HttpContext.Current.Request.Form.Get("errmsg") + " ---- mderrormessage : " + HttpContext.Current.Request.Form.Get("mderrormessage");

                            Logs.Exception(new Exception("Payment finish page. OrderId or secure3dhash does not match. intCollectionPayLog: " + payLog.intCollectionPayLog + " t:" + QueryStrings.t), "Payment Finish");
                        }

                        if (!bolApproved)
                        {
                            return Utilities.MsgHtml(Utilities.MsgType.Error, "Ödeme gerçekleşmedi!<p>" + strMDStatusText + "</p>", false);
                        }
                    }
                    else
                    {
                        Logs.Exception(new Exception("Payment finish page. Parameters is null. Token: " + QueryStrings.token + " t:" + QueryStrings.t), "Payment Finish");
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "Payment Finish");
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "Payment Finish DB");
                }
            }

            return Utilities.MsgHtml(Utilities.MsgType.Error, "Bir hata oluştu! Ödeme gerçekleşmiş olabilir. Lütfen bizimle iletişime geçiniz.<p>" + strMDStatusText + "</p>", false);
        }
        #endregion

        #region GetSHA1
        private string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        private string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }
        #endregion
    }
}