﻿using System.Web;
using System.Web.Mvc;

namespace CollectionPay.Classes
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string strPage = HttpContext.Current.Request.Url.AbsolutePath.ToLower();
            if (Sessions.CurrentUser != null)
            {
                if (Sessions.CurrentUser.strIP != Utilities.ClientIP || HttpContext.Current.Request.UrlReferrer == null)
                {
                    Sessions.CurrentUser = null;
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();
                }
            }

            if (Sessions.CurrentUser == null)
            {
                if (QueryStrings.t != "")
                {
                    HttpContext.Current.Response.Redirect("/" + QueryStrings.t);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("/");
                }
            }
        } 
    }
}