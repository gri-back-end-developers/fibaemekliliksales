﻿namespace CollectionPay.Classes.Data
{
    public class UserInfo
    {
        public int intCollectionUserID { get; set; }
        public decimal intPolID { get; set; }
        public decimal intPidID { get; set; }
        public string strToken { get; set; }
        public string strTCKN { get; set; }
        public string strEmail { get; set; }
        public string strIP { get; set; }
        public int intCallcenterID { get; set; }
    }
}