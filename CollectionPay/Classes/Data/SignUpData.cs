﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionPay.Classes.Data
{
    public class SignUpData
    {
        public string TCKN { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}