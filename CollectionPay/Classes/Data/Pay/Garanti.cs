﻿namespace CollectionPay.Classes.Data.Pay
{
    public class Garanti
    {
        public string mode { get; set; }
        public string apiversion { get; set; }
        public string terminalprovuserid { get; set; }
        public string terminaluserid { get; set; }
        public string terminalmerchantid { get; set; }
        public string txntype { get; set; }
        public string txnamount { get; set; }
        public string txncurrencycode { get; set; }
        public string txninstallmentcount { get; set; }
        public string orderid { get; set; }
        public string terminalid { get; set; }
        public string successurl { get; set; }
        public string errorurl { get; set; }
        public string customeremailaddress { get; set; }
        public string customeripaddress { get; set; }
        public string secure3dhash { get; set; }
        public string orderitemproductid1 { get; set; }
        public string orderitemprice1 { get; set; }
        public string orderitemdescription1 { get; set; }
        public string orderaddresscity1 { get; set; }
        public string orderaddresscountry1 { get; set; }
        public string orderaddressdistrict1 { get; set; }
        public string orderaddresslastname1 { get; set; }
        public string orderaddressname1 { get; set; }
        public string orderaddressphonenumber1 { get; set; }
        public string orderaddresstext1 { get; set; }
    }
}