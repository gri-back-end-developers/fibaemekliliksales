﻿namespace CollectionPay.Classes.Data.Pay
{
    public class PaymentInfo
    {
        public string PostPath { get; set; }
        public Card card { get; set; }
        public Garanti garanti { get; set; }
    }
}