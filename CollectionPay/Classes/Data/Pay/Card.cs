﻿namespace CollectionPay.Classes.Data.Pay
{
    public class Card
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string cardno { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string cvv2 { get; set; }
        public string paymentmethod { get; set; } 
    }
}