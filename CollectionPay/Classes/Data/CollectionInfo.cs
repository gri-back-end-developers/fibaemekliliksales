﻿namespace CollectionPay.Classes.Data
{
    public class CollectionInfo
    {
        public int intPolID { get; set; }
        public string strID { get; set; }
        public string strCollection { get; set; }
        public decimal flPrice { get; set; }
        public string strPrice { get; set; }
        public string strVadeTar { get; set; }
    }
}