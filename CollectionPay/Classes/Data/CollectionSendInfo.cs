﻿namespace CollectionPay.Classes.Data
{
    public class CollectionSendInfo
    {
        public int intPolID { get; set; }
        public int intCallcenterID { get; set; }
    }
}