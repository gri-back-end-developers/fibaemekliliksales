﻿using CollectionPay.Controllers;
using System.Configuration;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace CollectionPay
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_EndRequest()
        {
            if (Request.IsSecureConnection == true && HttpContext.Current.Request.Url.Scheme == "https")
            { 
                if (Request.Cookies.Count > 0)
                {
                    foreach (string s in Request.Cookies.AllKeys)
                    {
                        Request.Cookies[s].Secure = true;
                    }
                }
            }

            if (ConfigurationManager.AppSettings["Error"] == "on")
            {
                if (Context.Response.StatusCode == 404 || Context.Response.StatusCode == 500 || Context.Response.StatusCode == 505)
                {
                    Response.Clear();

                    var rd = new RouteData();
                    rd.Values["controller"] = "Error";
                    switch (Context.Response.StatusCode)
                    {
                        case 404:
                            rd.Values["action"] = "status404";
                            break;
                        case 500:
                            rd.Values["action"] = "status500";
                            break;
                        default:
                            rd.Values["action"] = "status505";
                            break;
                    }

                    IController c = new ErrorController();
                    c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
                }
            }
        }
    }
}