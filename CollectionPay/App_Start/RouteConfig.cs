﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CollectionPay
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 name: "login",
                 url: "{token}",
                 defaults: new { controller = "Default", action = "Login", token = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "change",
                url: "user/change/{t}/{token}",
                defaults: new { controller = "User", action = "Change", t = UrlParameter.Optional, token = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "new",
               url: "user/new/{t}/{token}",
               defaults: new { controller = "User", action = "New", t = UrlParameter.Optional, token = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Default", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}