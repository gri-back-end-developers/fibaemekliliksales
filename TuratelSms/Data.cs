﻿namespace TuratelSms
{
    public class Data
    {
        public string Path { get; set; }
        public string ChannelCode { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Originator { get; set; } 
    }
}
