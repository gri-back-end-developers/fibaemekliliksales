﻿using System;
using System.Collections.Generic; 
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TuratelSms
{
    public class Service
    {
        Data _data;
        public Service(Data data)
        {
            _data = data;
        }

        #region MainReport
        public string MainReport(string ApplicationID)
        {
            try
            {
                HttpWebRequest lRequest = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["TuratelSMSServiceReport"]);
                byte[] data = Encoding.ASCII.GetBytes(MainReportXML(ApplicationID));
                lRequest.Method = "POST";
                lRequest.Timeout = 600000;
                lRequest.ContentType = "text/html; charset=UTF-8";
                lRequest.ContentLength = data.Length;

                Stream strm = lRequest.GetRequestStream();
                strm.Write(data, 0, data.Length);

                HttpWebResponse resp = (HttpWebResponse)lRequest.GetResponse();
                StreamReader strmRdr = new StreamReader(resp.GetResponseStream(), Encoding.UTF8);

                return strmRdr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MainReportXML
        public string MainReportXML(string ApplicationID)
        {
            string strXmlStr = string.Empty;

            strXmlStr += "<MainReportRoot>" + Environment.NewLine;
            strXmlStr += "<Command>25</Command>" + Environment.NewLine;
            strXmlStr += "<PlatformID>1</PlatformID>" + Environment.NewLine;
            strXmlStr += "<ChannelCode>" + _data.ChannelCode.Trim() + "</ChannelCode>" + Environment.NewLine;
            strXmlStr += "<UserName>" + _data.UserName.Trim() + "</UserName>" + Environment.NewLine;
            strXmlStr += "<PassWord>" + _data.PassWord.Trim() + "</PassWord>" + Environment.NewLine;
            strXmlStr += "<Status>0</Status>" + Environment.NewLine;
            strXmlStr += "<ApplicationID>" + ApplicationID + "</ApplicationID>" + Environment.NewLine;
            strXmlStr += "<SDate>" + Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddMinutes(-1).ToString("ddMMyyyyhhmm") + "</SDate>" + Environment.NewLine;
            strXmlStr += "<FDate>" + Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddDays(1).ToString("ddMMyyyyhhmm") + "</FDate>" + Environment.NewLine;
            strXmlStr += "</MainReportRoot>";

            return strXmlStr;
        }
        #endregion

        #region Send
        public string Send(string strMsg, string strNumbers)
        {
            try
            {
                HttpWebRequest lRequest = (HttpWebRequest)WebRequest.Create(_data.Path);
                byte[] data = Encoding.ASCII.GetBytes(SendXML(strMsg, strNumbers));
                lRequest.Method = "POST";
                lRequest.Timeout = 600000;
                lRequest.ContentType = "text/html; charset=UTF-8";
                lRequest.ContentLength = data.Length;

                Stream strm = lRequest.GetRequestStream();
                strm.Write(data, 0, data.Length);

                HttpWebResponse resp = (HttpWebResponse)lRequest.GetResponse();
                StreamReader strmRdr = new StreamReader(resp.GetResponseStream(), Encoding.UTF8);

                return strmRdr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SendXML
        string SendXML(string strMsg, string strNumbers)
        { 
            string strXmlStr = string.Empty;

            strXmlStr += "<MainmsgBody>" + Environment.NewLine;
            strXmlStr += "<Command>0</Command>" + Environment.NewLine;
            strXmlStr += "<PlatformID>1</PlatformID>" + Environment.NewLine;
            strXmlStr += "<UserName>" + _data.UserName.Trim() + "</UserName>" + Environment.NewLine;
            strXmlStr += "<PassWord>" + _data.PassWord.Trim() + "</PassWord>" + Environment.NewLine;
            strXmlStr += "<ChannelCode>" + _data.ChannelCode.Trim() + "</ChannelCode>" + Environment.NewLine;
            strXmlStr += "<Mesgbody>" + CharReplace(strMsg).Trim() + "</Mesgbody>" + Environment.NewLine;
            strXmlStr += "<Numbers>" + strNumbers.Trim() + "</Numbers>" + Environment.NewLine;
            strXmlStr += "<Type>10</Type>" + Environment.NewLine;
            strXmlStr += "<Originator>" + _data.Originator.Trim() + "</Originator>" + Environment.NewLine;
            strXmlStr += "<SDate>" + DateTime.Now.ToString("ddMMyyyyhhmm") + "</SDate>" + Environment.NewLine;
            strXmlStr += "<EDate>" + DateTime.Now.ToString("ddMMyyyyhhmm") + "</EDate>" + Environment.NewLine;
            strXmlStr += "<Concat>1</Concat>" + Environment.NewLine;
            strXmlStr += "<IsTCKimlikNoPacket>0</IsTCKimlikNoPacket>" + Environment.NewLine;
            strXmlStr += "</MainmsgBody>";

            return strXmlStr;
        }
        #endregion

        #region CharReplace
        string CharReplace(string strValue)
        {
            strValue = strValue.Replace(Convert.ToChar(220), Convert.ToChar(85));
            strValue = strValue.Replace(Convert.ToChar(286), Convert.ToChar(71));
            strValue = strValue.Replace(Convert.ToChar(304), Convert.ToChar(73));
            strValue = strValue.Replace(Convert.ToChar(350), Convert.ToChar(83));
            strValue = strValue.Replace(Convert.ToChar(214), Convert.ToChar(79));
            strValue = strValue.Replace(Convert.ToChar(199), Convert.ToChar(67));
            strValue = strValue.Replace(Convert.ToChar(252), Convert.ToChar(117));
            strValue = strValue.Replace(Convert.ToChar(287), Convert.ToChar(103));
            strValue = strValue.Replace(Convert.ToChar(305), Convert.ToChar(105));
            strValue = strValue.Replace(Convert.ToChar(351), Convert.ToChar(115));
            strValue = strValue.Replace(Convert.ToChar(246), Convert.ToChar(111));
            strValue = strValue.Replace(Convert.ToChar(231), Convert.ToChar(99));
            strValue = strValue.Replace(Convert.ToChar(10), Convert.ToChar(32)); // NewLine
            strValue = strValue.Replace(Convert.ToChar(92), Convert.ToChar(47)); // BackSlash
            strValue = strValue.Replace('”', '"');
            strValue = strValue.Replace('’', Convert.ToChar(39));
            strValue = strValue.Replace('‘', Convert.ToChar(39));
            strValue = strValue.Replace("'", "''");

            return strValue;
        }
        #endregion
    }
}
