﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using WebSales.Controllers;

namespace WebSales
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes); 
        }

        protected void Application_EndRequest()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Error"] == "on")
            {
                if (Context.Response.StatusCode == 404 || Context.Response.StatusCode == 500 || Context.Response.StatusCode == 403)
                { 
                    Response.Clear();

                    var rd = new RouteData();
                    rd.Values["controller"] = "Error";
                    rd.DataTokens.Add("error", "1");

                    switch (Context.Response.StatusCode)
                    {
                        case 404:
                            rd.Values["action"] = "status404";
                            break;
                        case 500:
                            rd.Values["action"] = "status500";
                            break;
                        case 403:
                            rd.Values["action"] = "status403";
                            break;
                    }

                    IController c = new ErrorController();
                    c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
                }
            }
        }
    }
}