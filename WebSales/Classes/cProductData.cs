﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSales.Classes
{
    public class cSalesProduct
    {
        public cWebSaleLog webSaleLog { get; set; }
        public cProduct product { get; set; }
        public cCallcenter callcenter { get; set; }
        public List<cPaymentMethod> paymentMethods { get; set; } 
    }

    public class cWebSaleLog
    {
        public int intPacketID { get; set; }
        public bool bolIsRefresh { get; set; }
        public cWebSaleInfo saleInfo { get; set; }
        public cWebSalePay salePay { get; set; }
    }

    public class cWebSaleInfo
    {
        public string strAddress { get; set; }
        public string strBirthDate { get; set; }
        public string strEmail { get; set; }
        public string strMobile { get; set; }
        public string strName { get; set; }
        public string strPhone { get; set; }
        public string strSurname { get; set; }
        public string strTCKN { get; set; }
        public string strCountryCode { get; set; }
        public string strRegionCode { get; set; }
        public string strDistrictCode { get; set; }
        public bool bolTcknValid { get; set; }
        public byte? intQuestionType { get; set; }
        public bool bolContactPermission { get; set; }
        public cPolicyData PolicyData { get; set; }
        public cPolicyData PolicyData2 { get; set; }
        public string strPaymentOption { get; set; }
        public bool bolPolicyRenew { get; set; }
        public byte intPolicyRenewYear { get; set; }
    }

    public class cWebSalePay
    {
        public bool bolApproved { get; set; }
        public string strErrMsg { get; set; }
    }

    public class cProduct
    {
        public string name { get; set; }
        public string comment { get; set; }
        public string image { get; set; }
        public string detailurl { get; set; }
        public string imageTable { get; set; }
        public string question { get; set; }
        public bool refresh { get; set; }
        public string strDtSub2 { get; set; }
        public string intPaymentMethodIDs { get; set; }
        public List<cProductPackets> packets { get; set; }
    }

    public class cProductPackets
    {
        public int id { get; set; }
        public string name { get; set; }
        public string strComment { get; set; }
    }
}