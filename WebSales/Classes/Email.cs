#region Directives
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using WebSales.CrmWS;
#endregion

namespace WebSales.Classes
{
    public class Email
    {
        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString());
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());

                server.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                Logs.Error(null, ex, 0);
                return false;
            }
        }
        #endregion

        #region SendEmail
        public static bool SendEmail(string strSubject, string strBody, string strTo)
        {
            try
            {
                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["InfoMail"].ToString(), strTo);
                mailMessage.Subject = strSubject.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = strBody;

                SmtpClient server = new SmtpClient();
                //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                server.EnableSsl = Utilities.NullFixBool(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ToString());
                server.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                Logs.Error(null, ex, 0);
                return false;
            }
        }
        #endregion

        #region SendCRM
        public static bool SendCRM(string strOwnerId, string strQueueId, string strPhoneCallType, string strCustomerName, string strCustomerLastName, string strOfferNo, string strPhoneNumber, string strDescription, string strSubject)
        {
            try
            {
                if (!string.IsNullOrEmpty(strPhoneNumber))
                {
                    strPhoneNumber = strPhoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "");
                    if (strPhoneNumber.Length == 11)
                    {
                        strPhoneNumber = strPhoneNumber.Remove(0, 1);
                    }
                }

                using (FibaEmeklilikService crm = new FibaEmeklilikService())
                {
                    var request = new CreateOutBoundCallRequest
                    {
                        HeaderData = new RequestHeader
                        {
                            Password = ConfigurationManager.AppSettings["crmPassword"].ToString(),
                            Username = ConfigurationManager.AppSettings["crmUsername"].ToString()
                        },
                        CustomerLastName = strCustomerLastName,
                        CustomerName = strCustomerName,
                        OfferNo = string.IsNullOrEmpty(strOfferNo) ? strSubject : strOfferNo,
                        OwnerId = strOwnerId,
                        OwnerType = "team",
                        PhoneCallType = strPhoneCallType,
                        PhoneNumber = strPhoneNumber,
                        QueueId = strQueueId,
                        Description = strDescription
                    };

                    var result = crm.CreateOutBoundCall(request);
                    if (result.ResponseCode == "200")
                    {
                        Logs.Error(null, new Exception("SendCrm Success : " + result.ResponseMessage), 0);
                        return true;
                    }
                    else
                    {
                        Logs.Error(null, new Exception("SendCrm :" + result.ResponseMessage), 0);
                    }
                }
            }
            catch (Exception ex)
            { 
                Logs.Error(null, new Exception("SendCrm Ex : " + ex.ToString()), 0);
            }

            return false;
        }
        #endregion
    }
}