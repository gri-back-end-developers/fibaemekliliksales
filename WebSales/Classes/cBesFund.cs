﻿using ModelLibrary;
using System.Collections.Generic;
using System.Linq; 

namespace WebSales.Classes
{
    public class cBesFund
    {
        public static List<cFonList> SetFund(string strData, WebSales.FibaBES.Emkbasvurulibfondagilimtyl fonlar)
        {
            List<cFonList> _data = new List<cFonList>();

            if (!string.IsNullOrEmpty(strData))
            {
                System.Web.Script.Serialization.JavaScriptSerializer javaSer = new System.Web.Script.Serialization.JavaScriptSerializer();
                _data = javaSer.Deserialize<List<cFonList>>(strData);
            }

            using (DBEntities db = new DBEntities())
            {
                var besFunds = db.tblBesFunds.ToList();
                if (!string.IsNullOrEmpty(strData))
                {
                    foreach (var item in _data)
                    {
                        var fund = besFunds.Where(w => w.strCode == item.strCode).FirstOrDefault();
                        if (fund != null)
                        {
                            item.strBesFundYearlyRate = fund.flYearlyRate.ToString();
                            item.strBesFundDailyRate = fund.flDailyRate;
                            item.strBesFundYearlyFTGKRate = fund.flYearlyRateFTGK;
                        }
                    }
                }
                
                if (fonlar != null)
                {
                    foreach (var item in besFunds)
                    {
                        if (_data.Count(c => c.strCode == item.strCode) == 0)
                        {
                            var fund = fonlar.Where(w => w.fonkod == item.strCode).FirstOrDefault();
                            if (fund != null)
                            {
                                _data.Add(new cFonList
                                {
                                    strBesFundYearlyRate = item.flYearlyRate.ToString(),
                                    strBesFundDailyRate = item.flDailyRate,
                                    strBesFundYearlyFTGKRate = item.flYearlyRateFTGK,
                                    strCode = fund.fonkod,
                                    strRate = (fund.fonstandartoran * 100).ToString()
                                });
                            }
                        }
                    }
                }
            }

            return _data;
        }
    }
}