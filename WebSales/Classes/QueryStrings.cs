#region Directives
using System;
using System.Collections.Generic;
using System.Web;
#endregion

namespace WebSales.Classes
{
    public class QueryStrings
    {
        #region PacketID
        public static int PacketID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["p"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString() == "" ? 0 : Utilities.NullFixInt(cCrypto.DecryptDES(value.ToString()));
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region Token
        public static string Token
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["token"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region Tag
        public static int Tag
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["tag"];
                    if (value == null)
                    {
                        value = "0";
                    }
                    return Utilities.NullFixInt(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region Key
        public static string Key
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["k"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region CallcenterID
        public static int CallcenterID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["c"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString() == "" ? 0 : Utilities.NullFixInt(cCrypto.DecryptDES(value.ToString()));
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region SaleType
        public static string SaleType
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["saletype"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region page
        public static string page
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["page"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region BannerID
        public static int BannerID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["bid"];
                    if (value == null)
                    {
                        value = "0";
                    }
                    return Utilities.NullFixInt(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region BannerKey
        public static string BannerKey
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["bkey"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return value.ToString();
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region ID
        public static int ID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["id"];
                    if (value == null)
                    {
                        value = "0";
                    }
                    return Utilities.NullFixInt(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion
    }
}