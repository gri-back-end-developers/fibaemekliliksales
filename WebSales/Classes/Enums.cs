﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSales.Classes
{
    public class Enums
    {
        public enum WebSaleLogProductType : byte
        {
            PacketSelected = 1,
            InsuranceInformation = 2,
            HealtSelect = 3,
            InformationForm = 4,
            SaleForm = 5
        }

        public enum PayType : byte
        {
            Akbank3D = 1,
            Garanti3D = 2
        }

        public enum WebSaleLogBesPlanType : byte
        {
            ParticipantInformation = 6,
            ContributionInformation1 = 7,
            ContributionInformation2 = 8, 
            InformationForm = 4,
            SaleForm = 5,
            CreditInformation = 9,
            CreditPaymentPlan = 10
        }
    }
}