﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSales.Classes
{
    public class cSalesCredit
    {
        public cWebSaleCreditLog webSaleLog { get; set; }
        public cCredit credit { get; set; }
        public cCallcenter callcenter { get; set; }
        public List<cBanks> banks { get; set; }
        public List<cPaymentMethod> paymentMethods { get; set; } 
    }

    public class cCredit
    {
        public string name { get; set; }
        public string comment { get; set; }
        public string image { get; set; }
        public string detailurl { get; set; }
        public string imageTable { get; set; }
        public string question { get; set; }
        public string strDtSub2 { get; set; }
        public List<cCreditPackets> packets { get; set; }
    }

    public class cCreditPackets
    {
        public int id { get; set; }
        public string name { get; set; }
        public string strComment { get; set; }
        public List<cCreditAssurances> assurances { get; set; }
        public cCreditPolicy policy { get; set; }
    }

    public class cCreditAssurances
    {
        public string name { get; set; }
    }

    public class cCreditPolicy
    {
        public string strPolicyID { get; set; }
        public decimal flFirstPremium { get; set; }
        public decimal flTotalPremium { get; set; }
    }

    public class cWebSaleCreditLog
    {
        public int intCreditPacketID { get; set; }
        public cWebSaleCreditInfo saleInfo { get; set; }
        public cWebSalePay salePay { get; set; }
        public List<PaymentPlanResponse> paymentPlan { get; set; }
    }

    public class cWebSaleCreditInfo
    {
        public string strTCKN { get; set; }
        public string strName { get; set; }
        public string strSurname { get; set; }
        public decimal flCreditAmount { get; set; }
        public decimal flInterest { get; set; }
        public decimal flBSMV { get; set; }
        public decimal flKKDF { get; set; }
        public byte intCreditMonth { get; set; }
        public short intBankCityID { get; set; }
        public short intBankID { get; set; } 
        public short intBankBranchID { get; set; } 
        public string strAddress { get; set; }
        public string strBirthDate { get; set; }
        public string strEmail { get; set; }
        public string strMobile { get; set; }
        public string strPhone { get; set; }
        public string strCountryCode { get; set; }
        public string strRegionCode { get; set; }
        public string strDistrictCode { get; set; }
        public bool bolTcknValid { get; set; }
        public byte? intQuestionType { get; set; }
        public bool bolSalesAgreement { get; set; }
        public bool bolContactPermission { get; set; }
        public cPolicyData PolicyData { get; set; }
        public cPolicyData PolicyData2 { get; set; }
        public string strPaymentOption { get; set; } 
    }

    public class cCreditSessionKeys
    {
        public string strCreditID { get; set; }
        public int intCreditPacketID { get; set; }
    }

    public class cBanks
    {
        public short intBankID { get; set; }
        public string strName { get; set; } 
    }

    public class cPaymentMethod
    {
        public short intPaymentMethodID { get; set; }
        public string strName { get; set; }
    } 
}