#region Directives
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
#endregion

namespace WebSales.Classes
{
    public class Cookies
    {
        #region WebSaleLog
        public static WebSaleLog WebSaleLog
        {
            get
            {
                WebSaleLog saleLog = new WebSaleLog();
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["WebSaleLog"].Value;
                    if (objValue == null)
                        return saleLog;

                    if (!objValue.ToString().Contains("CryptoID"))
                        return saleLog;

                    JavaScriptSerializer java = new JavaScriptSerializer();
                    saleLog = java.Deserialize<WebSaleLog>(objValue.ToString());

                    string strSaletype = QueryStrings.SaleType;
                    if (string.IsNullOrEmpty(strSaletype))
                    {
                        strSaletype = HttpContext.Current.Request.Url.Segments[1].Replace("/", "");
                    }

                    if (strSaletype == saleLog.SaleType)
                    {
                        saleLog.ID = Utilities.NullFixInt(cCrypto.DecryptDES(saleLog.CryptoID));
                    }
                    else
                    {
                        saleLog = new WebSaleLog();
                    }
                }
                catch { }

                return saleLog;
            }
            set
            {
                try
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    WebSaleLog saleLog = value;
                    saleLog.CryptoID = cCrypto.EncryptDES(saleLog.ID.ToString());
                    saleLog.ID = 0;

                    HttpCookie cookie = new HttpCookie("WebSaleLog");
                    cookie.Value = java.Serialize(saleLog);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region Step
        public static int Step
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["Step"].Value;
                    if (objValue == null)
                    {
                        return 1;
                    }
                    return Utilities.NullFixInt(objValue.ToString());
                }
                catch
                {
                    return 1;
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("Step");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region StepInfo123
        public static string StepInfo123
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["stepinfo123"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("stepinfo123");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region StepInfo4
        public static string StepInfo4
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["stepinfo4"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("stepinfo4");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region salepage
        public static string salepage
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["salepage"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("salepage");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region CreditSessionKeys
        public static List<cCreditSessionKeys> CreditSessionKeys
        {
            get
            {
                List<cCreditSessionKeys> sessionKeys = new List<cCreditSessionKeys>();
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["CreditSessionKeys"].Value;
                    if (objValue == null)
                        return null;

                    JavaScriptSerializer java = new JavaScriptSerializer();
                    sessionKeys = java.Deserialize<List<cCreditSessionKeys>>(objValue.ToString());
                }
                catch { }

                return sessionKeys;
            }
            set
            {
                try
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();

                    HttpCookie cookie = new HttpCookie("CreditSessionKeys");
                    cookie.Value = java.Serialize(value);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region CrediNo
        public static string CrediNo
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["credino"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("credino");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region RemoveCreditSessionKeys
        public static void RemoveCreditSessionKeys()
        {
            HttpContext.Current.Response.Cookies.Remove("RemoveCreditSessionKeys");
            HttpContext.Current.Response.Cookies["RemoveCreditSessionKeys"].Expires = DateTime.Now.AddDays(-1);
        }
        #endregion

        #region RemoveCrediNo
        public static void RemoveCrediNo()
        {
            HttpContext.Current.Response.Cookies.Remove("credino");
            HttpContext.Current.Response.Cookies["credino"].Expires = DateTime.Now.AddDays(-1);
        }
        #endregion

        #region BannerKey
        public static string GetBannerKey(int intBannerID)
        { 
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["bkey" + intBannerID].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                } 
        }

        public static void SetBannerKey(int intBannerID, string strKey)
        {
            try
            {
                HttpCookie cookie = new HttpCookie("bkey" + intBannerID);
                cookie.Value = strKey;
                cookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch { }
        }
        #endregion

        #region Key
        public static string Key
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["key"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("key");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion
    }
}