﻿using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSales.Classes; 

namespace WebSales.Classes
{
    public class cBannerLog : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            LogSet();
        }

        public static void LogSet()
        {
            int intBannerID = QueryStrings.BannerID;
            if (intBannerID > 0)
            {
                DBEntities db = new DBEntities();
                int intWebSaleLogID = Cookies.WebSaleLog.ID;

                try
                {
                    string strBannerKey = QueryStrings.BannerKey;
                    if (!string.IsNullOrEmpty(strBannerKey))
                    {
                        Cookies.SetBannerKey(intBannerID, strBannerKey);
                    }

                    if (!string.IsNullOrEmpty(strBannerKey) && Cookies.WebSaleLog.ID > 0)
                    { 
                        Cookies.WebSaleLog = new WebSaleLog { ID = 0, SaleType = "" };
                        Cookies.Step = 1; 
                        Cookies.StepInfo123 = "";
                        Cookies.StepInfo4 = "";
                        Cookies.RemoveCrediNo();
                        Cookies.RemoveCreditSessionKeys(); 

                        HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.ToString(), false);
                        return;
                    }

                    if (db.tblBanners.Count(c => !c.bolIsDelete && c.intBannerID == intBannerID) > 0)
                    {
                        string strUrl = HttpContext.Current.Request.Url.AbsolutePath.Split('?')[0];
                        if (strUrl.Length > 1)
                        {
                            strUrl = strUrl.Substring(0, strUrl.Length - 2);
                        }

                        if (string.IsNullOrEmpty(strBannerKey) && strUrl.Split('/').Length == 4 && intWebSaleLogID == 0)
                        {
                            strBannerKey = Cookies.GetBannerKey(intBannerID);
                        }

                        if (!string.IsNullOrEmpty(strBannerKey))
                        {
                            var bannerLog = db.tblBannerLogs.Where(w => w.intBannerID == intBannerID && w.strKey == strBannerKey && w.strUrl == strUrl).FirstOrDefault();
                            if (bannerLog == null)
                            {
                                string strReferringSite = "";
                                try
                                {
                                    strReferringSite = HttpContext.Current.Request.ServerVariables["http_referer"];
                                }
                                catch { }

                                if (string.IsNullOrEmpty(strReferringSite))
                                {
                                    strReferringSite = "N/A";
                                }

                                var bannerLogFirst = db.tblBannerLogs.Where(w => w.intBannerID == intBannerID && w.strKey == strBannerKey).FirstOrDefault();
                                if (bannerLogFirst != null)
                                {
                                    strReferringSite = bannerLogFirst.strReferringSite;
                                }

                                bannerLog = new tblBannerLogs();
                                bannerLog.strKey = strBannerKey;
                                bannerLog.dtRegisterDate = DateTime.Now;
                                bannerLog.intBannerID = intBannerID;
                                bannerLog.strIP = Utilities.ClientIP;
                                bannerLog.intWebSaleLogID = intWebSaleLogID;
                                bannerLog.strReferringSite = strReferringSite;
                                bannerLog.strUrl = strUrl;

                                db.tblBannerLogs.Add(bannerLog);
                                db.SaveChanges();
                            }
                        }
                        else if (intWebSaleLogID > 0)
                        {
                            strBannerKey = Cookies.GetBannerKey(intBannerID);

                            var bannerLogs = db.tblBannerLogs.Where(w => w.intBannerID == intBannerID && w.strKey == strBannerKey && w.strUrl == strUrl).ToList();
                            if (bannerLogs != null && bannerLogs.Count > 0)
                            {
                                if (bannerLogs.Count == 1)
                                {
                                    var bannerLog = bannerLogs.First();
                                    if (bannerLog.intWebSaleLogID == 0)
                                    {
                                        bannerLog.intWebSaleLogID = intWebSaleLogID;
                                        db.SaveChanges();
                                    }
                                    else if (bannerLog.intWebSaleLogID > 0 && bannerLog.intWebSaleLogID != intWebSaleLogID)
                                    {
                                        tblBannerLogs bannerLogNew = new tblBannerLogs
                                        {
                                            dtRegisterDate = DateTime.Now,
                                            intBannerID = intBannerID,
                                            intWebSaleLogID = intWebSaleLogID,
                                            strIP = Utilities.ClientIP,
                                            strKey = bannerLog.strKey,
                                            strReferringSite = bannerLog.strReferringSite
                                        };

                                        db.tblBannerLogs.Add(bannerLogNew);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    if (bannerLogs.Count(c => c.intWebSaleLogID == intWebSaleLogID) == 0)
                                    {
                                        var bannerLog = bannerLogs.First();

                                        tblBannerLogs bannerLogNew = new tblBannerLogs
                                        {
                                            dtRegisterDate = DateTime.Now,
                                            intBannerID = intBannerID,
                                            intWebSaleLogID = intWebSaleLogID,
                                            strIP = Utilities.ClientIP,
                                            strKey = bannerLog.strKey,
                                            strReferringSite = bannerLog.strReferringSite
                                        };

                                        db.tblBannerLogs.Add(bannerLogNew);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Error(db, ex, intWebSaleLogID);
                }
                finally
                {
                    db.Dispose();
                }
            }
        }
    }
}