﻿using System;
using System.Web;
using System.Web.Script.Serialization;

namespace WebSales.Classes
{
    public class Sessions
    {
        #region Message
        public static string Message
        {
            get
            {
                if (HttpContext.Current.Session["Message"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Message"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion

        #region CardInfo
        public static cCardInfo CardInfo
        {
            get
            {
                if (HttpContext.Current.Session["CardInfo"] == null)
                {
                    return null;
                }
                else
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();
                    return java.Deserialize<cCardInfo>(cCrypto.DecryptDES(HttpContext.Current.Session["CardInfo"].ToString()));
                }
            }
            set
            {
                JavaScriptSerializer java = new JavaScriptSerializer();
                HttpContext.Current.Session["CardInfo"] = cCrypto.EncryptDES(java.Serialize(value));
            }
        }
        #endregion

        #region SalePage
        public static string SalePage
        {
            get
            {
                if (HttpContext.Current.Session["salepage"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["salepage"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["salepage"] = value;
            }
        }
        #endregion

        #region SalePageTime
        public static cSalePageTime SalePageTime
        {
            get
            {
                if (HttpContext.Current.Session["SalePageTime"] == null)
                {
                    return null;
                }
                else
                {
                    return (cSalePageTime)HttpContext.Current.Session["SalePageTime"];
                }
            }
            set
            {
                HttpContext.Current.Session["SalePageTime"] = value;
            }
        }
        #endregion

        #region CallcenterID
        public static int CallcenterID
        {
            get
            {
                if (HttpContext.Current.Session["CallcenterID"] == null)
                {
                    return Utilities.NullFixInt(System.Configuration.ConfigurationManager.AppSettings["DefaultCallcenter"]);
                }
                else
                {
                    return Utilities.NullFixInt(HttpContext.Current.Session["CallcenterID"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["CallcenterID"] = value;
            }
        }
        #endregion
    }
}