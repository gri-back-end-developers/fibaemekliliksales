﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSales.Classes
{
    public class cSalesBesPlan
    {
        public cWebSaleBesPlanLog webSaleLog { get; set; }
        public cBesPlan besPlan { get; set; }
        public cCallcenter callcenter { get; set; }
        public cWebSaleBesPlanPolicy policy { get; set; }
    }

    public class cBesPlan
    {
        public string name { get; set; }
        public string comment { get; set; }
        public string image { get; set; }
        public string detailurl { get; set; }
        public string imageTable { get; set; }
        public string strPlanCode { get; set; }
        public bool bolInitialCont { get; set; }
        public decimal flMinContAmount { get; set; }
        public decimal flInitialCont { get; set; }
        public string strDtSub2 { get; set; }
    }

    public class cWebSaleBesPlanLog
    {
        public int intBesPlanID { get; set; }
        public cWebSaleBesPlanInfo saleInfo { get; set; }
        public cWebSalePay salePay { get; set; }
    }

    public class cWebSaleBesPlanInfo
    {
        public string strAddress { get; set; }
        public string strBirthDate { get; set; }
        public string strEmail { get; set; }
        public string strMobile { get; set; }
        public string strName { get; set; }
        public string strPhone { get; set; }
        public string strSurname { get; set; }
        public string strTCKN { get; set; }
        public string strCountryCode { get; set; }
        public string strRegionCode { get; set; }
        public string strDistrictCode { get; set; }
        public string strPaymentOption { get; set; }
        public bool bolTcknValid { get; set; }
        public bool bolSalesAgreement { get; set; }
        public bool bolContactPermission { get; set; }
        public decimal flOrderlyContAmount { get; set; }
        public decimal flInitilalContAmount { get; set; }
        public decimal flExtraContAmount { get; set; }
        public byte intOrderlyContAmountPeriod { get; set; }
        public string FirstDueDate { get; set; }
        public string FonList { get; set; }
    }

    public class cWebSaleBesPlanPolicy
    {
        public string intPolID { get; set; }
        public string strMethod { get; set; }
        public string strMsg { get; set; }
    }

    public class cFonList
    {
        public string strCode { get; set; }
        public string strRate { get; set; }
        public string strBesFundYearlyRate { get; set; }
        public string strBesFundDailyRate { get; set; }
        public string strBesFundYearlyFTGKRate { get; set; }
    }
}