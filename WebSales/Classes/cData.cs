﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WebSales.Classes
{
    public class WebSaleLog
    {
        public int ID { get; set; }
        public string CryptoID { get; set; }
        public string SaleType { get; set; }
    }

    public class cPolicyData
    { 
        public decimal? PoliceNo { get; set; }
        public decimal? ToplamPirim { get; set; }
        public decimal? IlkPrim { get; set; }
        public string TeklifDurum { get; set; }
        public decimal? IlkPrimKomisyon { get; set; }
        public decimal? ToplamKomisyon { get; set; }
        public string KrediNo { get; set; }  
        public string Yenileme { get; set; } 
    }

    public class cProductList
    {
        public string url { get; set; }
        public string name { get; set; }
        public string spot { get; set; }
        public string image { get; set; }
        public string imagethumb { get; set; }
        public int intCount { get; set; }
        public string video { get; set; }
    }
    
    public class cCallcenter
    {
        public int intCallcenterID { get; set; }
        public string name { get; set; }
        public string strInformationForm { get; set; }
        public decimal flBSMV { get; set; }
        public decimal flKKDF { get; set; }
    }

    public class cCardInfo
    {
        public string namesurname { get; set; }
        public string cardno { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public string cvv2 { get; set; }
        public byte paymentmethod { get; set; }
        public string paymenttype { get; set; }
    }

    public class cPayData
    {
        public Enums.PayType PayType { get; set; }
        public cPayAkbank akbank { get; set; }
        public cPayGaranti garanti { get; set; }
    }

    public class cPayAkbank
    {
        public string clientId { get; set; }
        public string amount { get; set; }
        public string oid { get; set; }
        public string okUrl { get; set; }
        public string failUrl { get; set; }
        public string rnd { get; set; }
        public string taksit { get; set; }
        public string islemtipi { get; set; }
        public string hash { get; set; }
        public string faturaFirma { get; set; }
        public string Fadres { get; set; }
        public string Fadres2 { get; set; }
        public string Fil { get; set; }
        public string Filce { get; set; }
        public string tel { get; set; }
        public string fulkekod { get; set; } 
        public string desc1 { get; set; }
        public string id1 { get; set; }
    }

    public class cPayGaranti
    {
        public string mode { get; set; }
        public string apiversion { get; set; }
        public string terminalprovuserid { get; set; }
        public string terminaluserid { get; set; }
        public string terminalmerchantid { get; set; }
        public string txntype { get; set; }
        public string txnamount { get; set; }
        public string txncurrencycode { get; set; }
        public string txninstallmentcount { get; set; }
        public string orderid { get; set; }
        public string terminalid { get; set; }
        public string successurl { get; set; }
        public string errorurl { get; set; }
        public string customeremailaddress { get; set; }
        public string customeripaddress { get; set; }
        public string secure3dhash { get; set; }
        public string orderitemproductid1 { get; set; }
        public string orderitemprice1 { get; set; }
        public string orderitemdescription1 { get; set; }
        public string orderaddresscity1 { get; set; }
        public string orderaddresscountry1 { get; set; }
        public string orderaddressdistrict1 { get; set; }
        public string orderaddresslastname1 { get; set; }
        public string orderaddressname1 { get; set; }
        public string orderaddressphonenumber1 { get; set; }
        public string orderaddresstext1 { get; set; }
    }

    public class cPayResultData
    {
        public string strErrMsg { get; set; }
        public string strTransID { get; set; }
        public string strOrderID { get; set; }
        public string strHostrefnum { get; set; }
        public string strAuthCode { get; set; } 
    }

    public class cTcknInfo
    {
        public string strMaritalStatus { get; set; }
        public string strGender { get; set; }
        public string strFatherName { get; set; }
        public string strMotherName { get; set; }
        public string strBirthplace { get; set; }
        public string strBirthRegionCode { get; set; }
        public string strBirthDistrictCode { get; set; }
        public string strAdress { get; set; }
        public string strRegionCode { get; set; }
        public string strDistrictCode { get; set; }
        public string strCountryCode { get; set; }
    }

    public class cSalePageTime
    { 
        public DateTime time { get; set; } 
    }
}