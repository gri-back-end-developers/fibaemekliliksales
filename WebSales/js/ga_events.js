var FE = FE || {};

FE.GoogleAnalytics = FE.GoogleAnalytics || {};

FE.GoogleAnalytics.category = "emusteri";

FE.GoogleAnalytics.events = {
	aa: "ekran1 urunler geri donus",
	ab: "ekran1 detaylibilgi geri donus",
	aca: "ekran1 paketsecim 20",
	acb: "ekran1 paketsecim 30",
	acc: "ekran1 paketsecim 50",
	ad: "ekran1 devam",
	ba: "ekran2 urunler geri donus",
	bb: "ekran2 detaylibilgi geri donus",
	bc: "ekran2 devam",
	ca: "ekran3 urunler geri donus",
	cba: "ekran3 saglikbeyani OK",
	cbb: "ekran3 saglikbeyani arama",
	cc: "ekran3 devam",
	da: "ekran4 urunler geri donus",
	db: "ekran4 bilgilendirmeformu",
	dc: "ekran4 sozlesm",
	dd: "ekran4 devam",
	ea: "ekran5 urunler geri donus",
	eb: "ekran5 detaylibilgi geri donus",
	ec: "ekran5 devam"
};

FE.GoogleAnalytics.elements = {
	"step-1a": $('.js-return-1'),
	"step-1b": $('.js-detay-btn-1'),
	"step-1c1": $('.js-step-1-radio-1'),
	"step-1c2": $('.js-step-1-radio-2'),
	"step-1c3": $('.js-step-1-radio-3'),
	"step-1d": $('.js-next-step-btn-1'),
	"step-2a": $('.js-return-2'),
	"step-2b": $('.js-detay-btn-2'),
	"step-2c": $('.js-step-next-btn-2'),
	"step-3a": $('.js-return-3'),
	"step-3b1": $('.js-step-3-radio-1'),
	"step-3b2": $('.js-step-3-radio-2'),
	"step-3c": $('.js-next-step-btn-3'),
	"step-4a": $('.js-return-4'),
	"step-4b": $('.js-step-4-radio-1'),
	"step-4c": $('.js-step-4-radio-2'),
	"step-4d": $('.js-next-step-btn-4'),
	"step-5a": $('.js-return-5'),
	"step-5b": $('.js-detay-btn-5'),
	"step-5c": $('.js-step-next-btn-5')
};

FE.GoogleAnalytics.sendEvent = function(category, action, label) {
	console.log('event sended!!!');
	console.log('send', 'event', category, action, label, 1);
	ga && ga('send', 'event', {
		eventCategory: category,
		eventAction: action,
		eventLabel: label
	});
	return true;
}
FE.GoogleAnalytics.addEventListeners = function() { 
	// Step 1
	this.elements['step-1a'].on('click', this.sendByType('click', this.events['aa']));
	this.elements['step-1b'].on('click', this.sendByType('click', this.events['ab']));
	this.elements['step-1c1'].on('click', this.sendByType('click', this.events['aca']));
	this.elements['step-1c2'].on('click', this.sendByType('click', this.events['acb']));
	this.elements['step-1c3'].on('click', this.sendByType('click', this.events['acc']));
	this.elements['step-1d'].on('click', this.sendByType('click', this.events['ad']));
	// Step 2
	this.elements['step-2a'].on('click', this.sendByType('click', this.events['ba']));
	this.elements['step-2b'].on('click', this.sendByType('click', this.events['bb']));
	this.elements['step-2c'].on('click', this.sendByType('click', this.events['bc']));
	// Step 3
	this.elements['step-3a'].on('click', this.sendByType('click', this.events['cba']));
	this.elements['step-3b1'].on('click', this.sendByType('click', this.events['cba']));
	this.elements['step-3b2'].on('click', this.sendByType('click', this.events['cbb']));
	this.elements['step-3c'].on('click', this.sendByType('click', this.events['cc']));
	// Step 4
	this.elements['step-4a'].on('click', this.sendByType('click', this.events['da']));
	this.elements['step-4b'].on('click', this.sendByType('click', this.events['db']));
	this.elements['step-4c'].on('click', this.sendByType('click', this.events['dc']));
	this.elements['step-4d'].on('click', this.sendByType('click', this.events['dd']));
	// Step 5
	this.elements['step-5a'].on('click', this.sendByType('click', this.events['ea']));
	this.elements['step-5b'].on('click', this.sendByType('click', this.events['eb']));
	this.elements['step-5c'].on('click', this.sendByType('click', this.events['ec'])); 
};

FE.GoogleAnalytics.sendByType = function(action, label) {
	var category = this.category;
	this.sendEvent(category, action, label);
};

FE.GoogleAnalytics.sendFormEvent = function(action) {
	var category = this.category;
	var label = "test";
	this.sendEvent(category, action, label);
};

FE.GoogleAnalytics.init = function() {
	this.addEventListeners();
};

FE.GoogleAnalytics.init();

