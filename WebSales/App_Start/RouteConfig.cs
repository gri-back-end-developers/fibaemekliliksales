﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebSales
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

          //  routes.MapRoute(
          //     name: "test",
          //     url: "test/pay",
          //     defaults: new { controller = "Test", action = "Pay" }
          // );

          //  routes.MapRoute(
          //    name: "test1",
          //    url: "test/finish",
          //    defaults: new { controller = "Test", action = "Finish" }
          //);

            routes.MapRoute(
                name: "salesstep1",
                url: "sales/step1",
                defaults: new { controller = "Sales", action = "Step1" }
            );

            routes.MapRoute(
               name: "salesstep2",
               url: "sales/step2",
               defaults: new { controller = "Sales", action = "Step2" }
            );

            routes.MapRoute(
              name: "salesstep3",
              url: "sales/step3",
              defaults: new { controller = "Sales", action = "Step3" }
           );

            routes.MapRoute(
             name: "salesstep4",
             url: "sales/step4",
             defaults: new { controller = "Sales", action = "Step4" }
           );

            routes.MapRoute(
            name: "salesstep5",
            url: "sales/step5",
            defaults: new { controller = "Sales", action = "Step5" }
          );

            routes.MapRoute(
           name: "salesstep6",
           url: "sales/step6",
           defaults: new { controller = "Sales", action = "Step6" }
         );

            routes.MapRoute(
           name: "salespay",
           url: "sales/pay",
           defaults: new { controller = "Sales", action = "Pay" }
         );

            routes.MapRoute(
            name: "salespdf",
            url: "sales/pdf",
            defaults: new { controller = "Sales", action = "Pdf" }
          );

            routes.MapRoute(
        name: "salesinformationform",
        url: "sales/informationform",
        defaults: new { controller = "Sales", action = "InformationForm" }
      );

            routes.MapRoute(
              name: "saleCounties",
              url: "Sales/Counties",
              defaults: new { controller = "Sales", action = "Counties" }
          );

            routes.MapRoute(
            name: "saleBankBranches",
            url: "Sales/BankBranches",
            defaults: new { controller = "Sales", action = "BankBranches" }
        );

            routes.MapRoute(
         name: "salesfinish",
         url: "sales/finish",
         defaults: new { controller = "Sales", action = "Finish" }
       );

            routes.MapRoute(
      name: "error500",
      url: "Error/error-500",
      defaults: new { controller = "Error", action = "status500" }
    );

            routes.MapRoute(
    name: "error404",
    url: "Error/error-404",
    defaults: new { controller = "Error", action = "status404" }
  );

            routes.MapRoute(
                name: "productlist",
                url: "urunler",
                defaults: new { controller = "Product", action = "List" }
            );

            routes.MapRoute(
               name: "productlistkey",
               url: "{key}/urunler",
               defaults: new { controller = "Product", action = "List", key = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "getproducts",
               url: "product/getproducts",
               defaults: new { controller = "Product", action = "getproducts" }
           );

            routes.MapRoute(
              name: "salesproduct",
              url: "hayat/{id}/{name}/{step}",
              defaults: new { controller = "Sales", action = "Product", id = UrlParameter.Optional, name = UrlParameter.Optional }
           );

            routes.MapRoute(
            name: "salesbesplan",
            url: "bes/{id}/{name}/{step}",
            defaults: new { controller = "Sales", action = "BesPlan", id = UrlParameter.Optional, name = UrlParameter.Optional }
         );

            routes.MapRoute(
            name: "salescredit",
            url: "krediteminati/{id}/{name}/{step}",
            defaults: new { controller = "Sales", action = "Credit", id = UrlParameter.Optional, name = UrlParameter.Optional }
         );

            routes.MapRoute(
               name: "saleskey2",
               url: "hayat/{key}/{id}/{name}/{step}",
               defaults: new { controller = "Sales", action = "Product", key = UrlParameter.Optional, id = UrlParameter.Optional, name = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "saleskey",
              url: "bes/{key}/{id}/{name}/{step}",
              defaults: new { controller = "Sales", action = "BesPlan", key = UrlParameter.Optional, id = UrlParameter.Optional, name = UrlParameter.Optional }
           );

            routes.MapRoute(
            name: "saleskey3",
            url: "krediteminati/{key}/{id}/{name}/{step}",
            defaults: new { controller = "Sales", action = "Credit", key = UrlParameter.Optional, id = UrlParameter.Optional, name = UrlParameter.Optional }
         );

            routes.MapRoute(
            name: "saleskey31",
            url: "sales/BankCities",
            defaults: new { controller = "Sales", action = "BankCities" }
         );

            routes.MapRoute(
            name: "saleskey32",
            url: "sales/BankBranches",
            defaults: new { controller = "Sales", action = "BankBranches" }
         );

            routes.MapRoute(
           name: "smspolicy",
           url: "sms/police/{token}",
           defaults: new { controller = "Sms", action = "Policy", token = UrlParameter.Optional }
        );

            routes.MapRoute(
         name: "smsbes",
         url: "pdf/bes/{token}",
         defaults: new { controller = "Sms", action = "Bes", token = UrlParameter.Optional }
      );

            routes.MapRoute(
       name: "smslife",
       url: "pdf/life/{token}",
       defaults: new { controller = "Sms", action = "Life", token = UrlParameter.Optional }
    );

            routes.MapRoute(
     name: "smscredit",
     url: "pdf/credit/{token}",
     defaults: new { controller = "Sms", action = "Credit", token = UrlParameter.Optional }
  );

            routes.MapRoute(
          name: "smsdocument",
          url: "sms/document",
          defaults: new { controller = "Sms", action = "Document", token = UrlParameter.Optional }
       );

            routes.MapRoute(
         name: "formlogs",
         url: "logs/forms",
         defaults: new { controller = "Logs", action = "Forms" }
      );

            routes.MapRoute(
                name: "Default",
                url: "{key}",
                defaults: new { controller = "Product", action = "Index", key = UrlParameter.Optional }
            );
        }
    }
}