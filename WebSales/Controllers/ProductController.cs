﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using WebSales.Classes;
using System.Web.Script.Serialization;
using ModelLibrary;

namespace WebSales.Controllers
{
    public class ProductController : Controller
    {
        #region BindList
        private List<cProductList> BindList(string strKey, int intBannerID, bool bolSlider)
        {
            List<cProductList> listSlider = new List<cProductList>();
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    List<int> sDefaultCallcenterID = ConfigurationManager.AppSettings["DefaultCallcenter"].ToString().Split(',').Select(s => int.Parse(s)).ToList();

                    var productSliders = db.tblProducts.Where(w => !w.bolScratchWin && w.bolIsActive && !w.bolIsDelete && w.bolPublished && (bolSlider ? w.bolSlider : true) && w.bolWebSale && w.tblCallcenterProducts.Count(c => c.tblCallcenters.intSaleTypeID == 2 && (string.IsNullOrEmpty(strKey) ? sDefaultCallcenterID.Count(cS => cS == c.intCallcenterID) > 0 : c.tblCallcenters.strPublishKey == strKey) && c.bolSpecialSale) > 0).ToList();

                    var besSliders = db.tblBesPlans.Where(w => w.bolIsActive && !w.bolIsDelete && w.bolPublished && (bolSlider ? w.bolSlider : true) && w.bolWebSale && w.tblCallcenterBesPlans.Count(c => c.tblCallcenters.intSaleTypeID == 2 && (string.IsNullOrEmpty(strKey) ? sDefaultCallcenterID.Count(cS => cS == c.intCallcenterID) > 0 : c.tblCallcenters.strPublishKey == strKey) && c.bolSpecialSale) > 0).ToList();

                    var credits = db.tblCredits.Where(w => w.bolIsActive && !w.bolIsDelete && w.bolPublished && (bolSlider ? w.bolSlider : true) && w.bolWebSale && w.tblCallcenterCredits.Count(c => c.tblCallcenters.intSaleTypeID == 2 && (string.IsNullOrEmpty(strKey) ? sDefaultCallcenterID.Count(cS => cS == c.intCallcenterID) > 0 : c.tblCallcenters.strPublishKey == strKey) && c.bolSpecialSale) > 0).ToList();

                    string strProductImage = ConfigurationManager.AppSettings["ProductImage"];
                    string strProductImageThumb = ConfigurationManager.AppSettings["ProductImageThumb"];
                    string strProductVideo = ConfigurationManager.AppSettings["ProductVideo"];
                    string strBesPlanImage = ConfigurationManager.AppSettings["BesPlanImage"];
                    string strBesPlanImageThumb = ConfigurationManager.AppSettings["BesPlanThumb"];
                    string strBesPlanVideo = ConfigurationManager.AppSettings["BesPlanVideo"];
                    string strCreditImage = ConfigurationManager.AppSettings["CreditImage"];
                    string strCreditImageThumb = ConfigurationManager.AppSettings["CreditImageThumb"];
                    string strCreditVideo = ConfigurationManager.AppSettings["CreditVideo"];
                    int intProductSliders = 0;
                    int intBesSliders = 0;
                    int intCreditSliders = 0;

                    int intTotalCount = (productSliders != null ? productSliders.Count : 0) + (besSliders != null ? besSliders.Count : 0) + (credits != null ? credits.Count : 0);
                    for (int i = 0; i < intTotalCount; i++)
                    {
                        if (besSliders != null)
                        {
                            if (intBesSliders < besSliders.Count)
                            {
                                try
                                {
                                    listSlider.Add(new cProductList
                                    {
                                        image = strBesPlanImage + besSliders[intBesSliders].strImage,
                                        url = "/bes/" + (!string.IsNullOrEmpty(strKey) ? strKey + "/" : "") + besSliders[intBesSliders].intBesPlanID + "/" + Utilities.Friendly(besSliders[intBesSliders].strPlanName) + "/1" + (intBannerID > 0 ? "?bid=" + intBannerID : ""),
                                        name = besSliders[intBesSliders].strPlanName,
                                        spot = besSliders[intBesSliders].strPublishSpot,
                                        imagethumb = strBesPlanImageThumb + besSliders[intBesSliders].strImageThumb,
                                        video = string.IsNullOrEmpty(besSliders[intBesSliders].strVideo) ? "" : strBesPlanVideo + besSliders[intBesSliders].strVideo
                                    });
                                }
                                catch { }
                                intBesSliders++;
                            }
                        }

                        if (productSliders != null)
                        {
                            if (intProductSliders < productSliders.Count)
                            {
                                try
                                {
                                    listSlider.Add(new cProductList
                                    {
                                        image = strProductImage + productSliders[intProductSliders].strImage,
                                        url = "/hayat/" + (!string.IsNullOrEmpty(strKey) ? strKey + "/" : "") + productSliders[intProductSliders].intProductID + "/" + Utilities.Friendly(productSliders[intProductSliders].strName) + "/1" + (intBannerID > 0 ? "?bid=" + intBannerID : ""),
                                        name = productSliders[intProductSliders].strName,
                                        spot = productSliders[intProductSliders].strPublishSpot,
                                        imagethumb = strProductImageThumb + productSliders[intProductSliders].strImageThumb,
                                        video = string.IsNullOrEmpty(productSliders[intProductSliders].strVideo) ? "" : strProductVideo + productSliders[intProductSliders].strVideo
                                    });
                                }
                                catch { }
                                intProductSliders++;
                            }
                        }

                        if (credits != null)
                        {
                            if (intCreditSliders < credits.Count)
                            {
                                try
                                {
                                    listSlider.Add(new cProductList
                                    {
                                        image = strCreditImage + credits[intCreditSliders].strImage,
                                        url = "/krediteminati/" + (!string.IsNullOrEmpty(strKey) ? strKey + "/" : "") + credits[intCreditSliders].intCreditID + "/" + Utilities.Friendly(credits[intCreditSliders].strName) + "/1" + (intBannerID > 0 ? "?bid=" + intBannerID : ""),
                                        name = credits[intCreditSliders].strName,
                                        spot = credits[intCreditSliders].strPublishSpot,
                                        imagethumb = strCreditImageThumb + credits[intCreditSliders].strImageThumb,
                                        video = string.IsNullOrEmpty(credits[intCreditSliders].strVideo) ? "" : strCreditVideo + credits[intCreditSliders].strVideo
                                    });
                                }
                                catch { }
                                intCreditSliders++;
                            }
                        }
                    }
                }
            }
            catch 
            { }

            return listSlider;
        }
        #endregion

        #region Index
        [cBannerLog]
        public ActionResult Index()
        {
            bool bolClear = true;
            if (Sessions.SalePageTime != null)
            {
                var salepage = Sessions.SalePageTime.time;
                TimeSpan time = DateTime.Now.Subtract(salepage);
                if (time.TotalMinutes > 3)
                {
                    bolClear = true;
                }
                else
                {
                    bolClear = false;
                }
            }

            if (bolClear)
            {
                Cookies.WebSaleLog = new WebSaleLog { ID = 0, SaleType = "" };
                Cookies.Step = 1;
                Cookies.StepInfo123 = "";
                Cookies.StepInfo4 = "";
                Cookies.RemoveCrediNo();
                Cookies.RemoveCreditSessionKeys();
            }

            var _data = BindList(RouteData.Values["key"] != null ? RouteData.Values["key"].ToString() : "", QueryStrings.BannerID, true);

            return View(_data);
        }
        #endregion

        #region List
        [cBannerLog]
        public ActionResult List()
        {
            // internet explorerda sebebini bulamadığım bir nedenden dolayı satış ekranlarında burası da tetikleniyor. o yüzden kontrol ekledim
            //bool bolList = true;
            //if (Request.Browser.Browser.ToLower().Contains("explorer"))
            //{
            //    if (Session["step"] != null)
            //    {
            //        bolList = false;

            //        if (Session["step"].ToString() == "2")
            //            Session["step"] = null;
            //        else
            //            Session["step"] = "2";
            //    }
            //}

            //if (!bolList)
            //    return View(new cProductList());

            Cookies.WebSaleLog = new WebSaleLog { ID = 0, SaleType = "" };
            Cookies.Step = 1;
            Cookies.StepInfo123 = "";
            Cookies.StepInfo4 = "";
            Cookies.RemoveCrediNo();
            Cookies.RemoveCreditSessionKeys();

            var _data = BindList(RouteData.Values["key"] != null ? RouteData.Values["key"].ToString() : "", QueryStrings.BannerID, false);

            return View(_data);
        }
        #endregion

        #region GetProducts
        public JsonResult GetProducts(string key, int bid, int page)
        {
            using (DBEntities db = new DBEntities())
            {
                var _data = BindList(key, bid, false);

                int intCount = 0;
                if (_data.Count > 0)
                {
                    int intPageSize = Utilities.NullFixInt(ConfigurationManager.AppSettings["MainProductCount"]);
                    intCount = (_data.Count / intPageSize) + (_data.Count % intPageSize > 0 ? 1 : 0);
                    page = page > 0 ? page - 1 : page;

                    _data = _data.Skip(page * intPageSize).Take(intPageSize).ToList();

                    foreach (var item in _data)
                    {
                        item.intCount = intCount;
                    }
                }

                return Json(_data, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}