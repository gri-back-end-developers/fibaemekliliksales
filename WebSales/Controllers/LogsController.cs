﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSales.Classes;
using ModelLibrary;
using System.Net;
using System.IO;

namespace WebSales.Controllers
{
    public class LogsController : Controller
    {
        public ActionResult Forms(string type, string polid, string name, string surname)
        {
            HtmlString strHtml = null;

            try
            {
                decimal intPolID = Utilities.NullFixDecimal(polid);
                if (intPolID > 0 && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(surname))
                {
                    if (type == "mesafelisatis")
                    {
                        strHtml = new HtmlString(GetHtml(ConfigurationManager.AppSettings["siteUrl"] + "/html/mesafeli-sozlesme.html"));
                    }
                    else if (type == "bilgilendirme")
                    {
                        strHtml = new HtmlString(GetHtml(ConfigurationManager.AppSettings["AcenteUrl"] + "/ProductSale/infoform?name=" + name + " " + surname + "&c=1"));
                    }

                    using (DBEntities db = new DBEntities())
                    {
                        db.tblFormLogs.Add(new tblFormLogs
                        {
                            dtRegisterDate = DateTime.Now,
                            intPolID = (int)intPolID,
                            strFormType = type,
                            strNameSurname = name + " " + surname
                        });
                        db.SaveChanges();
                    }
                }
            }
            catch { }

            return View(strHtml);
        }

        private string GetHtml(string strUrl)
        {
            WebRequest request = WebRequest.Create(strUrl);

            using (WebResponse response = request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
    }
}