﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSales.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult status404()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult status500()
        {
            Response.StatusCode = 500;
            return View();
        }

        public ActionResult status403()
        {
            Response.StatusCode = 403;
            return Redirect("https://www.fibaemeklilik.com.tr/403.aspx");
        }
    }
}
