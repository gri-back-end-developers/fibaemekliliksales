﻿#region Directives
using ModelLibrary;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebSales.Classes;
#endregion

namespace WebSales.Controllers
{
    public class SalesController : Controller
    {
        #region Member variables
        cTcknInfo tcknInfo;
        #endregion

        #region CallcenterID
        private int CallcenterID(DBEntities db, string strPublishKey, int intProductID, int intBesPlanID, int intCreditID)
        {
            try
            {
                int intCallcenterID = 0;
                if (!string.IsNullOrEmpty(strPublishKey))
                {
                    strPublishKey = Utilities.SQLInjection(strPublishKey);

                    var callcenter = db.tblCallcenters.Where(w => w.strPublishKey == strPublishKey && w.bolIsActive && !w.bolIsDelete).FirstOrDefault();
                    if (callcenter != null)
                    {
                        intCallcenterID = callcenter.intCallcenterID;
                    }
                }
                else if (intProductID > 0)
                {
                    var callcenter = db.tblCallcenterProducts.Where(w => w.intProductID == intProductID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                    if (callcenter != null)
                    {
                        intCallcenterID = callcenter.intCallcenterID;
                    }
                }
                else if (intBesPlanID > 0)
                {
                    var callcenter = db.tblCallcenterBesPlans.Where(w => w.intBesPlanID == intBesPlanID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                    if (callcenter != null)
                    {
                        intCallcenterID = callcenter.intCallcenterID;
                    }
                }
                else if (intCreditID > 0)
                {
                    var callcenter = db.tblCallcenterCredits.Where(w => w.intCreditID == intCreditID && w.tblCallcenters.bolIsActive && !w.tblCallcenters.bolIsDelete && w.tblCallcenters.intSaleTypeID != 0).FirstOrDefault();
                    if (callcenter != null)
                    {
                        intCallcenterID = callcenter.intCallcenterID;
                    }
                }

                Sessions.CallcenterID = intCallcenterID;

                return intCallcenterID;
            }
            catch { }

            return 0;
        }
        #endregion

        #region TCKNValid
        private bool TCKNValid(string strCredit = null)
        {
            bool bolTCKN = false;

            AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient();
            try
            {
                var kpsBireySorgu = aileService.kpsBireySorgu(new AileBireySorgu.bireySorguInputType()
                {
                    cacheStyle = 1,
                    maxAge = 180,
                    kimlikNo = Utilities.NullFixLong(Request.Form["tckn"]),
                    kimlikNoSpecified = true
                });

                tcknInfo = new cTcknInfo();
                tcknInfo.strBirthplace = kpsBireySorgu.dogumYeri;
                tcknInfo.strFatherName = kpsBireySorgu.babaAdi;
                tcknInfo.strGender = kpsBireySorgu.cinsiyeti;
                tcknInfo.strMaritalStatus = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B";
                tcknInfo.strMotherName = kpsBireySorgu.anneAdi;

                if (strCredit != null)
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var kpsTCKisiAcikAdresSorgu = aileService.kpsTCKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(Request.Form["tckn"]),
                            kimlikNoSpecified = true
                        });

                        if (kpsTCKisiAcikAdresSorgu.kimlikNo > 0)
                        {
                            tcknInfo.strAdress = kpsTCKisiAcikAdresSorgu.acikAdres;
                            tcknInfo.strDistrictCode = kpsTCKisiAcikAdresSorgu.ilceKodu;
                            tcknInfo.strRegionCode = kpsTCKisiAcikAdresSorgu.ilKodu;
                            tcknInfo.strCountryCode = "TR";
                        }

                        tcknInfo.strBirthRegionCode = "";
                        tcknInfo.strBirthDistrictCode = "";

                        var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                        if (il != null)
                            tcknInfo.strBirthRegionCode = il.strCode.Trim();

                        var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                        if (ilce != null)
                            tcknInfo.strBirthDistrictCode = ilce.strCode.Trim();

                        if (tcknInfo.strBirthRegionCode == "" && tcknInfo.strBirthDistrictCode != "")
                        {
                            tcknInfo.strBirthRegionCode = tcknInfo.strBirthDistrictCode.Substring(0, 3);
                        }

                        if (tcknInfo.strBirthRegionCode == "")
                        {
                            tcknInfo.strBirthRegionCode = "99";
                        }

                        if (tcknInfo.strBirthRegionCode != "" && tcknInfo.strBirthDistrictCode == "")
                        {
                            tcknInfo.strBirthDistrictCode = "99900";
                        }
                    }
                }

                bool bolNameSurnameValid = kpsBireySorgu.adi.ToLower().Trim() + " " + kpsBireySorgu.soyadi.ToLower().Trim() == Request.Form["name"].ToLower().Trim() + " " + Request.Form["surname"].ToLower().Trim();
                bool bolBirthDateValid = kpsBireySorgu.dogumTarihi.ToString("dd-MM-yyyy") == Request.Form["day"] + "-" + Request.Form["month"] + "-" + Request.Form["year"];

                bolTCKN = bolNameSurnameValid && bolBirthDateValid;
            }
            catch
            {
                try
                {
                    var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new AileBireySorgu.bireySorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(Request.Form["tckn"]),
                        kimlikNoSpecified = true
                    });

                    tcknInfo = new cTcknInfo();
                    tcknInfo.strFatherName = kpsYabanciSorgu.babaAdi;
                    tcknInfo.strGender = kpsYabanciSorgu.cinsiyeti;

                    if (strCredit != null)
                    {
                        var kpsYabanciAcikAdresSorgu = aileService.kpsYabanciKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                        {
                            cacheStyle = 1,
                            maxAge = 180,
                            kimlikNo = Utilities.NullFixLong(Request.Form["tckn"]),
                            kimlikNoSpecified = true
                        });

                        if (kpsYabanciAcikAdresSorgu.kimlikNo > 0)
                        {
                            tcknInfo.strAdress = kpsYabanciAcikAdresSorgu.acikAdres;
                            tcknInfo.strDistrictCode = "99900";
                            tcknInfo.strRegionCode = "99";

                            using (DBEntities db = new DBEntities())
                            {
                                var uyr = db.tblMapping.Where(w => w.strType == "UYR" && !string.IsNullOrEmpty(w.strName.Trim()) && w.strCode == kpsYabanciSorgu.uyrukKod.ToString()).FirstOrDefault();

                                if (uyr != null)
                                    tcknInfo.strCountryCode = uyr != null ? uyr.strCountryCode : kpsYabanciSorgu.uyrukAd;
                            }
                        }

                        tcknInfo.strBirthRegionCode = "99";
                        tcknInfo.strBirthDistrictCode = "99900";
                    }

                    bool bolNameSurnameValid = kpsYabanciSorgu.adi.ToLower().Trim() + " " + kpsYabanciSorgu.soyadi.ToLower().Trim() == Request.Form["name"].ToLower().Trim() + " " + Request.Form["surname"].ToLower().Trim();
                    bool bolBirthDateValid = kpsYabanciSorgu.dogumTarihi.ToString("dd-MM-yyyy") == Request.Form["day"] + "-" + Request.Form["month"] + "-" + Request.Form["year"];

                    bolTCKN = bolNameSurnameValid && bolBirthDateValid;
                }
                catch { }
            }

            return bolTCKN;
        }
        #endregion

        #region Captcha
        private bool Captcha
        {
            get
            {
                var response = Request["g-recaptcha-response"];
                var client = new WebClient();
                var reply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", ConfigurationManager.AppSettings["CaptchaPrivateKey"], response));
                JObject JsonText = JObject.Parse(reply);
                dynamic captchaResponse = JsonText;
                if (captchaResponse.success == "false")
                    return false;
                else
                    return true;
            }
        }
        #endregion

        #region Finish
        public ActionResult Finish()
        {
            Session["step"] = "1";

            int intWebSaleLogID = QueryStrings.ID;
            string strToken = QueryStrings.Token;
            string strSaleType = QueryStrings.SaleType;

            using (DBEntities db = new DBEntities())
            {
                if (intWebSaleLogID == 0 || string.IsNullOrEmpty(strToken))
                {
                    Logs.Error(db, new Exception("ID veya token boş geldi. ID : " + intWebSaleLogID + " token : " + strToken), intWebSaleLogID);

                    Cookies.Step = 1;
                    Cookies.WebSaleLog = new WebSaleLog { ID = 0, SaleType = "" };
                    Session["SalePageTime"] = null;

                    return Redirect(Sessions.SalePage == "" ? "/" + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
                }

                try
                {
                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    if (string.IsNullOrEmpty(webSaleLog.strToken) || webSaleLog.strToken != strToken)
                    {
                        Logs.Error(db, new Exception("webSaleLog.strToken boş veya strToken farklı. token : " + strToken), intWebSaleLogID);

                        Cookies.Step = 1;
                        Cookies.WebSaleLog = new WebSaleLog { ID = 0, SaleType = "" };
                        Session["SalePageTime"] = null;

                        return Redirect(Sessions.SalePage == "" ? "/" + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
                    }

                    bool bolApproved = false;
                    cPayResultData payResultData = new cPayResultData();

                    Enums.PayType payType = Enums.PayType.Garanti3D;
                    if (strSaleType == "hayat")
                    {
                        payType = (Enums.PayType)webSaleLog.tblWebSaleInfo.First().intPayTypeID;
                    }
                    else if (strSaleType == "bes")
                    {
                        payType = (Enums.PayType)webSaleLog.tblWebSaleBesPlanInfo.First().intPayTypeID;
                    }
                    else if (strSaleType == "krediteminati")
                    {
                        payType = (Enums.PayType)webSaleLog.tblWebSaleCreditInfo.First().intPayTypeID;
                    }

                    var webSalePays = db.tblWebSalePay.Where(w => w.intWebSaleLogID == intWebSaleLogID).ToList();

                    if (payType == Enums.PayType.Akbank3D)
                    {
                        #region pay code
                        String[] odemeparametreleri = new String[] { "AuthCode", "Response", "HostRefNum", "ProcReturnCode", "TransId", "ErrMsg" };
                        System.Collections.IEnumerator e = Request.Form.GetEnumerator();

                        String hashparams = Request.Form.Get("HASHPARAMS");
                        String hashparamsval = Request.Form.Get("HASHPARAMSVAL");
                        String storekey = ConfigurationManager.AppSettings["StoreKey"];
                        String paramsval = "";
                        int index1 = 0, index2 = 0;
                        do
                        {
                            index2 = hashparams.IndexOf(":", index1);
                            String val = Request.Form.Get(hashparams.Substring(index1, index2 - index1)) == null ? "" : Request.Form.Get(hashparams.Substring(index1, index2 - index1));
                            paramsval += val;
                            index1 = index2 + 1;
                        }
                        while (index1 < hashparams.Length);

                        String hashval = paramsval + storekey;
                        String hashparam = Request.Form.Get("HASH");
                        SHA1 sha = new SHA1CryptoServiceProvider();
                        byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                        byte[] inputbytes = sha.ComputeHash(hashbytes);
                        String hash = Convert.ToBase64String(inputbytes);

                        String mdStatus = Request.Form.Get("mdStatus");

                        string strJson = "";
                        while (e.MoveNext())
                        {
                            String xkey = (String)e.Current;
                            String xval = Request.Form.Get(xkey);
                            bool ok = true;

                            for (int i = 0; i < odemeparametreleri.Length; i++)
                            {
                                if (xkey.Equals(odemeparametreleri[i]))
                                {
                                    ok = false;
                                    break;
                                }
                            }

                            if (ok)
                            {
                                strJson += ", \"" + xkey + "\" : \"" + xval + "\"";
                            }
                        }

                        if (strJson != "")
                        {
                            strJson = "[ {" + strJson.Remove(0, 1) + " } ]";
                        }

                        payResultData.strTransID = Request.Form.Get("TransId");
                        payResultData.strOrderID = Request.Form.Get("ReturnOid");
                        payResultData.strHostrefnum = Request.Form.Get("HostRefNum");
                        payResultData.strAuthCode = Request.Form.Get("AuthCode");

                        if (mdStatus.Equals("1") || mdStatus.Equals("2") || mdStatus.Equals("3") || mdStatus.Equals("4"))
                        {
                            if ("Approved".Equals(Request.Form.Get("Response")))
                            {
                                bolApproved = true;
                            }
                            else
                            {
                                bolApproved = false;
                                payResultData.strErrMsg = Request.Form.Get("ErrMsg");
                            }
                        }
                        else
                        {
                            bolApproved = false;
                            payResultData.strErrMsg = Request.Form.Get("ErrMsg");
                        }
                        #endregion
                    }
                    else if (payType == Enums.PayType.Garanti3D)
                    {
                        #region pay code
                        //string strMDStatusText = Request.Form.Get("errmsg");
                        //string strMDStatusText_ = Request.Form.Get("mderrormessage");
                        //string strStoreKey = ConfigurationManager.AppSettings["GarantiStoreKey"];
                        payResultData.strOrderID = Request.Form.Get("oid");
                        payResultData.strTransID = Server.UrlEncode(Request.Form.Get("transid"));
                        payResultData.strHostrefnum = Request.Form.Get("hostrefnum");
                        payResultData.strAuthCode = Request.Form.Get("authcode");

                        //String responseHashparams = Request.Form.Get("hashparams");
                        //String responseHashparamsval = Request.Form.Get("hashparamsval");
                        //String responseHash = Request.Form.Get("hash");

                        //if (responseHashparams != null && !"".Equals(responseHashparams))
                        //{
                        //    String digestData = "";
                        //    char[] separator = new char[] { ':' };
                        //    String[] paramList = responseHashparams.Split(separator);

                        //    foreach (String param in paramList)
                        //    {
                        //        digestData += Request.Form.Get(param) == null ? "" : Request.Form.Get(param);
                        //    }
                        //    digestData += strStoreKey;

                        //    System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                        //    byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(digestData);
                        //    byte[] inputbytes = sha.ComputeHash(hashbytes);
                        //    String hashCalculated = Convert.ToBase64String(inputbytes);

                        //    if (responseHash.Equals(hashCalculated) && Request.Form.Get("procreturncode") == "00")
                        //    {
                        //        bolApproved = true;
                        //    }
                        //}  

                        string strMode = Request.Form.Get("mode");
                        string strApiVersion = Request.Form.Get("apiversion");
                        string strTerminalProvUserID = Request.Form.Get("terminalprovuserid");
                        string strTerminalUserID = Request.Form.Get("terminaluserid");
                        string strTerminalMerchantID = Request.Form.Get("terminalmerchantid");
                        string strType = Request.Form.Get("txntype");
                        string strAmount = Request.Form.Get("txnamount");
                        string strCurrencyCode = Request.Form.Get("txncurrencycode");
                        string strInstallmentCount = Request.Form.Get("txninstallmentcount");
                        string strOrID = Request.Form.Get("orderid");
                        string strSuccessURL = Request.Form.Get("successurl");
                        string strErrorURL = Request.Form.Get("errorurl");
                        string strcustomeremailaddress = Request.Form.Get("customeremailaddress");
                        string strCustomeripaddress = Request.Form.Get("customeripaddress");
                        //string strOrderItemProductid1 = Request.Form.Get("orderitemproductid1");
                        //string strOrderItemPrice1 = Request.Form.Get("orderitemprice1");
                        //string strOrderItemDescription1 = Request.Form.Get("orderitemdescription1");
                        string strOrderAddressCity1 = Request.Form.Get("orderaddresscity1");
                        string strOrderAddressCountry1 = Request.Form.Get("orderaddresscountry1");
                        string strOrderAddressDistrict1 = Request.Form.Get("orderaddressdistrict1");
                        string strOrderAddressLastname1 = Request.Form.Get("orderaddresslastname1");
                        string strOrderAddressName1 = Request.Form.Get("orderaddressname1");
                        string strOrderAddressPhonenumber1 = Request.Form.Get("orderaddressphonenumber1");
                        string strOrderAddressText1 = Request.Form.Get("orderaddresstext1");
                        string strTerminalID = Request.Form.Get("clientid");
                        string _strTerminalID = "0" + strTerminalID;
                        string strStoreKey = ConfigurationManager.AppSettings["GarantiStoreKey"];
                        string strProvisionPassword = ConfigurationManager.AppSettings["GarantiProvisionPassword"];
                        string strCardholderPresentCode = "13";
                        string strMotoInd = "N";

                        //var CardInfo = Sessions.CardInfo;
                        //string strNumber = CardInfo.cardno;
                        //string strExpireDate = CardInfo.month + CardInfo.year;
                        //string strCVV2 = CardInfo.cvv2; 
                        string strNumber = "";
                        string strExpireDate = "";
                        string strCVV2 = "";
                        string strAuthenticationCode = Server.UrlEncode(Request.Form.Get("cavv"));
                        string strSecurityLevel = Server.UrlEncode(Request.Form.Get("eci"));
                        string strTxnID = Server.UrlEncode(Request.Form.Get("xid"));
                        string strMD = Server.UrlEncode(Request.Form.Get("md"));
                        string strMDStatus = Request.Form.Get("mdstatus");
                        string strMDStatusText = Request.Form.Get("mderrormessage");
                        string strHostAddress = ConfigurationManager.AppSettings["GarantiHostAdress"];
                        string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                        string ValidateHashData = GetSHA1(strTerminalID + strOrID + strAmount + strSuccessURL + strErrorURL + strType + strInstallmentCount + strStoreKey + SecurityData).ToUpper();
                        string strHashData = Request.Form.Get("secure3dhash");

                        string HashData = GetSHA1(strOrID + strTerminalID + strAmount + SecurityData).ToUpper();
                        string responseFromServer = "";

                        if (strHashData == ValidateHashData)
                        {
                            if (strMDStatus == "1" | strMDStatus == "2" | strMDStatus == "3" | strMDStatus == "4")
                            {
                                string strTaxNumber = "";
                                var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == Sessions.CallcenterID).FirstOrDefault();
                                if (callcenter != null)
                                {
                                    strTaxNumber = callcenter.strTaxNumber;
                                }

                                string strXML = null;
                                strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                    + "<GVPSRequest>"
                                        + "<Mode>" + strMode + "</Mode>"
                                        + "<Version>" + strApiVersion + "</Version>"
                                        + "<ChannelCode></ChannelCode>"
                                        + "<Terminal>"
                                            + "<ProvUserID>" + strTerminalProvUserID + "</ProvUserID>"
                                            + "<HashData>" + HashData + "</HashData>"
                                            + "<UserID>" + strTerminalUserID + "</UserID>"
                                            + "<ID>" + strTerminalID + "</ID>"
                                            + "<MerchantID>" + strTerminalMerchantID + "</MerchantID>"
                                        + "</Terminal>"
                                        + "<Customer>"
                                            + "<IPAddress>" + strCustomeripaddress + "</IPAddress>"
                                            + "<EmailAddress>" + strcustomeremailaddress + "</EmailAddress>"
                                        + "</Customer>"
                                        + "<Card>"
                                            + "<Number>" + strNumber + "</Number>"
                                            + "<ExpireDate>" + strExpireDate + "</ExpireDate>"
                                            + "<CVV2>" + strCVV2 + "</CVV2>"
                                        + "</Card>"
                                        + "<Order>"
                                            + "<OrderID>" + strOrID + "</OrderID>"
                                            + "<GroupID></GroupID>"
                                            + "<AddressList>"
                                                + "<Address>"
                                                    + "<Type>B</Type>"
                                                    + "<Name>" + strOrderAddressName1 + "</Name>"
                                                    + "<LastName>" + strOrderAddressLastname1 + "</LastName>"
                                                    + "<Company></Company>"
                                                    + "<Text>" + strOrderAddressText1 + "</Text>"
                                                    + "<District>" + strOrderAddressDistrict1 + "</District>"
                                                    + "<City>" + strOrderAddressCity1 + "</City>"
                                                    + "<PostalCode></PostalCode>"
                                                    + "<Country>" + strOrderAddressCountry1 + "</Country>"
                                                    + "<PhoneNumber>" + strOrderAddressPhonenumber1 + "</PhoneNumber>"
                                                + "</Address>"
                                            + "</AddressList>"
                                        + "</Order>"
                                        + "<Transaction>"
                                            + "<Type>" + strType + "</Type>"
                                            + "<InstallmentCnt>" + strInstallmentCount + "</InstallmentCnt>"
                                            + "<Amount>" + strAmount + "</Amount>"
                                            + "<CurrencyCode>" + strCurrencyCode + "</CurrencyCode>"
                                            + "<CardholderPresentCode>" + strCardholderPresentCode + "</CardholderPresentCode>"
                                            + "<MotoInd>" + strMotoInd + "</MotoInd>"
                                            + "<Secure3D>"
                                                + "<AuthenticationCode>" + strAuthenticationCode + "</AuthenticationCode>"
                                                + "<SecurityLevel>" + strSecurityLevel + "</SecurityLevel>"
                                                + "<TxnID>" + strTxnID + "</TxnID>"
                                                + "<Md>" + strMD + "</Md>"
                                            + "</Secure3D>"
                                         + "<Verification>"
                                            + "<Identity>" + strTaxNumber + "</Identity>"
                                         + "</Verification>"
                                        + "</Transaction>"
                                    + "</GVPSRequest>";

                                try
                                {
                                    string data = "data=" + strXML;

                                    WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                                    _WebRequest.Method = "POST";

                                    byte[] byteArray = Encoding.UTF8.GetBytes(data);
                                    _WebRequest.ContentType = "application/x-www-form-urlencoded";
                                    _WebRequest.ContentLength = byteArray.Length;

                                    Stream dataStream = _WebRequest.GetRequestStream();
                                    dataStream.Write(byteArray, 0, byteArray.Length);
                                    dataStream.Close();

                                    WebResponse _WebResponse = _WebRequest.GetResponse();
                                    dataStream = _WebResponse.GetResponseStream();

                                    string strStatusMessage = ((HttpWebResponse)_WebResponse).StatusDescription;

                                    StreamReader reader = new StreamReader(dataStream);
                                    responseFromServer = reader.ReadToEnd();

                                    if (responseFromServer.Contains("<ReasonCode>00</ReasonCode>"))
                                    {
                                        bolApproved = true;
                                    }
                                    else
                                    {
                                        bolApproved = false;
                                        payResultData.strErrMsg = "İşlem Başarısız";
                                    }

                                }
                                catch (Exception ex)
                                {
                                    bolApproved = false;
                                    payResultData.strErrMsg = ex.Message;
                                }

                            }
                            else
                            {
                                bolApproved = false;
                                payResultData.strErrMsg = "İşlem başarısız!";
                            }
                        }
                        else
                        {
                            bolApproved = false;
                            payResultData.strErrMsg = "Güvenlik Uyarısı. Sayısal Imza Geçerli Degil";
                        }

                        if (!bolApproved)
                        {
                            payResultData.strErrMsg = "errmsg : " + strMDStatusText + " ---- mderrormessage : " + strMDStatusText;
                        }
                        else
                        {
                            if (webSalePays.Count(c => c.strOrderID == payResultData.strOrderID) > 0)
                            {
                                string strSecure3dHash = Request.Form.Get("secure3dhash");
                                if (webSalePays.Count(c => c.strOrderID == payResultData.strOrderID && c.strSecure3dHash == strSecure3dHash) == 0)
                                {
                                    bolApproved = false;
                                    payResultData.strErrMsg = "Güvenlik hatası!! hash uyuşmuyor.";
                                }
                            }
                            else
                            {
                                bolApproved = false;
                                payResultData.strErrMsg = "Güvenlik hatası!! orderid uyuşmuyor.";
                            }
                        }

                        if (ConfigurationManager.AppSettings["SanalPosLogTxt"].ToString() == "1")
                        {
                            try
                            {
                                string strReq = "";
                                foreach (var item in Request.Form.AllKeys)
                                {
                                    strReq += item + " : " + Request.Form[item] + " --- ";
                                }

                                LogWrite(intWebSaleLogID, strReq + " ::: " + responseFromServer, "Logs");
                            }
                            catch { }
                        }
                        #endregion
                    }

                    webSaleLog.strToken = "";

                    string strOrderID = payResultData.strOrderID;

                    var webSalePay = webSalePays.Where(w => w.strOrderID == strOrderID && w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSalePay != null)
                    {
                        webSalePay.bolApproved = bolApproved;
                        webSalePay.strTransID = payResultData.strTransID;
                        webSalePay.strErrMsg = payResultData.strErrMsg;
                        webSalePay.strHostrefnum = payResultData.strHostrefnum;
                        webSalePay.strAuthCode = payResultData.strAuthCode;
                    }

                    JavaScriptSerializer java = new JavaScriptSerializer();

                    if (bolApproved && webSalePay != null)
                    {
                        webSaleLog.bolIsFinished = true;
                        try
                        {
                            if (strSaleType == "hayat" || strSaleType == "krediteminati")
                            {
                                BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();

                                cPolicyData policyData = new cPolicyData();
                                if (strSaleType == "hayat")
                                {
                                    var webSaleInfo = webSaleLog.tblWebSaleInfo.First();
                                    policyData = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData2);
                                }
                                else
                                {
                                    var webSaleInfo = webSaleLog.tblWebSaleCreditInfo.First();
                                    policyData = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData);
                                }

                                BankaWebV2.BankawebV2PolresUser offerPolicy = new BankaWebV2.BankawebV2PolresUser();
                                if (strSaleType == "hayat")
                                {
                                    int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 0, strSaleType, "Finish", "Input", "{ \"tkl\" : \"" + policyData.PoliceNo + "\", \"insCan\" : \"1\", \"pKredino2\": \"\", \"krd\" : \"\" }", "teklifpolicelesme");

                                    offerPolicy = bankaWeb.teklifpolicelesme(policyData.PoliceNo, 1, "", "");

                                    Logs.WSLog(intWSLogID, intWebSaleLogID, 0, strSaleType, "New", "Output", java.Serialize(offerPolicy), "teklifpolicelesme");

                                    webSalePay.intPolicy = Utilities.NullFixInt(policyData.PoliceNo.Value.ToString());
                                }
                                else
                                {
                                    int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 0, strSaleType, "Finish", "Input", "{ \"tkl\" : \"" + policyData.PoliceNo + "\", \"insCan\" : \"1\", \"pKredino2\": \"" + policyData.KrediNo + "\", \"krd\" : \"" + policyData.KrediNo + "\" }", "teklifpolicelesme");

                                    var teklifpolicelesme = bankaWeb.teklifpolicelesme(policyData.PoliceNo, 1, policyData.KrediNo, policyData.KrediNo);

                                    Logs.WSLog(intWSLogID, intWebSaleLogID, 0, strSaleType, "Finish", "Output", java.Serialize(teklifpolicelesme), "teklifpolicelesme");

                                    webSalePay.intPolicy = Utilities.NullFixInt(teklifpolicelesme.policeno.ToString());
                                }

                                // bankaWeb.tanzimsmsgonder(webSalePay.intPolicy);

                                var CardInfo = Sessions.CardInfo;

                                using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                                {
                                    int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 0, strSaleType, "Finish", "Input", "", "spostaksitli3d");

                                    tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == CardInfo.paymentmethod).First();

                                    var spostaksitli3d = fibaws.spostaksitli3d(new FIBAWS.FibawsSanalposin3dUser
                                    {
                                        authcode = payResultData.strAuthCode,
                                        cvv = Utilities.NullFixDecimal(CardInfo.cvv2),
                                        expmonth = CardInfo.month,
                                        expyear = CardInfo.year,
                                        groupid = null,
                                        hostrefnum = payResultData.strHostrefnum,
                                        kartno = CardInfo.cardno,
                                        orderid = payResultData.strOrderID,
                                        polid = webSalePay.intPolicy,
                                        taksitadedi = paymentMethod.intInstallment,
                                        transid = payResultData.strTransID,
                                        tutar = CardInfo.paymentmethod == 3 ? policyData.IlkPrim : policyData.ToplamPirim,
                                        user = null
                                    });

                                    Logs.WSLog(intWSLogID, intWebSaleLogID, 0, strSaleType, "Finish", "Output", java.Serialize(spostaksitli3d), "spostaksitli3d");

                                    webSalePay.strSpostaksitli3dIsSuccess = spostaksitli3d.issuccess;
                                }

                                if (strSaleType == "hayat")
                                {
                                    //Logs.WSLog(0, intWebSaleLogID, 0, strSaleType, "Finish", "Input", "{ \"pPolid\" : \"" + offerPolicy.policeno + "\", \"pMuhasebehesapno\" : \"\", \"pTutar\": \"" + offerPolicy.toplamprim + "\", \"pKartno\" : \"" + CardInfo.cardno + "\" }", "tahsilatolustur");

                                    //fibaws2.tahsilatolustur(offerPolicy.policeno, "", offerPolicy.toplamprim, CardInfo.cardno);
                                }
                            }
                            else if (strSaleType == "bes")
                            {
                                var webSaleInfo = webSaleLog.tblWebSaleBesPlanPolicy.Where(w => w.strMethod == "AKAWS-00000" && w.strMsg == "Islem Basarili").First();

                                webSalePay.intPolicy = Utilities.NullFixInt(webSaleInfo.intPolID);

                                var CardInfo = Sessions.CardInfo;

                                using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                                {
                                    int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 0, strSaleType, "Finish", "Input", "", "spostaksitli3d");

                                    var spostaksitli3d = fibaws.spostaksitli3d(new FIBAWS.FibawsSanalposin3dUser
                                    {
                                        authcode = payResultData.strAuthCode,
                                        cvv = Utilities.NullFixDecimal(CardInfo.cvv2),
                                        expmonth = CardInfo.month,
                                        expyear = CardInfo.year,
                                        groupid = null,
                                        hostrefnum = payResultData.strHostrefnum,
                                        kartno = CardInfo.cardno,
                                        orderid = payResultData.strOrderID,
                                        polid = webSalePay.intPolicy,
                                        taksitadedi = CardInfo.paymentmethod,
                                        transid = payResultData.strTransID,
                                        tutar = webSalePay.flPrice,
                                        user = null
                                    });

                                    Logs.WSLog(intWSLogID, intWebSaleLogID, 0, strSaleType, "Finish", "Output", java.Serialize(spostaksitli3d), "spostaksitli3d");

                                    webSalePay.strSpostaksitli3dIsSuccess = spostaksitli3d.issuccess;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.Error(db, ex, intWebSaleLogID);
                        }
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { WarningCard(); });</script>";
                        Cookies.Step = strSaleType == "krediteminati" ? 6 : 5;

                        Logs.Error(db, new Exception(payResultData.strErrMsg), intWebSaleLogID);
                    }

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logs.Error(db, ex, intWebSaleLogID);

                    Sessions.Message = "<script>$(function () { WarningCard(); });</script>";
                    Cookies.Step = strSaleType == "krediteminati" ? 6 : 5;
                }
            }

            Session["SalePageTime"] = null;

            return Redirect(Sessions.SalePage == "" ? "/" + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
        }
        #endregion

        #region CreatePolicy
        cPolicyData CreatePolicy(int intStep, DBEntities db, tblWebSaleInfo webSaleInfo, bool bolFirst, int intWebSaleLogID, ref string strMsg)
        {
            cPolicyData policyData = new cPolicyData();

            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                service.Open();

                try
                {
                    var packet = db.tblPackets.Where(w => w.intPacketID == webSaleInfo.tblWebSaleLogs.intPacketID).FirstOrDefault();
                    if (packet != null)
                    {
                        BankaWebV2.BankawebV2PolreqUser user = new BankaWebV2.BankawebV2PolreqUser();

                        #region adres
                        user.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                        {
                            adres = webSaleInfo.strAddress,
                            adrestip = "E",
                            email = webSaleInfo.strEmail,
                            ilcekod = webSaleInfo.strDistrictCode.Trim(),
                            iletisimno = webSaleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", ""),
                            iletisimtip = "CEP",
                            ilkod = webSaleInfo.strRegionCode.Trim(),
                            ulkekod = webSaleInfo.strCountryCode.Trim()
                        };
                        #endregion

                        #region ozluk
                        AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient();

                        if (webSaleInfo.strCountryCode == "TR")
                        {
                            var kpsBireySorgu = aileService.kpsBireySorgu(new AileBireySorgu.bireySorguInputType()
                            {
                                cacheStyle = 1,
                                maxAge = 180,
                                kimlikNo = Utilities.NullFixLong(webSaleInfo.strTCKN),
                                kimlikNoSpecified = true
                            });

                            string dogumil = "";
                            var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                            if (il != null)
                                dogumil = il.strCode.Trim();

                            string dogumilce = "";
                            var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                            if (ilce != null)
                                dogumilce = ilce.strCode.Trim();

                            if (dogumil == "" && dogumilce != "")
                            {
                                dogumil = dogumilce.Substring(0, 3);
                            }

                            if (dogumil == "")
                            {
                                dogumil = "99";
                            }

                            if (dogumil != "" && dogumilce == "")
                            {
                                dogumilce = "99900";
                            }

                            user.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                            {
                                ad = webSaleInfo.strName,
                                anneadi = kpsBireySorgu.anneAdi,
                                babaad = kpsBireySorgu.babaAdi,
                                cinsiyet = kpsBireySorgu.cinsiyeti,
                                dogumil = dogumil.Trim(),
                                dogumilce = dogumilce.Trim(),
                                dogumtarih = kpsBireySorgu.dogumTarihi,
                                medenidurum = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B",
                                soyad = webSaleInfo.strSurname,
                                tckimlikno = kpsBireySorgu.tckimlikNo.ToString(),
                                musteritip = "G",
                                meslekkod = "065",
                                uyruk = webSaleInfo.strCountryCode
                            };
                        }
                        else
                        {
                            var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new AileBireySorgu.bireySorguInputType()
                            {
                                cacheStyle = 1,
                                maxAge = 180,
                                kimlikNo = Utilities.NullFixLong(webSaleInfo.strTCKN),
                                kimlikNoSpecified = true
                            });

                            user.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                            {
                                ad = webSaleInfo.strName,
                                anneadi = "",
                                babaad = kpsYabanciSorgu.babaAdi,
                                cinsiyet = kpsYabanciSorgu.cinsiyeti,
                                dogumil = "99",
                                dogumilce = "99900",
                                dogumtarih = kpsYabanciSorgu.dogumTarihi,
                                medenidurum = "",
                                soyad = webSaleInfo.strSurname,
                                tckimlikno = kpsYabanciSorgu.tckimlikNo.ToString(),
                                musteritip = "G",
                                meslekkod = "065",
                                uyruk = webSaleInfo.strCountryCode
                            };
                        }
                        #endregion

                        #region araci
                        string strPublishKey = !string.IsNullOrEmpty(Request.Form["pathKey"]) ? Request.Form["pathKey"] : "";
                        var intCallcenterID = CallcenterID(db, strPublishKey, webSaleInfo.tblWebSaleLogs.tblPackets.intProductID, 0, 0);
                        if (intCallcenterID > 0)
                        {
                            var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                            user.araci = new BankaWebV2.Bankawebv2polreqAracireqUser
                            {
                                bankasube = Utilities.NullFixDecimal(callcenter.strBankNo),
                                sicilno = callcenter.strRegisterNo
                            };
                        }
                        #endregion

                        #region kredi
                        BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                        foreach (var assurance in packet.tblAssurances.Where(w => !w.bolIsDelete).ToList())
                        {
                            yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                            {
                                bedel = assurance.flPrice,
                                tmntkod = assurance.strCode,
                                yil = assurance.intYear
                            });
                        }

                        if (bolFirst)
                        {
                            Cookies.CrediNo = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                        }

                        user.kredi = new BankaWebV2.Bankawebv2polreqKredireqUser
                        {
                            kredisure = packet.intCrediTime,
                            kredidoviztip = packet.strCrediCurrencyType,
                            kreditaksittip = "S",
                            kredidonustutar = packet.flCrediReturnPrice,
                            yillikkreditaksit = yillikkreditaksit,
                            kreditutar = 1,
                            kreditip = packet.strIntegrationTariffCode,
                            sure = packet.intTime,
                            kredino = "x" + Utilities.NullFixLong(Cookies.CrediNo)
                        };
                        #endregion

                        #region odeme
                        if (bolFirst)
                        {
                            user.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                            {
                                odemetip = "NKT",
                                dovizkod = "TL",
                                primodemeperiyod = webSaleInfo.tblWebSaleLogs.tblPackets.strPremiumPaymentPeriod,
                                primodemesure = webSaleInfo.tblWebSaleLogs.tblPackets.intPremiumPaymentTime,
                            };
                        }
                        else
                        {
                            int intPaymentMethodID = Utilities.NullFixInt(Request.Form["paymentmethod"]);

                            tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                            user.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                            {
                                primodemeperiyod = paymentMethod.strPeriod,
                                primodemesure = paymentMethod.intInstallment,
                                odemetip = "KRD",
                                dovizkod = "TL",
                                kartno = Request.Form["cardno"].Replace(" ", ""),
                                sonkullanmatarih = Request.Form["month"] + "20" + Request.Form["year"]
                            };
                        }
                        #endregion

                        #region yenileme
                        string strYenileme = "";
                        if (!bolFirst && packet.tblProducts.bolIsRefresh)
                        {
                            bool bolRefresh = Utilities.NullFixBool(Request.Form["policyrenew"]);
                            if (bolRefresh)
                            {
                                int intRefresh = Utilities.NullFixInt(Request.Form["policyrenewyear"]); ;

                                user.yenileme = "T";
                                user.yenilemesayisi = intRefresh;

                                strYenileme = "T - " + intRefresh;
                            }
                            else
                            {
                                user.yenileme = "F";
                                strYenileme = "F";
                            }
                        }
                        #endregion

                        JavaScriptSerializer java = new JavaScriptSerializer();
                        int intWSLogID = Logs.WSLog(0, intWebSaleLogID, intStep, "hayat", "CreatePolicy", "Input", java.Serialize(user), "policelesme");

                        var police = service.policelesme(user, "T");

                        Logs.WSLog(intWSLogID, intWebSaleLogID, intStep, "hayat", "CreatePolicy", "Output", java.Serialize(police), "policelesme");

                        if (police.cevap == null)
                        {
                            policyData.IlkPrim = police.ilkprim.Value;
                            //policyData.ToplamPirim = packet.intCrediTime <= 12 ? police.toplamprim.Value : Convert.ToDecimal(((decimal)(police.toplamprim.Value * 12 / packet.intCrediTime)).ToString("0.00"));
                            policyData.ToplamPirim = police.toplamprim.Value;
                            policyData.PoliceNo = police.policeno.Value;
                            policyData.IlkPrimKomisyon = police.ilkprimkomisyon.Value;
                            policyData.KrediNo = police.kredino;
                            policyData.TeklifDurum = police.teklifdurum;
                            policyData.ToplamKomisyon = police.toplamkomisyon.Value;
                            policyData.Yenileme = strYenileme;

                            //policyData.IlkPrim = 10;
                            //policyData.ToplamPirim = 100;
                            //policyData.PoliceNo = 123456;
                            //policyData.IlkPrimKomisyon = 10;
                            //policyData.KrediNo = "11";
                            //policyData.TeklifDurum = "1";
                            //policyData.ToplamKomisyon = 100;
                            //policyData.Yenileme = "f";
                        }
                        else if (police.cevap.Contains("Err|"))
                        {
                            strMsg = police.cevap.Split('|')[1].Trim();
                            strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                            //Kredi kartı ile ilgili bir mesaj dönerse hataya düşmemesi gerekiyor
                            if (!strMsg.Contains("GetKKSanalPosBankaNo"))
                                throw new Exception(strMsg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Error(db, ex, intWebSaleLogID);
                }
                finally
                {
                    service.Close();
                    service.Abort();
                }
            }

            return policyData;
        }
        #endregion

        #region Counties
        public JsonResult Counties(string id)
        {
            using (DBEntities db = new DBEntities())
            {
                var ilceler = db.tblMapping.Where(w => w.strType == "ILC" && !string.IsNullOrEmpty(w.strName.Trim()) && w.strCode.Substring(0, 3) == id).OrderBy(o => o.strName).Select(s => new
                {
                    s.strCode,
                    s.strName
                }).ToList();

                return Json(ilceler, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Pdf
        [HttpGet]
        public ActionResult Pdf()
        {
            Session["step"] = "1";

            try
            {
                string strPoliceNo = cCrypto.DecryptDES(Request.QueryString["policy"]);
                string strUrl = "";

                if (QueryStrings.SaleType == "hayat")
                {
                    strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=hytpoliceweb&desname=/akademi/mail/Police_" + strPoliceNo + "_w1.pdf&p_polid=" + strPoliceNo + "&cmdkey=fibahayat&server=" + ConfigurationManager.AppSettings["SalePdfserver"];
                }
                else if (QueryStrings.SaleType == "bes")
                {
                    strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=sozlesme_bes_matbaa.rdf&desname=" + strPoliceNo + DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") + ".pdf&server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + strPoliceNo;
                }

                Response.ContentEncoding = Encoding.GetEncoding("windows-1254");
                Response.Charset = "windows-1254";
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                Response.ContentType = "application/pdf";
                Response.Expires = 0;
                Response.Write(Utilities.UrlGet(strUrl, "GET"));
            }
            catch (Exception ex)
            {
                Logs.Error(null, ex, Cookies.WebSaleLog.ID);
            }

            return View();
        }
        #endregion

        #region Pay
        [HttpGet]
        public ActionResult Pay()
        {
            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Sessions.SalePage == "" ? "/" + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
            }

            string strSaleType = QueryStrings.SaleType;
            string strPublishKey = Request.QueryString["k"] != null ? Request.QueryString["k"].ToString() : "";
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();

                    if (!string.IsNullOrEmpty(webSaleLog.strToken))
                    {
                        Cookies.Step = strSaleType == "krediteminati" ? 6 : 5;
                        return Redirect(Sessions.SalePage == "" ? "/" + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
                    }

                    JavaScriptSerializer java = new JavaScriptSerializer();
                    cPayData payData = new cPayData();
                    var webSalePay = new tblWebSalePay();

                    decimal flPrice = 0;
                    string strDescription = "", strProductid = "", strAddress = "", strName = "", strCountryCode = "", strSurname = "", strRegionCode = "", strDistrictCode = "", strMobile = "", strEmail = "";
                    int intCallcenterID = 0;
                    decimal? PolicyNo = 0;

                    if (strSaleType == "hayat")
                    {
                        var webSaleInfo = webSaleLog.tblWebSaleInfo.First();

                        flPrice = Sessions.CardInfo.paymentmethod == 3 ? java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).IlkPrim.Value : java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).ToplamPirim.Value;
                        payData.PayType = (Enums.PayType)webSaleInfo.intPayTypeID;
                        strAddress = webSaleInfo.strAddress;
                        strName = webSaleInfo.strName;
                        strSurname = webSaleInfo.strSurname;
                        strCountryCode = webSaleInfo.strCountryCode;
                        strRegionCode = webSaleInfo.strRegionCode;
                        strDistrictCode = webSaleInfo.strDistrictCode;
                        strMobile = webSaleInfo.strMobile;
                        strEmail = webSaleInfo.strEmail;
                        strDescription = webSaleLog.tblPackets.strName;
                        strProductid = webSaleLog.intPacketID.ToString();

                        intCallcenterID = CallcenterID(db, strPublishKey, webSaleInfo.tblWebSaleLogs.tblPackets.intProductID, 0, 0);

                        if (!string.IsNullOrEmpty(webSaleInfo.strPolicyData2))
                        {
                            PolicyNo = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData2).PoliceNo;
                        }
                        else if (!string.IsNullOrEmpty(webSaleInfo.strPolicyData))
                        {
                            PolicyNo = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).PoliceNo;
                        }
                    }
                    else if (strSaleType == "bes")
                    {
                        var webSaleInfo = webSaleLog.tblWebSaleBesPlanInfo.First();

                        if (webSaleLog.tblBesPlans.bolInitialCont)
                        {
                            flPrice = webSaleInfo.flOrderlyContAmount;
                        }
                        else if (webSaleInfo.flInitilalContAmount > 0)
                        {
                            flPrice = webSaleInfo.flOrderlyContAmount + webSaleInfo.flInitilalContAmount;
                        }
                        else
                        {
                            flPrice = webSaleInfo.flOrderlyContAmount;
                        }

                        payData.PayType = (Enums.PayType)webSaleInfo.intPayTypeID;
                        strAddress = webSaleInfo.strAddress;
                        strName = webSaleInfo.strName;
                        strSurname = webSaleInfo.strSurname;
                        strCountryCode = webSaleInfo.strCountryCode;
                        strRegionCode = webSaleInfo.strRegionCode;
                        strDistrictCode = webSaleInfo.strDistrictCode;
                        strMobile = webSaleInfo.strMobile;
                        strEmail = webSaleInfo.strEmail;
                        strDescription = webSaleLog.tblBesPlans.strPlanName;
                        strProductid = webSaleLog.intBesPlanID.ToString();

                        intCallcenterID = CallcenterID(db, strPublishKey, 0, webSaleLog.intBesPlanID, 0);

                        var webSaleBesPlanPol = webSaleLog.tblWebSaleBesPlanPolicy.Where(w => w.strMethod == "AKAWS-00000" && w.strMsg == "Islem Basarili").FirstOrDefault();
                        if (webSaleBesPlanPol != null)
                        {
                            PolicyNo = Utilities.NullFixDecimal(webSaleBesPlanPol.intPolID);
                        }
                    }
                    else if (strSaleType == "krediteminati")
                    {
                        var webSaleInfo = webSaleLog.tblWebSaleCreditInfo.First();

                        flPrice = Sessions.CardInfo.paymentmethod == 3 ? java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).IlkPrim.Value : java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).ToplamPirim.Value;
                        payData.PayType = (Enums.PayType)webSaleInfo.intPayTypeID;
                        strAddress = webSaleInfo.strAddress;
                        strName = webSaleInfo.strName;
                        strSurname = webSaleInfo.strSurname;
                        strCountryCode = webSaleInfo.strCountryCode;
                        strRegionCode = webSaleInfo.strRegionCode;
                        strDistrictCode = webSaleInfo.strDistrictCode;
                        strMobile = webSaleInfo.strMobile;
                        strEmail = webSaleInfo.strEmail;
                        strDescription = webSaleLog.tblCreditPackets.strName;
                        strProductid = webSaleLog.intCreditPacketID.ToString();

                        intCallcenterID = CallcenterID(db, strPublishKey, 0, 0, webSaleLog.tblCreditPackets.intCreditID);

                        if (!string.IsNullOrEmpty(webSaleInfo.strPolicyData))
                        {
                            PolicyNo = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData).PoliceNo;
                        }

                        webSalePay = db.tblWebSalePay.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    }

                    webSalePay.bolApproved = false;
                    webSalePay.dtRegisterDate = DateTime.Now;
                    webSalePay.intWebSaleLogID = intWebSaleLogID;
                    webSalePay.strOrderID = (DateTime.Now.Day >= 10 ? DateTime.Now.Day.ToString() : "0" + DateTime.Now.Day) + (DateTime.Now.Month >= 10 ? DateTime.Now.Month.ToString() : "0" + DateTime.Now.Month) + DateTime.Now.Year.ToString() + "_FIBA_" + (PolicyNo.HasValue ? PolicyNo.Value.ToString() : "0") + "_" + new Random().Next(10000000, 99999999).ToString();
                    webSalePay.strTransID = "";
                    webSalePay.strErrMsg = "";
                    webSalePay.intPolicy = 0;
                    webSalePay.flPrice = flPrice;

                    webSaleLog.strToken = Utilities.RandomString(20);

                    if (payData.PayType == Enums.PayType.Akbank3D)
                    {
                        cPayAkbank akbank = new cPayAkbank();
                        akbank.clientId = ConfigurationManager.AppSettings["AkbankClientID"];

                        akbank.amount = webSalePay.flPrice.ToString();

                        akbank.oid = webSalePay.strOrderID;
                        akbank.okUrl = ConfigurationManager.AppSettings["siteUrl"] + "/sales/finish?token=" + webSaleLog.strToken + "&saletype=" + strSaleType + (QueryStrings.BannerID > 0 ? "&bid=" + QueryStrings.BannerID : "") + (string.IsNullOrEmpty(strPublishKey) ? "" : "&k=" + strPublishKey);
                        akbank.failUrl = ConfigurationManager.AppSettings["siteUrl"] + "/sales/finish?token=" + webSaleLog.strToken + "&saletype=" + strSaleType + (QueryStrings.BannerID > 0 ? "&bid=" + QueryStrings.BannerID : "") + (string.IsNullOrEmpty(strPublishKey) ? "" : "&k=" + strPublishKey);
                        akbank.rnd = DateTime.Now.ToString();

                        var CardInfo = Sessions.CardInfo;
                        if (CardInfo.paymentmethod == 2)
                        {
                            tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == CardInfo.paymentmethod).First();
                            akbank.taksit = paymentMethod.intInstallment.ToString();
                        }
                        else
                        {
                            akbank.taksit = "";
                        }
                        akbank.islemtipi = "Auth";

                        string storekey = ConfigurationManager.AppSettings["AkbankStoreKey"];
                        string hashstr = akbank.clientId + akbank.oid + akbank.amount + akbank.okUrl + akbank.failUrl + akbank.islemtipi + akbank.taksit + akbank.rnd + storekey;
                        System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                        byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
                        byte[] inputbytes = sha.ComputeHash(hashbytes);
                        akbank.hash = Convert.ToBase64String(inputbytes);

                        akbank.desc1 = strDescription;
                        akbank.Fadres = strAddress.Length > 250 ? strAddress.Substring(0, 250) : strAddress;
                        akbank.Fadres2 = strAddress.Length > 250 ? strAddress.Substring(250, strAddress.Length - 250) : "";
                        akbank.faturaFirma = strName + " " + strSurname;
                        akbank.Fil = db.tblMapping.Where(w => w.strCode == strRegionCode).First().strName;
                        akbank.Filce = db.tblMapping.Where(w => w.strCode == strDistrictCode).First().strName;
                        akbank.fulkekod = strCountryCode.ToLower();
                        akbank.id1 = strProductid;
                        akbank.tel = strMobile;

                        payData.akbank = akbank;
                        webSalePay.strSecure3dHash = akbank.hash;
                    }
                    else if (payData.PayType == Enums.PayType.Garanti3D)
                    {
                        string strAmount = webSalePay.flPrice.ToString();

                        cPayGaranti garanti = new cPayGaranti();
                        garanti.mode = ConfigurationManager.AppSettings["GarantiMode"];
                        garanti.apiversion = "v0.01";
                        garanti.terminalprovuserid = ConfigurationManager.AppSettings["GarantiTerminalProvUserId"];
                        garanti.txntype = "sales";
                        garanti.txnamount = (Utilities.NullFixDecimal(strAmount) * 100).ToString().Replace(".", ",").Split(',')[0];
                        garanti.txncurrencycode = "949";

                        var CardInfo = Sessions.CardInfo;

                        if (CardInfo.paymentmethod == 2)
                        {
                            tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == CardInfo.paymentmethod).First();
                            garanti.txninstallmentcount = paymentMethod.intInstallment.ToString();
                        }
                        else
                        {
                            garanti.txninstallmentcount = "";
                        }

                        if (intCallcenterID > 0)
                        {
                            var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                            garanti.terminaluserid = callcenter.strCallcenterCode;
                        }

                        garanti.orderid = webSalePay.strOrderID;
                        garanti.customeripaddress = Request.Url.ToString().Contains("localhost:") ? "85.105.198.251" : Request.UserHostAddress;
                        garanti.customeremailaddress = strEmail;
                        garanti.orderaddresscity1 = Utilities.ConvertTurkish(db.tblMapping.Where(w => w.strCode == strRegionCode).First().strName);
                        garanti.orderaddresscountry1 = strCountryCode.ToLower();
                        garanti.orderaddressdistrict1 = Utilities.ConvertTurkish(db.tblMapping.Where(w => w.strCode == strDistrictCode).First().strName);
                        garanti.orderaddresslastname1 = Utilities.ConvertTurkish(strSurname);
                        garanti.orderaddressname1 = Utilities.ConvertTurkish(strName);
                        garanti.orderaddressphonenumber1 = strMobile.Replace("(", "").Replace(")", "").Replace(" ", "");
                        garanti.orderaddresstext1 = Utilities.ConvertTurkish(strAddress);
                        //garanti.orderitemdescription1 = Utilities.ConvertTurkish(strDescription);
                        //garanti.orderitemprice1 = strAmount.Replace(",", ".");
                        //garanti.orderitemproductid1 = strProductid;

                        garanti.terminalid = ConfigurationManager.AppSettings["GarantiTerminalId"];
                        string _strTerminalID = "0" + garanti.terminalid;
                        garanti.terminalmerchantid = ConfigurationManager.AppSettings["GarantiMerchantId"];
                        string strStoreKey = ConfigurationManager.AppSettings["GarantiStoreKey"]; //3D Secure şifresi
                        string strProvisionPassword = ConfigurationManager.AppSettings["GarantiProvisionPassword"]; //TerminalProvUserID şifresi
                        garanti.successurl = ConfigurationManager.AppSettings["siteUrl"] + "/sales/finish?id=" + intWebSaleLogID + "&token=" + webSaleLog.strToken + "&saletype=" + strSaleType + (QueryStrings.BannerID > 0 ? "&bid=" + QueryStrings.BannerID : "") + (string.IsNullOrEmpty(strPublishKey) ? "" : "&k=" + strPublishKey);
                        garanti.errorurl = ConfigurationManager.AppSettings["siteUrl"] + "/sales/finish?id=" + intWebSaleLogID + "&token=" + webSaleLog.strToken + "&saletype=" + strSaleType + (QueryStrings.BannerID > 0 ? "&bid=" + QueryStrings.BannerID : "") + (string.IsNullOrEmpty(strPublishKey) ? "" : "&k=" + strPublishKey);
                        string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                        garanti.secure3dhash = GetSHA1(garanti.terminalid + garanti.orderid + garanti.txnamount + garanti.successurl + garanti.errorurl + garanti.txntype + garanti.txninstallmentcount + strStoreKey + SecurityData).ToUpper();

                        payData.garanti = garanti;
                        webSalePay.strSecure3dHash = garanti.secure3dhash;
                    }

                    if (strSaleType != "krediteminati")
                    {
                        db.tblWebSalePay.Add(webSalePay);
                    }
                    db.SaveChanges();

                    if (ConfigurationManager.AppSettings["SanalPosLogTxt"].ToString() == "1")
                    {
                        LogWrite(intWebSaleLogID, java.Serialize(payData), "Logs");
                    }

                    Sessions.SalePageTime = new cSalePageTime { time = DateTime.Now };

                    return View(payData);
                }
            }
            catch (Exception ex)
            {
                Logs.Error(null, ex, intWebSaleLogID);

                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Cookies.Step = strSaleType == "krediteminati" ? 6 : 5;

                return Redirect(Sessions.SalePage == "" ? (string.IsNullOrEmpty(strPublishKey) ? "/" : "/" + strPublishKey) + (QueryStrings.BannerID > 0 ? "?bid=" + QueryStrings.BannerID : "") : Sessions.SalePage);
            }
        }

        #region GetSHA1
        private string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        private string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }
        #endregion

        #endregion

        #region InformationForm
        [HttpGet]
        public ActionResult InformationForm()
        {
            using (DBEntities db = new DBEntities())
            {
                int intCallcenterID = QueryStrings.CallcenterID;

                var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID && !w.bolIsDelete && w.bolIsActive).FirstOrDefault();
                if (callcenter != null)
                {
                    return View(callcenter);
                }
            }

            return View();
        }
        #endregion

        #region Steps

        #region Step1
        [HttpPost]
        public ActionResult Step1()
        {
            Session["step"] = "1";
            string strSaletype = QueryStrings.SaleType;

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                if (strSaletype == "hayat")
                {
                    ProductStep1();

                    return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "2" + Request.Form["query"]);
                }
                else if (strSaletype == "bes" || strSaletype == "krediteminati")
                {
                    bool bolTCKN = true;
                    bool bolCaptcha = true;

                    if (!Request.Url.ToString().Contains("localhost:8037"))
                    {
                        bolCaptcha = Captcha;

                        if (bolCaptcha)
                        {
                            bolTCKN = TCKNValid(strSaletype == "krediteminati" ? "1" : null);
                        }
                    }
                    else
                    {
                        tcknInfo = new cTcknInfo();
                        tcknInfo.strBirthplace = "ARHAVİ";
                        tcknInfo.strFatherName = "RAMİZ";
                        tcknInfo.strGender = "E";
                        tcknInfo.strMaritalStatus = "E";
                        tcknInfo.strMotherName = "MAKBULE";
                        tcknInfo.strBirthRegionCode = "99";
                        tcknInfo.strBirthDistrictCode = "99900";
                        tcknInfo.strAdress = "beylikdüzü";
                        tcknInfo.strRegionCode = "99";
                        tcknInfo.strDistrictCode = "99900";
                        tcknInfo.strCountryCode = "TR";
                    }

                    bool bolNext = true;
                    if (strSaletype == "bes")
                    {
                        BesPlanStep1(bolTCKN);
                    }
                    else
                    {
                        if (Utilities.NullFixInt(Request.Form["intCreditMonth"]) > 240 || Utilities.NullFixInt(Request.Form["intCreditMonth"]) == 0)
                        {
                            Sessions.Message = "<script>$(function () { WarningCreditMonth(); });</script>";
                            bolNext = false;
                        }
                        else
                        {
                            bool bolTariff = true;
                            using (FIBAWS.fibawsClient fibaws = new FIBAWS.fibawsClient())
                            {
                                var policeSorgu = fibaws.getpolicesorgu(new FIBAWS.FibawsPolicesorguinrecUser
                                {
                                    branskod = "H",
                                    statu = "MM",
                                    kimlikno = Request.Form["tckn"],
                                    acenteno = "500008"
                                });

                                if (policeSorgu != null && policeSorgu.pollist != null && policeSorgu.pollist.Count > 0)
                                {
                                    List<string> tariffCodes = ConfigurationManager.AppSettings["tariffcodes"].Split(',').ToList();

                                    var pollist = policeSorgu.pollist.Where(w => (tariffCodes.Count(c => c == w.tarifeno) > 0)).ToList();
                                    if (pollist != null && pollist.Count > 0)
                                    {
                                        DateTime dtNowData = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                                        foreach (var item in pollist)
                                        {
                                            if (Utilities.NullFixDate(item.polbittar) > dtNowData)
                                            {
                                                bolTariff = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            if (bolTariff)
                            {
                                CreditStep1(bolTCKN);
                            }
                            else
                            {
                                Sessions.Message = "<script>$(function () { WarningTariffCode(); });</script>";
                            }
                        }
                    }

                    if (bolNext)
                    {
                        if (bolTCKN && bolCaptcha)
                        {
                            Cookies.Step = 2;
                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "2" + Request.Form["query"]);
                        }
                        else if (!bolCaptcha)
                        {
                            Sessions.Message = "<script>$(function () { WarningCaptcha(); });</script>";
                        }
                        else if (!bolTCKN)
                        {
                            Sessions.Message = "<script>$(function () { WarningIdentity(); });</script>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);
        }
        #endregion

        #region Step2
        [HttpPost]
        public ActionResult Step2()
        {
            Session["step"] = "1";

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 2;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "2" + Request.Form["query"]);
            }

            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                string strSaletype = QueryStrings.SaleType;

                if (strSaletype == "hayat")
                {
                    bool bolTCKN = true;
                    bool bolCaptcha = true;

                    if (!Request.Url.ToString().Contains("localhost:8037"))
                    {
                        bolCaptcha = Captcha;

                        if (bolCaptcha)
                        {
                            bolTCKN = TCKNValid();
                        }
                    }

                    bool bolNext = ProductStep2(intWebSaleLogID, bolTCKN);

                    if (bolTCKN && bolCaptcha)
                    {
                        if (bolNext)
                        {
                            Cookies.Step = 3;
                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "3" + Request.Form["query"]);
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                        }
                    }
                    else if (!bolCaptcha)
                    {
                        Sessions.Message = "<script>$(function () { WarningCaptcha(); });</script>";
                    }
                    else if (!bolTCKN)
                    {
                        Sessions.Message = "<script>$(function () { WarningIdentity(); });</script>";
                    }
                }
                else if (strSaletype == "bes")
                {
                    if (BesPlanStep2())
                    {
                        Cookies.Step = 3;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "3" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "krediteminati")
                {
                    if (CreditStep2())
                    {
                        if (Request.Form["hdButtonType"] == "next")
                        {
                            Cookies.Step = 3;
                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "3" + Request.Form["query"]);
                        }
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);

        }
        #endregion

        #region Step3
        [HttpPost]
        public ActionResult Step3()
        {
            Session["step"] = "1";

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 3;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "3" + Request.Form["query"]);
            }

            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                string strSaletype = QueryStrings.SaleType;

                if (strSaletype == "hayat")
                {
                    if (ProductStep3(intWebSaleLogID))
                    {
                        Cookies.Step = 4;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "4" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "bes")
                {
                    if (BesPlanStep3())
                    {
                        Cookies.Step = 4;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "4" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "krediteminati")
                {
                    if (CreditStep3())
                    {
                        Cookies.Step = 4;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "4" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);
        }
        #endregion

        #region Step4
        [HttpPost]
        public ActionResult Step4()
        {
            Session["step"] = "1";

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 4;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "4" + Request.Form["query"]);
            }

            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                string strSaletype = QueryStrings.SaleType;

                if (strSaletype == "hayat")
                {
                    if (ProductStep4(intWebSaleLogID))
                    {
                        Cookies.Step = 5;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "5" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "bes")
                {
                    if (BesPlanStep4())
                    {
                        Cookies.Step = 5;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "5" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "krediteminati")
                {
                    if (!CreditStep4(intWebSaleLogID))
                    {
                        Cookies.Step = 5;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "5" + Request.Form["query"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);
        }
        #endregion

        #region Step5
        [HttpPost]
        public ActionResult Step5()
        {
            Session["step"] = "1";

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 5;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "5" + Request.Form["query"]);
            }

            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                string strSaletype = QueryStrings.SaleType;

                if (strSaletype == "hayat")
                {
                    if (ProductStep5(intWebSaleLogID))
                    {
                        if (Request.Form["paymentoption"] == "contact")
                        {
                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"]);
                        }
                        else
                        {
                            string strQuery = "";
                            if (!string.IsNullOrEmpty(Request.Form["query"]))
                            {
                                strQuery = "&" + Request.Form["query"].Substring(1, Request.Form["query"].Length - 1);
                            }

                            if (!string.IsNullOrEmpty(Request.Form["pathKey"]))
                            {
                                strQuery += "&k=" + Request.Form["pathKey"];
                            }

                            return Redirect("/sales/pay?saletype=hayat" + strQuery);
                        }
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
                else if (strSaletype == "bes")
                {
                    if (Request.Form["paymentoption"] == "card")
                    {
                        bool bolCaptcha = true;
                        if (!Request.Url.ToString().Contains("localhost:8037"))
                        {
                            bolCaptcha = Captcha;
                        }

                        if (bolCaptcha)
                        {
                            tblWebSaleBesPlanInfo webSaleInfo = new tblWebSaleBesPlanInfo();

                            int intPolID = BesPlanStep5(ref webSaleInfo, false);
                            if (intPolID > 0)
                            {
                                Cookies.Step = 6;
                                Sessions.SalePage = Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"];

                                string strQuery = "";
                                if (!string.IsNullOrEmpty(Request.Form["query"]))
                                {
                                    strQuery = "&" + Request.Form["query"].Substring(1, Request.Form["query"].Length - 1);
                                }

                                if (!string.IsNullOrEmpty(Request.Form["pathKey"]))
                                {
                                    strQuery += "&k=" + Request.Form["pathKey"];
                                }

                                return Redirect("/sales/pay?saletype=bes" + strQuery);
                            }
                            else
                            {
                                Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                            }
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { WarningCaptcha(); });</script>";
                        }
                    }
                    else if (Request.Form["paymentoption"] == "contact")
                    {
                        tblWebSaleBesPlanInfo webSaleInfo = new tblWebSaleBesPlanInfo();

                        int intPolID = BesPlanStep5(ref webSaleInfo, true);
                        if (intPolID > 0)
                        {
                            Cookies.Step = 6;

                            decimal flPrice = 0;
                            if (webSaleInfo.tblWebSaleLogs.tblBesPlans.bolInitialCont)
                            {
                                flPrice = webSaleInfo.flOrderlyContAmount;
                            }
                            else if (webSaleInfo.flInitilalContAmount > 0)
                            {
                                flPrice = webSaleInfo.flOrderlyContAmount + webSaleInfo.flInitilalContAmount;
                            }
                            else
                            {
                                flPrice = webSaleInfo.flOrderlyContAmount;
                            }

                            //string strBody = Utilities.GetFileText(Server.MapPath("~/html/beniara-bes.html"));

                            //strBody = strBody.Replace("#police#", intPolID.ToString());
                            //strBody = strBody.Replace("#urun#", webSaleInfo.tblWebSaleLogs.tblBesPlans.strPlanName);
                            //strBody = strBody.Replace("#adsoyad#", webSaleInfo.strName + " " + webSaleInfo.strSurname);
                            //strBody = strBody.Replace("#tel#", webSaleInfo.strPhone);
                            //strBody = strBody.Replace("#cep#", webSaleInfo.strMobile);
                            //strBody = strBody.Replace("#prim#", flPrice.ToString());

                            //string[] strEmail = ConfigurationManager.AppSettings["BeniAraInfoMail"].Split(';');
                            //foreach (var item in strEmail)
                            //{
                            //    Email.SendEmail(ConfigurationManager.AppSettings["BeniAraTitle"], strBody, item);
                            //}

                            CrmSend("beniarabes", webSaleInfo.tblWebSaleLogs.tblBesPlans.strPlanName, "", webSaleInfo.strName, webSaleInfo.strSurname, webSaleInfo.strPhone, webSaleInfo.strMobile, flPrice.ToString(), intPolID.ToString(), ConfigurationManager.AppSettings["BeniAraTitle"]);

                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"]);
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                        }
                    }
                }
                else if (strSaletype == "krediteminati")
                {
                    if (CreditStep5(intWebSaleLogID))
                    {
                        Cookies.Step = 6;
                        return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"]);
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);

                try
                {
                    var st = new System.Diagnostics.StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    int intLine = frame.GetFileLineNumber();

                    LogWrite(intWebSaleLogID, "Line : " + intLine + " ---- " + ex.ToString(), "ex");
                }
                catch { }
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);
        }
        #endregion

        #region Step6
        [HttpPost]
        public ActionResult Step6()
        {
            Session["step"] = "1";

            if (Request.QueryString["page"] != null)
            {
                Cookies.Step = 6;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"]);
            }

            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            if (intWebSaleLogID == 0)
            {
                Cookies.Step = 1;
                return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "1" + Request.Form["query"]);
            }

            try
            {
                string strSaletype = QueryStrings.SaleType;

                if (strSaletype == "krediteminati")
                {
                    if (CreditStep6(intWebSaleLogID))
                    {
                        if (Request.Form["paymentoption"] == "contact")
                        {
                            return Redirect(Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "7" + Request.Form["query"]);
                        }
                        else
                        {
                            string strQuery = "";
                            if (!string.IsNullOrEmpty(Request.Form["query"]))
                            {
                                strQuery = "&" + Request.Form["query"].Substring(1, Request.Form["query"].Length - 1);
                            }

                            if (!string.IsNullOrEmpty(Request.Form["pathKey"]))
                            {
                                strQuery += "&k=" + Request.Form["pathKey"];
                            }

                            return Redirect("/sales/pay?saletype=krediteminati" + strQuery);
                        }
                    }
                    else
                    {
                        Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                Logs.Error(null, ex, Cookies.Step);
            }

            return Redirect(Request.Form["path"] + Request.Form["query"]);
        }
        #endregion

        #endregion

        #region Product
        //[cBannerLog]
        [HttpGet]
        public ActionResult Product()
        {
            cSalesProduct salesData = new cSalesProduct();

            using (DBEntities db = new DBEntities())
            {
                string strProductImage = ConfigurationManager.AppSettings["ProductImage"];
                int intProductID = Utilities.NullFixInt(RouteData.Values["id"].ToString());
                string strPublishKey = RouteData.Values["key"] != null ? Utilities.SQLInjection(RouteData.Values["key"].ToString()) : "";

                var product = db.tblProducts.Where(w => w.intProductID == intProductID && (!string.IsNullOrEmpty(strPublishKey) ? w.tblCallcenterProducts.Count(c => c.tblCallcenters.intSaleTypeID == 2 && c.bolSpecialSale && c.tblCallcenters.strPublishKey == strPublishKey) > 0 : true) && w.bolWebSale && w.bolIsActive && !w.bolIsDelete && w.bolPublished && !w.bolScratchWin).Select(s => new cProduct
                {
                    comment = s.strPublishComment,
                    detailurl = s.strDetailUrl,
                    image = strProductImage + s.strImage,
                    imageTable = strProductImage + s.strImageTable,
                    name = s.strName,
                    question = s.strQuestion,
                    refresh = s.bolIsRefresh,
                    strDtSub2 = s.strDtSub2,
                    intPaymentMethodIDs = s.intPaymentMethodIDs
                }).FirstOrDefault();

                if (product != null)
                {
                    var packets = db.tblPackets.Where(w => w.intProductID == intProductID && !w.bolIsDelete && w.tblAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).Select(s => new cProductPackets
                    {
                        id = s.intPacketID,
                        name = s.strName,
                        strComment = s.strComment
                    }).ToList();

                    if (packets != null)
                    {
                        product.packets = new List<cProductPackets>();
                        product.packets.AddRange(packets);
                    }

                    salesData.product = product;
                }
                else
                {
                    return Redirect((!string.IsNullOrEmpty(strPublishKey) ? "/" + strPublishKey : "") + "/urunler");
                }

                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 1)
                {
                    var webLogs = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webLogs != null)
                    {
                        salesData.webSaleLog = new cWebSaleLog();
                        salesData.webSaleLog.intPacketID = webLogs.intPacketID;
                        salesData.webSaleLog.bolIsRefresh = product.refresh;

                        if (webLogs.tblWebSaleInfo.FirstOrDefault() != null)
                        {
                            JavaScriptSerializer java = new JavaScriptSerializer();

                            salesData.webSaleLog.saleInfo = webLogs.tblWebSaleInfo.Select(s => new cWebSaleInfo
                            {
                                strAddress = s.strAddress,
                                strBirthDate = s.strBirthDate,
                                strEmail = s.strEmail,
                                strMobile = s.strMobile,
                                strName = s.strName,
                                strPhone = s.strPhone,
                                strSurname = s.strSurname,
                                strTCKN = s.strTCKN,
                                PolicyData = string.IsNullOrEmpty(s.strPolicyData) ? null : java.Deserialize<cPolicyData>(s.strPolicyData),
                                PolicyData2 = string.IsNullOrEmpty(s.strPolicyData2) ? null : java.Deserialize<cPolicyData>(s.strPolicyData2),
                                bolTcknValid = s.bolTcknValid,
                                intQuestionType = s.intQuestionType,
                                strCountryCode = s.strCountryCode,
                                strDistrictCode = s.strDistrictCode,
                                strRegionCode = s.strRegionCode,
                                strPaymentOption = s.strPaymentOption,
                                bolPolicyRenew = s.bolPolicyRenew,
                                intPolicyRenewYear = s.intPolicyRenewYear,
                                bolContactPermission = s.bolContactPermission
                            }).First();
                        }

                        if (webLogs.tblWebSalePay.FirstOrDefault() != null)
                        {
                            salesData.webSaleLog.salePay = new cWebSalePay();
                            salesData.webSaleLog.salePay.bolApproved = webLogs.tblWebSalePay.First().bolApproved;
                            salesData.webSaleLog.salePay.strErrMsg = webLogs.tblWebSalePay.First().strErrMsg;
                        }

                        if (Request.Url.Segments[Request.Url.Segments.Length - 1] == "5")
                        {
                            salesData.paymentMethods = new List<cPaymentMethod>();
                            if (product != null)
                            {
                                if (!string.IsNullOrEmpty(product.intPaymentMethodIDs))
                                {
                                    List<byte> listPaymentMethods = new List<byte>();
                                    string[] strpM = product.intPaymentMethodIDs.Split(',');
                                    for (int i = 0; i < strpM.Length; i++)
                                    {
                                        if (listPaymentMethods.Count(c => c == byte.Parse(strpM[i])) == 0)
                                        {
                                            listPaymentMethods.Add(byte.Parse(strpM[i]));
                                        }
                                    }

                                    salesData.paymentMethods = db.tblPaymentMethods.Where(w => listPaymentMethods.Count(c => c == w.intPaymentMethodID) > 0).Select(s => new cPaymentMethod
                                    {
                                        intPaymentMethodID = s.intPaymentMethodID,
                                        strName = s.strName
                                    }).ToList();
                                }
                            }
                        }
                    }
                }

                var intCallcenterID = CallcenterID(db, strPublishKey, intProductID, 0, 0);
                if (intCallcenterID > 0)
                {
                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                    salesData.callcenter = new cCallcenter()
                    {
                        intCallcenterID = intCallcenterID,
                        strInformationForm = callcenter.strInformationForm,
                        name = callcenter.strName
                    };
                }
            }

            return View(salesData);
        }
        #endregion

        #region ProductSteps

        #region ProductStep1
        private void ProductStep1()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID == 0)
                {
                    tblWebSaleLogs webSaleLog = new tblWebSaleLogs();
                    webSaleLog.dtRegisterDate = DateTime.Now;
                    webSaleLog.intPacketID = Utilities.NullFixInt(Request.Form["packet"]);
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.PacketSelected;
                    webSaleLog.strIP = Utilities.ClientIP;

                    db.tblWebSaleLogs.Add(webSaleLog);
                    db.SaveChanges();

                    Cookies.WebSaleLog = new WebSaleLog { ID = webSaleLog.intWebSaleLogID, SaleType = "hayat" };
                }
                else
                {
                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleLog != null)
                    {
                        webSaleLog.intPacketID = Utilities.NullFixInt(Request.Form["packet"]);
                        db.SaveChanges();

                        Cookies.WebSaleLog = new WebSaleLog { ID = intWebSaleLogID, SaleType = "hayat" };
                    }
                }

                Cookies.Step = 2;
            }
        }
        #endregion

        #region ProductStep2
        private bool ProductStep2(int intWebSaleLogID, bool bolTCKN)
        {
            using (DBEntities db = new DBEntities())
            {
                var webSaleInfo = db.tblWebSaleInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                bool bolInsert = webSaleInfo == null;

                if (bolInsert)
                {
                    webSaleInfo = new tblWebSaleInfo();
                    webSaleInfo.bolContactPermission = false;
                    webSaleInfo.bolPolicyRenew = false;
                    webSaleInfo.dtRegisterDate = DateTime.Now;
                    webSaleInfo.intPolicyRenewYear = 0;
                    webSaleInfo.intWebSaleLogID = Cookies.WebSaleLog.ID;
                }
                else
                {
                    webSaleInfo.dtUpdateDate = DateTime.Now;
                }

                webSaleInfo.bolTcknValid = bolTCKN;
                webSaleInfo.strAddress = Utilities.SQLInjection(Request.Form["address"]);
                webSaleInfo.strBirthDate = Request.Form["day"] + "/" + Request.Form["month"] + "/" + Request.Form["year"];
                webSaleInfo.strEmail = Utilities.SQLInjection(Request.Form["email"]);
                webSaleInfo.strMobile = Utilities.SQLInjection(Request.Form["mobile"]);
                webSaleInfo.strName = Utilities.SQLInjection(Request.Form["name"]);
                webSaleInfo.strPhone = Utilities.SQLInjection(Request.Form["phone"]);
                webSaleInfo.strSurname = Utilities.SQLInjection(Request.Form["surname"]);
                webSaleInfo.strTCKN = Utilities.SQLInjection(Request.Form["tckn"]);
                webSaleInfo.strCountryCode = Request.Form["countryCode"];
                webSaleInfo.strDistrictCode = Request.Form["countryCode"] == "TR" ? Request.Form["districtCode"] : "99900";
                webSaleInfo.strRegionCode = Request.Form["countryCode"] == "TR" ? Request.Form["regionCode"] : "99";

                if (bolInsert)
                    db.tblWebSaleInfo.Add(webSaleInfo);

                var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.InsuranceInformation;

                db.SaveChanges();

                return true;
            }
        }
        #endregion

        #region ProductStep3
        private bool ProductStep3(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                var webSaleInfo = db.tblWebSaleInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                webSaleInfo.intQuestionType = Utilities.NullFixByte(Request.Form["question"]);
                webSaleInfo.strQuestionAnswer = webSaleInfo.intQuestionType == 0 ? "Hayır" : "Evet";
                webSaleInfo.dtUpdateDate = DateTime.Now;

                var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.HealtSelect;

                db.SaveChanges();

                if (webSaleInfo.intQuestionType == 0)
                {
                    return true;
                }
                else
                {
                    //string strBody = Utilities.GetFileText(Server.MapPath("~/html/beniara-saglik.html"));

                    //strBody = strBody.Replace("#urun#", webSaleInfo.tblWebSaleLogs.tblPackets.tblProducts.strName);
                    //strBody = strBody.Replace("#paket#", webSaleInfo.tblWebSaleLogs.tblPackets.strName);
                    //strBody = strBody.Replace("#adsoyad#", webSaleInfo.strName + " " + webSaleInfo.strSurname);
                    //strBody = strBody.Replace("#tel#", webSaleInfo.strPhone);
                    //strBody = strBody.Replace("#cep#", webSaleInfo.strMobile);

                    //string[] strEmail = ConfigurationManager.AppSettings["BeniAraInfoMail"].Split(';');
                    //foreach (var item in strEmail)
                    //{
                    //Email.SendEmail(ConfigurationManager.AppSettings["BeniAraSaglikTitle"], strBody, item);
                    //}

                    CrmSend("beniarasaglik", webSaleInfo.tblWebSaleLogs.tblPackets.tblProducts.strName, webSaleInfo.tblWebSaleLogs.tblPackets.strName, webSaleInfo.strName, webSaleInfo.strSurname, webSaleInfo.strPhone, webSaleInfo.strMobile, "", "", ConfigurationManager.AppSettings["BeniAraSaglikTitle"]);

                    return true;
                }
            }
        }
        #endregion

        #region ProductStep4
        private bool ProductStep4(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                var webSaleInfo = db.tblWebSaleInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                string strMsg = "";

                var policyData = CreatePolicy(4, db, webSaleInfo, true, intWebSaleLogID, ref strMsg);

                if (string.IsNullOrEmpty(strMsg) && policyData.PoliceNo > 0)
                {
                    JavaScriptSerializer java = new JavaScriptSerializer();

                    webSaleInfo.bolContactPermission = Utilities.NullFixBool(Request.Form["ContactPermission"]);
                    webSaleInfo.strPolicyData = java.Serialize(policyData);
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.InformationForm;

                    db.SaveChanges();

                    return true;
                }
                else
                {
                    Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                    return false;
                }
            }
        }
        #endregion

        #region ProductStep5
        private bool ProductStep5(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                try
                {
                    var webSaleInfo = db.tblWebSaleInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();

                    webSaleInfo.strPaymentOption = Request.Form["paymentoption"];
                    webSaleInfo.bolPolicyRenew = Utilities.NullFixBool(Request.Form["policyrenew"]);
                    webSaleInfo.intPolicyRenewYear = Utilities.NullFixByte(Request.Form["policyrenewyear"]);
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.SaleForm;

                    db.SaveChanges();

                    if (webSaleInfo.strPaymentOption == "contact")
                    {
                        Cookies.Step = 6;

                        JavaScriptSerializer java = new JavaScriptSerializer();
                        var policy = java.Deserialize<cPolicyData>(webSaleInfo.strPolicyData);

                        //string strBody = Utilities.GetFileText(Server.MapPath("~/html/beniara.html"));

                        //strBody = strBody.Replace("#police#", policy.PoliceNo.ToString());
                        //strBody = strBody.Replace("#urun#", webSaleInfo.tblWebSaleLogs.tblPackets.tblProducts.strName);
                        //strBody = strBody.Replace("#paket#", webSaleInfo.tblWebSaleLogs.tblPackets.strName);
                        //strBody = strBody.Replace("#adsoyad#", webSaleInfo.strName + " " + webSaleInfo.strSurname);
                        //strBody = strBody.Replace("#tel#", webSaleInfo.strPhone);
                        //strBody = strBody.Replace("#cep#", webSaleInfo.strMobile);
                        //strBody = strBody.Replace("#prim#", policy.ToplamPirim.ToString());

                        //string[] strEmail = ConfigurationManager.AppSettings["BeniAraInfoMail"].Split(';');
                        //foreach (var item in strEmail)
                        //{
                        //    Email.SendEmail(ConfigurationManager.AppSettings["BeniAraTitle"], strBody, item);
                        //}

                        CrmSend("beniara", webSaleInfo.tblWebSaleLogs.tblPackets.tblProducts.strName, webSaleInfo.tblWebSaleLogs.tblPackets.strName, webSaleInfo.strName, webSaleInfo.strSurname, webSaleInfo.strPhone, webSaleInfo.strMobile, policy.ToplamPirim.ToString(), policy.PoliceNo.ToString(), ConfigurationManager.AppSettings["BeniAraTitle"]);

                        return true;
                    }
                    else
                    {
                        bool bolCaptcha = true;
                        if (!Request.Url.ToString().Contains("localhost:8037"))
                        {
                            bolCaptcha = Captcha;
                        }

                        if (bolCaptcha)
                        {
                            string strMsg = "";

                            var policyData = CreatePolicy(5, db, webSaleInfo, false, intWebSaleLogID, ref strMsg);

                            if (string.IsNullOrEmpty(strMsg) && policyData.PoliceNo > 0)
                            {
                                JavaScriptSerializer java = new JavaScriptSerializer();

                                webSaleInfo.strPolicyData2 = java.Serialize(policyData);
                                webSaleInfo.intPayTypeID = (byte)Enums.PayType.Garanti3D;

                                webSaleInfo.tblWebSaleLogs.strToken = "";

                                db.SaveChanges();

                                Sessions.CardInfo = new cCardInfo
                                {
                                    cardno = Request.Form["cardno"].Replace(" ", ""),
                                    cvv2 = Request.Form["cvv2"],
                                    month = Request.Form["month"],
                                    namesurname = Request.Form["namesurname"],
                                    paymentmethod = Utilities.NullFixByte(Request.Form["paymentmethod"]),
                                    year = Request.Form["year"],
                                    paymenttype = Request.Form["paymenttype"]
                                };

                                Cookies.Step = 6;
                                Sessions.SalePage = Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "6" + Request.Form["query"];

                                return true;
                            }
                            else
                            {
                                Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                            }
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { WarningCaptcha(); });</script>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    Logs.Error(db, ex, intWebSaleLogID);
                }
            }

            return false;
        }
        #endregion

        #endregion

        #region BesPlan
        // [cBannerLog]
        [HttpGet]
        public ActionResult BesPlan()
        {
            cSalesBesPlan salesData = new cSalesBesPlan();

            using (DBEntities db = new DBEntities())
            {
                string strBesPlanImage = ConfigurationManager.AppSettings["BesPlanImage"];
                int intBesPlanID = Utilities.NullFixInt(RouteData.Values["id"].ToString());
                string strPublishKey = RouteData.Values["key"] != null ? Utilities.SQLInjection(RouteData.Values["key"].ToString()) : "";

                var besPlan = db.tblBesPlans.Where(w => w.intBesPlanID == intBesPlanID && (!string.IsNullOrEmpty(strPublishKey) ? w.tblCallcenterBesPlans.Count(c => c.tblCallcenters.intSaleTypeID == 2 && c.bolSpecialSale && c.tblCallcenters.strPublishKey == strPublishKey) > 0 : true) && w.bolWebSale && w.bolIsActive && !w.bolIsDelete && w.bolPublished).Select(s => new cBesPlan
                {
                    comment = s.strPublishComment,
                    detailurl = s.strDetailUrl,
                    image = strBesPlanImage + s.strImage,
                    imageTable = strBesPlanImage + s.strImageTable,
                    name = s.strPlanName,
                    strPlanCode = s.strPlanCode,
                    bolInitialCont = s.bolInitialCont,
                    flMinContAmount = s.flMinContAmount,
                    flInitialCont = s.flInitialCont,
                    strDtSub2 = s.strDtSub2
                }).FirstOrDefault();

                if (besPlan != null)
                {
                    salesData.besPlan = besPlan;
                }
                else
                {
                    return Redirect((!string.IsNullOrEmpty(strPublishKey) ? "/" + strPublishKey : "") + "/urunler");
                }

                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 1)
                {
                    var webLogs = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webLogs != null)
                    {
                        salesData.webSaleLog = new cWebSaleBesPlanLog();
                        salesData.webSaleLog.intBesPlanID = webLogs.intBesPlanID;

                        if (webLogs.tblWebSaleBesPlanInfo.FirstOrDefault() != null)
                        {
                            JavaScriptSerializer java = new JavaScriptSerializer();

                            salesData.webSaleLog.saleInfo = webLogs.tblWebSaleBesPlanInfo.Select(s => new cWebSaleBesPlanInfo
                            {
                                strAddress = s.strAddress,
                                strBirthDate = s.strBirthDate,
                                strEmail = s.strEmail,
                                strMobile = s.strMobile,
                                strName = s.strName,
                                strPhone = s.strPhone,
                                strSurname = s.strSurname,
                                strTCKN = s.strTCKN,
                                bolTcknValid = s.bolTcknValid,
                                strCountryCode = s.strCountryCode,
                                strDistrictCode = s.strDistrictCode,
                                strRegionCode = s.strRegionCode,
                                strPaymentOption = s.strPaymentOption,
                                bolSalesAgreement = s.bolSalesAgreement,
                                bolContactPermission = s.bolContactPermission,
                                //FirstDueDate = s.FirstDueDate,
                                flExtraContAmount = s.flExtraContAmount,
                                flInitilalContAmount = s.flInitilalContAmount,
                                flOrderlyContAmount = s.flOrderlyContAmount,
                                FonList = s.FonList,
                                intOrderlyContAmountPeriod = s.intOrderlyContAmountPeriod != null ? s.intOrderlyContAmountPeriod.Value : (byte)0
                            }).First();
                        }

                        if (webLogs.tblWebSalePay.FirstOrDefault() != null)
                        {
                            salesData.webSaleLog.salePay = new cWebSalePay();
                            salesData.webSaleLog.salePay.bolApproved = webLogs.tblWebSalePay.First().bolApproved;
                            salesData.webSaleLog.salePay.strErrMsg = webLogs.tblWebSalePay.First().strErrMsg;
                        }

                        var besPlanPocicy = webLogs.tblWebSaleBesPlanPolicy.Where(w => w.strMethod == "AKAWS-00000" && w.strMsg == "Islem Basarili").FirstOrDefault();
                        if (besPlanPocicy != null)
                        {
                            salesData.policy = new cWebSaleBesPlanPolicy
                            {
                                intPolID = besPlanPocicy.intPolID,
                                strMethod = besPlanPocicy.strMethod,
                                strMsg = besPlanPocicy.strMsg
                            };
                        }
                    }
                }

                var intCallcenterID = CallcenterID(db, strPublishKey, 0, intBesPlanID, 0);
                if (intCallcenterID > 0)
                {
                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                    salesData.callcenter = new cCallcenter()
                    {
                        intCallcenterID = intCallcenterID,
                        strInformationForm = callcenter.strInformationForm,
                        name = callcenter.strName
                    };
                }
            }

            return View(salesData);
        }
        #endregion

        #region BesPlanSteps

        #region BesPlanStep1
        private void BesPlanStep1(bool bolTCKN)
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID == 0)
                {
                    tblWebSaleLogs webSaleLog = new tblWebSaleLogs();
                    webSaleLog.dtRegisterDate = DateTime.Now;
                    webSaleLog.intBesPlanID = string.IsNullOrEmpty(Request.Form["pathKey"]) ? Utilities.NullFixInt(Request.Form["path"].Split('/')[2].ToString()) : Utilities.NullFixInt(Request.Form["path"].Split('/')[3].ToString());
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.ParticipantInformation;
                    webSaleLog.strIP = Utilities.ClientIP;

                    db.tblWebSaleLogs.Add(webSaleLog);
                    db.SaveChanges();

                    tblWebSaleBesPlanInfo webSaleInfo = new tblWebSaleBesPlanInfo();
                    webSaleInfo.bolContactPermission = false;
                    webSaleInfo.dtRegisterDate = DateTime.Now;
                    webSaleInfo.intWebSaleLogID = webSaleLog.intWebSaleLogID;
                    webSaleInfo.bolTcknValid = bolTCKN;
                    webSaleInfo.strAddress = Utilities.SQLInjection(Request.Form["address"]);
                    webSaleInfo.strBirthDate = Request.Form["day"] + "/" + Request.Form["month"] + "/" + Request.Form["year"];
                    webSaleInfo.strEmail = Utilities.SQLInjection(Request.Form["email"]);
                    webSaleInfo.strMobile = Utilities.SQLInjection(Request.Form["mobile"]);
                    webSaleInfo.strName = Utilities.SQLInjection(Request.Form["name"]);
                    webSaleInfo.strPhone = Utilities.SQLInjection(Request.Form["phone"]);
                    webSaleInfo.strSurname = Utilities.SQLInjection(Request.Form["surname"]);
                    webSaleInfo.strTCKN = Utilities.SQLInjection(Request.Form["tckn"]);
                    webSaleInfo.strCountryCode = Request.Form["countryCode"];
                    webSaleInfo.strDistrictCode = Request.Form["countryCode"] == "TR" ? Request.Form["districtCode"] : "99900";
                    webSaleInfo.strRegionCode = Request.Form["countryCode"] == "TR" ? Request.Form["regionCode"] : "99";
                    webSaleInfo.strBirthplace = tcknInfo.strBirthplace;
                    webSaleInfo.strFatherName = tcknInfo.strFatherName;
                    webSaleInfo.strGender = tcknInfo.strGender;
                    webSaleInfo.strMaritalStatus = tcknInfo.strMaritalStatus;
                    webSaleInfo.strMotherName = tcknInfo.strMotherName;

                    db.tblWebSaleBesPlanInfo.Add(webSaleInfo);
                    db.SaveChanges();

                    Cookies.WebSaleLog = new WebSaleLog { ID = webSaleLog.intWebSaleLogID, SaleType = "bes" };
                }
                else
                {
                    var webSaleInfo = db.tblWebSaleBesPlanInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        webSaleInfo.strAddress = Utilities.SQLInjection(Request.Form["address"]);
                        webSaleInfo.strBirthDate = Request.Form["day"] + "/" + Request.Form["month"] + "/" + Request.Form["year"];
                        webSaleInfo.strEmail = Utilities.SQLInjection(Request.Form["email"]);
                        webSaleInfo.strMobile = Utilities.SQLInjection(Request.Form["mobile"]);
                        webSaleInfo.strName = Utilities.SQLInjection(Request.Form["name"]);
                        webSaleInfo.strPhone = Utilities.SQLInjection(Request.Form["phone"]);
                        webSaleInfo.strSurname = Utilities.SQLInjection(Request.Form["surname"]);
                        webSaleInfo.strTCKN = Utilities.SQLInjection(Request.Form["tckn"]);
                        webSaleInfo.strCountryCode = Request.Form["countryCode"];
                        webSaleInfo.strDistrictCode = Request.Form["countryCode"] == "TR" ? Request.Form["districtCode"] : "99900";
                        webSaleInfo.strRegionCode = Request.Form["countryCode"] == "TR" ? Request.Form["regionCode"] : "99";
                        webSaleInfo.strBirthplace = tcknInfo.strBirthplace;
                        webSaleInfo.strFatherName = tcknInfo.strFatherName;
                        webSaleInfo.strGender = tcknInfo.strGender;
                        webSaleInfo.strMaritalStatus = tcknInfo.strMaritalStatus;
                        webSaleInfo.strMotherName = tcknInfo.strMotherName;
                        webSaleInfo.dtUpdateDate = DateTime.Now;
                    }

                    db.SaveChanges();

                    Cookies.WebSaleLog = new WebSaleLog { ID = intWebSaleLogID, SaleType = "bes" };
                }
            }
        }
        #endregion

        #region BesPlanStep2
        private bool BesPlanStep2()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 0)
                {
                    var webSaleInfo = db.tblWebSaleBesPlanInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        //webSaleInfo.flExtraContAmount = Utilities.NullFixDecimal(Request.Form["flExtraContAmount"]);
                        webSaleInfo.flInitilalContAmount = webSaleInfo.tblWebSaleLogs.tblBesPlans.bolInitialCont ? Utilities.NullFixDecimal(Request.Form["flInitilalContAmount"]) : 0;
                        webSaleInfo.flOrderlyContAmount = Utilities.NullFixDecimal(Request.Form["flOrderlyContAmount"]);
                        //webSaleInfo.FirstDueDate = Request.Form["FirstDueDateDay"] + "/" + Request.Form["FirstDueDateMonth"] + "/" + Request.Form["FirstDueDateYear"];
                        webSaleInfo.FirstDueDate = DateTime.Now.ToString("dd/MM/yyyy");
                        webSaleInfo.intOrderlyContAmountPeriod = Utilities.NullFixByte(Request.Form["intOrderlyContAmountPeriod"]);
                        webSaleInfo.dtUpdateDate = DateTime.Now;

                        var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                        webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.ContributionInformation1;

                        db.SaveChanges();

                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region BesPlanStep3
        private bool BesPlanStep3()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 0)
                {
                    var webSaleInfo = db.tblWebSaleBesPlanInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        List<cFonList> fonList = new List<cFonList>();

                        int intFonListCount = Utilities.NullFixInt(Request.Form["FonListCount"]);
                        for (int i = 0; i < intFonListCount; i++)
                        {
                            string strOran = Request.Form["fonstandartoran" + i];

                            fonList.Add(new cFonList
                            {
                                strCode = Request.Form["fonkod" + i],
                                strRate = string.IsNullOrEmpty(strOran) ? "0" : strOran
                            });
                        }

                        JavaScriptSerializer javaSer = new JavaScriptSerializer();
                        webSaleInfo.FonList = javaSer.Serialize(fonList);
                        webSaleInfo.dtUpdateDate = DateTime.Now;

                        var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                        webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.ContributionInformation2;

                        db.SaveChanges();

                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region BesPlanStep4
        private bool BesPlanStep4()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 0)
                {
                    var webSaleInfo = db.tblWebSaleBesPlanInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        webSaleInfo.bolSalesAgreement = Utilities.NullFixBool(Request.Form["satis_sozlesmesi"]);
                        webSaleInfo.bolContactPermission = Utilities.NullFixBool(Request.Form["ContactPermission"]);
                        webSaleInfo.dtUpdateDate = DateTime.Now;

                        var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                        webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.InformationForm;

                        db.SaveChanges();

                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region BesPlanStep5
        private int BesPlanStep5(ref tblWebSaleBesPlanInfo webSaleInfo, bool bolContact)
        {
            int intWebSaleLogID = Cookies.WebSaleLog.ID;
            try
            {
                JavaScriptSerializer java = new JavaScriptSerializer();

                using (DBEntities db = new DBEntities())
                {
                    if (intWebSaleLogID > 0)
                    {
                        var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();

                        webSaleInfo = db.tblWebSaleBesPlanInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                        if (webSaleInfo != null)
                        {
                            var besPlanPolicy = db.tblWebSaleBesPlanPolicy.Where(w => w.intWebSaleLogID == intWebSaleLogID && w.strMethod == "AKAWS-00000" && w.strMsg == "Islem Basarili").FirstOrDefault();
                            if (besPlanPolicy == null)
                            {
                                tblCallcenters callcenter = null;
                                var intCallcenterID = CallcenterID(db, !string.IsNullOrEmpty(Request.Form["pathKey"]) ? Request.Form["pathKey"] : "", 0, webSaleLog.intBesPlanID, 0);
                                if (intCallcenterID > 0)
                                {
                                    callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).FirstOrDefault();
                                }

                                WebSales.FibaBES.FibaBesWebServiceClient service = new WebSales.FibaBES.FibaBesWebServiceClient();

                                #region genbilgi
                                var genbilgi = new FibaBES.EmkbasvurulibgeneltypUser();
                                genbilgi.odetip = "K";
                                genbilgi.iskanunivaris = "T";

                                var taksitsayisi = service.gettaksitsayisi(new WebSales.FibaBES.EmkblibgataksitinputtypUser
                                {
                                    dovkod = "TL",
                                    isga = "T",
                                    tarifeno = webSaleLog.tblBesPlans.strPlanCode
                                });

                                if (taksitsayisi != null && taksitsayisi.result != null && taksitsayisi.result.Count > 0)
                                {
                                    genbilgi.gataksitsayisi = Utilities.NullFixDecimal(taksitsayisi.result[0].extraf1);
                                }

                                genbilgi.ilkvadetar = webSaleInfo.FirstDueDate.Replace(".", "").Replace("/", "");
                                genbilgi.tarifeno = webSaleLog.tblBesPlans.strPlanCode;
                                genbilgi.portfoy = ConfigurationManager.AppSettings["InterMediaryCode"];
                                genbilgi.kptaksitsayisi = webSaleInfo.intOrderlyContAmountPeriod;
                                genbilgi.dovkod = "TL";
                                genbilgi.katkipayi = webSaleInfo.flOrderlyContAmount;
                                genbilgi.finansaldanisman = callcenter.strRegisterNo;
                                genbilgi.baslangickp = webSaleInfo.flInitilalContAmount;
                                genbilgi.basvurutar = DateTime.Now.ToString("dd-MM-yyyy").Replace("-", "");
                                genbilgi.bkodetip = "K";
                                //genbilgi.ekkatkipayi = webSaleInfo.flExtraContAmount;
                                genbilgi.iskatodeyen = "T";

                                if (webSaleLog.tblBesPlans.intGroupID.HasValue)
                                {
                                    genbilgi.grupid = webSaleLog.tblBesPlans.intGroupID.Value;
                                }
                                #endregion

                                #region perbilgi
                                var perbilgi = PerBilgi(db, webSaleInfo);
                                #endregion

                                #region odeyenbilgi
                                //var odeyenbilgi = new FibaBES.EmkbasvurulibpersontypUser();
                                //if (webSaleInfo.bolDifferentPaid)
                                //    odeyenbilgi = OdeyenBilgi(db, webSaleInfo);
                                #endregion

                                #region fnbilgi
                                var fnbilgi = new FibaBES.Emkbasvurulibfontyl();

                                List<cFonList> fonList = new List<cFonList>();
                                JavaScriptSerializer javaSer = new JavaScriptSerializer();
                                fonList = javaSer.Deserialize<List<cFonList>>(webSaleInfo.FonList);

                                decimal intTotal = 0;
                                foreach (var item in fonList)
                                {
                                    intTotal += Utilities.NullFixDecimal(item.strRate);
                                }

                                foreach (var item in fonList)
                                {
                                    if (intTotal == 0 && item.strCode == "FEN")
                                    {
                                        fnbilgi.Add(new FibaBES.EmkbasvurulibfontypUser
                                        {
                                            fonkod = item.strCode,
                                            fonoran = 1
                                        });
                                    }
                                    else
                                    {
                                        fnbilgi.Add(new FibaBES.EmkbasvurulibfontypUser
                                        {
                                            fonkod = item.strCode,
                                            fonoran = (Utilities.NullFixDecimal(item.strRate) / 100)
                                        });
                                    }
                                }
                                #endregion

                                #region kkbilgi
                                var kkbilgi = new FibaBES.EmkbasvurulibkkarttypUser();
                                if (Request.Form["paymentoption"] == "card")
                                {
                                    kkbilgi.cvv1 = Utilities.NullFixDecimal(Request.Form["cvv2"]);
                                    kkbilgi.sonkultar1 = "01" + Request.Form["month"] + Request.Form["year"];
                                    kkbilgi.kkartno1 = Request.Form["cardno"].Replace(" ", "");
                                    kkbilgi.ad1 = perbilgi.ad;
                                    kkbilgi.soyad1 = perbilgi.soyad;
                                    kkbilgi.kktip1 = Request.Form["paymenttype"] == "1" ? "V" : "M";
                                }
                                #endregion

                                var uretim = new FibaBES.EmkbasvuruliburetiminputtypUser
                                {
                                    perbilgi = perbilgi,
                                    genbilgi = genbilgi,
                                    odeyenbilgi = null,
                                    userbilgi = new FibaBES.EmkbasvurulibuserinputtypUser
                                    {
                                        password = ConfigurationManager.AppSettings["BesWsPss"],
                                        kullanicikodu = callcenter.strRegisterNo,
                                        bankano = null,
                                        username = ConfigurationManager.AppSettings["BesWsUserName"],
                                        subeno = Utilities.NullFixDecimal(callcenter.strBankNo)
                                    },
                                    kkbilgi = kkbilgi,
                                    fnbilgi = fnbilgi,
                                    hspbilgi = null
                                };

                                int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Input", java.Serialize(uretim), "setbasvuru");

                                var result = service.setbasvuru(uretim);

                                Logs.WSLog(intWSLogID, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Output", java.Serialize(result), "setbasvuru");

                                if (result.polid != null)
                                {
                                    Logs.LogPolicy(intWebSaleLogID, "setbasvuru", result.polid.Value.ToString(), result.sonuc.hataaciklama);

                                    if (!bolContact)
                                    {
                                        FibaBES.EmkbasvurulibuserinputtypUser pUserbilgi = new FibaBES.EmkbasvurulibuserinputtypUser
                                        {
                                            password = System.Configuration.ConfigurationManager.AppSettings["BesWsPss"],
                                            kullanicikodu = System.Configuration.ConfigurationManager.AppSettings["BesWsUserCode"],
                                            bankano = null,
                                            username = System.Configuration.ConfigurationManager.AppSettings["BesWsUserName"],
                                            subeno = Utilities.NullFixDecimal(callcenter.strBankNo)
                                        };

                                        intWSLogID = Logs.WSLog(0, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Input", java.Serialize(pUserbilgi), "settanzim");

                                        var tanzim = service.settanzim(result.polid.Value, pUserbilgi);

                                        Logs.WSLog(intWSLogID, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Output", java.Serialize(tanzim), "settanzim");

                                        if (tanzim.polid != null)
                                        {
                                            BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();
                                            //bankaWeb.tanzimsmsgonder(tanzim.polid);

                                            Logs.LogPolicy(intWebSaleLogID, tanzim.sonuc.hatakodu, tanzim.polid.Value.ToString(), tanzim.sonuc.hataaciklama);

                                            webSaleInfo.tblWebSaleLogs.strToken = "";
                                            webSaleInfo.strPaymentOption = Request.Form["paymentoption"];

                                            webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.SaleForm;

                                            if (Request.Form["paymentoption"] == "card")
                                            {
                                                webSaleInfo.intPayTypeID = (byte)Enums.PayType.Garanti3D;
                                                Sessions.CardInfo = new cCardInfo
                                                {
                                                    cardno = Request.Form["cardno"].Replace(" ", ""),
                                                    cvv2 = Request.Form["cvv2"],
                                                    month = Request.Form["month"],
                                                    namesurname = Request.Form["namesurname"],
                                                    paymentmethod = 0,
                                                    year = Request.Form["year"],
                                                    paymenttype = Request.Form["paymenttype"]
                                                };
                                            }

                                            db.SaveChanges();

                                            return Utilities.NullFixInt(tanzim.polid.Value.ToString());
                                        }
                                        else
                                        {
                                            Logs.LogPolicy(intWebSaleLogID, "settanzim", "", "hatakodu : " + tanzim.sonuc.hatakodu + " ----- hataaciklama : " + tanzim.sonuc.hataaciklama);
                                        }
                                    }
                                    else
                                    {
                                        webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.SaleForm;
                                        webSaleInfo.strPaymentOption = Request.Form["paymentoption"];

                                        db.SaveChanges();

                                        return Utilities.NullFixInt(result.polid.Value.ToString());
                                    }
                                }
                                else
                                {
                                    Logs.LogPolicy(intWebSaleLogID, "setbasvuru", "", "hatakodu : " + result.sonuc.hatakodu + " ----- hataaciklama : " + result.sonuc.hataaciklama);
                                }
                            }
                            else
                            {
                                webSaleInfo.tblWebSaleLogs.strToken = "";
                                webSaleInfo.strPaymentOption = Request.Form["paymentoption"];
                                webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.SaleForm;
                                db.SaveChanges();

                                if (Request.Form["paymentoption"] == "card")
                                {
                                    string strName = Request.Form["namesurname"];
                                    string strSurname = "";
                                    string[] strNameSurname = strName.Split(' ');
                                    if (strNameSurname.Length > 1)
                                    {
                                        strName = "";
                                        for (int i = 0; i < strNameSurname.Length - 1; i++)
                                        {
                                            strName = strName + " " + strNameSurname[i];
                                        }

                                        strName = strName.Trim();
                                        strSurname = strNameSurname[strNameSurname.Length - 1];
                                    }

                                    int intWSLogID = Logs.WSLog(0, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Input", "", "getodemearacdeg");

                                    INTBES.NTBESClient intbes = new INTBES.NTBESClient();
                                    var odemearacdeg = intbes.getodemearacdeg(Utilities.NullFixDecimal(besPlanPolicy.intPolID), "K", new INTBES.IntbesPerkartrecUser
                                    {
                                        adi = strName,
                                        bankano = 62,
                                        cvv = Utilities.NullFixDecimal(Request.Form["cvv2"]),
                                        hesapgun = Utilities.NullFixDecimal(webSaleInfo.FirstDueDate.Split('/')[0]),
                                        kartno = Request.Form["cardno"].Replace(" ", ""),
                                        karttip = "V",
                                        sonkultar = "01" + Request.Form["month"] + Request.Form["year"],
                                        soyadi = strSurname
                                    }, null);

                                    Logs.WSLog(intWSLogID, intWebSaleLogID, 5, "bes", "BesPlanStep5", "Output", java.Serialize(odemearacdeg), "getodemearacdeg");

                                    Sessions.CardInfo = new cCardInfo
                                    {
                                        cardno = Request.Form["cardno"].Replace(" ", ""),
                                        cvv2 = Request.Form["cvv2"],
                                        month = Request.Form["month"],
                                        namesurname = Request.Form["namesurname"],
                                        paymentmethod = 0,
                                        year = Request.Form["year"],
                                        paymenttype = Request.Form["paymenttype"]
                                    };
                                }

                                return Utilities.NullFixInt(besPlanPolicy.intPolID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    var st = new System.Diagnostics.StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    int intLine = frame.GetFileLineNumber();

                    LogWrite(webSaleInfo.intWebSaleLogID, "Line : " + intLine + " ---- " + ex.ToString(), "ex");
                }
                catch { }

                Logs.Error(null, ex, intWebSaleLogID);
            }

            return 0;
        }

        #region PerBilgi
        private FibaBES.EmkbasvurulibpersontypUser PerBilgi(DBEntities db, tblWebSaleBesPlanInfo saleInfo)
        {
            var perbilgi = new FibaBES.EmkbasvurulibpersontypUser();
            perbilgi.iskpsadres = "T";
            perbilgi.adrestip = "E";
            perbilgi.il = saleInfo.strRegionCode;
            perbilgi.tckimlikno = saleInfo.strTCKN;
            perbilgi.ulke = saleInfo.strCountryCode;
            perbilgi.ceptel = saleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", "");
            perbilgi.adres = saleInfo.strAddress;
            perbilgi.iskolu = "";
            perbilgi.ulke = saleInfo.strCountryCode;
            perbilgi.evtel = saleInfo.strPhone.Replace("(", "").Replace(")", "").Replace(" ", "");

            var ulkeler = db.tblMapping.Where(w => w.strType == "UYR" && w.strName.Trim() == saleInfo.strCountryCode).FirstOrDefault();
            if (ulkeler != null)
                perbilgi.dogulke = ulkeler.strCountryCode;

            perbilgi.calsekli = "3";
            perbilgi.egitimdurum = "4";
            perbilgi.uyruk = saleInfo.strCountryCode;
            perbilgi.meslek = "028";
            perbilgi.ilce = saleInfo.strDistrictCode;
            perbilgi.kimliktur = "NUF";
            perbilgi.email = saleInfo.strEmail;
            perbilgi.ad = saleInfo.strName;
            perbilgi.dogtar = saleInfo.strBirthDate.Replace("-", "").Replace(".", "").Replace("/", "");
            perbilgi.medenihal = saleInfo.strMaritalStatus == "EVLİ" ? "E" : "B";
            perbilgi.cinsiyet = saleInfo.strGender;
            perbilgi.babaadi = saleInfo.strFatherName;
            perbilgi.anaadi = saleInfo.strMotherName;
            perbilgi.soyad = saleInfo.strSurname;

            if (saleInfo.strCountryCode == "TR")
            {
                var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == saleInfo.strBirthplace.Trim()).FirstOrDefault();
                if (il != null)
                    perbilgi.dogil = il.strCode.Trim();

                var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == saleInfo.strBirthplace.Trim()).FirstOrDefault();
                if (ilce != null)
                    perbilgi.dogilce = ilce.strCode.Trim();

                if (perbilgi.dogil == "" && perbilgi.dogilce != "")
                    perbilgi.dogil = perbilgi.dogilce.Substring(0, 3);

                if (perbilgi.dogil == "")
                    perbilgi.dogil = "99";

                if (perbilgi.dogil != "" && perbilgi.dogilce == "")
                    perbilgi.dogilce = "99900";
            }
            else
            {
                perbilgi.dogil = "99";
                perbilgi.dogilce = "99900";
            }

            return perbilgi;
        }
        #endregion

        #region OdeyenBilgi
        private FibaBES.EmkbasvurulibpersontypUser OdeyenBilgi(DBEntities db, tblWebSaleBesPlanInfo webSaleInfo)
        {
            var odeyenbilgi = new FibaBES.EmkbasvurulibpersontypUser();
            odeyenbilgi.iskpsadres = "T";
            odeyenbilgi.adrestip = "E";
            odeyenbilgi.il = webSaleInfo.strDistrictCode;
            odeyenbilgi.tckimlikno = webSaleInfo.strTCKN;
            odeyenbilgi.bolgekodu = webSaleInfo.strRegionCode;
            odeyenbilgi.ulke = webSaleInfo.strCountryCode;
            odeyenbilgi.ceptel = webSaleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", "");
            odeyenbilgi.adres = webSaleInfo.strAddress;
            odeyenbilgi.iskolu = "";
            odeyenbilgi.ulke = webSaleInfo.strCountryCode;
            odeyenbilgi.evtel = webSaleInfo.strPhone.Replace("(", "").Replace(")", "").Replace(" ", "");

            var ulkeler = db.tblMapping.Where(w => w.strType == "UYR" && w.strName.Trim() == webSaleInfo.strCountryCode).FirstOrDefault();
            if (ulkeler != null)
                odeyenbilgi.dogulke = ulkeler.strCountryCode;

            odeyenbilgi.calsekli = "3";
            odeyenbilgi.egitimdurum = "4";
            odeyenbilgi.uyruk = webSaleInfo.strCountryCode;
            odeyenbilgi.meslek = "028";
            odeyenbilgi.ilce = webSaleInfo.strDistrictCode;
            odeyenbilgi.kimliktur = "NUF";
            odeyenbilgi.email = webSaleInfo.strEmail;
            odeyenbilgi.ad = webSaleInfo.strName;
            odeyenbilgi.dogtar = webSaleInfo.strBirthDate.Replace("-", "").Replace(".", "").Replace("/", "");
            odeyenbilgi.medenihal = webSaleInfo.strMaritalStatus;
            odeyenbilgi.cinsiyet = webSaleInfo.strGender;
            odeyenbilgi.babaadi = webSaleInfo.strFatherName;
            odeyenbilgi.anaadi = webSaleInfo.strMotherName;
            odeyenbilgi.soyad = webSaleInfo.strSurname;

            if (webSaleInfo.strCountryCode == "TR")
            {
                var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == webSaleInfo.strBirthplace.Trim()).FirstOrDefault();
                if (il != null)
                    odeyenbilgi.dogil = il.strCode.Trim();

                var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == webSaleInfo.strBirthplace.Trim()).FirstOrDefault();
                if (ilce != null)
                    odeyenbilgi.dogilce = ilce.strCode.Trim();

                if (odeyenbilgi.dogil == "" && odeyenbilgi.dogilce != "")
                    odeyenbilgi.dogil = odeyenbilgi.dogilce.Substring(0, 3);

                if (odeyenbilgi.dogil == "")
                    odeyenbilgi.dogil = "99";

                if (odeyenbilgi.dogil != "" && odeyenbilgi.dogilce == "")
                    odeyenbilgi.dogilce = "99900";
            }
            else
            {
                odeyenbilgi.dogil = "99";
                odeyenbilgi.dogilce = "99900";
            }

            return odeyenbilgi;
        }
        #endregion

        #endregion

        #endregion

        #region Credit
        //[cBannerLog]
        [HttpGet]
        public ActionResult Credit()
        {
            cSalesCredit salesData = new cSalesCredit();

            using (DBEntities db = new DBEntities())
            {
                string strCreditImage = ConfigurationManager.AppSettings["CreditImage"];
                int intCreditID = Utilities.NullFixInt(RouteData.Values["id"].ToString());
                string strPublishKey = RouteData.Values["key"] != null ? Utilities.SQLInjection(RouteData.Values["key"].ToString()) : "";
                int intStep = Cookies.Step;
                int intWebSaleLogID = Cookies.WebSaleLog.ID;

                var intCallcenterID = CallcenterID(db, strPublishKey, 0, 0, intCreditID);
                if (intCallcenterID > 0)
                {
                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                    salesData.callcenter = new cCallcenter()
                    {
                        intCallcenterID = intCallcenterID,
                        strInformationForm = callcenter.strInformationFormCredit,
                        flBSMV = callcenter.flBSMV,
                        flKKDF = callcenter.flKKDF,
                        name = callcenter.strName
                    };
                }

                if (intStep == 6)
                {
                    if (intWebSaleLogID > 0)
                    {
                        salesData.banks = new List<cBanks>();
                        salesData.banks.AddRange(db.tblBanks.Where(w => w.bolIsActive && !w.bolIsDelete).Select(s => new cBanks
                        {
                            intBankID = s.intBankID,
                            strName = s.strName
                        }).OrderBy(o => o.strName).ToList());

                        salesData.paymentMethods = new List<cPaymentMethod>();

                        string intPaymentMethodIDs = db.tblCredits.Where(w => w.intCreditID == intCreditID).First().intPaymentMethodIDs;
                        if (!string.IsNullOrEmpty(intPaymentMethodIDs))
                        {
                            List<byte> listPaymentMethods = new List<byte>();
                            string[] strpM = intPaymentMethodIDs.Split(',');
                            for (int i = 0; i < strpM.Length; i++)
                            {
                                if (listPaymentMethods.Count(c => c == byte.Parse(strpM[i])) == 0)
                                {
                                    listPaymentMethods.Add(byte.Parse(strpM[i]));
                                }
                            }

                            salesData.paymentMethods = db.tblPaymentMethods.Where(w => listPaymentMethods.Count(c => c == w.intPaymentMethodID) > 0).Select(s => new cPaymentMethod
                            {
                                intPaymentMethodID = s.intPaymentMethodID,
                                strName = s.strName
                            }).ToList();
                        }
                    }
                }

                var credit = db.tblCredits.Where(w => w.intCreditID == intCreditID && (!string.IsNullOrEmpty(strPublishKey) ? w.tblCallcenterCredits.Count(c => c.tblCallcenters.intSaleTypeID == 2 && c.bolSpecialSale && c.tblCallcenters.strPublishKey == strPublishKey) > 0 : true) && w.bolWebSale && w.bolIsActive && !w.bolIsDelete && w.bolPublished).Select(s => new cCredit
                {
                    comment = s.strPublishComment,
                    detailurl = s.strDetailUrl,
                    image = strCreditImage + s.strImage,
                    imageTable = strCreditImage + s.strImageTable,
                    name = s.strName,
                    question = s.strQuestion,
                    strDtSub2 = s.strDtSub2
                }).FirstOrDefault();

                if (credit != null)
                {
                    salesData.credit = credit;
                }
                else
                {
                    return Redirect((!string.IsNullOrEmpty(strPublishKey) ? "/" + strPublishKey : "") + "/urunler");
                }

                if (intWebSaleLogID > 0)
                {
                    var webLogs = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webLogs != null)
                    {
                        var creditPackets = db.tblCreditPackets.Where(w => w.intCreditID == intCreditID && !w.bolIsDelete && w.tblCreditAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).Select(s => new
                        {
                            id = s.intCreditPacketID,
                            name = s.strName,
                            strComment = s.strComment,
                            assurances = s.tblCreditAssurances.Where(w => !w.bolIsDelete),
                            policy = s.tblCreditPacketPolicy.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault()
                        }).ToList();

                        if (creditPackets != null)
                        {
                            credit.packets = new List<cCreditPackets>();
                            foreach (var item in creditPackets)
                            {
                                credit.packets.Add(new cCreditPackets
                                {
                                    assurances = item.assurances.Select(s => new cCreditAssurances
                                    {
                                        name = s.strName
                                    }).ToList(),
                                    id = item.id,
                                    name = item.name,
                                    strComment = item.strComment,
                                    policy = item.policy != null ? new cCreditPolicy
                                    {
                                        flFirstPremium = item.policy.flFirstPremium,
                                        flTotalPremium = item.policy.flTotalPremium,
                                        strPolicyID = item.policy.strPolicyID
                                    } : null
                                });
                            }
                        }

                        salesData.webSaleLog = new cWebSaleCreditLog();
                        salesData.webSaleLog.intCreditPacketID = webLogs.intCreditPacketID;

                        if (webLogs.tblWebSaleCreditInfo.FirstOrDefault() != null)
                        {
                            JavaScriptSerializer java = new JavaScriptSerializer();

                            salesData.webSaleLog.saleInfo = webLogs.tblWebSaleCreditInfo.Select(s => new cWebSaleCreditInfo
                            {
                                strTCKN = s.strTCKN,
                                strName = s.strName,
                                strSurname = s.strSurname,
                                flCreditAmount = s.flCreditAmount,
                                flInterest = s.flInterest,
                                flBSMV = s.flBSMV,
                                flKKDF = s.flKKDF,
                                intCreditMonth = s.intCreditMonth,
                                intBankCityID = s.intBankCityID,
                                intBankID = s.intBankID,
                                intBankBranchID = s.intBankBranchID,
                                strAddress = s.strAddress,
                                strBirthDate = s.strBirthDate,
                                strEmail = s.strEmail,
                                strMobile = s.strMobile,
                                strPhone = s.strPhone,
                                PolicyData = string.IsNullOrEmpty(s.strPolicyData) ? null : java.Deserialize<cPolicyData>(s.strPolicyData),
                                bolTcknValid = s.bolTcknValid,
                                intQuestionType = s.intQuestionType,
                                strCountryCode = s.strCountryCode,
                                strDistrictCode = s.strDistrictCode,
                                strRegionCode = s.strRegionCode,
                                strPaymentOption = s.strPaymentOption,
                                bolContactPermission = s.bolContactPermission,
                                bolSalesAgreement = s.bolSalesAgreement
                            }).First();
                        }

                        if (webLogs.tblWebSalePay.FirstOrDefault() != null)
                        {
                            salesData.webSaleLog.salePay = new cWebSalePay();
                            salesData.webSaleLog.salePay.bolApproved = webLogs.tblWebSalePay.First().bolApproved;
                            salesData.webSaleLog.salePay.strErrMsg = webLogs.tblWebSalePay.First().strErrMsg;
                        }

                        if (webLogs.tblWebSaleCreditPaymentPlans.FirstOrDefault() != null)
                        {
                            salesData.webSaleLog.paymentPlan = new List<PaymentPlanResponse>();
                            foreach (var item in webLogs.tblWebSaleCreditPaymentPlans)
                            {
                                salesData.webSaleLog.paymentPlan.Add(new PaymentPlanResponse
                                {
                                    installAmount = item.flInstallAmount.ToString("0.00"),
                                    remainingAmount = item.flRemainingAmount.ToString("0.00"),
                                    installment = item.intInstallment
                                });
                            }
                        }
                    }
                }
            }

            return View(salesData);
        }
        #endregion

        #region CreditSteps

        #region CreditStep1
        private void CreditStep1(bool bolTCKN)
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID == 0)
                {
                    tblWebSaleLogs webSaleLog = new tblWebSaleLogs();
                    webSaleLog.dtRegisterDate = DateTime.Now;
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.CreditInformation;
                    webSaleLog.strIP = Utilities.ClientIP;

                    db.tblWebSaleLogs.Add(webSaleLog);
                    db.SaveChanges();

                    tblWebSaleCreditInfo webSaleInfo = new tblWebSaleCreditInfo();
                    webSaleInfo.bolContactPermission = false;
                    webSaleInfo.dtRegisterDate = DateTime.Now;
                    webSaleInfo.intWebSaleLogID = webSaleLog.intWebSaleLogID;
                    webSaleInfo.bolTcknValid = bolTCKN;
                    webSaleInfo.strBirthDate = Request.Form["day"] + "/" + Request.Form["month"] + "/" + Request.Form["year"];
                    webSaleInfo.strName = Utilities.SQLInjection(Request.Form["name"]);
                    webSaleInfo.strSurname = Utilities.SQLInjection(Request.Form["surname"]);
                    webSaleInfo.strTCKN = Utilities.SQLInjection(Request.Form["tckn"]);
                    webSaleInfo.flBSMV = Utilities.NullFixDecimal(Request.Form["flBSMV"]);
                    webSaleInfo.flCreditAmount = Utilities.NullFixDecimal(Request.Form["flCreditAmount"]);
                    webSaleInfo.flInterest = Utilities.NullFixDecimal(Request.Form["flInterest"]);
                    webSaleInfo.flKKDF = Utilities.NullFixDecimal(Request.Form["flKKDF"]);
                    webSaleInfo.intCreditMonth = Utilities.NullFixByte(Request.Form["intCreditMonth"]);
                    webSaleInfo.strMobile = Utilities.SQLInjection(Request.Form["mobile"]);
                    webSaleInfo.strBirthplace = tcknInfo.strBirthplace;
                    webSaleInfo.strFatherName = tcknInfo.strFatherName;
                    webSaleInfo.strGender = tcknInfo.strGender;
                    webSaleInfo.strMaritalStatus = tcknInfo.strMaritalStatus;
                    webSaleInfo.strMotherName = tcknInfo.strMotherName;
                    webSaleInfo.strBirthDistrictCode = tcknInfo.strBirthDistrictCode;
                    webSaleInfo.strBirthRegionCode = tcknInfo.strBirthRegionCode;
                    webSaleInfo.strAddress = tcknInfo.strAdress;
                    webSaleInfo.strRegionCode = tcknInfo.strRegionCode;
                    webSaleInfo.strDistrictCode = tcknInfo.strDistrictCode;
                    webSaleInfo.strCountryCode = tcknInfo.strCountryCode;

                    db.tblWebSaleCreditInfo.Add(webSaleInfo);
                    db.SaveChanges();

                    Cookies.WebSaleLog = new WebSaleLog { ID = webSaleLog.intWebSaleLogID, SaleType = "krediteminati" };
                }
                else
                {
                    var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        string strBirthDate = Request.Form["day"] + "/" + Request.Form["month"] + "/" + Request.Form["year"];
                        string strName = Utilities.SQLInjection(Request.Form["name"]);
                        string strSurname = Utilities.SQLInjection(Request.Form["surname"]);
                        string strTCKN = Utilities.SQLInjection(Request.Form["tckn"]);
                        decimal flBSMV = Utilities.NullFixDecimal(Request.Form["flBSMV"]);
                        decimal flCreditAmount = Utilities.NullFixDecimal(Request.Form["flCreditAmount"]);
                        decimal flInterest = Utilities.NullFixDecimal(Request.Form["flInterest"]);
                        decimal flKKDF = Utilities.NullFixDecimal(Request.Form["flKKDF"]);
                        byte intCreditMonth = Utilities.NullFixByte(Request.Form["intCreditMonth"]);
                        string strMobile = Utilities.SQLInjection(Request.Form["mobile"]);

                        if (webSaleInfo.strBirthDate != strBirthDate
                            || webSaleInfo.strName != strName
                                || webSaleInfo.strSurname != strSurname
                                    || webSaleInfo.strTCKN != strTCKN
                                        || webSaleInfo.flBSMV != flBSMV
                                            || webSaleInfo.flCreditAmount != flCreditAmount
                                                || webSaleInfo.flInterest != flInterest
                                                    || webSaleInfo.flKKDF != flKKDF
                                                        || webSaleInfo.intCreditMonth != intCreditMonth
                                                            || webSaleInfo.strMobile != strMobile)
                        {
                            db.Database.ExecuteSqlCommand("delete from tblWebSaleCreditPaymentPlans where intWebSaleLogID = {0}; delete from tblCreditPacketPolicy where intWebSaleLogID = {1}", intWebSaleLogID, intWebSaleLogID);
                            db.SaveChanges();
                        }

                        webSaleInfo.strBirthDate = strBirthDate;
                        webSaleInfo.strName = strName;
                        webSaleInfo.strSurname = strSurname;
                        webSaleInfo.strTCKN = strTCKN;
                        webSaleInfo.flBSMV = flBSMV;
                        webSaleInfo.flCreditAmount = flCreditAmount;
                        webSaleInfo.flInterest = flInterest;
                        webSaleInfo.flKKDF = flKKDF;
                        webSaleInfo.intCreditMonth = intCreditMonth;
                        webSaleInfo.strMobile = strMobile;
                        webSaleInfo.strBirthplace = tcknInfo.strBirthplace;
                        webSaleInfo.strFatherName = tcknInfo.strFatherName;
                        webSaleInfo.strGender = tcknInfo.strGender;
                        webSaleInfo.strMaritalStatus = tcknInfo.strMaritalStatus;
                        webSaleInfo.strMotherName = tcknInfo.strMotherName;
                        webSaleInfo.dtUpdateDate = DateTime.Now;
                    }

                    db.SaveChanges();

                    Cookies.WebSaleLog = new WebSaleLog { ID = intWebSaleLogID, SaleType = "krediteminati" };
                }
            }
        }
        #endregion

        #region CreditStep2
        private bool CreditStep2()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;
                if (intWebSaleLogID > 0)
                {
                    var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                    if (webSaleInfo != null)
                    {
                        if (Request.Form["hdButtonType"] == "offer")
                        {
                            db.Database.ExecuteSqlCommand("delete from tblWebSaleCreditPaymentPlans where intWebSaleLogID = {0}", intWebSaleLogID);
                            db.SaveChanges();

                            for (byte i = 1; i <= webSaleInfo.intCreditMonth; i++)
                            {
                                db.tblWebSaleCreditPaymentPlans.Add(new tblWebSaleCreditPaymentPlans
                                {
                                    dtRegisterDate = DateTime.Now,
                                    flInstallAmount = Utilities.NullFixDecimal(Request.Form["flInstallAmount"]),
                                    flRemainingAmount = Utilities.NullFixDecimal(Request.Form["flRemainingAmount" + i]),
                                    intInstallment = i,
                                    intWebSaleLogID = intWebSaleLogID
                                });
                            }
                            db.SaveChanges();

                            #region Service
                            List<cCreditSessionKeys> creditSessionKeys = new List<cCreditSessionKeys>();

                            int intCreditID = Utilities.NullFixInt(Request.Form["pathID"]);

                            var creditPackets = db.tblCreditPackets.Where(w => w.intCreditID == intCreditID && !w.bolIsDelete && w.tblCreditAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).ToList();

                            foreach (var item in creditPackets)
                            {
                                var packetPolicy = db.tblCreditPacketPolicy.Where(w => w.intWebSaleLogID == intWebSaleLogID && w.intCreditPacketID == item.intCreditPacketID).FirstOrDefault();
                                if (packetPolicy != null)
                                {
                                    db.tblCreditPacketPolicy.Remove(packetPolicy);
                                }

                                if (CreateCreditPolicy(db, 2, intWebSaleLogID, intCreditID, webSaleInfo, item, false, ref creditSessionKeys) == null)
                                {
                                    break;
                                }
                            }

                            Cookies.CreditSessionKeys = creditSessionKeys;
                            #endregion
                        }
                        else
                        {
                            var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                            webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogBesPlanType.CreditPaymentPlan;
                            webSaleLog.intCreditPacketID = Utilities.NullFixInt(Request.Form["selectedPacket"]);

                            db.SaveChanges();
                        }

                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region CreditStep3
        private bool CreditStep3()
        {
            using (DBEntities db = new DBEntities())
            {
                int intWebSaleLogID = Cookies.WebSaleLog.ID;

                var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                if (webSaleInfo != null)
                {
                    webSaleInfo.strAddress = Utilities.SQLInjection(Request.Form["address"]);
                    webSaleInfo.strEmail = Utilities.SQLInjection(Request.Form["email"]);
                    webSaleInfo.strPhone = Utilities.SQLInjection(Request.Form["phone"]);
                    webSaleInfo.strCountryCode = Request.Form["countryCode"];
                    webSaleInfo.strDistrictCode = Request.Form["countryCode"] == "TR" ? Request.Form["districtCode"] : "99900";
                    webSaleInfo.strRegionCode = Request.Form["countryCode"] == "TR" ? Request.Form["regionCode"] : "99";
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    db.SaveChanges();

                    return true;
                }
            }

            return false;
        }
        #endregion

        #region CreditStep4
        private bool CreditStep4(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                if (webSaleInfo != null)
                {
                    webSaleInfo.intQuestionType = Utilities.NullFixByte(Request.Form["question"]);
                    webSaleInfo.strQuestionAnswer = webSaleInfo.intQuestionType == 0 ? "Hayır" : "Evet";
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.HealtSelect;

                    db.SaveChanges();

                    if (webSaleInfo.intQuestionType == 0)
                    {
                        return false;
                    }
                    else
                    {
                        //string strBody = Utilities.GetFileText(Server.MapPath("~/html/beniara-saglik.html"));

                        //strBody = strBody.Replace("#urun#", webSaleInfo.tblWebSaleLogs.tblCreditPackets.tblCredits.strName);
                        //strBody = strBody.Replace("#paket#", webSaleInfo.tblWebSaleLogs.tblCreditPackets.strName);
                        //strBody = strBody.Replace("#adsoyad#", webSaleInfo.strName + " " + webSaleInfo.strSurname);
                        //strBody = strBody.Replace("#tel#", webSaleInfo.strPhone);
                        //strBody = strBody.Replace("#cep#", webSaleInfo.strMobile);

                        //string[] strEmail = ConfigurationManager.AppSettings["BeniAraInfoMail"].Split(';');
                        //foreach (var item in strEmail)
                        //{
                        //    Email.SendEmail(ConfigurationManager.AppSettings["BeniAraSaglikTitle"], strBody, item);
                        //}

                        CrmSend("beniarasaglik", webSaleInfo.tblWebSaleLogs.tblCreditPackets.tblCredits.strName, webSaleInfo.tblWebSaleLogs.tblCreditPackets.strName, webSaleInfo.strName, webSaleInfo.strSurname, webSaleInfo.strPhone, webSaleInfo.strMobile, "", "", ConfigurationManager.AppSettings["BeniAraSaglikTitle"]);

                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region CreditStep5
        private bool CreditStep5(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).FirstOrDefault();
                if (webSaleInfo != null)
                {
                    webSaleInfo.bolSalesAgreement = Utilities.NullFixBool(Request.Form["bolSalesAgreement"]);
                    webSaleInfo.bolContactPermission = Utilities.NullFixBool(Request.Form["ContactPermission"]);
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.InformationForm;

                    db.SaveChanges();

                    return true;
                }
            }

            return false;
        }
        #endregion

        #region CreditStep6
        private bool CreditStep6(int intWebSaleLogID)
        {
            using (DBEntities db = new DBEntities())
            {
                try
                {
                    var webSaleInfo = db.tblWebSaleCreditInfo.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();

                    webSaleInfo.strPaymentOption = Request.Form["paymentoption"];
                    webSaleInfo.intBankCityID = Utilities.NullFixShort(Request.Form["intBankCityID"]);
                    webSaleInfo.intBankID = Utilities.NullFixShort(Request.Form["intBankID"]);
                    webSaleInfo.intBankBranchID = Utilities.NullFixShort(Request.Form["intBankBranchID"]);
                    webSaleInfo.dtUpdateDate = DateTime.Now;

                    var webSaleLog = db.tblWebSaleLogs.Where(w => w.intWebSaleLogID == intWebSaleLogID).First();
                    webSaleLog.intWebSaleLogTypeID = (byte)Enums.WebSaleLogProductType.SaleForm;

                    db.SaveChanges();

                    if (webSaleInfo.strPaymentOption == "contact")
                    {
                        var packetPolicy = db.tblCreditPacketPolicy.Where(w => w.intWebSaleLogID == intWebSaleLogID && w.intCreditPacketID == webSaleLog.intCreditPacketID).First();

                        //string strBody = Utilities.GetFileText(Server.MapPath("~/html/beniara.html"));

                        //strBody = strBody.Replace("#police#", packetPolicy.strPolicyID);
                        //strBody = strBody.Replace("#urun#", webSaleInfo.tblWebSaleLogs.tblCreditPackets.tblCredits.strName);
                        //strBody = strBody.Replace("#paket#", webSaleInfo.tblWebSaleLogs.tblCreditPackets.strName);
                        //strBody = strBody.Replace("#adsoyad#", webSaleInfo.strName + " " + webSaleInfo.strSurname);
                        //strBody = strBody.Replace("#tel#", webSaleInfo.strPhone);
                        //strBody = strBody.Replace("#cep#", webSaleInfo.strMobile);
                        //strBody = strBody.Replace("#prim#", packetPolicy.flTotalPremium.ToString());

                        //string[] strEmail = ConfigurationManager.AppSettings["BeniAraInfoMail"].Split(';');
                        //foreach (var item in strEmail)
                        //{
                        //    Email.SendEmail(ConfigurationManager.AppSettings["BeniAraTitle"], strBody, item);
                        //}

                        CrmSend("beniara", webSaleInfo.tblWebSaleLogs.tblCreditPackets.tblCredits.strName, webSaleInfo.tblWebSaleLogs.tblCreditPackets.strName, webSaleInfo.strName, webSaleInfo.strSurname, webSaleInfo.strPhone, webSaleInfo.strMobile, packetPolicy.strPolicyID, packetPolicy.flTotalPremium.ToString(), ConfigurationManager.AppSettings["BeniAraTitle"]);

                        Cookies.Step = 7;

                        return true;
                    }
                    else
                    {
                        bool bolCaptcha = true;
                        if (!Request.Url.ToString().Contains("localhost:8037"))
                        {
                            bolCaptcha = Captcha;
                        }

                        if (bolCaptcha)
                        {
                            JavaScriptSerializer java = new JavaScriptSerializer();
                            int intCreditID = Utilities.NullFixInt(Request.Form["pathID"]);
                            List<cCreditSessionKeys> creditSessionKeys = Cookies.CreditSessionKeys;

                            var policyData = CreateCreditPolicy(db, 6, intWebSaleLogID, intCreditID, webSaleInfo, webSaleLog.tblCreditPackets, true, ref creditSessionKeys);

                            if (policyData != null)
                            {
                                webSaleInfo.strPolicyData = java.Serialize(policyData);
                                webSaleInfo.intPayTypeID = (byte)Enums.PayType.Garanti3D;

                                webSaleInfo.tblWebSaleLogs.strToken = "";

                                db.SaveChanges();

                                Sessions.CardInfo = new cCardInfo
                                {
                                    cardno = Request.Form["cardno"].Replace(" ", ""),
                                    cvv2 = Request.Form["cvv2"],
                                    month = Request.Form["month"],
                                    namesurname = Request.Form["namesurname"],
                                    paymentmethod = Utilities.NullFixByte(Request.Form["paymentmethod"]),
                                    year = Request.Form["year"],
                                    paymenttype = Request.Form["paymenttype"]
                                };

                                Cookies.Step = 7;
                                Sessions.SalePage = Request.Form["path"].Substring(0, Request.Form["path"].Length - 1) + "7" + Request.Form["query"];

                                return true;
                            }
                            else
                            {
                                Logs.Error(db, new Exception("policyData is null"), intWebSaleLogID);
                                Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                            }
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { WarningCaptcha(); });</script>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Sessions.Message = "<script>$(function () { ErrorPage(); });</script>";
                    Logs.Error(db, ex, intWebSaleLogID);
                }
            }

            return false;
        }
        #endregion

        #endregion

        #region CreateCreditPolicy
        cPolicyData CreateCreditPolicy(DBEntities db, int intStep, int intWebSaleLogID, int intCreditID, tblWebSaleCreditInfo webSaleInfo, tblCreditPackets item, bool bolStep6, ref List<cCreditSessionKeys> creditSessionKeys)
        {
            cPolicyData policyData = new cPolicyData();
            BankaWebV2.bankaweb_v2Client bankaWeb = new BankaWebV2.bankaweb_v2Client();

            try
            {
                BankaWebV2.BankawebV2PolreqUser polreqUser = new BankaWebV2.BankawebV2PolreqUser();

                polreqUser.yenileme = "F";
                polreqUser.isaylikazalan = "T";

                if (bolStep6 && webSaleInfo.intBankBranchID > 0)
                {
                    var bankBranch = db.tblBankBranch.Where(w => w.intBankBranchID == webSaleInfo.intBankBranchID).FirstOrDefault();
                    if (bankBranch != null)
                    {
                        polreqUser.sigetid = bankBranch.intWsID;
                    }
                }

                #region odeme
                if (bolStep6)
                {
                    int intPaymentMethodID = Utilities.NullFixInt(Request.Form["paymentmethod"]);

                    tblPaymentMethods paymentMethod = db.tblPaymentMethods.Where(w => w.intPaymentMethodID == intPaymentMethodID).First();

                    polreqUser.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                    {
                        primodemeperiyod = paymentMethod.strPeriod,
                        primodemesure = paymentMethod.intInstallment,
                        odemetip = "KRD",
                        dovizkod = "TL",
                        kartno = Request.Form["cardno"].Replace(" ", ""),
                        sonkullanmatarih = Request.Form["month"] + "20" + Request.Form["year"],
                        cvv = Utilities.NullFixDecimal(Request.Form["cvc"])
                    };
                }
                else
                {
                    polreqUser.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                    {
                        primodemeperiyod = item.strPremiumPaymentPeriod,
                        primodemesure = item.intPremiumPaymentTime,
                        odemetip = "NKT",
                        dovizkod = "TL"
                    };
                }
                #endregion

                #region ozluk
                polreqUser.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                {
                    ad = webSaleInfo.strName,
                    anneadi = webSaleInfo.strMotherName,
                    babaad = webSaleInfo.strFatherName,
                    cinsiyet = webSaleInfo.strGender,
                    dogumil = webSaleInfo.strBirthRegionCode.Trim(),
                    dogumilce = webSaleInfo.strBirthDistrictCode.Trim(),
                    dogumtarih = Utilities.NullFixDate(webSaleInfo.strBirthDate),
                    medenidurum = webSaleInfo.strMaritalStatus,
                    soyad = webSaleInfo.strSurname,
                    tckimlikno = webSaleInfo.strTCKN,
                    musteritip = "G",
                    meslekkod = "003",
                    uyruk = webSaleInfo.strCountryCode,
                    sirkettip = "A"
                };
                #endregion

                #region adres
                if (bolStep6)
                {
                    polreqUser.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                    {
                        adres = webSaleInfo.strAddress,
                        adrestip = "E",
                        ilcekod = webSaleInfo.strDistrictCode.Trim(),
                        iletisimtip = "CEP",
                        ilkod = webSaleInfo.strRegionCode.Trim(),
                        ulkekod = webSaleInfo.strCountryCode.Trim(),
                        iletisimno = webSaleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", ""),
                    };
                }
                else
                {
                    polreqUser.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                    {
                        adres = webSaleInfo.strAddress,
                        adrestip = "E",
                        ilcekod = webSaleInfo.strDistrictCode.Trim(),
                        iletisimtip = "CEP",
                        ilkod = webSaleInfo.strRegionCode.Trim(),
                        ulkekod = webSaleInfo.strCountryCode.Trim(),
                        iletisimno = webSaleInfo.strMobile.Replace("(", "").Replace(")", "").Replace(" ", ""),
                    };
                }
                #endregion

                #region araci
                string strPublishKey = !string.IsNullOrEmpty(Request.Form["pathKey"]) ? Request.Form["pathKey"] : "";
                var intCallcenterID = CallcenterID(db, strPublishKey, 0, 0, intCreditID);
                if (intCallcenterID > 0)
                {
                    var callcenter = db.tblCallcenters.Where(w => w.intCallcenterID == intCallcenterID).First();
                    polreqUser.araci = new BankaWebV2.Bankawebv2polreqAracireqUser
                    {
                        bankasube = Utilities.NullFixDecimal(callcenter.strBankNo),
                        sicilno = callcenter.strRegisterNo
                    };
                }
                #endregion

                #region kredi
                BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                foreach (var paymentItem in webSaleInfo.tblWebSaleLogs.tblWebSaleCreditPaymentPlans.ToList())
                {
                    foreach (var assurance in item.tblCreditAssurances.ToList())
                    {
                        if (assurance.strCoefficientType == "1")
                        {
                            if (paymentItem.intInstallment == 1)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = 1,
                                    tmntkod = assurance.strCode,
                                    yil = 1,
                                    bedel = webSaleInfo.flCreditAmount * assurance.flCoefficient
                                });
                            }

                            int intInstallment = paymentItem.intInstallment + 1;
                            int intInstallmentMax = webSaleInfo.tblWebSaleLogs.tblWebSaleCreditPaymentPlans.OrderByDescending(o => o.intInstallment).First().intInstallment;

                            if (intInstallment <= intInstallmentMax)
                            {
                                yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                                {
                                    ay = intInstallment,
                                    tmntkod = assurance.strCode,
                                    yil = intInstallment > 12 ? (intInstallment / 12) + (intInstallment % 12 > 0 ? 1 : 0) : 1,
                                    bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                                });
                            }
                        }
                        else
                        {
                            yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                            {
                                ay = paymentItem.intInstallment,
                                tmntkod = assurance.strCode,
                                yil = paymentItem.intInstallment > 12 ? (paymentItem.intInstallment / 12) + (paymentItem.intInstallment % 12 > 0 ? 1 : 0) : 1,
                                bedel = assurance.strCoefficientType == "1" ? paymentItem.flRemainingAmount * assurance.flCoefficient : paymentItem.flInstallAmount * assurance.flCoefficient
                            });
                        }
                    }
                }

                cCreditSessionKeys creditSessionKey;

                if (bolStep6)
                {
                    if (creditSessionKeys == null || creditSessionKeys.Count == 0)
                    {
                        creditSessionKeys = new List<cCreditSessionKeys>();
                        creditSessionKey = new cCreditSessionKeys
                        {
                            intCreditPacketID = item.intCreditPacketID,
                            strCreditID = "x" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString()
                        };
                        creditSessionKeys.Add(creditSessionKey);

                        Cookies.CreditSessionKeys = creditSessionKeys;
                    }
                    else
                    {
                        creditSessionKey = creditSessionKeys.Where(w => w.intCreditPacketID == item.intCreditPacketID).First();
                    }
                }
                else
                {
                    creditSessionKey = new cCreditSessionKeys
                    {
                        intCreditPacketID = item.intCreditPacketID,
                        strCreditID = "x" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString()
                    };
                    creditSessionKeys.Add(creditSessionKey);
                }

                polreqUser.kredi = new BankaWebV2.Bankawebv2polreqKredireqUser
                {
                    kredisure = webSaleInfo.intCreditMonth,
                    kredidoviztip = "TL",
                    kreditaksittip = "S",
                    sure = webSaleInfo.intCreditMonth,
                    kredino = creditSessionKey.strCreditID,
                    kredino2 = creditSessionKey.strCreditID,
                    yillikkreditaksit = yillikkreditaksit,
                    kreditutar = webSaleInfo.flCreditAmount,
                    kreditip = item.strIntegrationTariffCode,
                    kredidonustutar = webSaleInfo.flCreditAmount
                };
                #endregion

                JavaScriptSerializer jss = new JavaScriptSerializer();
                int intWSLogID = Logs.WSLog(0, intWebSaleLogID, intStep, "krediteminatı", "CreateCreditPolicy", "Input", jss.Serialize(polreqUser), "policelesme");

                var policy = bankaWeb.policelesme(polreqUser, "T");

                Logs.WSLog(intWSLogID, intWebSaleLogID, intStep, "krediteminatı", "CreateCreditPolicy", "Output", jss.Serialize(policy), "policelesme");

                if (policy.policeno != null)
                {
                    if (bolStep6)
                    {
                        policyData.IlkPrim = policy.ilkprim.Value;
                        policyData.ToplamPirim = policy.toplamprim.Value;
                        policyData.PoliceNo = policy.policeno.Value;
                        policyData.IlkPrimKomisyon = policy.ilkprimkomisyon.Value;
                        policyData.KrediNo = policy.kredino;
                        policyData.TeklifDurum = policy.teklifdurum;
                        policyData.ToplamKomisyon = policy.toplamkomisyon.Value;

                        db.tblWebSalePay.Add(new tblWebSalePay
                        {
                            bolApproved = false,
                            dtRegisterDate = DateTime.Now,
                            intWebSaleLogID = intWebSaleLogID,
                            strTransID = "",
                            strErrMsg = "",
                            intPolicy = 0
                        });

                        db.SaveChanges();
                    }
                    else
                    {
                        if (policy.teklifdurum == "UW")
                        {
                            Sessions.Message = "<script>$(function () { ErrorPolicy2(); });</script>";
                            policyData = null;
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { ScrollBottom(); });</script>";

                            db.tblCreditPacketPolicy.Add(new tblCreditPacketPolicy
                            {
                                dtRegisterDate = DateTime.Now,
                                flFirstPremium = policy.ilkprim.Value,
                                flTotalPremium = policy.toplamprim.Value,
                                intCreditPacketID = item.intCreditPacketID,
                                intWebSaleLogID = intWebSaleLogID,
                                strPolicyID = policy.policeno.Value.ToString()
                            });
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    bool bolUW = false;
                    if (policy != null)
                    {
                        if (policy.teklifdurum == "UW")
                        {
                            Sessions.Message = "<script>$(function () { ErrorPolicy2(); });</script>";
                            bolUW = true;
                        }
                    }

                    policyData = null;

                    if (!bolUW)
                    {
                        string strMsg = "";
                        if (policy.cevap.Contains("Err|"))
                        {
                            strMsg = policy.cevap.Split('|')[1].Trim();
                            strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                            if (strMsg.Split('>').Length > 0)
                            {
                                strMsg = strMsg.Split('>')[strMsg.Split('>').Length - 1].Trim();
                                if (strMsg.Split(':').Length > 0)
                                {
                                    if (strMsg.Split('-').Length > 0)
                                    {
                                        strMsg = strMsg.Split('-')[0].Trim();
                                    }
                                    else
                                    {
                                        strMsg = strMsg.Split(':')[strMsg.Split(':').Length - 1].Trim();
                                    }
                                }
                            }
                        }

                        if (strMsg == "")
                        {
                            if (policy != null)
                            {
                                Logs.Error(db, new Exception("policy.policeno is null --> cevap : " + policy.cevap), intWebSaleLogID);
                            }

                            Sessions.Message = "<script>$(function () { ErrorPolicy(); });</script>";
                        }
                        else
                        {
                            Sessions.Message = "<script>$(function () { ErrorMessage('" + strMsg + "'); });</script>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                policyData = null;
                Logs.Error(db, ex, intWebSaleLogID);
            }

            return policyData;
        }
        #endregion

        #region LogWrite
        private void LogWrite(int intWebSaleLogID, string strValue, string strFile)
        {
            try
            {
                using (FileStream fs = new FileStream(Utilities.AppPathServer + "/Views/Sales/" + strFile + ".txt", FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("");
                        sw.WriteLine("LogID: " + intWebSaleLogID);
                        sw.WriteLine("");
                        sw.WriteLine(strValue);
                    }
                }
            }
            catch { }
        }
        #endregion

        #region BankCities
        public JsonResult BankCities(string bankid)
        {
            using (DBEntities db = new DBEntities())
            {
                var intBankID = Utilities.NullFixShort(bankid);

                var bankCities = db.tblBankCity.Where(w => !w.bolIsDelete && w.bolIsActive && w.tblBankCities.Count(c => c.intBankID == intBankID) > 0).OrderBy(o => o.strCity).Select(s => new
                {
                    s.intBankCityID,
                    s.strCity
                }).ToList();

                return Json(bankCities, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region BankBranches
        public JsonResult BankBranches(string cityid, string bankid)
        {
            using (DBEntities db = new DBEntities())
            {
                var intBankCityID = Utilities.NullFixShort(cityid);
                var intBankID = Utilities.NullFixShort(bankid);

                var banks = db.tblBankBranch.Where(w => !w.bolIsDelete && w.bolIsActive && w.intBankCityID == intBankCityID &&
                    w.tblBanks.tblBankCities.Count(c => c.intBankID == intBankID) > 0).OrderBy(o => o.strName).Select(s => new
                    {
                        s.intBankBranchID,
                        s.strName
                    }).ToList();

                return Json(banks, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CrmSend
        private void CrmSend(string strType, string strProductName, string strPacketName, string strName, string strSurname, string strPhone, string strMobile, string strPrice, string strPolID, string strSubject)
        {
            string strOwnerId = "";
            string strQueueId = "";
            string strPhoneCallType = "";
            string strBody = "";

            if (strType == "beniara")
            {
                strOwnerId = "crmBeniAraOwnerId";
                strQueueId = "crmBeniAraQueueId";
                strPhoneCallType = "crmBeniAraPhoneCallType";

                strBody += "Kredi kartı girişi yapılmadı, FİBA Beni Arasın aracılığıyla gönderildi.";
                strBody += Environment.NewLine + "Poliçe : " + strPolID;
                strBody += Environment.NewLine + "Prim : " + strPrice;
                strBody += Environment.NewLine + "Ürün : " + strProductName;
                strBody += Environment.NewLine + "Paket : " + strPacketName;
                strBody += Environment.NewLine + "Ad Soyad : " + strName + " " + strSurname;
                strBody += Environment.NewLine + "Telefon Numarası : " + strPhone;
                strBody += Environment.NewLine + "Cep Telefonu Numarası : " + strMobile;
            }
            else if (strType == "beniarabes")
            {
                strOwnerId = "crmBeniAraBesOwnerId";
                strQueueId = "crmBeniAraBesQueueId";
                strPhoneCallType = "crmBeniAraBesPhoneCallType";

                strBody += "Kredi kartı girişi yapılmadı, FİBA Beni Arasın aracılığıyla gönderildi.";
                strBody += Environment.NewLine + "Poliçe : " + strPolID;
                strBody += Environment.NewLine + "Prim : " + strPrice;
                strBody += Environment.NewLine + "Ürün : " + strProductName;
                strBody += Environment.NewLine + "Ad Soyad : " + strName + " " + strSurname;
                strBody += Environment.NewLine + "Telefon Numarası : " + strPhone;
                strBody += Environment.NewLine + "Cep Telefonu Numarası : " + strMobile;
            }
            else if (strType == "beniarasaglik")
            {
                strOwnerId = "crmBeniAraSaglikOwnerId";
                strQueueId = "crmBeniAraSaglikQueueId";
                strPhoneCallType = "crmBeniAraSaglikPhoneCallType";

                strBody += "Sağlık sorusuna devam edilmedi.";
                strBody += Environment.NewLine + "Ürün : " + strProductName;
                strBody += Environment.NewLine + "Paket : " + strPacketName;
                strBody += Environment.NewLine + "Ad Soyad : " + strName + " " + strSurname;
                strBody += Environment.NewLine + "Telefon Numarası : " + strPhone;
                strBody += Environment.NewLine + "Cep Telefonu Numarası : " + strMobile;
            }

            Email.SendCRM(ConfigurationManager.AppSettings[strOwnerId].ToString(), ConfigurationManager.AppSettings[strQueueId].ToString(), ConfigurationManager.AppSettings[strPhoneCallType].ToString(), strName, strSurname, "", (string.IsNullOrEmpty(strPhone) ? strMobile : strPhone), strBody, strSubject);
        }
        #endregion
    }
}
