﻿#region Directives
using ModelLibrary;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebSales.Classes;
#endregion

namespace WebSales.Controllers
{
    public class TestController : Controller
    {
        string strO = "12345632";

        public EmptyResult Finish()
        {
            string strAuthenticationCode = Server.UrlEncode(Request.Form.Get("cavv"));
            string strSecurityLevel = Server.UrlEncode(Request.Form.Get("eci"));
            string strTxnID = Server.UrlEncode(Request.Form.Get("xid"));
            string strMD = Server.UrlEncode(Request.Form.Get("md"));
            string strMDStatus = Request.Form.Get("mdstatus");
            string strHostAddress = "https://sanalposprovtest.garanti.com.tr/VPServlet";
            string SecurityData = GetSHA1("123qweASD/030691297").ToUpper();
            string ValidateHashData = GetSHA1("30691297" + strO + "100http://localhost:8037/test/finishhttp://localhost:8037/test/finishsales12345678" + SecurityData).ToUpper();
            string strHashData = Request.Form.Get("secure3dhash");

            string HashData = GetSHA1(strO + "30691297100" + SecurityData).ToUpper();

            if (strHashData == ValidateHashData)
            {
                if (strMDStatus == "1" | strMDStatus == "2" | strMDStatus == "3" | strMDStatus == "4")
                {
                    string strXML = null;
                    strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<GVPSRequest>"
                            + "<Mode>TEST</Mode>"
                            + "<Version>v0.01</Version>"
                            + "<Terminal>"
                                + "<ProvUserID>PROVAUT</ProvUserID>"
                                + "<HashData>" + HashData + "</HashData>"
                                + "<UserID>SANALUSER</UserID>"
                                + "<ID>30691297</ID>"
                                + "<MerchantID>7000679</MerchantID>"
                            + "</Terminal>"
                            + "<Customer>"
                                + "<IPAddress>85.105.198.251</IPAddress>"
                                + "<EmailAddress>fatih.aspnet@gricreative.com</EmailAddress>"
                            + "</Customer>"
                            + "<Card>"
                                + "<Number></Number>"
                                + "<ExpireDate></ExpireDate>"
                                + "<CVV2></CVV2>"
                            + "</Card>"
                            + "<Order>"
                                + "<OrderID>" + strO + "</OrderID>"
                                + "<GroupID></GroupID>"
                            + "</Order>"
                            + "<Transaction>"
                                + "<Type>sales</Type>"
                                + "<InstallmentCnt></InstallmentCnt>"
                                + "<Amount>100</Amount>"
                                + "<CurrencyCode>949</CurrencyCode>"
                                + "<CardholderPresentCode>13</CardholderPresentCode>"
                                + "<MotoInd>N</MotoInd>"
                                + "<Secure3D>"
                                    + "<AuthenticationCode>" + strAuthenticationCode + "</AuthenticationCode>"
                                    + "<SecurityLevel>" + strSecurityLevel + "</SecurityLevel>"
                                    + "<TxnID>" + strTxnID + "</TxnID>"
                                    + "<Md>13</Md>"
                                + "</Secure3D>"
                            + "</Transaction>"
                        + "</GVPSRequest>";

                    string data = "data=" + strXML;

                    WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                    _WebRequest.Method = "POST";

                    byte[] byteArray = Encoding.UTF8.GetBytes(data);
                    _WebRequest.ContentType = "application/x-www-form-urlencoded";
                    _WebRequest.ContentLength = byteArray.Length;

                    Stream dataStream = _WebRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    WebResponse _WebResponse = _WebRequest.GetResponse();
                    dataStream = _WebResponse.GetResponseStream();

                    string strStatusMessage = ((HttpWebResponse)_WebResponse).StatusDescription;

                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();

                    if (responseFromServer.Contains("<ReasonCode>00</ReasonCode>"))
                    {
                    }
                }
            }

            return new EmptyResult();
        }

        public ActionResult Pay(string token)
        {
            Sessions.CardInfo = new cCardInfo
            {
                cardno = "4282209004348015",
                cvv2 = "123",
                month = "09",
                namesurname = "d",
                year = "18"
            };

            cPayData payData = new cPayData();

            cPayGaranti garanti = new cPayGaranti();
            garanti.mode = "TEST";
            garanti.apiversion = "v0.01";
            garanti.terminalprovuserid = "PROVAUT";
            garanti.txntype = "sales";
            garanti.txnamount = "100";
            garanti.txncurrencycode = "949";
            garanti.txninstallmentcount = "";
            garanti.terminaluserid = "SANALUSER";
            garanti.orderid = strO;
            garanti.terminalid = "30691297";
            string _strTerminalID = "0" + garanti.terminalid;
            garanti.terminalmerchantid = "7000679";
            garanti.successurl = "http://localhost:8037/test/finish";
            garanti.errorurl = "http://localhost:8037/test/finish";
            string SecurityData = GetSHA1("123qweASD/030691297").ToUpper();
            garanti.secure3dhash = GetSHA1("30691297" + strO + "100http://localhost:8037/test/finishhttp://localhost:8037/test/finishsales12345678" + SecurityData).ToUpper();

            payData.garanti = garanti;

            return View(payData);
        }

        #region GetSHA1
        private string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        private string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }
        #endregion
    }
}