﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSales.Classes;
using ModelLibrary;

namespace WebSales.Controllers
{
    public class SmsController : Controller
    {
        public EmptyResult Policy(string token)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    var scratchWin = db.tblScratchWinSales.Where(w => w.bolSuccess && w.strSmsToken == token).FirstOrDefault();
                    if (scratchWin != null)
                    {
                        string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=hytpoliceweb&desname=/akademi/mail/Police_" + scratchWin.intPolicyID.ToString() + "_w1.pdf&p_polid=" + scratchWin.intPolicyID.ToString() + "&cmdkey=fibahayat&server=" + ConfigurationManager.AppSettings["SalePdfserver"];

                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                        Response.Charset = "windows-1254";
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                        Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                        Response.ContentType = "application/pdf";
                        Response.Expires = 0;
                        Response.Write(Utilities.UrlGet(strUrl, "GET"));
                        Response.End();
                    }
                }
            }
            catch { }

            return new EmptyResult();
        }

        public EmptyResult Bes(string token)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblBesSaleLogs saleLog = db.tblBesSaleLogs.Where(w => w.InfoFormToken == token).FirstOrDefault();
                    if (saleLog != null)
                    {
                        string strPoliceNo = saleLog.tblBesSaleLogPoliceler.First().intPolID.ToString();
                        string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=sozlesme_bes_matbaa.rdf&desname=" + strPoliceNo + DateTime.Now.ToString("dd/MM/yyyy").Replace(".", "/") + ".pdf&server=" + ConfigurationManager.AppSettings["SalePdfserver"] + "&desformat=PDF&destype=cache&cmdkey=fibauser&p_polid=" + strPoliceNo;

                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                        Response.Charset = "windows-1254";
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                        Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                        Response.ContentType = "application/pdf";
                        Response.Expires = 0;
                        Response.Write(Utilities.UrlGet(strUrl, "GET"));
                        Response.End();
                    }
                }
            }
            catch { }

            return new EmptyResult();
        }

        public EmptyResult Document()
        {
            try
            {
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                Response.Charset = "windows-1254";
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                Response.AddHeader("content-disposition", "attachment; filename=Police_Bilgilendirme_Formu.pdf");
                Response.ContentType = "application/pdf";
                Response.Expires = 0;
                Response.TransmitFile(Server.MapPath("~/document/Hayat_Sigortalari_Bilgi_Formu-1.pdf"));
                Response.End();
            }
            catch { }

            return new EmptyResult();
        }

        public EmptyResult Life(string token)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblSaleLogs saleLog = db.tblSaleLogs.Where(w => w.InfoFormToken == token).FirstOrDefault();
                    if (saleLog != null)
                    {
                        var salePolice = saleLog.tblSaleLogPoliceler.Where(w => w.strService == "teklifpolicelesme").FirstOrDefault();
                        if (salePolice != null)
                        {
                            string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=hytpoliceweb&desname=/akademi/mail/Police_" + salePolice.PoliceNo + "_w1.pdf&p_polid=" + salePolice.PoliceNo + "&cmdkey=fibahayat&server=" + ConfigurationManager.AppSettings["SalePdfserver"];

                            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                            Response.Charset = "windows-1254";
                            Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                            Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                            Response.ContentType = "application/pdf";
                            Response.Expires = 0;
                            Response.Write(Utilities.UrlGet(strUrl, "GET"));
                            Response.End();
                        }
                    }
                }
            }
            catch { }

            return new EmptyResult();
        }

        public EmptyResult Credit(string token)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblCreditSaleLogs saleLog = db.tblCreditSaleLogs.Where(w => w.InfoFormToken == token).FirstOrDefault();
                    if (saleLog != null)
                    {
                        var saleLogInfo = saleLog.tblCreditSaleLogInfo.Where(w => w.bolSuccessPay).FirstOrDefault();
                        if (saleLogInfo != null)
                        {
                            var salePolicy = saleLog.tblCreditPacketPolicy.Where(w => !string.IsNullOrEmpty(w.strPolicyID)).FirstOrDefault();
                            if (saleLog != null)
                            {
                                string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=hytpoliceweb&desname=/akademi/mail/Police_" + salePolicy.strPolicyID + "_w1.pdf&p_polid=" + salePolicy.strPolicyID + "&cmdkey=fibahayat";

                                Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
                                Response.Charset = "windows-1254";
                                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                                Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                                Response.ContentType = "application/pdf";
                                Response.Expires = 0;
                                Response.Write(Utilities.UrlGet(strUrl, "GET"));
                                Response.End();
                            }
                        }
                    }
                }
            }
            catch { }

            return new EmptyResult();
        }
    }
}