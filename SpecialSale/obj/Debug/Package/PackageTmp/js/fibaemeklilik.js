/*! fibaemeklilik 2016-01-29 */
"use strict";
var lang = $("html").attr("lang"),
    FibaEmeklilik = FibaEmeklilik || {};
FibaEmeklilik.debug = !0, FibaEmeklilik.init = function () {
    var a = this;
    $(".owl-carousel").length > 0 && $(".owl-carousel").owlCarousel({
        loop: !0,
        margin: 0,
        nav: !0,
        autoplay: !0,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !0,
        transitionStyle: "fade",
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1e3: {
                items: 1
            }
        }
    }), $('[type="radio"]').inputStilci({
        radio: !0
    }), $('[type="checkbox"]').inputStilci({
        checkbox: !0
    }), $('.tel, [type="tel"]').mask("(999) 999 99 99"), $(".credit-card").mask("9999 9999 9999 9999"), $(".tcno").mask("99999999999"), $(".cvc").mask("999"), $(".js-datepicker").datepicker(), $("select").selectpicker(), $(".js-popup-info").bind("click", function (b) {
        b.preventDefault();
        var c = $(this).attr("href");
        $.ajax({
            url: c,
            type: "GET",
            success: function (b) {
                a.popup.add(".bilgilendirme", b)
            }
        })
    }), $(".js-odeme_secenegi").bind("change", function (a) {
        var b = $(this).val();
        $(".payment_option").hide(), $("#" + b).show()
    }), $.validator.addMethod("validEmail", function (a, b) {
        var c = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return c.test(a)
    }, "GeÃ§erli bir e-posta adresi yazÄ±nÄ±z."), $(".js_validate_form").length > 0 && $(".js_validate_form").each(function () {
        $(this).validate({
            rules: {
                email: {
                    required: !0,
                    validEmail: !0
                },
                hiddenRecaptcha: {
                    required: function () {
                        return "" == grecaptcha.getResponse() ? !0 : !1
                    }
                }
            },
            submitHandler: function (a) {
                a.submit()
            }
        })
    }), $(".js-yenile").bind("change", function (a) {
        var b = $(this).data("val");
        1 == b ? $("#divPolicyrenewyear").removeClass("hide") : $("#divPolicyrenewyear").addClass("hide")
    }), $("#btnNext").click(function () {
        var a = $("#form1");
        a.valid() && "1" != $("input[name=question]:checked").val() && ($(this).hide(), $(".form__bottom").append('<img src="/img/ajax-loader.gif"/>'))
    }), this.contentHeight(), $(".js-popup").on("click", function (a) {
        a.preventDefault();
        var b = $(this).attr("href"),
            c = $(this).attr("data-video");
        Popup.ajax("body", {
            url: b
        }, function () {
            $(".video-temp").attr("src", c)
        })
    }), $(window).resize(function () {
        a.contentHeight()
    })
}, FibaEmeklilik.contentHeight = function () {
    var a = $("#header").outerHeight(!0),
        b = $("#content").find(".subpage"),
        c = $("#footer").outerHeight(!0),
        d = $(window).height(),
        e = d - (a + c);
    b.css({
        "min-height": e
    })
}, FibaEmeklilik.popup = function () {
    function a(a, e, f) {
        g = c(e, f), h = g.find(".fiba-popup__inner"), i = g.find(".fiba-popup__close"), $(a).css({
            position: "relative"
        }).find(".fiba-popup").remove(), $(a).append(g), g.fadeIn(200, b), i.on("click", d)
    }

    function b() {
        return h.addClass("active"), !0
    }

    function c(a, b) {
        return $('<div class="fiba-popup"><div class="fiba-popup__inner"><a class="fiba-popup__close icon-remove" href="javascript:;">Kapat</a>' + (void 0 != b || null != b ? '<img class="fiba-popup__img" src="/img/' + b + '.png" alt="' + b + '"/>' : "") + '<div class="fiba-popup__text">' + a + "</div></div></div>")
    }

    function d() {
        return e(), !0
    }

    function e() {
        return g.fadeOut(200, f), i.off("click"), !0
    }

    function f(a) {
        return g.remove(), $(a).css({
            position: ""
        }), !0
    }
    var g, h, i;
    return {
        add: a,
        close: e
    }
}(), FibaEmeklilik.PagePost = function (a, b, c, d) {
    $("<form>", {
        html: '<input type="text" name="' + b + '" value="' + c + '"/><input type="text" name="query" value="' + d + '"/>',
        method: "post",
        action: a
    }).appendTo("body").submit()
}, FibaEmeklilik.init();
var popup = FibaEmeklilik.popup;
$(function () {
    var a = ($(".fancy-popup a img").attr("src"), $(".fancy-popup").length);
    if (a > 0) {
        var b = '<a target="_blank" href="https://online.fibaemeklilik.com.tr/katilim-sartlari.html" style="display:block;" class="fancy-popup"><img class="popup-img" width="600" src="https://www.fibaemeklilik.com.tr/Upload/Images/59d47add6872406dab5620b532916218.png"> </a>';
        Popup.add("body", b)
    }
});