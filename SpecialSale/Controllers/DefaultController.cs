﻿#region Directives
using ModelLibrary;
using Newtonsoft.Json.Linq;
using SpecialSale.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endregion

namespace SpecialSale.Controllers
{
    public class DefaultController : Controller
    {
        #region TCKNValid
        private bool TCKNValid(ref cTcknInfo tcknInfo, string strTckn)
        {
            AileBireySorgu.KpsBireySorguServiceClient aileService = new AileBireySorgu.KpsBireySorguServiceClient();
            try
            {
                var kpsBireySorgu = aileService.kpsBireySorgu(new AileBireySorgu.bireySorguInputType()
                {
                    cacheStyle = 1,
                    maxAge = 180,
                    kimlikNo = Utilities.NullFixLong(strTckn),
                    kimlikNoSpecified = true
                });

                tcknInfo.strName = kpsBireySorgu.adi;
                tcknInfo.strSurname = kpsBireySorgu.soyadi;
                tcknInfo.strBirthplace = kpsBireySorgu.dogumYeri;
                tcknInfo.strFatherName = kpsBireySorgu.babaAdi;
                tcknInfo.strGender = kpsBireySorgu.cinsiyeti;
                tcknInfo.strMaritalStatus = kpsBireySorgu.medeniHali == "EVLİ" ? "E" : "B";
                tcknInfo.strMotherName = kpsBireySorgu.anneAdi;
                tcknInfo.strBirthDate = kpsBireySorgu.dogumTarihi;

                using (DBEntities db = new DBEntities())
                {
                    var kpsTCKisiAcikAdresSorgu = aileService.kpsTCKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTckn),
                        kimlikNoSpecified = true
                    });

                    if (kpsTCKisiAcikAdresSorgu.kimlikNo > 0)
                    {
                        tcknInfo.strAddress = kpsTCKisiAcikAdresSorgu.acikAdres;
                        tcknInfo.strDistrictCode = kpsTCKisiAcikAdresSorgu.ilceKodu;
                        tcknInfo.strRegionCode = kpsTCKisiAcikAdresSorgu.ilKodu;
                        tcknInfo.strCountryCode = "TR";
                    }

                    tcknInfo.strBirthRegionCode = "";
                    tcknInfo.strBirthDistrictCode = "";

                    var il = db.tblMapping.Where(w => w.strType == "ILK" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                    if (il != null)
                        tcknInfo.strBirthRegionCode = il.strCode.Trim();

                    var ilce = db.tblMapping.Where(w => w.strType == "ILC" && w.strName == kpsBireySorgu.dogumYeri.Trim()).FirstOrDefault();
                    if (ilce != null)
                        tcknInfo.strBirthDistrictCode = ilce.strCode.Trim();

                    if (tcknInfo.strBirthRegionCode == "" && tcknInfo.strBirthDistrictCode != "")
                    {
                        tcknInfo.strBirthRegionCode = tcknInfo.strBirthDistrictCode.Substring(0, 3);
                    }

                    if (tcknInfo.strBirthRegionCode == "")
                    {
                        tcknInfo.strBirthRegionCode = "99";
                    }

                    if (tcknInfo.strBirthRegionCode != "" && tcknInfo.strBirthDistrictCode == "")
                    {
                        tcknInfo.strBirthDistrictCode = "99900";
                    }
                }

                return true;
            }
            catch
            {
                try
                {
                    var kpsYabanciSorgu = aileService.kpsYabanciBireySorgu(new AileBireySorgu.bireySorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTckn),
                        kimlikNoSpecified = true
                    });

                    tcknInfo.strName = kpsYabanciSorgu.adi;
                    tcknInfo.strSurname = kpsYabanciSorgu.soyadi;
                    tcknInfo.strFatherName = kpsYabanciSorgu.babaAdi;
                    tcknInfo.strGender = kpsYabanciSorgu.cinsiyeti;
                    tcknInfo.strBirthDate = kpsYabanciSorgu.dogumTarihi;

                    var kpsYabanciAcikAdresSorgu = aileService.kpsYabanciKisiAcikAdresSorgu(new AileBireySorgu.kisiAdresSorguInputType()
                    {
                        cacheStyle = 1,
                        maxAge = 180,
                        kimlikNo = Utilities.NullFixLong(strTckn),
                        kimlikNoSpecified = true
                    });

                    if (kpsYabanciAcikAdresSorgu.kimlikNo > 0)
                    {
                        tcknInfo.strAddress = kpsYabanciAcikAdresSorgu.acikAdres;
                        tcknInfo.strDistrictCode = "99900";
                        tcknInfo.strRegionCode = "99";

                        using (DBEntities db = new DBEntities())
                        {
                            var uyr = db.tblMapping.Where(w => w.strType == "UYR" && !string.IsNullOrEmpty(w.strName.Trim()) && w.strCode == kpsYabanciSorgu.uyrukKod.ToString()).FirstOrDefault();

                            if (uyr != null)
                                tcknInfo.strCountryCode = uyr != null ? uyr.strCountryCode : kpsYabanciSorgu.uyrukAd;
                        }
                    }

                    tcknInfo.strBirthRegionCode = "99";
                    tcknInfo.strBirthDistrictCode = "99900";

                    return true;
                }
                catch { }
            }

            return false;
        }
        #endregion

        #region GetCallcenter
        tblCallcenters GetCallcenter(int intCallcenterID = 0)
        {
            using (DBEntities db = new DBEntities())
            {
                if (intCallcenterID > 0)
                {
                    return db.tblCallcenters.Where(w => w.bolIsActive && !w.bolIsDelete && w.bolSpecialSale && w.intCallcenterID == intCallcenterID).FirstOrDefault();
                }
                else
                {
                    string strKey = RouteData.Values["key"] != null ? RouteData.Values["key"].ToString() : "";

                    return db.tblCallcenters.Where(w => w.bolIsActive && !w.bolIsDelete && w.bolSpecialSale && w.strPublishKey == strKey).FirstOrDefault();
                }
            }
        }
        #endregion

        #region Index Get
        [HttpGet]
        public ActionResult Index(string product = "")
        {
            cSale sale = new cSale();
            sale.selectedProduct = product;

            using (DBEntities db = new DBEntities())
            {
                var callcenter = GetCallcenter();
                if (callcenter != null)
                {
                    sale.strCallcenterName = callcenter.strName;
                    sale.intCallcenterID = callcenter.intCallcenterID;

                    var products = db.tblProducts.Where(w => w.bolIsActive && !w.bolIsDelete && w.bolSpecialSale && w.tblCallcenterProducts.Count(c => c.intCallcenterID == callcenter.intCallcenterID) > 0).ToList();

                    if (products != null || products.Count > 0)
                    {
                        sale.products = new List<cSaleData>();

                        foreach (var item in products)
                        {
                            sale.products.Add(new cSaleData
                            {
                                id = item.intProductID.ToString(),
                                text = item.strName
                            });
                        }

                        if (sale.products.Count > 0)
                        {
                            int intProductID = Utilities.NullFixInt(!string.IsNullOrEmpty(product) ? product : sale.products[0].id);

                            var packets = db.tblPackets.Where(w => w.intProductID == intProductID && !w.bolIsDelete && w.tblAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).ToList();

                            if (packets != null || packets.Count > 0)
                            {
                                sale.packets = new List<cSaleData>();

                                foreach (var item in packets)
                                {
                                    sale.packets.Add(new cSaleData
                                    {
                                        id = item.intPacketID.ToString(),
                                        text = item.strName
                                    });
                                }
                            }
                            else
                            {
                                sale = null;
                            }
                        }
                    }
                    else
                    {
                        sale = null;
                    }
                }
                else
                {
                    sale = null;
                }
            }

            ViewBag.paytype = "kredi";

            return View(sale);
        }
        #endregion

        #region Index Post
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index()
        {
            string strTckn = Request.Form["tckn"];

            ViewBag.packet = Request.Form["packet"];
            ViewBag.tckn = strTckn;
            ViewBag.email = Request.Form["e_mail"];
            ViewBag.mobile = Request.Form["mobile"];
            ViewBag.address = Request.Form["address"];
            ViewBag.paytype = Request.Form["paytype"];

            if (string.IsNullOrEmpty(Request.Form["packet"]) || string.IsNullOrEmpty(strTckn) || string.IsNullOrEmpty(Request.Form["mobile"]) || string.IsNullOrEmpty(Request.Form["bilgilendirme_formu"]) || string.IsNullOrEmpty(Request.Form["satis_sozlesmesi"]) || string.IsNullOrEmpty(Request.Form["mesafeli_satis"]))
            {
                string strMsg = "";
                if (string.IsNullOrEmpty(Request.Form["packet"]))
                {
                    strMsg = "Paket seçiniz!";
                }
                else if (string.IsNullOrEmpty(strTckn))
                {
                    strMsg = "Kimlik numarasını giriniz!";
                }
                else if (string.IsNullOrEmpty(Request.Form["mobile"]))
                {
                    strMsg = "Cep telefonu numarasını giriniz!";
                }
                else if (string.IsNullOrEmpty(Request.Form["bilgilendirme_formu"]))
                {
                    strMsg = "Lütfen Bilgilendirme Formu’nu okuyup onaylayınız!";
                }
                else if (string.IsNullOrEmpty(Request.Form["satis_sozlesmesi"]))
                {
                    strMsg = "Lütfen Kullanıcı Sözleşmesi ve Kişisel Verileri Koruma Kanunu Kapsamında Bilgilendirme Metni’ni okuyup onaylayınız!";
                }
                else if (string.IsNullOrEmpty(Request.Form["mesafeli_satis"]))
                {
                    strMsg = "Lütfen Mesafeli Satış Sözleşmesi’ni ve Sağlık Beyanı’nı okuyup onaylayınız!";
                }

                ViewBag.Message = "<script>$(function () { ErrorMsg('" + strMsg + "'); });</script>";
            }
            else
            {
                string strCaptcha = cCrypto.DecryptDES(Request.Form["hdCapctha"]);
                if (strCaptcha == Request.Form["txtCaptcha"])
                {
                    cTcknInfo tcknInfo = new cTcknInfo();
                    bool bolValid = TCKNValid(ref tcknInfo, strTckn);

                    if (bolValid)
                    {
                        if (string.IsNullOrEmpty(tcknInfo.strAddress))
                        {
                            tcknInfo.strAddress = Request.Form["address"];
                        }

                        if (!string.IsNullOrEmpty(tcknInfo.strAddress))
                        {
                            using (DBEntities db = new DBEntities())
                            {
                                int intPacketID = Utilities.NullFixInt(Request.Form["packet"]);

                                bool bolExist = false;

                                List<tblSpecialSaleLogs> existLog = db.tblSpecialSaleLogs.Where(w => w.bolIsFinished && w.strTckn == strTckn && w.dtRegisterDate.Day == DateTime.Now.Day && w.dtRegisterDate.Month == DateTime.Now.Month && w.dtRegisterDate.Year == DateTime.Now.Year).ToList();
                                if (existLog != null && existLog.Count > 0)
                                {
                                    foreach (var item in existLog)
                                    {
                                        var packetExist = db.tblPackets.Where(w => w.intPacketID == item.intPacketID).FirstOrDefault();
                                        var packetSelected = db.tblPackets.Where(w => w.intPacketID == intPacketID).FirstOrDefault();
                                        if (packetExist != null && packetSelected != null && packetExist.intProductID == packetSelected.intProductID)
                                        {
                                            bolExist = true;
                                            break;
                                        }
                                    }
                                }

                                if (bolExist)
                                {
                                    ViewBag.Message = "<script>$(function () { ErrorMsg('Bu kimlik numarasıyla bugün üretim yapılmıştır. Bugün tekrar üretim yapılamaz.'); });</script>";
                                }
                                else
                                {
                                    tblSpecialSaleLogs specialSaleLog = new tblSpecialSaleLogs
                                    {
                                        bolIsFinished = false,
                                        dtRegisterDate = DateTime.Now,
                                        intPacketID = Utilities.NullFixInt(Request.Form["packet"]),
                                        strEmail = Request.Form["e_mail"],
                                        strIP = Utilities.ClientIP,
                                        strMobile = Request.Form["mobile"],
                                        strTckn = strTckn,
                                        strPayType = Request.Form["paytype"]
                                    };

                                    db.tblSpecialSaleLogs.Add(specialSaleLog);
                                    db.SaveChanges();

                                    string strMsg = "";
                                    cPolicyData policyData = CreatePolicy(db, tcknInfo, specialSaleLog.intSpecialSaleLogID, specialSaleLog.intPacketID, ref strMsg);

                                    JavaScriptSerializer jss = new JavaScriptSerializer();
                                    specialSaleLog.strPolicyData = jss.Serialize(policyData);
                                    specialSaleLog.strMsg = strMsg;

                                    db.SaveChanges();

                                    if (string.IsNullOrEmpty(strMsg) && policyData.PoliceNo > 0)
                                    {
                                        //int intWSLogID = Logs.WSLog(0, specialSaleLog.intSpecialSaleLogID, 0, "ozelurun_hayat", "Finish", "Input", "{ \"tkl\" : \"" + policyData.PoliceNo + "\", \"insCan\" : \"1\", \"pKredino2\": \"\", \"krd\" : \"\" }", "teklifpolicelesme");

                                        //using (FIBAWS2.fibaws2Client fibaws2 = new FIBAWS2.fibaws2Client())
                                        //{
                                        //    var offerPolicy = fibaws2.teklifpolicelesme(policyData.PoliceNo, 1, "", "");

                                        //    Logs.WSLog(intWSLogID, specialSaleLog.intSpecialSaleLogID, 0, "ozelurun_hayat", "New", "Output", jss.Serialize(offerPolicy), "teklifpolicelesme");

                                        specialSaleLog.intPolicy = Utilities.NullFixInt(policyData.PoliceNo.Value.ToString());
                                        specialSaleLog.bolIsFinished = true;
                                        db.SaveChanges();
                                        //}

                                        Session["Policy"] = policyData.PoliceNo;
                                        Session["SpecialSaleLogID"] = specialSaleLog.intSpecialSaleLogID;

                                        return Redirect(Request.Url.AbsolutePath);
                                    }
                                    else
                                    {
                                        ViewBag.Message = "<script>$(function () { ErrorMsg('Poliçe oluşturulamadı tekrar deneyiniz!'); });</script>";
                                    }
                                }
                            }
                        }
                        else
                        {
                            ViewBag.ShowAddress = "1";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "<script>$(function () { ErrorMsg('Kimlik numarasını hatalı girdiniz!'); });</script>";
                    }
                }
                else
                {
                    ViewBag.Message = "<script>$(function () { ErrorMsg('Güvenlik kodunu hatalı girdiniz!'); });</script>";
                }
            }

            return Index(Request.Form["product"]);
        }
        #endregion

        #region CreatePolicy
        cPolicyData CreatePolicy(DBEntities db, cTcknInfo tcknInfo, int intSpecialSaleLogID, int intPacketID, ref string strMsg)
        {
            cPolicyData policyData = new cPolicyData();

            using (BankaWebV2.bankaweb_v2Client service = new BankaWebV2.bankaweb_v2Client())
            {
                try
                {
                    var packet = db.tblPackets.Where(w => w.intPacketID == intPacketID).FirstOrDefault();
                    if (packet != null)
                    {
                        BankaWebV2.BankawebV2PolreqUser user = new BankaWebV2.BankawebV2PolreqUser();

                        #region adres
                        user.adres = new BankaWebV2.Bankawebv2polreqOzlukadresreUser
                        {
                            adres = tcknInfo.strAddress,
                            adrestip = "E",
                            email = Request.Form["e_mail"],
                            ilcekod = tcknInfo.strDistrictCode.Trim(),
                            iletisimno = Request.Form["mobile"].Replace("(", "").Replace(")", "").Replace(" ", ""),
                            iletisimtip = "CEP",
                            ilkod = tcknInfo.strRegionCode.Trim(),
                            ulkekod = tcknInfo.strCountryCode.Trim()
                        };
                        #endregion

                        #region ozluk
                        user.ozluk = new BankaWebV2.Bankawebv2polreqOzlukreqUser
                        {
                            ad = tcknInfo.strName,
                            anneadi = tcknInfo.strMotherName,
                            babaad = tcknInfo.strFatherName,
                            cinsiyet = tcknInfo.strGender,
                            dogumil = tcknInfo.strBirthRegionCode,
                            dogumilce = tcknInfo.strBirthDistrictCode,
                            dogumtarih = tcknInfo.strBirthDate,
                            medenidurum = tcknInfo.strMaritalStatus,
                            soyad = tcknInfo.strSurname,
                            tckimlikno = Request.Form["tckn"],
                            musteritip = "G",
                            meslekkod = "065",
                            uyruk = tcknInfo.strCountryCode
                        };
                        #endregion

                        #region araci
                        var callcenter = GetCallcenter();
                        if (callcenter != null)
                        {
                            user.araci = new BankaWebV2.Bankawebv2polreqAracireqUser
                            {
                                bankasube = Utilities.NullFixDecimal(callcenter.strBankNo),
                                sicilno = callcenter.strRegisterNo
                            };
                        }
                        #endregion

                        #region kredi
                        BankaWebV2.Bankawebv2primsiminBedeltabl yillikkreditaksit = new BankaWebV2.Bankawebv2primsiminBedeltabl();

                        foreach (var assurance in packet.tblAssurances.ToList())
                        {
                            yillikkreditaksit.Add(new BankaWebV2.BnktypesV2BedelTabloUser
                            {
                                bedel = assurance.flPrice,
                                tmntkod = assurance.strCode,
                                yil = assurance.intYear
                            });
                        }

                        user.kredi = new BankaWebV2.Bankawebv2polreqKredireqUser
                        {
                            kredisure = packet.intCrediTime,
                            kredidoviztip = packet.strCrediCurrencyType,
                            kreditaksittip = "S",
                            kredidonustutar = packet.flCrediReturnPrice,
                            yillikkreditaksit = yillikkreditaksit,
                            kreditutar = 1,
                            kreditip = packet.strIntegrationTariffCode,
                            sure = packet.intTime,
                            kredino = "x" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString()
                        };
                        #endregion

                        #region odeme
                        user.odeme = new BankaWebV2.Bankawebv2polreqOdemereqUser
                        {
                            odemetip = "NKT",
                            dovizkod = "TL",
                            primodemeperiyod = packet.strPremiumPaymentPeriod,
                            primodemesure = packet.intPremiumPaymentTime,
                        };
                        #endregion

                        JavaScriptSerializer jss = new JavaScriptSerializer();
                        int intWSLogID = Logs.WSLog(0, intSpecialSaleLogID, 0, "ozelurun_hayat", "CreatePolicy", "Input", jss.Serialize(user), "policelesme");

                        var police = service.policelesme(user, "T");

                        Logs.WSLog(intWSLogID, intSpecialSaleLogID, 0, "ozelurun_hayat", "CreatePolicy", "Output", jss.Serialize(police), "policelesme");

                        if (police.cevap == null)
                        {
                            policyData.IlkPrim = police.ilkprim.Value;
                            policyData.ToplamPirim = police.toplamprim.Value;
                            policyData.PoliceNo = police.policeno.Value;
                            policyData.IlkPrimKomisyon = police.ilkprimkomisyon.Value;
                            policyData.KrediNo = police.kredino;
                            policyData.TeklifDurum = police.teklifdurum;
                            policyData.ToplamKomisyon = police.toplamkomisyon.Value;
                            policyData.Yenileme = "";
                        }
                        else if (police.cevap.Contains("Err|"))
                        {
                            strMsg = police.cevap.Split('|')[1].Trim();
                            strMsg = strMsg.Trim(new char[] { '\n', '\r' }).Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\t", String.Empty).Replace(Environment.NewLine, "");

                            //Kredi kartı ile ilgili bir mesaj dönerse hataya düşmemesi gerekiyor
                            if (!strMsg.Contains("GetKKSanalPosBankaNo"))
                                throw new Exception(strMsg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Error(db, ex, intSpecialSaleLogID);
                }
            }

            return policyData;
        }
        #endregion

        #region Pdf
        [HttpGet]
        public ActionResult Pdf()
        {
            try
            {
                string strPoliceNo = cCrypto.DecryptDES(Request.QueryString["policy"]);
                string strUrl = "http://" + ConfigurationManager.AppSettings["ip"] + "/reports/rwservlet?report=hytpoliceweb&desname=/akademi/mail/Police_" + strPoliceNo + "_w1.pdf&p_polid=" + strPoliceNo + "&cmdkey=fibahayat&server=" + ConfigurationManager.AppSettings["SalePdfserver"];

                Response.ContentEncoding = Encoding.GetEncoding("windows-1254");
                Response.Charset = "windows-1254";
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=windows-1254' />");
                Response.AddHeader("content-disposition", "attachment; filename=police_" + DateTime.Now.ToShortDateString().Replace(".", "_") + ".pdf");
                Response.ContentType = "application/pdf";
                Response.Expires = 0;
                Response.Write(UrlGet(strUrl, "GET"));
            }
            catch (Exception ex)
            {
                Logs.Error(null, ex, Utilities.NullFixInt(Session["SpecialSaleLogID"].ToString()));
            }

            return View();
        }

        string UrlGet(string Url, string Method)
        {
            HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
            myRequest.Method = Method;
            myRequest.ContentType = "application/pdf";

            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            Stream encodingStream = myResponse.GetResponseStream();
            StreamReader read = new StreamReader(encodingStream, Encoding.GetEncoding("utf-8"));
            string result = read.ReadToEnd();
            myResponse.Close();

            return result;
        }
        #endregion

        #region Packets
        public JsonResult Packets(string product)
        {
            List<cSaleData> _data = new List<cSaleData>();

            using (DBEntities db = new DBEntities())
            {
                int intProductID = Utilities.NullFixInt(product);

                var packets = db.tblPackets.Where(w => w.intProductID == intProductID && !w.bolIsDelete && w.tblAssurances.Where(c => !c.bolIsDelete).FirstOrDefault() != null).ToList();

                if (packets != null || packets.Count > 0)
                {
                    foreach (var item in packets)
                    {
                        _data.Add(new cSaleData
                        {
                            id = item.intPacketID.ToString(),
                            text = item.strName
                        });
                    }
                }

                return Json(_data, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region InfoForm
        public ActionResult InfoForm()
        {
            cInfoForm infoForm = null;

            try
            {
                if (Request.QueryString["c"] != null && Request.QueryString["t"] != null)
                {
                    tblCallcenters callcenter = GetCallcenter(Utilities.NullFixInt(cCrypto.DecryptDES(Request.QueryString["c"])));

                    if (callcenter != null)
                    {
                        cTcknInfo tcknInfo = new cTcknInfo();
                        bool bolValid = TCKNValid(ref tcknInfo, cCrypto.DecryptDES(Request.QueryString["t"]));
                        if (bolValid)
                        {
                            infoForm = new cInfoForm();
                            infoForm.strAddress = callcenter.strAddress;
                            infoForm.strFax = callcenter.strFax;
                            infoForm.strPhone = callcenter.strPhone;
                            infoForm.strTradeName = callcenter.strTradeName;
                            infoForm.strNameSurname = tcknInfo.strName + " " + tcknInfo.strSurname;
                        }
                        else
                        {
                            ViewBag.Msg = "Kimlik numarasını hatalı girdiniz!";
                        }
                    }
                }
            }
            catch { }

            return View(infoForm);
        }
        #endregion

        #region Crypto
        public JsonResult Crypto(string value)
        {
            return Json(new
            {
                value = cCrypto.EncryptDES(value)
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Capctha
        public ActionResult Capctha()
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            string a = cCrypto.DecryptDES(Request.QueryString["key"]);
            var captcha = string.Format("{0}", a);

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(120, 50))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                int i, r, x, y;
                var pen = new Pen(Color.Yellow);
                for (i = 1; i < 10; i++)
                {
                    pen.Color = Color.FromArgb(
                    (rand.Next(0, 255)),
                    (rand.Next(0, 255)),
                    (rand.Next(0, 255)));

                    r = rand.Next(0, (120 / 3));
                    x = rand.Next(0, 120);
                    y = rand.Next(0, 50);

                    gfx.DrawEllipse(pen, x - r, y - r, r, r);
                }

                gfx.DrawString(captcha, new Font("Tahoma", 20), Brushes.Gray, 9, 8);

                Response.ContentType = "image/png";
                Response.Clear();
                Response.BufferOutput = true;
                bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);

                Response.Flush();

                return View();
            }
        }
        #endregion
    }
}
