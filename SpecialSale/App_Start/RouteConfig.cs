﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SpecialSale
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "url1",
                url: "Packets",
                defaults: new { controller = "Default", action = "Packets" }
            );

            routes.MapRoute(
                name: "url2",
                url: "Pdf",
                defaults: new { controller = "Default", action = "Pdf" }
            );

            routes.MapRoute(
                name: "url3",
                url: "bilgilendirmeformu",
                defaults: new { controller = "Default", action = "InfoForm" }
            );

            routes.MapRoute(
               name: "url4",
               url: "Crypto",
               defaults: new { controller = "Default", action = "Crypto" }
           );

            routes.MapRoute(
               name: "url5",
               url: "Capctha",
               defaults: new { controller = "Default", action = "Capctha" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{key}",
                defaults: new { controller = "Default", action = "Index", key = UrlParameter.Optional }
            );
        }
    }
}