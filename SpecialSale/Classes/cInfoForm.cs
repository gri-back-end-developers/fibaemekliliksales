﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpecialSale.Classes
{
    public class cInfoForm
    {
        public string strTradeName { get; set; }
        public string strAddress { get; set; }
        public string strPhone { get; set; }
        public string strFax { get; set; }
        public string strNameSurname { get; set; }
    }
}