﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpecialSale.Classes
{
    public class cPolicyData
    {
        public decimal? PoliceNo { get; set; }
        public decimal? ToplamPirim { get; set; }
        public decimal? IlkPrim { get; set; }
        public string TeklifDurum { get; set; }
        public decimal? IlkPrimKomisyon { get; set; }
        public decimal? ToplamKomisyon { get; set; }
        public string KrediNo { get; set; }
        public string Yenileme { get; set; }
    }
}