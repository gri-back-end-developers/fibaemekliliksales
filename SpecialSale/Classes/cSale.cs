﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpecialSale.Classes
{
    public class cSale
    {
        public int intCallcenterID { get; set; }
        public string strCallcenterName { get; set; }
        public string selectedProduct { get; set; }
        public List<cSaleData> products { get; set; }
        public List<cSaleData> packets { get; set; }
    }

    public class cSaleData
    {
        public string id { get; set; }
        public string text { get; set; }
    } 
}