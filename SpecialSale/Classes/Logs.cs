﻿using ModelLibrary;
using System;
using System.Diagnostics; 

namespace SpecialSale.Classes
{
    public class Logs
    {
        #region Error
        public static void Error(DBEntities db, Exception ex, int intLogID)
        {
            try
            {
                if (db == null)
                    db = new DBEntities();

                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                tblWebSaleException exceptions = new tblWebSaleException()
                { 
                    intLogID = intLogID,
                    strException = "Line : " + intLine + " -- " + ex.ToString(),
                    dtRegisterDate = DateTime.Now
                };

                db.tblWebSaleException.Add(exceptions);
                db.SaveChanges(); 
            }
            catch { } 
        }
        #endregion

        #region LogPolicy
        public static void LogPolicy(int intWebSaleLogID, string strMethod, string intPolID, string strMsg)
        {
            try
            {
                DBEntities db = new DBEntities();

                tblWebSaleBesPlanPolicy policy = new tblWebSaleBesPlanPolicy();

                policy.intWebSaleLogID = intWebSaleLogID;
                policy.dtRegisterDate = DateTime.Now;
                policy.strMethod = strMethod;
                policy.intPolID = intPolID;
                policy.strMsg = strMsg;
                policy.bolSendMail = false;

                db.tblWebSaleBesPlanPolicy.Add(policy);
                db.SaveChanges();
            }
            catch { }
        }
        #endregion

        #region WSLog
        public static int WSLog(int intWSLogID, int intSaleLogID, int intStep, string strSaleType, string strClassName, string strMethodType, string strJson, string strMethod)
        {
            DBEntities db = new DBEntities();
            try
            {
                tblWSLogs wslog = new tblWSLogs
                {
                    dtRegisterDate = DateTime.Now,
                    intReleationWSLogID = intWSLogID,
                    intSaleLogID = intSaleLogID,
                    intStep = (byte)intStep,
                    intUserID = 0,
                    strClassName = strClassName,
                    strJson = strJson,
                    strMethod = strMethod,
                    strMethodType = strMethodType,
                    strProject = "online",
                    strSaleType = strSaleType
                };

                db.tblWSLogs.Add(wslog);
                db.SaveChanges();

                intWSLogID = wslog.intWSLogID;
            }
            catch { }

            db.Dispose();

            return intWSLogID;
        }
        #endregion
    }
}